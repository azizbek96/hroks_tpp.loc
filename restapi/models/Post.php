<?php


namespace restapi\models;


class Post extends \common\models\Post
{
    public function fields()
    {
        return [
            'id',
            'name',
            'lastname'
        ];
    }

    public function extraFields()
    {
        return [
            'description',
            'updated_by'

        ];
    }
}