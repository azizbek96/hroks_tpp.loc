<?php
namespace restapi\controllers;

use yii\rest\ActiveController;

class UserController extends CorsController
{
    public $modelClass = 'restapi\models\Users';
}