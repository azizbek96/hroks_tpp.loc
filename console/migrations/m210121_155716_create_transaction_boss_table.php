<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction_boss}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%customers}}`
 */
class m210121_155716_create_transaction_boss_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transaction_boss}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'customer_id' => $this->integer(),
            'amount' => $this->decimal(20,3),
            'inventory' => $this->decimal(20,3),
            'type' => $this->integer(),
            'status' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-transaction_boss-user_id}}',
            '{{%transaction_boss}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-transaction_boss-user_id}}',
            '{{%transaction_boss}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-transaction_boss-customer_id}}',
            '{{%transaction_boss}}',
            'customer_id'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-transaction_boss-customer_id}}',
            '{{%transaction_boss}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-transaction_boss-user_id}}',
            '{{%transaction_boss}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-transaction_boss-user_id}}',
            '{{%transaction_boss}}'
        );

        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-transaction_boss-customer_id}}',
            '{{%transaction_boss}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-transaction_boss-customer_id}}',
            '{{%transaction_boss}}'
        );

        $this->dropTable('{{%transaction_boss}}');
    }
}
