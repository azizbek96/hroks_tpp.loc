<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%department_responsible}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%departments}}`
 * - `{{%employee}}`
 */
class m210111_175132_create_department_responsible_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%department_responsible}}', [
            'id' => $this->primaryKey(),
            'department_id' => $this->integer(),
            'employee_id' => $this->integer(),
            'begin_date' => $this->date(),
            'end_date' => $this->date(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-department_responsible-department_id}}',
            '{{%department_responsible}}',
            'department_id'
        );

        // add foreign key for table `{{%departments}}`
        $this->addForeignKey(
            '{{%fk-department_responsible-department_id}}',
            '{{%department_responsible}}',
            'department_id',
            '{{%departments}}',
            'id',
            'CASCADE'
        );

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-department_responsible-employee_id}}',
            '{{%department_responsible}}',
            'employee_id'
        );

        // add foreign key for table `{{%employee}}`
        $this->addForeignKey(
            '{{%fk-department_responsible-employee_id}}',
            '{{%department_responsible}}',
            'employee_id',
            '{{%employee}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%departments}}`
        $this->dropForeignKey(
            '{{%fk-department_responsible-department_id}}',
            '{{%department_responsible}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-department_responsible-department_id}}',
            '{{%department_responsible}}'
        );

        // drops foreign key for table `{{%employee}}`
        $this->dropForeignKey(
            '{{%fk-department_responsible-employee_id}}',
            '{{%department_responsible}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-department_responsible-employee_id}}',
            '{{%department_responsible}}'
        );

        $this->dropTable('{{%department_responsible}}');
    }
}
