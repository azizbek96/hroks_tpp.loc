<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'fullname' => $this->string(255),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'type' => $this->integer()->defaultValue(1),
            'group_id' => $this->integer(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex(
            'idx-user-group_id',
            'user',
            'group_id'
        );

        $this->addForeignKey(
            'fk-user-group_id',
            'user',
            'group_id',
            'groups',
            'id'
        );
        /** Insert Default Admin User
        * Login: admin
        * Pass: 123456
        */
        $this->batchInsert('user',[
            'username',
            'fullname',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email',
            'type',
            'group_id',
            'status',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
            ],
            [
                [
                    'admin',
                    'admin',
                    'qSn5DH7kIedrNo1YXIpZFNz-f67qWF8U',
                    '$2y$13$aZ/f2Da2PefDCBMfdpywEue5mHekRQA2lh12Bt/B0Ziuy94gxBU0q',
                    NULL,
                    'admin@admin.com',
                    1,
                    1,
                    1,
                    1,
                    1,
                    1505282484,
                    1505282484
                ]
            ]
        );

    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-user-group_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-group_id',
            'user'
        );

        $this->dropTable('{{%user}}');
    }
}
