<?php

use yii\db\Migration;

/**
 * Class m210125_134906_insert_add_product_categories_table
 */
class m210125_134906_insert_add_product_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_item', ['name' => 'product-categories', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'product-categories/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'product-categories/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'product-categories/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'product-categories/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'product-categories/view', 'type' => '2', 'description' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('auth_item', ['name' => 'product-categories', 'type' => '3', 'description' => '']);
        $this->delete('auth_item', ['name' => 'product-categories/index', 'type' => '2', 'description' => '']);
        $this->delete('auth_item', ['name' => 'product-categories/delete', 'type' => '2', 'description' => '']);
        $this->delete('auth_item', ['name' => 'product-categories/update', 'type' => '2', 'description' => '']);
        $this->delete('auth_item', ['name' => 'product-categories/create', 'type' => '2', 'description' => '']);
        $this->delete('auth_item', ['name' => 'product-categories/view', 'type' => '2', 'description' => '']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210125_134906_insert_add_product_categories_table cannot be reverted.\n";

        return false;
    }
    */
}
