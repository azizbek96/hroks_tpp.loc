<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%measurements}}`.
 */
class m210121_022118_add_koefficient_column_to_measurements_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%measurements}}', 'koefficient', $this->decimal(20,1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%measurements}}', 'koefficient');
    }
}
