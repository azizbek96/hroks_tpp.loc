<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc_items}}`.
 */
class m210205_175346_add_karopka_quantity_column_to_doc_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc_items}}', 'karopka_quantity', $this->decimal(20,3)->defaultValue(0));
        $this->addColumn('{{%doc_items}}', 'karopka_inventory', $this->decimal(20,3)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('doc_items','karopka_count');
        $this->dropColumn('doc_items','karopka_inventory');    }
}
