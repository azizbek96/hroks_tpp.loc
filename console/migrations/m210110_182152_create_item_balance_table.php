<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%doc}}`
 * - `{{%products}}`
 * - `{{%clients}}`
 */
class m210110_182152_create_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_balance}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'count' => $this->decimal(20,3)->defaultValue(0),
            'inventory' => $this->decimal(20,3)->defaultValue(0),
            'doc_id' => $this->integer(),
            'doc_type' => $this->smallInteger(2)->defaultValue(1),
            'entity_type' => $this->smallInteger(2)->defaultValue(1),
            'customer_id' => $this->integer(),
            'measurement_id' => $this->integer(),
            'department_id' => $this->integer(),
            'dep_area_id' => $this->integer(),
            'currency' => $this->smallInteger()->defaultValue(1),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `doc_id`
        $this->createIndex(
            '{{%idx-item_balance-doc_id}}',
            '{{%item_balance}}',
            'doc_id'
        );

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-item_balance-product_id}}',
            '{{%item_balance}}',
            'product_id'
        );

        // add foreign key for table `{{%products}}`
        $this->addForeignKey(
            '{{%fk-item_balance-product_id}}',
            '{{%item_balance}}',
            'product_id',
            '{{%products}}',
            'id'
        );

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-item_balance-customer_id}}',
            '{{%item_balance}}',
            'customer_id'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-item_balance-customer_id}}',
            '{{%item_balance}}',
            'customer_id',
            '{{%customers}}',
            'id'
        );

        // creates index for column `measurement_id`
        $this->createIndex(
            '{{%idx-item_balance-measurement_id}}',
            '{{%item_balance}}',
            'measurement_id'
        );

        // add foreign key for table `{{%clients}}`
        $this->addForeignKey(
            '{{%fk-item_balance-measurement_id}}',
            '{{%item_balance}}',
            'measurement_id',
            '{{%measurements}}',
            'id'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-item_balance-department_id}}',
            '{{%item_balance}}',
            'department_id'
        );

        // add foreign key for table `{{%departments}}`
        $this->addForeignKey(
            '{{%fk-item_balance-department_id}}',
            '{{%item_balance}}',
            'department_id',
            '{{%departments}}',
            'id'
        );

        // creates index for column `dep_area_id`
        $this->createIndex(
            '{{%idx-item_balance-dep_area_id}}',
            '{{%item_balance}}',
            'dep_area_id'
        );

        // add foreign key for table `{{%dep_area}}`
        $this->addForeignKey(
            '{{%fk-item_balance-dep_area_id}}',
            '{{%item_balance}}',
            'dep_area_id',
            '{{%dep_area}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops index for column `doc_id`
        $this->dropIndex(
            '{{%idx-item_balance-doc_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%products}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-product_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-item_balance-product_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-customer_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-item_balance-customer_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%measurements}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-measurement_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `measurement_id`
        $this->dropIndex(
            '{{%idx-item_balance-measurement_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%departments}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-department_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-item_balance-department_id}}',
            '{{%item_balance}}'
        );

        // drops foreign key for table `{{%dep_area}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-dep_area_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `dep_area_id`
        $this->dropIndex(
            '{{%idx-item_balance-dep_area_id}}',
            '{{%item_balance}}'
        );

        $this->dropTable('{{%item_balance}}');
    }
}
