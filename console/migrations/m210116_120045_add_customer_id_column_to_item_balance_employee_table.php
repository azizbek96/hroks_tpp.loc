<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance_employee}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%customers}}`
 */
class m210116_120045_add_customer_id_column_to_item_balance_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance_employee}}', 'customer_id', $this->integer());

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-customer_id}}',
            '{{%item_balance_employee}}',
            'customer_id'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-customer_id}}',
            '{{%item_balance_employee}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-customer_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-customer_id}}',
            '{{%item_balance_employee}}'
        );

        $this->dropColumn('{{%item_balance_employee}}', 'customer_id');
    }
}
