<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance_boss}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%doc_items}}`
 */
class m210123_060549_add_doc_item_id_column_to_item_balance_boss_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance_boss}}', 'doc_item_id', $this->integer());

        // creates index for column `doc_item_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-doc_item_id}}',
            '{{%item_balance_boss}}',
            'doc_item_id'
        );

        // add foreign key for table `{{%doc_items}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-doc_item_id}}',
            '{{%item_balance_boss}}',
            'doc_item_id',
            '{{%doc_items}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%doc_items}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-doc_item_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `doc_item_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-doc_item_id}}',
            '{{%item_balance_boss}}'
        );

        $this->dropColumn('{{%item_balance_boss}}', 'doc_item_id');
    }
}
