<?php

use yii\db\Migration;

class m090121_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%groups}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'token' => $this->string()->notNull()->unique(),
            'type' => $this->integer()->defaultValue(1),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
        $this->batchInsert('groups',[
            'name',
            'token',
            'type',
            'status',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at'
        ],[
            ['super-admin',1,1,1,1,1,1505282484,1505282484]
          ]
        );
    }

    public function down()
    {
        $this->dropTable('{{%groups}}');
    }
}
