<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_rel_employee}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%employee}}`
 */
class m210111_174328_create_user_rel_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_rel_employee}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'employee_id' => $this->integer(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}',
            'employee_id'
        );

        // add foreign key for table `{{%employee}}`
        $this->addForeignKey(
            '{{%fk-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}',
            'employee_id',
            '{{%employee}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_rel_employee-user_id}}',
            '{{%user_rel_employee}}'
        );

        // drops foreign key for table `{{%employee}}`
        $this->dropForeignKey(
            '{{%fk-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-user_rel_employee-employee_id}}',
            '{{%user_rel_employee}}'
        );

        $this->dropTable('{{%user_rel_employee}}');
    }
}
