<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%measurements}}`.
 */
class m210114_170622_add_token_column_to_measurements_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%measurements}}', 'token', $this->string(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%measurements}}', 'token');
    }
}
