<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%customers}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 */
class m210115_062206_add_user_id_column_to_customers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%customers}}', 'user_id', $this->integer());

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-customers-user_id}}',
            '{{%customers}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-customers-user_id}}',
            '{{%customers}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-customers-user_id}}',
            '{{%customers}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-customers-user_id}}',
            '{{%customers}}'
        );

        $this->dropColumn('{{%customers}}', 'user_id');
    }
}
