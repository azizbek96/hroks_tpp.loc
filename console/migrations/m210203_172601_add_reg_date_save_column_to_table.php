<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%}}`.
 */
class m210203_172601_add_reg_date_save_column_to_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc}}', 'reg_date_save', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%doc}}', 'reg_date_save');
    }
}
