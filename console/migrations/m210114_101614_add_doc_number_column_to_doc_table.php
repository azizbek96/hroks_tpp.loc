<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc}}`.
 */
class m210114_101614_add_doc_number_column_to_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc}}', 'doc_number', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%doc}}', 'doc_number');
    }
}
