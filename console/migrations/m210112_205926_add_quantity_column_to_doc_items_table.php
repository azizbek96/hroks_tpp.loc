<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc_items}}`.
 */
class m210112_205926_add_quantity_column_to_doc_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc_items}}', 'quantity', $this->decimal(20,3));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%doc_items}}', 'quantity');
    }
}
