<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance_employee}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%doc}}`
 * - `{{%measurements}}`
 */
class m210115_165128_add_doc_id_column_to_measurement_id_column_to_item_balance_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance_employee}}', 'doc_id', $this->integer());
        $this->addColumn('{{%item_balance_employee}}', 'measurement_id', $this->integer());

        // creates index for column `doc_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-doc_id}}',
            '{{%item_balance_employee}}',
            'doc_id'
        );

        // add foreign key for table `{{%doc}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-doc_id}}',
            '{{%item_balance_employee}}',
            'doc_id',
            '{{%doc}}',
            'id',
            'CASCADE'
        );

        // creates index for column `measurement_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-measurement_id}}',
            '{{%item_balance_employee}}',
            'measurement_id'
        );

        // add foreign key for table `{{%measurements}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-measurement_id}}',
            '{{%item_balance_employee}}',
            'measurement_id',
            '{{%measurements}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%doc}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-doc_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `doc_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-doc_id}}',
            '{{%item_balance_employee}}'
        );

        // drops foreign key for table `{{%measurements}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-measurement_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `measurement_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-measurement_id}}',
            '{{%item_balance_employee}}'
        );

        $this->dropColumn('{{%item_balance_employee}}', 'doc_id');
        $this->dropColumn('{{%item_balance_employee}}', 'measurement_id');
    }
}
