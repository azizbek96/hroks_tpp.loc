<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%postion}}`.
 */
class m210112_053637_add_status_column_to_postion_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%position}}', 'status', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%position}}', 'status');
    }
}
