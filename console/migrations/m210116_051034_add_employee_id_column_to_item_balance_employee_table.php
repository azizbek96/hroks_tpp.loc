<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance_employee}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%employee}}`
 */
class m210116_051034_add_employee_id_column_to_item_balance_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance_employee}}', 'employee_id', $this->integer());

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-employee_id}}',
            '{{%item_balance_employee}}',
            'employee_id'
        );

        // add foreign key for table `{{%employee}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-employee_id}}',
            '{{%item_balance_employee}}',
            'employee_id',
            '{{%employee}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%employee}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-employee_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-employee_id}}',
            '{{%item_balance_employee}}'
        );

        $this->dropColumn('{{%item_balance_employee}}', 'employee_id');
    }
}
