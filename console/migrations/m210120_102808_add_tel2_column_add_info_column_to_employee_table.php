<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%employee}}`.
 */
class m210120_102808_add_tel2_column_add_info_column_to_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'tel2', $this->string(20));
        $this->addColumn('{{%employee}}', 'add_info', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'tel2');
        $this->dropColumn('{{%employee}}', 'add_info');
    }
}
