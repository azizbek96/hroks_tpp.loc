<?php

use yii\db\Migration;

/**
 * Class m210119_192640_insert_permission_table
 */
class m210119_192640_insert_permission_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_item', ['name' => 'shop', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'shop/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'shop/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'shop/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'shop/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'shop/delete', 'type' => '2', 'description' => '']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210119_192640_insert_permission_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210119_192640_insert_permission_table cannot be reverted.\n";

        return false;
    }
    */
}
