<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%product_categories}}`
 */
class m210109_152746_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'cat_id' => $this->integer(),
            'add_info' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `cat_id`
        $this->createIndex(
            '{{%idx-products-cat_id}}',
            '{{%products}}',
            'cat_id'
        );

        // add foreign key for table `{{%product_categories}}`
        $this->addForeignKey(
            '{{%fk-products-cat_id}}',
            '{{%products}}',
            'cat_id',
            '{{%product_categories}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%product_categories}}`
        $this->dropForeignKey(
            '{{%fk-products-cat_id}}',
            '{{%products}}'
        );

        // drops index for column `cat_id`
        $this->dropIndex(
            '{{%idx-products-cat_id}}',
            '{{%products}}'
        );

        $this->dropTable('{{%products}}');
    }
}
