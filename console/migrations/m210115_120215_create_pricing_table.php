<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pricing}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%products}}`
 */
class m210115_120215_create_pricing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pricing}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'price' => $this->decimal(20,3),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-pricing-product_id}}',
            '{{%pricing}}',
            'product_id'
        );

        // add foreign key for table `{{%products}}`
        $this->addForeignKey(
            '{{%fk-pricing-product_id}}',
            '{{%pricing}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%products}}`
        $this->dropForeignKey(
            '{{%fk-pricing-product_id}}',
            '{{%pricing}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-pricing-product_id}}',
            '{{%pricing}}'
        );

        $this->dropTable('{{%pricing}}');
    }
}
