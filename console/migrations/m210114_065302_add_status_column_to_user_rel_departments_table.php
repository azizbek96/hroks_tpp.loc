<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_rel_departents}}`.
 */
class m210114_065302_add_status_column_to_user_rel_departments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_rel_departments}}', 'status', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_rel_departments}}', 'status');
    }
}
