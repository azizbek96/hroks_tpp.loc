<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dep_area}}`.
 */
class m210109_152651_create_dep_area_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("create table dep_area
(
    id            int auto_increment
        primary key,
    name          varchar(255)         null,
    code          varchar(50)          null,
    dep_id        int                  null,
    parent_id     int                  null,
    type          smallint             null,
    add_info      text                 null,
    status        smallint   default 1 null,
    created_by    int                  null,
    updated_by    int                  null,
    created_at    int                  null,
    updated_at    int                  null,
    root          int                  null,
    lft           int                  null,
    rgt           int                  null,
    lvl           smallint             null,
    icon          varchar(255)         null,
    icon_type     smallint   default 1 null,
    active        tinyint(1) default 1 null,
    selected      tinyint(1) default 0 null,
    disabled      tinyint(1) default 0 null,
    readonly      tinyint(1) default 0 null,
    visible       tinyint(1) default 1 null,
    collapsed     tinyint(1) default 0 null,
    movable_u     tinyint(1) default 1 null,
    movable_d     tinyint(1) default 1 null,
    movable_l     tinyint(1) default 1 null,
    movable_r     tinyint(1) default 1 null,
    removable     tinyint(1) default 1 null,
    removable_all tinyint(1) default 0 null,
    child_allowed tinyint(1) default 1 null,
    token         varchar(30)          null,
    constraint `fk-dep_area-dep_id`
        foreign key (dep_id) references departments (id)
)
    charset = utf8;
        create index `idx-dep_area-dep_id`
            on dep_area (dep_id);
        create index `idx-dep_area-parent_id`
            on dep_area (parent_id);
        create index tree_NK1
            on dep_area (root);
        create index tree_NK2
            on dep_area (lft);
        create index tree_NK3
            on dep_area (rgt);
        create index tree_NK4
            on dep_area (lvl);
        create index tree_NK5
            on dep_area (active);");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dep_area}}');
    }
}
