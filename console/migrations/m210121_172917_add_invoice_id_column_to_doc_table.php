<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc}}`.
 */
class m210121_172917_add_invoice_id_column_to_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc}}', 'invoice_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%doc}}', 'invoice_id');
    }
}
