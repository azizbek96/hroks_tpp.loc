<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%doc}}`
 */
class m210113_174651_add_parent_id_column_to_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc}}', 'parent_id', $this->integer());

        // creates index for column `parent_id`
        $this->createIndex(
            '{{%idx-doc-parent_id}}',
            '{{%doc}}',
            'parent_id'
        );

        // add foreign key for table `{{%doc}}`
        $this->addForeignKey(
            '{{%fk-doc-parent_id}}',
            '{{%doc}}',
            'parent_id',
            '{{%doc}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%doc}}`
        $this->dropForeignKey(
            '{{%fk-doc-parent_id}}',
            '{{%doc}}'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            '{{%idx-doc-parent_id}}',
            '{{%doc}}'
        );

        $this->dropColumn('{{%doc}}', 'parent_id');
    }
}
