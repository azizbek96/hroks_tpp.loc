<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%doc_items}}`
 */
class m210113_042918_add_doc_item_id_column_to_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance}}', 'doc_item_id', $this->integer());

        // creates index for column `doc_item_id`
        $this->createIndex(
            '{{%idx-item_balance-doc_item_id}}',
            '{{%item_balance}}',
            'doc_item_id'
        );

        // add foreign key for table `{{%doc_items}}`
        $this->addForeignKey(
            '{{%fk-item_balance-doc_item_id}}',
            '{{%item_balance}}',
            'doc_item_id',
            '{{%doc_items}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%doc_items}}`
        $this->dropForeignKey(
            '{{%fk-item_balance-doc_item_id}}',
            '{{%item_balance}}'
        );

        // drops index for column `doc_item_id`
        $this->dropIndex(
            '{{%idx-item_balance-doc_item_id}}',
            '{{%item_balance}}'
        );

        $this->dropColumn('{{%item_balance}}', 'doc_item_id');
    }
}
