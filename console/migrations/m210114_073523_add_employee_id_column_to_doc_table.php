<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%employee}}`
 */
class m210114_073523_add_employee_id_column_to_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc}}', 'employee_id', $this->integer());

        // creates index for column `employee_id`
        $this->createIndex(
            '{{%idx-doc-employee_id}}',
            '{{%doc}}',
            'employee_id'
        );

        // add foreign key for table `{{%employee}}`
        $this->addForeignKey(
            '{{%fk-doc-employee_id}}',
            '{{%doc}}',
            'employee_id',
            '{{%employee}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%employee}}`
        $this->dropForeignKey(
            '{{%fk-doc-employee_id}}',
            '{{%doc}}'
        );

        // drops index for column `employee_id`
        $this->dropIndex(
            '{{%idx-doc-employee_id}}',
            '{{%doc}}'
        );

        $this->dropColumn('{{%doc}}', 'employee_id');
    }
}
