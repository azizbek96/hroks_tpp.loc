<?php

use yii\db\Migration;

/**
 * Class m210120_113306_insesrt_positions
 */
class m210120_113306_insesrt_positions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_item', ['name' => 'position', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'position/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'position/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'position/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'position/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'position/view', 'type' => '2', 'description' => '']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210120_113306_insesrt_positions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210120_113306_insesrt_positions cannot be reverted.\n";

        return false;
    }
    */
}
