<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%item_balance_boss}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%products}}`
 */
class m210123_050853_add_product_id_column_to_item_balance_boss_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%item_balance_boss}}', 'product_id', $this->integer());

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-product_id}}',
            '{{%item_balance_boss}}',
            'product_id'
        );

        // add foreign key for table `{{%products}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-product_id}}',
            '{{%item_balance_boss}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%products}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-product_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-product_id}}',
            '{{%item_balance_boss}}'
        );

        $this->dropColumn('{{%item_balance_boss}}', 'product_id');
    }
}
