<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%measurements}}`.
 */
class m210109_154229_create_measurements_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%measurements}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'add_info' => $this->text(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%measurements}}');
    }
}
