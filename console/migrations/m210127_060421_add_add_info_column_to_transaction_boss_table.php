<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%transaction_boss}}`.
 */
class m210127_060421_add_add_info_column_to_transaction_boss_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%transaction_boss}}', 'add_info', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%transaction_boss}}', 'add_info');
    }
}
