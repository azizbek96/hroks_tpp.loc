<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc}}`.
 */
class m210202_061638_add_amount_column_to_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%doc}}', 'amount', $this->decimal(20,3));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%doc}}', 'amount');
    }
}
