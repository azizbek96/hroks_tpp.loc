<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_balance_boss}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%customers}}`
 * - `{{%doc}}`
 * - `{{%measurements}}`
 * - `{{%departments}}`
 * - `{{%dep_area}}`
 */
class m210121_163436_create_item_balance_boss_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_balance_boss}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'customer_id' => $this->integer(),
            'reg_date' => $this->integer(),
            'doc_id' => $this->integer(),
            'measurement_id' => $this->integer(),
            'currency' => $this->integer(),
            'department_id' => $this->integer(),
            'dep_area_id' => $this->integer(),
            'amount' => $this->decimal(20,3),
            'inventory' => $this->decimal(20,3),
            'type' => $this->smallInteger(),
            'status' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-user_id}}',
            '{{%item_balance_boss}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-user_id}}',
            '{{%item_balance_boss}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-customer_id}}',
            '{{%item_balance_boss}}',
            'customer_id'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-customer_id}}',
            '{{%item_balance_boss}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE'
        );

        // creates index for column `doc_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-doc_id}}',
            '{{%item_balance_boss}}',
            'doc_id'
        );

        // add foreign key for table `{{%doc}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-doc_id}}',
            '{{%item_balance_boss}}',
            'doc_id',
            '{{%doc}}',
            'id',
            'CASCADE'
        );

        // creates index for column `measurement_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-measurement_id}}',
            '{{%item_balance_boss}}',
            'measurement_id'
        );

        // add foreign key for table `{{%measurements}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-measurement_id}}',
            '{{%item_balance_boss}}',
            'measurement_id',
            '{{%measurements}}',
            'id',
            'CASCADE'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-department_id}}',
            '{{%item_balance_boss}}',
            'department_id'
        );

        // add foreign key for table `{{%departments}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-department_id}}',
            '{{%item_balance_boss}}',
            'department_id',
            '{{%departments}}',
            'id',
            'CASCADE'
        );

        // creates index for column `dep_area_id`
        $this->createIndex(
            '{{%idx-item_balance_boss-dep_area_id}}',
            '{{%item_balance_boss}}',
            'dep_area_id'
        );

        // add foreign key for table `{{%dep_area}}`
        $this->addForeignKey(
            '{{%fk-item_balance_boss-dep_area_id}}',
            '{{%item_balance_boss}}',
            'dep_area_id',
            '{{%dep_area}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-user_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-user_id}}',
            '{{%item_balance_boss}}'
        );

        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-customer_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-customer_id}}',
            '{{%item_balance_boss}}'
        );

        // drops foreign key for table `{{%doc}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-doc_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `doc_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-doc_id}}',
            '{{%item_balance_boss}}'
        );

        // drops foreign key for table `{{%measurements}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-measurement_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `measurement_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-measurement_id}}',
            '{{%item_balance_boss}}'
        );

        // drops foreign key for table `{{%departments}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-department_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-department_id}}',
            '{{%item_balance_boss}}'
        );

        // drops foreign key for table `{{%dep_area}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_boss-dep_area_id}}',
            '{{%item_balance_boss}}'
        );

        // drops index for column `dep_area_id`
        $this->dropIndex(
            '{{%idx-item_balance_boss-dep_area_id}}',
            '{{%item_balance_boss}}'
        );

        $this->dropTable('{{%item_balance_boss}}');
    }
}
