<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%access_token}}`.
 */
class m210422_145303_add_column_to_access_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'access_token', $this->string(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user','access_token');
    }
}
