<?php

use yii\db\Migration;

/**
 * Class m210205_141237_add_box_count_to_item_balance_table
 */
class m210205_141237_add_box_count_to_item_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('item_balance','karopka_count', $this->decimal(20,3)->defaultValue(0));
        $this->addColumn('item_balance','karopka_inventory', $this->decimal(20,3)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('item_balance','karopka_count');
        $this->dropColumn('item_balance','karopka_inventory');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210205_141237_add_box_count_to_item_balance_table cannot be reverted.\n";

        return false;
    }
    */
}
