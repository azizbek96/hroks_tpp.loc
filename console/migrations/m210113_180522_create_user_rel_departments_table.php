<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_rel_departments}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%departments}}`
 */
class m210113_180522_create_user_rel_departments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_rel_departments}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'department_id' => $this->integer(),
            'type' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-user_rel_departments-user_id}}',
            '{{%user_rel_departments}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-user_rel_departments-user_id}}',
            '{{%user_rel_departments}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-user_rel_departments-department_id}}',
            '{{%user_rel_departments}}',
            'department_id'
        );

        // add foreign key for table `{{%departments}}`
        $this->addForeignKey(
            '{{%fk-user_rel_departments-department_id}}',
            '{{%user_rel_departments}}',
            'department_id',
            '{{%departments}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-user_rel_departments-user_id}}',
            '{{%user_rel_departments}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-user_rel_departments-user_id}}',
            '{{%user_rel_departments}}'
        );

        // drops foreign key for table `{{%departments}}`
        $this->dropForeignKey(
            '{{%fk-user_rel_departments-department_id}}',
            '{{%user_rel_departments}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-user_rel_departments-department_id}}',
            '{{%user_rel_departments}}'
        );

        $this->dropTable('{{%user_rel_departments}}');
    }
}
