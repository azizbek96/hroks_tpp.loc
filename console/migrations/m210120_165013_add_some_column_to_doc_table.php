<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%doc}}`.
 */
class m210120_165013_add_some_column_to_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('doc','invoice_count', $this->smallInteger()->null());
        $this->addColumn('doc','invoice_number', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('doc','invoice_count');
        $this->dropColumn('doc','invoice_number');
    }
}
