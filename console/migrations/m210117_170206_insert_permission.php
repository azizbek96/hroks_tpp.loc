<?php

use yii\db\Migration;

/**
 * Class m210117_170206_insert_permission
 */
class m210117_170206_insert_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('auth_item', ['name' => 'auth-assignment', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-assignment/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-assignment/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-assignment/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-assignment/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-assignment/view', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'auth-item', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item/update', 'type' => '2', 'description' => '']);


        $this->insert('auth_item', ['name' => 'auth-item-child', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item-child/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item-child/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item-child/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item-child/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'auth-item-child/view', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'permission', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'permission/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'permission/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'permission/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'permission/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'permission/view', 'type' => '2', 'description' => '']);


        $this->insert('auth_item', ['name' => 'user', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user/delete', 'type' => '2', 'description' => '']);


        $this->insert('auth_item', ['name' => 'super-admin', 'type' => '1', 'description' => '']);
        $this->insert('auth_item', ['name' => 'Tarqatuvchi', 'type' => '1', 'description' => '']);


        // Auth-item-child inserts
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/index']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/delete']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/update']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/create']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-assignment/view']);

        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/index']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/create']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/delete']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/view']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item/update']);

        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/index']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/delete']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/view']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/update']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'auth-item-child/create']);

        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'user/index']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'user/delete']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'user/view']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'user/update']);
        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'user/create']);
//        $this->insert('auth_item_child', ['parent' => 'super-admin', 'child' => 'user/reset']);


        $this->insert('auth_assignment', ['item_name' => 'super-admin', 'user_id' => '1']);
        //////

        $this->insert('auth_item', ['name' => 'user-rel-employee', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-employee/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-employee/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-employee/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-employee/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-employee/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'user-rel-departments', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-departments/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-departments/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-departments/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-departments/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'user-rel-departments/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'products', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'products/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'products/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'products/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'products/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'products/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'employee', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'employee/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'employee/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'employee/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'employee/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'employee/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'departments', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'departments/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'departments/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'departments/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'departments/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'departments/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'dep-area', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'dep-area/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'dep-area/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'dep-area/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'dep-area/delete', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'dep-area/view', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'department-responsible', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'department-responsible/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'department-responsible/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'department-responsible/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'department-responsible/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'department-responsible/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'pricing', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'pricing/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'pricing/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'pricing/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'pricing/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'pricing/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'customers', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'customers/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'customers/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'customers/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'customers/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'customers/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'measurements', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'measurements/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'measurements/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'measurements/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'measurements/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'measurements/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'regions', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'regions/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'regions/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'regions/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'regions/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'regions/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'transaction', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'transaction/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'transaction/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'transaction/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'transaction/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'transaction/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'doc/kirim', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/kirim/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/kirim/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/kirim/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/kirim/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/kirim/delete', 'type' => '2', 'description' => '']);


        $this->insert('auth_item', ['name' => 'doc/topshirish', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/topshirish/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/topshirish/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/topshirish/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/topshirish/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/topshirish/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'doc/hiobdan_chiqarish', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/hiobdan_chiqarish/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/hiobdan_chiqarish/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/hiobdan_chiqarish/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/hiobdan_chiqarish/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/hiobdan_chiqarish/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'doc/qayta_qabul', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qayta_qabul/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qayta_qabul/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qayta_qabul/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qayta_qabul/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qayta_qabul/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'doc/qabul', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qabul/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qabul/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qabul/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qabul/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/qabul/delete', 'type' => '2', 'description' => '']);

        $this->insert('auth_item', ['name' => 'doc/sotish_employee', 'type' => '3', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/sotish_employee/index', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/sotish_employee/create', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/sotish_employee/update', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/sotish_employee/view', 'type' => '2', 'description' => '']);
        $this->insert('auth_item', ['name' => 'doc/sotish_employee/delete', 'type' => '2', 'description' => '']);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210117_170206_insert_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210117_170206_insert_permission cannot be reverted.\n";

        return false;
    }
    */
}
