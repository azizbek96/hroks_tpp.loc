<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%employee}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%position}}`
 */
class m210120_110616_add_position_id_column_to_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'position_id', $this->integer());

        // creates index for column `position_id`
        $this->createIndex(
            '{{%idx-employee-position_id}}',
            '{{%employee}}',
            'position_id'
        );

        // add foreign key for table `{{%position}}`
        $this->addForeignKey(
            '{{%fk-employee-position_id}}',
            '{{%employee}}',
            'position_id',
            '{{%position}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%position}}`
        $this->dropForeignKey(
            '{{%fk-employee-position_id}}',
            '{{%employee}}'
        );

        // drops index for column `position_id`
        $this->dropIndex(
            '{{%idx-employee-position_id}}',
            '{{%employee}}'
        );

        $this->dropColumn('{{%employee}}', 'position_id');
    }
}
