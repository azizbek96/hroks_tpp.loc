<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%department_responsible}}`.
 */
class m210112_053702_add_status_column_to_department_responsible_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%department_responsible}}', 'status', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%department_responsible}}', 'status');
    }
}
