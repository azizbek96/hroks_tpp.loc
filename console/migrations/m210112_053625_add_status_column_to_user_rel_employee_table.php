<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%user_rel_employee}}`.
 */
class m210112_053625_add_status_column_to_user_rel_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_rel_employee}}', 'status', $this->smallInteger());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_rel_employee}}', 'status');
    }
}
