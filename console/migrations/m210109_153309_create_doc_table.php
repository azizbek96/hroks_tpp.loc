<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%doc}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%customers}}`
 */
class m210109_153309_create_doc_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%doc}}', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer(),
            'department_id' => $this->integer(),
            'reg_date' => $this->date(),
            'add_info' => $this->text(),
            'type' => $this->smallInteger()->defaultValue(1),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-doc-customer_id}}',
            '{{%doc}}',
            'customer_id'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-doc-customer_id}}',
            '{{%doc}}',
            'customer_id',
            '{{%customers}}',
            'id'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-doc-department_id}}',
            '{{%doc}}',
            'department_id'
        );

        // add foreign key for table `{{%departments}}`
        $this->addForeignKey(
            '{{%fk-doc-department_id}}',
            '{{%doc}}',
            'department_id',
            '{{%departments}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-doc-customer_id}}',
            '{{%doc}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-doc-customer_id}}',
            '{{%doc}}'
        );

        // drops foreign key for table `{{%departments}}`
        $this->dropForeignKey(
            '{{%fk-doc-department_id}}',
            '{{%doc}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-doc-department_id}}',
            '{{%doc}}'
        );

        $this->dropTable('{{%doc}}');
    }
}
