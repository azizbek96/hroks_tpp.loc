<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%customers}}`
 */
class m210115_100009_create_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transaction}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'customer_id' => $this->integer(),
            'amount' => $this->decimal(20,3),
            'inventory' => $this->decimal(20,3),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-transaction-user_id}}',
            '{{%transaction}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-transaction-user_id}}',
            '{{%transaction}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `customer_id`
        $this->createIndex(
            '{{%idx-transaction-customer_id}}',
            '{{%transaction}}',
            'customer_id'
        );

        // add foreign key for table `{{%customers}}`
        $this->addForeignKey(
            '{{%fk-transaction-customer_id}}',
            '{{%transaction}}',
            'customer_id',
            '{{%customers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-transaction-user_id}}',
            '{{%transaction}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-transaction-user_id}}',
            '{{%transaction}}'
        );

        // drops foreign key for table `{{%customers}}`
        $this->dropForeignKey(
            '{{%fk-transaction-customer_id}}',
            '{{%transaction}}'
        );

        // drops index for column `customer_id`
        $this->dropIndex(
            '{{%idx-transaction-customer_id}}',
            '{{%transaction}}'
        );

        $this->dropTable('{{%transaction}}');
    }
}
