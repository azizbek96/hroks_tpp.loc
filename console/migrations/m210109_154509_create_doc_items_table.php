<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%doc_items}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%doc}}`
 * - `{{%products}}`
 * - `{{%measurements}}`
 */
class m210109_154509_create_doc_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%doc_items}}', [
            'id' => $this->primaryKey(),
            'doc_id' => $this->integer(),
            'product_id' => $this->integer(),
            'dep_area_id' => $this->integer(),
            'measurement_id' => $this->integer(),
            'incoming_price' => $this->decimal(20,3)->defaultValue(0),
            'sold_price' => $this->decimal(20,3)->defaultValue(0),
            'currency' => $this->smallInteger()->defaultValue(1),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `doc_id`
        $this->createIndex(
            '{{%idx-doc_items-doc_id}}',
            '{{%doc_items}}',
            'doc_id'
        );

        // add foreign key for table `{{%doc}}`
        $this->addForeignKey(
            '{{%fk-doc_items-doc_id}}',
            '{{%doc_items}}',
            'doc_id',
            '{{%doc}}',
            'id',
            'CASCADE'
        );

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-doc_items-product_id}}',
            '{{%doc_items}}',
            'product_id'
        );

        // add foreign key for table `{{%products}}`
        $this->addForeignKey(
            '{{%fk-doc_items-product_id}}',
            '{{%doc_items}}',
            'product_id',
            '{{%products}}',
            'id'
        );

        // creates index for column `measurement_id`
        $this->createIndex(
            '{{%idx-doc_items-measurement_id}}',
            '{{%doc_items}}',
            'measurement_id'
        );

        // add foreign key for table `{{%measurements}}`
        $this->addForeignKey(
            '{{%fk-doc_items-measurement_id}}',
            '{{%doc_items}}',
            'measurement_id',
            '{{%measurements}}',
            'id'
        );
        // creates index for column `dep_area_id`
        $this->createIndex(
            '{{%idx-doc_items-dep_area_id}}',
            '{{%doc_items}}',
            'dep_area_id'
        );

        // add foreign key for table `{{%dep_area}}`
        $this->addForeignKey(
            '{{%fk-doc_items-dep_area_id}}',
            '{{%doc_items}}',
            'dep_area_id',
            '{{%dep_area}}',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%doc}}`
        $this->dropForeignKey(
            '{{%fk-doc_items-doc_id}}',
            '{{%doc_items}}'
        );

        // drops index for column `doc_id`
        $this->dropIndex(
            '{{%idx-doc_items-doc_id}}',
            '{{%doc_items}}'
        );

        // drops foreign key for table `{{%products}}`
        $this->dropForeignKey(
            '{{%fk-doc_items-product_id}}',
            '{{%doc_items}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-doc_items-product_id}}',
            '{{%doc_items}}'
        );

        // drops foreign key for table `{{%measurements}}`
        $this->dropForeignKey(
            '{{%fk-doc_items-measurement_id}}',
            '{{%doc_items}}'
        );

        // drops index for column `measurement_id`
        $this->dropIndex(
            '{{%idx-doc_items-measurement_id}}',
            '{{%doc_items}}'
        );

        // drops foreign key for table `{{%dep_area}}`
        $this->dropForeignKey(
            '{{%fk-doc_items-dep_area_id}}',
            '{{%doc_items}}'
        );

        // drops index for column `dep_area_id`
        $this->dropIndex(
            '{{%idx-doc_items-dep_area_id}}',
            '{{%doc_items}}'
        );

        $this->dropTable('{{%doc_items}}');
    }
}
