<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%item_balance_employee}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%products}}`
 * - `{{%user}}`
 * - `{{%departments}}`
 */
class m210115_095622_create_item_balance_employee_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%item_balance_employee}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'user_id' => $this->integer(),
            'amount' => $this->decimal(20,3),
            'inventory' => $this->decimal(20,3),
            'status' => $this->smallInteger(),
            'type' => $this->smallInteger(),
            'department_id' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-product_id}}',
            '{{%item_balance_employee}}',
            'product_id'
        );

        // add foreign key for table `{{%products}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-product_id}}',
            '{{%item_balance_employee}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-user_id}}',
            '{{%item_balance_employee}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-user_id}}',
            '{{%item_balance_employee}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `department_id`
        $this->createIndex(
            '{{%idx-item_balance_employee-department_id}}',
            '{{%item_balance_employee}}',
            'department_id'
        );

        // add foreign key for table `{{%departments}}`
        $this->addForeignKey(
            '{{%fk-item_balance_employee-department_id}}',
            '{{%item_balance_employee}}',
            'department_id',
            '{{%departments}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%products}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-product_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-product_id}}',
            '{{%item_balance_employee}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-user_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-user_id}}',
            '{{%item_balance_employee}}'
        );

        // drops foreign key for table `{{%departments}}`
        $this->dropForeignKey(
            '{{%fk-item_balance_employee-department_id}}',
            '{{%item_balance_employee}}'
        );

        // drops index for column `department_id`
        $this->dropIndex(
            '{{%idx-item_balance_employee-department_id}}',
            '{{%item_balance_employee}}'
        );

        $this->dropTable('{{%item_balance_employee}}');
    }
}
