<?php

use yii\web\View;

/* @var $this yii\web\View */


$this->title = Yii::t('app', 'Vodiysitrus.uz');
?>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#"><?php echo $this->title ?></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<header class="masthead text-center text-white d-flex">
    <div class="container my-auto">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <h1 class="text-uppercase">
                    <strong style="font-size: 2rem; padding: 10px;">Сўнги Янгиликлар</strong>
                </h1>
                <hr>
            </div>
            <div class="col-lg-8 mx-auto">
                <div class="owl-carousel">
                    <?php if(!empty($news)):?>
                    <?php foreach ($news as $item):?>
                        <div>
                            <p class="text-faded mb-5" style="background: #20a0eaab;padding: 15px;color: #fff;">
                               <?= $item['content_uz']?>
                            </p>
                        </div>
                    <?php endforeach;endif;?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center" style="background: #20a0eaab;padding: 15px;color: #fff;">
                Буюртмалар учун: <i class="fa fa-phone fa-3x mb-3 sr-contact-1"></i>
                <p>(+998 91) 156 71 44 </p>
            </div>
        </div>
    </div>
</header>

<?php
$this->registerJs(
    "$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav: false,
    responsiveClass:true,
    dots: true,
    autoplay: true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:true,
            loop:false
        }
    }
}) 
  ",
    View::POS_READY,
    'owl-slider'
);
?>



