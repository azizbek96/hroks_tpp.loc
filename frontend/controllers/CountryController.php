<?php

namespace frontend\controllers;

use yii\rest\ActiveController;

/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends ActiveController
{
    public $modelClass = 'frontend\models\Country';

    public function actionIndex(){
        return $this->render('index');
    }
}


