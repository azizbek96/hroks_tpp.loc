<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'js/loader.css',
        'css/pnotify.css',
        'css/default.css',
    ];
    public $js = [
        'js/site.js',
        'js/pnotify.js',
         'js/default.js',
        'js/MyNumber.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
