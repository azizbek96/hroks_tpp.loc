<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\SqlDataProvider;

/**
 * TransactionSearch represents the model behind the search form of `backend\models\Transaction`.
 */
class ReportSearch extends Report
{

    public $name;
    public $summa;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'reg_date', 'customer_id', 'status', 'type', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'inventory'], 'number'],
            [['inventory_item', 'department_are', 'department', 'product'], 'safe'],
            [['contragent', 'contragent_id', 'employee'], 'safe'],
            [['name'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchItemBalance($status)
    {

        $params = Yii::$app->request->get();

        $this->load($params);
        $where = '';
        if (!empty($this->product)) {
            $where .= sprintf(" AND p.name LIKE '%%%s%%'", $this->product);
        }
        if (!empty($this->department)) {
            $where .= sprintf(" AND dep.name LIKE '%%%s%%'", $this->department);
        }
        if (!empty($this->department_are)) {
            $where .= sprintf(" AND depa.name LIKE '%%%s%%'", $this->department_are);
        }
        if (!empty($this->inventory_item)) {
            $where .= sprintf(" AND itb.inventory = %d", $this->inventory_item);
        }
        $sql = sprintf("SELECT itb.id,
                                       p.name as product,
                                       dep.name      as department,
                                       depa.name     AS department_area,
                                       itb.inventory AS inventory_item,
                                       itb.status,
                                       itb.karopka_inventory AS karopka_count,
                                       m.name as birlik,
                                       m.id as measurement_id
                                FROM item_balance as itb
                                         LEFT JOIN products p on itb.product_id = p.id
                                         LEFT JOIN departments as dep ON dep.id = itb.department_id
                                         LEFT JOIN dep_area as depa ON depa.id = itb.dep_area_id
                                         LEFT JOIN doc ON doc.id = itb.doc_item_id
                                         LEFT JOIN measurements m ON itb.measurement_id = m.id
                                         LEFT JOIN (SELECT *
                                                    FROM item_balance AS i_b
                                                    WHERE i_b.id in (SELECT MAX(i_b2.id)
                                                                     FROM item_balance AS i_b2
                                                                     GROUP BY i_b2.product_id)) AS i_b ON i_b.product_id = p.id
                                         LEFT JOIN doc_items as doci ON doci.id = itb.doc_item_id
                                WHERE itb.id in (SELECT MAX(i_b2.id)
                                                 FROM item_balance AS i_b2
                                                 GROUP BY i_b2.product_id, i_b2.department_id, i_b2.dep_area_id,i_b2.measurement_id)  %s", $where);
        if ($status === 0) {
            $sql .= " AND  itb.inventory = 0 and itb.karopka_inventory = 0";
        }
        if ($status === 1) {
            $sql .= " AND  itb.inventory > 0 and itb.karopka_inventory > 0";
        }
        $count = Yii::$app->db->createCommand(
            str_replace("SELECT itb.id,
                                       p.name as product,
                                       dep.name      as department,
                                       depa.name     AS department_area,
                                       itb.inventory AS inventory_item,
                                       itb.status,
                                       itb.karopka_inventory AS karopka_count,
                                       m.name as birlik,
                                       m.id as measurement_id
                                FROM", "SELECT COUNT(*) FROM", $sql))->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC
                ],
                'attributes' => [
                    'id' => [
                        'asc' => ['id' => SORT_ASC],
                        'desc' => ['id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'product' => [
                        'asc' => ['name_uz' => SORT_ASC],
                        'desc' => ['name_uz' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'department_area' => [
                        'asc' => ['name_en' => SORT_ASC],
                        'desc' => ['name_en' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'inventory' => [
                        'asc' => ['name_ru' => SORT_ASC],
                        'desc' => ['name_ru' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchItemBalanceBoss($status)
    {

        $params = Yii::$app->request->get();

        $this->load($params);
        $where = '';
        if (!empty($this->product)) {
            $where .= sprintf(" AND p.name LIKE '%%%s%%'", $this->product);
        }
        if (!empty($this->department)) {
            $where .= sprintf(" AND dep.name LIKE '%%%s%%'", $this->department);
        }
        if (!empty($this->department_are)) {
            $where .= sprintf(" AND depa.name LIKE '%%%s%%'", $this->department_are);
        }
        if (!empty($this->inventory_item)) {
            $where .= sprintf(" AND itb.inventory = %d", $this->inventory_item);
        }
        $sql = sprintf("SELECT itb.id,
                                       p.name as product,
                                       dep.name      as department,
                                       depa.name     AS department_area,
                                       itb.inventory AS inventory_item,
                                       itb.status,
                                       m.name as birlik,
                                       m.id as measurement_id
                                FROM item_balance_boss as itb
                                         LEFT JOIN products p on itb.product_id = p.id
                                         LEFT JOIN departments as dep ON dep.id = itb.department_id
                                         LEFT JOIN dep_area as depa ON depa.id = itb.dep_area_id
                                         LEFT JOIN doc ON doc.id = itb.doc_item_id
                                         LEFT JOIN measurements m ON itb.measurement_id = m.id
                                         LEFT JOIN (SELECT *
                                                    FROM item_balance_boss AS i_b
                                                    WHERE i_b.id in (SELECT MAX(i_b2.id)
                                                                     FROM item_balance_boss AS i_b2
                                                                     GROUP BY i_b2.product_id)) AS i_b ON i_b.product_id = p.id
                                         LEFT JOIN doc_items as doci ON doci.id = itb.doc_item_id
                                WHERE itb.id in (SELECT MAX(i_b2.id)
                                                 FROM item_balance_boss AS i_b2
                                                 GROUP BY i_b2.product_id, i_b2.department_id, i_b2.dep_area_id , i_b2.measurement_id)  %s order by dep.name asc", $where);
        if ($status === 0) {
            $sql .= " AND  itb.inventory = 0";
        }
        $count = Yii::$app->db->createCommand(
            str_replace("SELECT itb.id,
                                       p.name as product,
                                       dep.name      as department,
                                       depa.name     AS department_area,
                                       itb.inventory AS inventory_item,
                                       itb.status,
                                       m.name as birlik,
                                       m.id as measurement_id
                                FROM", "SELECT COUNT(*) FROM", $sql))->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC
                ],
                'attributes' => [
                    'id' => [
                        'asc' => ['id' => SORT_ASC],
                        'desc' => ['id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'product' => [
                        'asc' => ['name_uz' => SORT_ASC],
                        'desc' => ['name_uz' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'department_area' => [
                        'asc' => ['name_en' => SORT_ASC],
                        'desc' => ['name_en' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'inventory' => [
                        'asc' => ['name_ru' => SORT_ASC],
                        'desc' => ['name_ru' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchContragentBoss()
    {
        $params = \Yii::$app->request->get();
        $this->load($params);
        $where = '';
        if (!empty($this->contragent)) {
            $where .= sprintf(" where  c.name LIKE '%%%s%%'", $this->contragent);
        }
        $sql = sprintf("SELECT c.id as contragent_id,
                                   c.name as contragent,
                                   (SELECT SUM(TB_2.amount) FROM transaction_boss AS TB_2 WHERE TB_2.type = %d AND c.id = TB_2.customer_id AND TB_2.status = %d) AS dept,
                                   (SELECT SUM(TB_2.amount) FROM transaction_boss AS TB_2 WHERE TB_2.type = %d AND c.id = TB_2.customer_id AND TB_2.status = %d) AS paid,
                                   ((SELECT SUM(TB_2.amount) FROM transaction_boss AS TB_2 WHERE TB_2.type = %d AND c.id = TB_2.customer_id AND TB_2.status = %d) + (SELECT SUM(TB_2.amount) FROM transaction_boss AS TB_2 WHERE TB_2.type = %d AND c.id = TB_2.customer_id AND TB_2.status = %d)) as saldo
                            FROM transaction_boss AS TB
                                     LEFT JOIN customers c on c.id = TB.customer_id
                            %s group by c.id", self::T_SUPPLIER_BOSS_DEPT, self::STATUS_ACTIVE, self::T_SUPPLIER_BOSS_PAID, self::STATUS_ACTIVE, self::T_SUPPLIER_BOSS_DEPT, self::STATUS_ACTIVE, self::T_SUPPLIER_BOSS_PAID, self::STATUS_ACTIVE, $where);
        $count = \Yii::$app->db->createCommand(
            str_replace("SELECT * FROM", "SELECT COUNT(*) FROM", $sql))->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'sort' => [
                'defaultOrder' => [
                    'contragent_id' => SORT_ASC
                ],
                'attributes' => [
                    'contragent_id' => [
                        'asc' => ['contragent_id' => SORT_ASC],
                        'desc' => ['contragent_id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'contragent' => [
                        'asc' => ['contragent' => SORT_ASC],
                        'desc' => ['contragent' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
                'forcePageParam' => true
            ]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchMijozlardanAgentlarningHaqqi()
    {
        $params = \Yii::$app->request->get();
        $this->load($params);
        $where = '';
        if (!empty($this->contragent)) {
            $where .= sprintf(" AND c.name LIKE '%%%s%%'", $this->contragent);
        }
        if (!empty($this->employee)) {
            $where .= sprintf(" AND t.user_id = %d", $this->employee);
        }
        if (!empty($this->contragent_id)) {
            $where .= sprintf(" AND t.user_id = %d", $this->contragent_id);
        }
        $sql = "SELECT
                        c.id   as contragent_id,
                        c.name as contragent,
                        c.address,
                        c.tel1,
                        c.fullname as c_n,
                        t.user_id as user_id,
                        (SELECT
                             e.fullname
                         FROM    transaction as t2
                                     LEFT JOIN user u on t2.user_id = u.id
                                     LEFT JOIN user_rel_employee ure on u.id = ure.user_id
                                     LEFT JOIN employee e on ure.employee_id = e.id
                                     LEFT JOIN position p on e.position_id = p.id
                         WHERE p.token ='AGENT' and u.id = t.user_id GROUP BY e.id
                        ) as employee,
                        (SELECT SUM(t_2.amount) FROM transaction as t_2
                         WHERE t_2.status = 1
                           and t_2.type = 1
                           and t_2.customer_id = c.id) as paid,
                        (SELECT SUM(t_2.amount) FROM transaction as t_2
                         WHERE t_2.status = 1
                           and t_2.type = 2
                           and t_2.customer_id = c.id) as dept,
                        ((SELECT SUM(t_2.amount) FROM transaction as t_2
                          WHERE t_2.status = 1
                            and t_2.type = 1
                            and t_2.customer_id = c.id)+(SELECT SUM(t_2.amount) FROM transaction as t_2
                                                         WHERE t_2.status = 1
                                                           and t_2.type = 2
                                                           and t_2.customer_id = c.id)) as saldo
                    
                    FROM transaction AS t
                             LEFT JOIN customers c on c.id = t.customer_id
                    WHERE t.status = 1 and c.type = 2  %s
                    GROUP BY c.id, c.name";
        $sql = sprintf($sql, $where);
        $count = \Yii::$app->db->createCommand(
            str_replace("SELECT * FROM", "SELECT COUNT(*) FROM", $sql))->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'sort' => [
                'defaultOrder' => [
                    'employee' => SORT_DESC,
                    'contragent' => SORT_DESC
                ],
                'attributes' => [
                    'user_id' => [
                        'asc' => ['user_id' => SORT_ASC],
                        'desc' => ['user_id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'employee' => [
                        'asc' => ['employee' => SORT_ASC],
                        'desc' => ['employee' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'contragent' => [
                        'asc' => ['contragent' => SORT_ASC],
                        'desc' => ['contragent' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }

    public function searchAgentdagiMahsulotlar()
    {
        $params = \Yii::$app->request->get();
        $this->load($params);
        $where = '';
        if (!empty($this->product)) {
            $where .= sprintf(" AND  p.name LIKE '%%%s%%'", $this->product);
        }
        if (!empty($this->employee)) {
            $where .= sprintf(" AND e.id = %d", $this->employee);
        }
        $sql = "select d.id as doc_id,
                       di.id as doc_item_id,
                       p.name as praduct,
                       p.id as p_id,
                       di.quantity as di_quantity,
                       p.id as product_id,
                       di.sold_price,
                       ibe.inventory as ibe_inventory,
                       d.employee_id,
                       e.fullname as agent,
                       e.id as e_id,
                       di.measurement_id as m_id,
                       di.currency
                from item_balance_employee ibe
                         left join products p on ibe.product_id = p.id
                         left join doc d on ibe.doc_id = d.id
                         left join doc_items di on d.id = di.doc_id
                         left join employee e on d.employee_id = e.id
                where ibe.id IN (select MAX(ibe2.id) from item_balance_employee ibe2 where ibe2.employee_id = e.id GROUP BY ibe2.product_id, ibe2.measurement_id)
                  AND di.product_id = ibe.product_id AND ibe.inventory > 0 %s GROUP BY ibe.product_id, ibe.measurement_id,ibe.employee_id";
        $sql = sprintf($sql,$where);
        $count = Yii::$app->db->createCommand($sql)->queryAll();
        $i = 0;
        foreach ($count as $item) {
            $i++;
        }
        $count = \Yii::$app->db->createCommand(
            str_replace("SELECT * FROM", "SELECT COUNT(*) FROM", $sql))->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $i,
            'sort' => [
                'defaultOrder' => [
                    'agent' => SORT_ASC
                ],
                'attributes' => [
                    'agent' => [
                        'asc' => ['agent' => SORT_ASC],
                        'desc' => ['agent' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'ibe_inventory' => [
                        'asc' => ['ibe_inventory' => SORT_ASC],
                        'desc' => ['ibe_inventory' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }
    private $_filtered = false;
    /**
     * @return ArrayDataProvider
     */
    public function searchReportAgentAllSell(){
        $params = \Yii::$app->request->get();
        if ($this->load($params) && $this->validate()) {
            $this->_filtered = true;
        }
        $provider = new ArrayDataProvider([
            'allModels' => $this->getData(),
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' =>[
                    'name' => SORT_DESC
                ],
                'attributes' => ['product','name','paid', 'dept'],
            ],
        ]);
        return $provider;
    }
    protected function getData()
    {
        $data = Report::getReportAgentSellModel();
        if ($this->_filtered) {
            $data = array_filter($data, function ($value) {
                $conditions = [true];
                if (!empty($this->product)) {
                    $conditions[] = stripos(mb_strtolower($value['product']), mb_strtolower($this->product)) !== false;
                }
                if (!empty($this->name)) {
                    $conditions[] = strpos($value['name'], $this->name) !== false;
                }
                return array_product($conditions);
            });
        }
        return $data;
    }
}
