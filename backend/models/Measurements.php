<?php

namespace backend\models;

use common\models\BaseModel;
use common\models\DocItems;
use common\models\ItemBalance;
use Yii;

/**
 * This is the model class for table "measurements".
 *
 * @property int $id
 * @property string $name
 * @property string $add_info
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 * @property string $token
 *
 * @property DocItems[] $docItems
 * @property ItemBalance[] $itemBalances
 * @property string $koefficient [decimal(20,1)]
 */
class Measurements extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'measurements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','koefficient', 'token'], 'required'],
            [['add_info','token'], 'string'],
            [['status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'token' => Yii::t('app', 'Token'),
            'add_info' => Yii::t('app', 'Add Info'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocItems()
    {
        return $this->hasMany(DocItems::className(), ['measurement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['measurement_id' => 'id']);
    }
}
