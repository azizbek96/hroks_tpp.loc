<?php


namespace backend\models;


use common\models\BaseModel;
use common\models\Doc;
use Yii;
use yii\data\SqlDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $customer_id
 * @property float|null $amount
 * @property float|null $inventory
 * @property int|null $status
 * @property int|null $type
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $inventory_item
 * @property int|null $product
 * @property int|null $department
 * @property int|null $department_are
 * @property int|null $employee
 *
 * @property Customers $customer
 * @property User $user
 * @property int $reg_date [int]
 * @property string $add_info [varchar(255)]
 */
class Report extends BaseModel
{
    public $inventory_item;
    public $product;
    public $department;
    public $department_are;
    public $contragent_id;
    public $contragent;
    public $employee;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    const SENARIO_PAID_EMPLOYEE = 1;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'customer_id', 'status', 'type', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'inventory'], 'number'],
            [['amount', 'reg_date'], 'required'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'customer_id' => Yii::t('app', 'Customer'),
            'amount' => Yii::t('app', 'Amount'),
            'inventory' => Yii::t('app', 'Inventory'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getAllCustomer()
    {
        $active = Transaction::STATUS_ACTIVE;
        $data = "SELECT
                    concat(e.fullname,'(', u.username,')') as name,
                    u.id as id
                 FROM user_rel_employee AS ure   
                    LEFT JOIN user u on u.id = ure.user_id
                    LEFT JOIN employee e on ure.employee_id = e.id
                    LEFT JOIN position p on e.position_id = p.id
                 WHERE u.status = {$active} 
                 and e.status = {$active}
                 and ure.status = {$active} 
                 and p.token ='AGENT'
                    ";
        return ArrayHelper::map(Yii::$app->db->createCommand($data)->queryAll(), 'id', 'name');
    }

    public static function getAgent()
    {
        $active = Transaction::STATUS_ACTIVE;
        $data = "SELECT
                    concat(e.fullname,'(', u.username,')') as name,
                    u.id as id
                 FROM user_rel_employee AS ure   
                    LEFT JOIN user u on u.id = ure.user_id
                    LEFT JOIN employee e on ure.employee_id = e.id
                     LEFT JOIN position p on e.position_id = p.id
                 WHERE u.status = {$active} 
                 and p.token ='AGENT'
                    ";
        return ArrayHelper::map(Yii::$app->db->createCommand($data)->queryAll(), 'id', 'name');
    }

    public static function getReport($user_id)
    {
        $params['paid_days'] = [];
        $params['dept_days'] = [];
        $params['month_paid'] = [];
        $params['month_dept'] = [];
        $status = Transaction::STATUS_ACTIVE;
        $paid = Transaction::T_EMPLOYEE_PAID;
        $dept = Transaction::T_EMPLOYEE_DEPT;
        $user = $user_id;
        $sql_paid = "SELECT
                            from_unixtime(reg_date, '%d.%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$paid} AND t.user_id = {$user}               
                        group by EXTRACT(DAY FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $days_paid = Yii::$app->db->createCommand($sql_paid)->queryAll();

        $sql_dept = "SELECT
                            from_unixtime(reg_date, '%d.%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$dept} AND t.user_id = {$user}               
                        group by EXTRACT(DAY FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $days_dept = Yii::$app->db->createCommand($sql_dept)->queryAll();

        $sql_paid_m = "SELECT
                            from_unixtime(reg_date, '%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$paid} AND t.user_id = {$user}               
                        group by EXTRACT(MONTH FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $month_paid = Yii::$app->db->createCommand($sql_paid_m)->queryAll();


        $sql_dept_m = "SELECT
                            from_unixtime(reg_date, '%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$dept} AND t.user_id = {$user}               
                        group by EXTRACT(MONTH FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $month_dept = Yii::$app->db->createCommand($sql_dept_m)->queryAll();


        if (!empty($days_paid)) {
            $params['paid_days'] = $days_paid;
        }
        if (!empty($days_dept)) {
            $params['dept_days'] = $days_dept;
        }

        if (!empty($month_dept)) {
            $params['month_dept'] = $month_dept;
        }
        if (!empty($month_paid)) {
            $params['month_paid'] = $month_paid;
        }
        return $params;
    }

    public static function getKirimAjax($p, $c, $start, $end)
    {
        $sql = "SELECT
                                d.reg_date as sana,
                                c.name as contraget,
                                p.name as product,
                                sum(di.quantity) as amount,
                                m.koefficient AS birlik,
                                m.name as b_name,
                                m.token as token
                    FROM doc d
                             LEFT JOIN doc_items di on d.id = di.doc_id
                             LEFT JOIN measurements m on di.measurement_id = m.id
                             LEFT JOIN customers c on d.customer_id = c.id
                             LEFT JOIN products p on di.product_id = p.id
                    WHERE d.status = %d 
                      AND d.customer_id is not null 
                      AND d.type = %d 
                    ";
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        if (!empty($start) && !empty($end)) {
            $sql .= " AND (d.reg_date >= '{$start}' and d.reg_date <= '{$end}')";
        }
        $item = !empty ($p) ? $item = " AND p.id in (" . implode(',', $p) . ")" : $item = ' ';
        $sql .= $item;
        $contragent = !empty ($c) ? $contragent = " AND c.id in (" . implode(',', $c) . ")" : $contragent = ' ';
        $sql .= $contragent;

        $sql .= " GROUP BY d.reg_date , c.name, p.name, m.koefficient";
        $sql .= " Order by d.reg_date asc";

        return Yii::$app->db->createCommand(sprintf($sql, self::STATUS_SAVED, Doc::DOC_TYPE_INCOMING))->queryAll();
    }

    public static function getKirimAjaxBoss($p, $c, $start, $end)
    {
        $sql = "SELECT
                                d.reg_date as sana,
                                c.name as contraget,
                                p.name as product,
                                sum(di.quantity) as amount,
                                m.koefficient AS birlik,
                                m.name as b_name,
                                m.token as token
                    FROM doc d
                             LEFT JOIN doc_items di on d.id = di.doc_id
                             LEFT JOIN measurements m on di.measurement_id = m.id
                             LEFT JOIN customers c on d.customer_id = c.id
                             LEFT JOIN products p on di.product_id = p.id
                    WHERE d.status = %d 
                      AND d.customer_id is not null 
                      AND d.type = %d 
                    ";
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        if (!empty($start) && !empty($end)) {
            $sql .= " AND (d.reg_date >= '{$start}' and d.reg_date <= '{$end}')";
        }
        $item = !empty ($p) ? $item = " AND p.id in (" . implode(',', $p) . ")" : $item = ' ';
        $sql .= $item;
        $contragent = !empty ($c) ? $contragent = " AND c.id in (" . implode(',', $c) . ")" : $contragent = ' ';
        $sql .= $contragent;

        $sql .= " GROUP BY d.reg_date , c.name, p.name, m.koefficient";
        $sql .= " Order by d.reg_date asc";

        return Yii::$app->db->createCommand(sprintf($sql, self::STATUS_SAVED, Doc::DOC_TYPE_INCOMING_BOSS))->queryAll();
    }

    public static function getReturnAjaxBoss($p, $c, $start, $end)
    {
        $sql = "SELECT
                                d.reg_date as sana,
                                c.name as contraget,
                                p.name as product,
                                sum(di.quantity) as amount,
                                m.koefficient AS birlik,
                                m.name as b_name,
                                m.token as token
                    FROM doc d
                             LEFT JOIN doc_items di on d.id = di.doc_id
                             LEFT JOIN measurements m on di.measurement_id = m.id
                             LEFT JOIN customers c on d.customer_id = c.id
                             LEFT JOIN products p on di.product_id = p.id
                    WHERE d.status = %d 
                      AND d.customer_id is not null 
                      AND d.type = %d 
                    ";
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        if (!empty($start) && !empty($end)) {
            $sql .= " AND (d.reg_date >= '{$start}' and d.reg_date <= '{$end}')";
        }
        $item = !empty ($p) ? $item = " AND p.id in (" . implode(',', $p) . ")" : $item = ' ';
        $sql .= $item;
        $contragent = !empty ($c) ? $contragent = " AND c.id in (" . implode(',', $c) . ")" : $contragent = ' ';
        $sql .= $contragent;

        $sql .= " GROUP BY d.reg_date , c.name, p.name, m.koefficient";
        $sql .= " Order by p.name asc";

        return Yii::$app->db->createCommand(sprintf($sql, self::STATUS_SAVED, Doc::DOC_TYPE_RETURN_BOSS))->queryAll();
    }
    public static function getReportHisobdanChiqarish($p, $c, $start, $end)
    {
        $sql = "SELECT
                                d.reg_date as sana,
                                c.name as contraget,
                                p.name as product,
                                sum(di.quantity) as amount,
                                m.koefficient AS birlik,
                                m.name as b_name,
                                m.token as token
                    FROM doc d
                             LEFT JOIN doc_items di on d.id = di.doc_id
                             LEFT JOIN measurements m on di.measurement_id = m.id
                             LEFT JOIN customers c on d.customer_id = c.id
                             LEFT JOIN products p on di.product_id = p.id
                    WHERE d.status = %d  
                      AND d.type = %d 
                    ";

        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        if (!empty($start) && !empty($end)) {
            $sql .= " AND (d.reg_date >= '{$start}' and d.reg_date <= '{$end}')";
        }
        $item = !empty ($p) ? $item = " AND p.id in (" . implode(',', $p) . ")" : $item = ' ';
        $sql .= $item;
        $contragent = !empty ($c) ? $contragent = " AND c.id in (" . implode(',', $c) . ")" : $contragent = ' ';
        $sql .= $contragent;

        $sql .= " GROUP BY d.reg_date , p.name, m.koefficient";
        $sql .= " Order by d.reg_date desc";

        return Yii::$app->db->createCommand(sprintf($sql, self::STATUS_SAVED, Doc::DOC_TYPE_OUTGOING))->queryAll();
    }
    public static function getSellAgent()
    {
        $sql = "SELECT
                        (
                            SELECT
                                concat(e.fullname,'(', u.username,')')
                            FROM user_rel_employee AS ure
                                     LEFT JOIN employee e on ure.employee_id = e.id
                                     LEFT JOIN position p on e.position_id = p.id
                            WHERE u.status = 1
                              and e.status = 1
                              and ure.status = 1
                              and p.token ='AGENT'
                              and ure.user_id = u.id
                        ) as name,
                        (
                            SELECT
                                u.id
                            FROM user_rel_employee AS ure
                                     LEFT JOIN employee e on ure.employee_id = e.id
                                     LEFT JOIN position p on e.position_id = p.id
                            WHERE u.status = 1
                              and e.status = 1
                              and ure.status = 1
                              and p.token ='AGENT'
                              and ure.user_id = u.id
                        ) as user_id,
                        (SELECT SUM(t.amount) FROM transaction t where t.user_id = u.id and t.type = %d and t.status = 1) as paid,
                        (SELECT SUM(t.amount) FROM transaction t where t.user_id = u.id and t.type = %d and t.status = 1) as dept,
                        sotgan.product,
                        sotgan.summa,
                        sotgan.price,
                        sotgan.birlik,
                        sotgan.product_id,
                        sotgan.m_id
                    FROM user u
                             LEFT JOIN (SELECT p.name           product,
                                               p.id             product_id,
                                               sum(di.quantity) summa,
                                               di.sold_price    price,
                                               m.name           birlik,
                                               m.id             m_id,
                                               doc.created_by   user_id
                                        FROM doc_items di
                                                 LEFT JOIN doc on doc.id = di.doc_id
                                                 LEFT JOIN products p on di.product_id = p.id
                                                 LEFT JOIN measurements m on di.measurement_id = m.id
                                        WHERE doc.type = %d  and doc.status = %d  GROUP BY p.id, di.sold_price, m.id,doc.created_by
                    ) sotgan ON sotgan.user_id = u.id
                    where sotgan.user_id is not null";
        $data = Yii::$app->db->createCommand(sprintf($sql, Report::T_EMPLOYEE_PAID, Report::T_EMPLOYEE_DEPT,Doc::DOC_TYPE_EMPLOYEE_SELL,Doc::STATUS_SAVED))->queryAll();

        return $data;
    }
    public static function getReturnAgent()
    {
        $sql = "SELECT
                        (
                            SELECT
                                concat(e.fullname,'(', u.username,')')
                            FROM user_rel_employee AS ure
                                     LEFT JOIN employee e on ure.employee_id = e.id
                                     LEFT JOIN position p on e.position_id = p.id
                            WHERE u.status = 1
                              and e.status = 1
                              and ure.status = 1
                              and p.token ='AGENT'
                              and ure.user_id = u.id
                        ) as name,
                        (
                            SELECT
                                u.id
                            FROM user_rel_employee AS ure
                                     LEFT JOIN employee e on ure.employee_id = e.id
                                     LEFT JOIN position p on e.position_id = p.id
                            WHERE u.status = 1
                              and e.status = 1
                              and ure.status = 1
                              and p.token ='AGENT'
                              and ure.user_id = u.id
                        ) as user_id,
                        qaytargan.product,
                        qaytargan.summa,
                        qaytargan.price,
                        qaytargan.birlik,
                        qaytargan.product_id,
                        qaytargan.m_id
                    FROM user u
                             LEFT JOIN (SELECT p.name           product,
                                               p.id             product_id,
                                               sum(di.quantity) summa,
                                               di.sold_price    price,
                                               m.name           birlik,
                                               m.id             m_id,
                                               doc.created_by   user_id
                                        FROM doc_items di
                                                 LEFT JOIN doc on doc.id = di.doc_id
                                                 LEFT JOIN products p on di.product_id = p.id
                                                 LEFT JOIN measurements m on di.measurement_id = m.id
                                        WHERE doc.type = %d  and doc.status = %d  GROUP BY p.id, di.sold_price, m.id,doc.created_by
                    ) qaytargan ON qaytargan.user_id = u.id
                    where qaytargan.user_id is not null";
        $data = Yii::$app->db->createCommand(sprintf($sql, Doc::DOC_TYPE_RETURN_AGENT,Doc::STATUS_SAVED))->queryAll();

        return $data;
    }
    public static function getReportAgentSellModel(){
        $sell = Report::getSellAgent();
        $return = Report::getReturnAgent();
        $i = 0;
        $params = [];
        foreach ($sell as $item){
            foreach ($return as $ret){
                if
                (
                    ($item['product_id'] == $ret['product_id']) &&
                    ($item['user_id'] == $ret['user_id']) &&
                    ($item['price'] == $ret['price']) &&
                    ($item['m_id'] == $ret['m_id'])
                )
                {
                    $item['summa'] -= $ret['summa'];
                }
            }
            $params[$i]['name'] = $item['name'];
            $params[$i]['product'] = $item['product'];
            $params[$i]['price'] = $item['price'];
            $params[$i]['summa'] = ($item['summa']);
            $params[$i]['m_id'] = $item['m_id'];
            $params[$i]['paid'] = $item['paid'];
            $params[$i]['dept'] = $item['dept'];
            $params[$i]['status'] = 1;
            $i++;

        }
        $bool = false;
        $dept = 0;
        $paid = 0;
        foreach ($return as $item){
            $bool = false;
            foreach ($sell as $sells){
                if
                (
                    ($item['product_id'] == $sells['product_id']) &&
                    ($item['user_id'] == $sells['user_id'])
                )
                {
                    $bool = false;
                    break;
                }elseif ($item['user_id'] == $sells['user_id']){
                    $dept = $sells['dept'];
                    $paid = $sells['paid'];
                    $bool = true;
                }
            }
            if ($bool){
                $params[$i]['name'] = $item['name'];
                $params[$i]['product'] = $item['product'];
                $params[$i]['price'] = $item['price'];
                $params[$i]['summa'] = ($item['summa']);
                $params[$i]['dept'] = $dept;
                $params[$i]['paid'] = $paid;
                $params[$i]['m_id'] = $item['m_id'];
                $params[$i]['status'] = 0;
                $i++;
            }

        }
        return  $params;
    }
}
