<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_rel_employee".
 *
 * @property int $id
 * @property int $user_id
 * @property int $employee_id
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $updated_at
 * @property int $status
 *
 * @property Employee $employee
 * @property User $user
 */
class UserRelEmployee extends \common\models\BaseModel
{
    public $employee;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_rel_employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'created_by', 'updated_by', 'updated_at','status'], 'integer'],
            ['employee', 'safe'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function afterFind()
    {
        $id = $this->user_id;
        $employee = Employee::find()
             ->alias('e')
            ->select('e.id as id')
            ->leftJoin('user_rel_employee as ure','ure.employee_id = e.id')
            ->where(['ure.user_id' => $id])->all();

        $this->employee = ArrayHelper::getColumn($employee, 'id');
        return parent::afterFind();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'employee_id' => Yii::t('app', 'Xodim'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getEmployeeList()
    {
        $sql = "SELECT 
                       e.fullname as name, 
                       e.id FROM  employee as e
                LEFT JOIN position as p ON p.id = e.position_id
                where e.status = %d AND p.token ='AGENT' order by e.fullname asc";
        $data= Yii::$app->db->createCommand(sprintf($sql,self::STATUS_ACTIVE))->queryAll();
        return ArrayHelper::map($data, 'id','name');
    }

    public function getUserList()
    {
        $sql = "SELECT concat(fullname,'(', username,')') as name, id FROM user where status = %d order by fullname asc";
        $data= Yii::$app->db->createCommand(sprintf($sql,self::STATUS_ACTIVE))->queryAll();
        return ArrayHelper::map($data, 'id','name');
    }

    public static function getUserEmployee($user_id)
    {
        $depIds = self::find()
            ->select(['employee_id'])
            ->where(['user_id' => $user_id])
            ->asArray()
            ->all();

        if (!empty($depIds)) {

            $allIds = ArrayHelper::getColumn($depIds, 'employee_id');
            $departmentNames = Employee::find()
                ->select(["fullname"])
                ->where(['in', 'id', $allIds])
                ->andWhere(['status' => self::STATUS_ACTIVE])
                ->asArray()
                ->all();
            $res = '';

            foreach ($departmentNames as $name) {

                $res .= "<span class='p-1 badge badge-info mb-1 ml-1'>{$name['fullname']}</span>";

            }

            return $res;
        }
        return null;
    }
}
