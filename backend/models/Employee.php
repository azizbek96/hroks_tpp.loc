<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $fullname
 * @property string $birth_date
 * @property string $address
 * @property string $phone
 * @property string $status
 * @property string $passport
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $updated_at
 * @property string $tel2 [varchar(20)]
 * @property string $add_info [varchar(255)]
 * @property int $position_id [int]
 */
class Employee extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['birth_date'], 'safe'],
            [['created_at', 'created_by', 'updated_by', 'updated_at','status'], 'integer'],
            [['fullname','add_info','tel2'], 'string', 'max' => 80],
            [['address', 'passport'], 'string', 'max' => 255],
            ['passport','unique'],
            [['phone','fullname','passport','birth_date','address','position_id'],'required'],
            [['phone'], 'string', 'max' => 60],
        ];
    }
    public function ve($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->birth_date = date('Y-m-d', strtotime($this->birth_date));
            return true;
        }

        return false;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fullname' => Yii::t('app', 'Fullname'),
            'birth_date' => Yii::t('app', 'Birth Date'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'passport' => Yii::t('app', 'Passport'),
            'tel2' => Yii::t('app', 'Tel2'),
            'position_id' => Yii::t('app', 'Lavozim'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocs()
    {
        return $this->hasMany(Doc::className(), ['employee_id' => 'id']);
    }
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['id' => 'position_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['employee_id' => 'id']);
    }
}
