<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "position".
 *
 * @property int $id
 * @property string $name
 * @property string $token
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $updated_at
 * @property int $status [smallint]
 */
class Position extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'position';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_by', 'updated_at'], 'integer'],
            [['name', 'token'], 'string', 'max' => 60],
            ['token' , 'unique'],
            [['token','name'] , 'required']
        ];
    }
    public function getEmployee()
    {
        return $this->hasMany(Employee::className(), ['position_id' => 'id']);
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'token' => Yii::t('app', 'Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
    public static function getPositionList(){
        return self::find()
            ->select(['name', 'id'])
            ->where(['status' => self::STATUS_ACTIVE])
            ->indexBy('id')->column();
    }
}
