<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

/**
 * TransactionSearch represents the model behind the search form of `backend\models\Transaction`.
 */
class TransactionSearch extends Transaction
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'reg_date', 'contragent_id','customer_id', 'status', 'type', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'inventory'], 'number'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
        // bypass scenarios() implementation in the parent class
    {
        return Model::scenarios();
    }

    /**
     * @param $status
     * @return ActiveDataProvider
     */
    public function search($status)
    {

        $params = Yii::$app->request->get();
        $query = Transaction::find();
        switch ($status) {
            case self::T_EMPLOYEE_PAID:
                $query->where(['!=', 'status', self::STATUS_DELETED])
                    ->andWhere(['type' => self::T_EMPLOYEE_PAID]);
                break;
            case self::T_PAID:
                $query->where(['!=', 'status', self::STATUS_DELETED])
                    ->andWhere(['user_id' => Yii::$app->user->id])
                    ->andWhere(['type' => self::T_PAID]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'reg_date' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'customer_id' => $this->customer_id,
            'amount' => $this->amount,
            'inventory' => $this->inventory,
            'type' => $this->type,
        ]);

        return $dataProvider;
    }

    public function searchAgentContragent()
    {
        $params = \Yii::$app->request->get();
        $this->load($params);
        $where = '';
        if (!empty($this->contragent_id)) {
            $where .= sprintf("and c.id = %d", $this->contragent_id);
        }
        $sql = "SELECT
                       c.id   as contragent_id,
                       c.name as contragent,
                       (SELECT SUM(t_2.amount) FROM transaction as t_2
                                                        WHERE t_2.status = 1
                                                          and t_2.type = 1
                                                          and t_2.customer_id = c.id) as paid,
                       (SELECT SUM(t_2.amount) FROM transaction as t_2
                                                            WHERE t_2.status = 1
                                                              and t_2.type = 2
                                                              and t_2.customer_id = c.id) as dept,
                       ((SELECT SUM(t_2.amount) FROM transaction as t_2
                         WHERE t_2.status = 1
                           and t_2.type = 1
                           and t_2.customer_id = c.id)+(SELECT SUM(t_2.amount) FROM transaction as t_2
                                            WHERE t_2.status = 1
                                              and t_2.type = 2
                                              and t_2.customer_id = c.id)) as saldo
                
                FROM transaction AS t
                         LEFT JOIN customers c on c.id = t.customer_id
                WHERE t.status = %d and c.type = %d and t.created_by = %d  %s
                GROUP BY c.id, c.name ORDER BY c.name";
        $sql = sprintf($sql, self::STATUS_ACTIVE, self::CUSTOMERS_TYPE_CLIENT, Yii::$app->user->id, $where);
        $count = \Yii::$app->db->createCommand(
            str_replace("SELECT * FROM", "SELECT COUNT(*) FROM", $sql))->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => $sql,
            'totalCount' => $count,
            'sort' => [
                'defaultOrder' => [
                    'contragent_id' => SORT_ASC
                ],
                'attributes' => [
                    'contragent_id' => [
                        'asc' => ['contragent_id' => SORT_ASC],
                        'desc' => ['contragent_id' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                    'contragent' => [
                        'asc' => ['contragent' => SORT_ASC],
                        'desc' => ['contragent' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
                'forcePageParam' => true
            ]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }
    public function searchOwen($status)
    {

        $params = Yii::$app->request->get();
        $query = Transaction::find();
        $query->where(['!=', 'status', self::STATUS_DELETED])
            ->andWhere(['user_id' => Yii::$app->user->id])
            ->andWhere(['type' => self::T_PAID]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'reg_date' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'customer_id' => $this->customer_id,
            'amount' => $this->amount,
            'inventory' => $this->inventory,
            'type' => $this->type,
        ]);

        return $dataProvider;
    }
}
