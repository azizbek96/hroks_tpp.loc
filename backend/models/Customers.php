<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "customers".
 *
 * @property int $id
 * @property string $name
 * @property int $region_id
 * @property string $address
 * @property string $fullname
 * @property string $tel1
 * @property string $tel2
 * @property string $add_info
 * @property int $type
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Doc[] $docs
 * @property ItemBalance[] $itemBalances
 * @property int $user_id [int]
 */
class Customers extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','region_id','fullname','tel1' ,'address'], 'required'],
            [['region_id', 'type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['address', 'add_info'], 'string'],
            [['name', 'fullname', 'tel1', 'tel2'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'region_id' => Yii::t('app', 'Region ID'),
            'address' => Yii::t('app', 'Address'),
            'fullname' => Yii::t('app', 'Fullname'),
            'tel1' => Yii::t('app', 'Tel1'),
            'tel2' => Yii::t('app', 'Tel2'),
            'add_info' => Yii::t('app', 'Add Info'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocs()
    {
        return $this->hasMany(Doc::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['customer_id' => 'id']);
    }
    public static function getCurrentAgent(){
        $sql = "SELECT * FROM customers where user_id = %d and status = %d";
        $model = Yii::$app->db->createCommand(sprintf($sql,Yii::$app->user->id,self::STATUS_ACTIVE))->queryAll();

        return ArrayHelper::map($model, 'id', 'name');
    }
}
