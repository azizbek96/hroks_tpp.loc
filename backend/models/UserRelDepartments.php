<?php

namespace backend\models;

use app\modules\structure\models\Department;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_rel_departments".
 *
 * @property int $id
 * @property int $user_id
 * @property int $department_id
 * @property int $type
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Departments $department
 * @property User $user
 */
class UserRelDepartments extends \common\models\BaseModel
{
    /**
     * @var $in ;
     * @var $out ;
     */
    public $in;
    public $out;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_rel_departments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id',  'department_id', 'type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['in', 'out'], 'safe'],
            [['user_id'], 'required'],
        ];
    }
    public function afterFind()
    {
        $id = $this->user_id;
        $departmentReceiving =  Departments::find()
            ->leftJoin('user_rel_departments','user_rel_departments.department_id = departments.id')
            ->where(['departments.status' => self::STATUS_ACTIVE])
            ->andWhere(['user_rel_departments.type' => self::RECIVING])
            ->andWhere(['user_rel_departments.user_id'=>$id])
            ->asArray()
            ->all();
        $departmentOutputting =  Departments::find()
            ->leftJoin('user_rel_departments','user_rel_departments.department_id = departments.id')
            ->where(['departments.status' => self::STATUS_ACTIVE])
            ->andWhere(['user_rel_departments.type' => self::OUTGOING])
            ->andWhere(['user_rel_departments.user_id'=>$id])
            ->asArray()
            ->all();
        $this->out = ArrayHelper::getColumn($departmentOutputting,'id');
        $this->in = ArrayHelper::getColumn($departmentReceiving,'id');
        return parent::afterFind();
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'department_id' => Yii::t('app', 'Department ID'),
            'type' => Yii::t('app', 'Type'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserList()
    {
        return User::find()
            ->select(['fullname', 'id'])
            ->where(['status' => self::STATUS_ACTIVE])
            ->indexBy('id')->column();
    }
    public function getUserListDep()
    {
        return User::find()
            ->select(['fullname', 'id'])
            ->where(['status' => self::STATUS_ACTIVE])
            ->indexBy('id')->column();
    }

    public function getDepartmentList()
    {
        return Departments::find()
            ->select(['name', 'id'])
            ->where(['status' => self::STATUS_ACTIVE])
            ->indexBy('id')->column();
    }

    public static function getUserDepartmentReciving($user_id)
    {
        $depIds = self::find()
            ->select(['department_id'])
            ->where(['user_id' => $user_id])
            ->andWhere(['type' => self::RECIVING])
            ->asArray()
            ->all();

        if (!empty($depIds)) {

            $allIds = ArrayHelper::getColumn($depIds, 'department_id');
            $departmentNames = Departments::find()
                ->select(["name"])
                ->where(['in', 'id', $allIds])
                ->andWhere(['status' => self::STATUS_ACTIVE])
                ->asArray()
                ->all();
            $res = '';

            foreach ($departmentNames as $name) {

                $res .= "<span class='p-1 label label-info mb-1 ml-1'>{$name['name']}</span>";

            }

            return $res;
        }
        return null;
    }

    public static function getUserDepartmentOutgoing($user_id)
    {
        $depIds = self::find()
            ->select(['department_id'])
            ->where(['user_id' => $user_id])
            ->andWhere(['type' => self::OUTGOING])
            ->asArray()
            ->all();

        if (!empty($depIds)) {

            $allIds = ArrayHelper::getColumn($depIds, 'department_id');
            $departmentNames = Departments::find()
                ->select(["name"])
                ->where(['in', 'id', $allIds])
                ->andWhere(['status' => self::STATUS_ACTIVE])
                ->asArray()
                ->all();
            $res = '';

            foreach ($departmentNames as $name) {

                $res .= "<span class='p-1 label label-success mb-1 ml-1'>{$name['name']}</span>";

            }

            return $res;
        }
        return null;
    }
}
