<?php

namespace backend\models;

use Cassandra\Date;
use DateTime;
use phpDocumentor\Reflection\Types\Self_;
use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $customer_id
 * @property float|null $amount
 * @property float|null $inventory
 * @property int|null $status
 * @property int|null $type
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Customers $customer
 * @property Customers $contragent
 * @property User $user
 * @property int $reg_date [int]
 * @property string $add_info [varchar(255)]
 */
class Transaction extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public  $contragent_id;
    public static function tableName()
    {
        return 'transaction';
    }
    const SCENARIO_SELLING_AGENT = 'AGENTGA_BERISH';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'customer_id', 'status', 'type', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'inventory'], 'number'],
            [['contragent','add_info'], 'safe'],
            [['amount', 'reg_date'], 'required'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'customer_id' => Yii::t('app', 'Customer'),
            'amount' => Yii::t('app', 'Amount')."(UZS)",
            'inventory' => Yii::t('app', 'Inventory'),
            'reg_date' => Yii::t('app', 'Ro`yhatga olingan sana'),
            'status' => Yii::t('app', 'Status'),
            'add_info' => Yii::t('app', 'Izoh'),
            'type' => Yii::t('app', 'Type'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Customer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getReport($customer_id)
    {
        $params['paid_days'] = [];
        $params['dept_days'] = [];
        $params['month_dept'] = [];
        $params['month_paid'] = [];
        $status = self::STATUS_ACTIVE;
        $paid = self::T_PAID;
        $dept = self::T_DEPT;
        $customer = $customer_id;
        $sql_paid = "SELECT
                            from_unixtime(reg_date, '%d.%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$paid} AND t.customer_id = {$customer}               
                        group by EXTRACT(DAY FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $days_paid = Yii::$app->db->createCommand($sql_paid)->queryAll();

        $sql_dept = "SELECT
                            from_unixtime(reg_date, '%d.%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$dept} AND t.customer_id = {$customer}               
                        group by EXTRACT(DAY FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $days_dept = Yii::$app->db->createCommand($sql_dept)->queryAll();

        $sql_paid_m = "SELECT
                            from_unixtime(reg_date, '%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$paid} AND t.customer_id = {$customer}               
                        group by EXTRACT(MONTH FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $month_paid = Yii::$app->db->createCommand($sql_paid_m)->queryAll();


        $sql_dept_m = "SELECT
                            from_unixtime(reg_date, '%m.%Y') as date,
                            sum(amount) as sum,
                            t.inventory as qoldiq
                            FROM transaction t
                        WHERE t.status = {$status}
                            AND t.type = {$dept} AND t.customer_id = {$customer}               
                        group by EXTRACT(MONTH FROM DATE(FROM_UNIXTIME(t.reg_date)))";
        $month_dept = Yii::$app->db->createCommand($sql_dept_m)->queryAll();


        if (!empty($days_paid)) {
            $params['paid_days'] = $days_paid;
        }
        if (!empty($days_dept)) {
            $params['dept_days'] = $days_dept;
        }

        if (!empty($month_dept)) {
            $params['month_dept'] = $month_dept;
        }
        if (!empty($month_paid)) {
            $params['month_paid'] = $month_paid;
        }
        return $params;
    }

    public static function getCurrentSaldo()
    {
        $params['balance'] = 0;
        $params['paid_month'] = 0;
        $params['dept_month'] = 0;
        $dept_sum = 0;
        $paid_sum = 0;
        $dept = self::find()
            ->where([
                'status' => self::STATUS_ACTIVE,
                'type' => self::T_EMPLOYEE_DEPT,
                'user_id' => Yii::$app->user->id
            ])->orderBy(['id' => SORT_DESC])->asArray()->one();
        $paid = self::find()
            ->select('sum(amount) as amount')
            ->where([
                'status' => self::STATUS_ACTIVE,
                'type' => self::T_EMPLOYEE_PAID,
                'user_id' => Yii::$app->user->id,
            ])->orderBy(['id' => SORT_DESC])->asArray()->one();

        $begin_date = "01.".date('m').".".date('Y');
        $end_date = date('d.m.Y H:i:s');//date('d')+1 .".".date('m').".".date('Y');

        $paid_month = self::find()
            ->select('sum(amount) as amount')
            ->where([
                'status' => self::STATUS_ACTIVE,
                'type' => self::T_EMPLOYEE_PAID,
                'user_id' => Yii::$app->user->id,
            ])
            ->andWhere(['>=', 'reg_date', strtotime($begin_date)])
            ->andWhere(['<=', 'reg_date', strtotime($end_date)])
            ->asArray()->one();
        $dept_month = self::find()
            ->select('sum(amount) as amount')
            ->where([
                'status' => self::STATUS_ACTIVE,
                'type' => self::T_EMPLOYEE_DEPT,
                'user_id' => Yii::$app->user->id
            ])
            ->andWhere(['>=', 'reg_date', strtotime($begin_date)])
            ->andWhere(['<=', 'reg_date', strtotime($end_date)])
            ->asArray()->one();

        if (!empty($dept)) {
            $dept_sum = $dept['inventory'];
        }
        if (!empty($paid)) {
            $paid_sum = $paid['amount'];
        }
        if (!empty($paid_month)){
            $params['paid_month'] = $paid_month['amount'];
        }

        if (!empty($dept_month)){
            $params['dept_month'] = $dept_month['amount'];
        }

        $params['balance'] = $dept_sum + $paid_sum;

        return $params;
    }
    public static function getPrice(){
        $sql = "SELECT p2.name as name,
                       p.price as price
                From pricing p
                         left join products p2 on p2.id = p.product_id
                where p.status = %d order by  p2.name asc";
        return Yii::$app->db->createCommand(sprintf($sql, self::STATUS_ACTIVE))->queryAll();
    }
}
