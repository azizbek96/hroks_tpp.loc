<?php
namespace backend\models;

use yii\base\Model;

/**
 * Signup form
 */
class ChangeDateForm extends Model
{
    public $changed_date;
    public $sub_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['changed_date', 'safe'],
            ['sub_id', 'integer'],
        ];
    }
}
