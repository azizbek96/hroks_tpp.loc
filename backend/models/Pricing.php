<?php

namespace backend\models;

use common\models\BaseModel;
use Yii;

/**
 * This is the model class for table "pricing".
 *
 * @property int $id
 * @property int $product_id
 * @property string $price
 * @property int $status
 * @property int $type
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Products $product
 */
class Pricing extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pricing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'status', 'type', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['price','product_id'], 'number'],
            [['price','product_id'], 'required'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
    public static function getPriceProduct($product_id){
        if(is_numeric($product_id)){
            $model = Pricing::find()
                ->where(['status' => Pricing::STATUS_ACTIVE])
                ->andWhere(['product_id' => $product_id])
                ->asArray()
                ->one();
            return $model['price'];
        }
        return 0;

    }
}
