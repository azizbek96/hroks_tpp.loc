<?php
namespace backend\models;

use yii\base\Model;

/**
 * Refresh form
 */
class RefreshForm extends Model
{
    public $count;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['count', 'integer'],
        ];
    }
}
