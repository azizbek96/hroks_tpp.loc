<?php

namespace backend\models;

use common\models\Doc;
use Yii;

/**
 * This is the model class for table "item_balance_boss".
 *
 * @property int $id
 * @property int $user_id
 * @property int $customer_id
 * @property int $reg_date
 * @property int $doc_id
 * @property int $measurement_id
 * @property int $currency
 * @property int $department_id
 * @property int $dep_area_id
 * @property string $amount
 * @property string $inventory
 * @property int $type
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Customers $customer
 * @property DepArea $depArea
 * @property Departments $department
 * @property Doc $doc
 * @property Measurements $measurement
 * @property User $user
 * @property int $product_id [int]
 * @property int $doc_item_id [int]
 */
class ItemBalanceBoss extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_balance_boss';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'customer_id','doc_item_id', 'product_id','reg_date', 'doc_id', 'measurement_id', 'currency', 'department_id', 'dep_area_id', 'type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'inventory'], 'number'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['dep_area_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepArea::className(), 'targetAttribute' => ['dep_area_id' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['doc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doc::className(), 'targetAttribute' => ['doc_id' => 'id']],
            [['measurement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Measurements::className(), 'targetAttribute' => ['measurement_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'reg_date' => Yii::t('app', 'Reg Date'),
            'doc_id' => Yii::t('app', 'Doc ID'),
            'measurement_id' => Yii::t('app', 'Measurement ID'),
            'currency' => Yii::t('app', 'Currency'),
            'department_id' => Yii::t('app', 'Department ID'),
            'dep_area_id' => Yii::t('app', 'Dep Area ID'),
            'amount' => Yii::t('app', 'Amount'),
            'inventory' => Yii::t('app', 'Inventory'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepArea()
    {
        return $this->hasOne(DepArea::className(), ['id' => 'dep_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Doc::className(), ['id' => 'doc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasurement()
    {
        return $this->hasOne(Measurements::className(), ['id' => 'measurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
