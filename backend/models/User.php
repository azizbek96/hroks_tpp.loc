<?php

namespace backend\models;

use common\models\Groups;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $fullname
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $type
 * @property int $group_id
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $role
 *
 * @property Groups $group
 * @property UserRelEmployee[] $userRelEmployees
 */
class User extends \common\models\BaseModel
{
    public $rule;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->andWhere(['status' => self::STATUS_ACTIVE])->one();
    }
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fullname','username', 'password_hash'],'default', 'value' => null],
            [['fullname','username', 'password_hash'],'required'],
            [['type', 'group_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['username', 'fullname', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username' ,'email'],'unique'],
            [['username', 'password_hash' ,'email'],'string' , 'min' => 4],
            [['role'], 'safe'],
            [['password_reset_token'], 'unique'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }
    public function afterFind()
    {
        $id = $this->id;
        $itemName = AuthAssignment::find()->where(['user_id' => $id])->asArray()->all();
        $this->rule = ArrayHelper::getColumn($itemName, 'item_name');
        return parent::afterFind();
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'fullname' => Yii::t('app', 'Fullname'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email' => Yii::t('app', 'Email'),
            'type' => Yii::t('app', 'Type'),
            'group_id' => Yii::t('app', 'Group ID'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRelEmployees()
    {
        return $this->hasMany(UserRelEmployee::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }
}
