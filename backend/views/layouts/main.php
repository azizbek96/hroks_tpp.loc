<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use  common\components\PermissionHelper as P;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?= Url::base() . "/js/loader.js" ?>"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/ico" href="<?= Url::base() . "/fruits.png" ?>">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="load_screen">
    <div class="loader text-center">
        <div class="loader-content">
            <div class="align-self-center aylanish">
            </div>
        </div>
    </div>
</div>
<div class="wrap">
    <?php
    if (!empty(Yii::$app->user->id) && Yii::$app->user->identity->type === 1) {

        $settings = [
            'label' => Yii::t('app', 'Sozlamalar'),
            'visible' =>
                P::can('user/index') ||
                P::can('employee/index') ||
                P::can('auth-item/index') ||
                P::can('permission/index') ||
                P::can('auth-assignment/index'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Foydalanuvchilar'),
                    'url' => ['/admin/user'],
                    'visible' => P::can('user/index'),
                ],
                [
                    'label' => 'Ishchilar',
                    'url' => ['/employee'],
                    'visible' => P::can('employee/index'),
                ],
                [
                    'label' => 'Rollar',
                    'url' => ['/admin/auth-item/index'],
                    'visible' => P::can('auth-item/index'),

                ],
                [
                    'label' => 'Ruxsatlar',
                    'url' => ['/admin/permission/index'],
                    'visible' => P::can('permission/index'),
                ],
                [
                    'label' => Yii::t('app', 'Parolni o\'zgartirish'),
                    'url' => ['/admin/user/reset'],
                    'visible' => P::can('user/reset'),
                ],
            ],
        ];
        $directory = [
            'label' => Yii::t('app', 'Kataloglar'),
            'visible' =>
                P::can('product-categories/index') ||
                P::can('products/index') ||
                P::can('position/index') ||
                P::can('pricing/index') ||
                P::can('customers/index') ||
                P::can('measurements/index') ||
                P::can('groups/index') ||
                P::can('position/index') ||
                P::can('departments/index') ||
                P::can('dep-area/index') ||
                P::can('user-rel-employee/index') ||
                P::can('user-rel-departments/index'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Product categories'),
                    'url' => ['/product-categories'],
                    'visible' => P::can('product-categories/index'),
                ],
                [
                    'label' => Yii::t('app', 'Products'),
                    'url' => ['/products'],
                    'visible' => P::can('products/index'),
                ],
                [
                    'label' => Yii::t('app', 'Lavozim'),
                    'url' => ['/position'],
                    'visible' => P::can('position/index'),
                ],
                [
                    'label' => Yii::t('app', 'Narxlash'),
                    'url' => ['/pricing'],
                    'visible' => P::can('pricing/index'),
                ],
                [
                    'label' => Yii::t('app', 'Contragent'),
                    'url' => ['/customers'],
                    'visible' => P::can('customers/index'),
                ],
                [
                    'label' => Yii::t('app', 'O\'lchovlar'),
                    'url' => ['/measurements'],
                    'visible' => P::can('measurements/index'),
                ],
                [
                    'label' => Yii::t('app', 'Guruxlar'),
                    'url' => ['/groups'],
                    'visible' => P::can('groups/index'),
                ],
                [
                    'label' => Yii::t('app', 'Bo\'limlar'),
                    'url' => ['/departments'],
                    'visible' => P::can('departments/index'),
                ],
                [
                    'label' => Yii::t('app', 'Bo\'lim hududi'),
                    'url' => ['/dep-area'],
                    'visible' => P::can('dep-area/index'),
                ],
                [
                    'label' => Yii::t('app', 'Ishchilarni biriktirish'),
                    'url' => ['/user-rel-employee'],
                    'visible' => P::can('user-rel-employee/index'),
                ],
                [
                    'label' => Yii::t('app', 'Bo\'limlarni biriktirish'),
                    'url' => ['/user-rel-departments'],
                    'visible' => P::can('user-rel-departments/index'),
                ],
            ]
        ];
        $warehouse = [
            'label' => Yii::t('app', 'Warehouse'),
            'visible' =>
                P::can('doc/kirim/index') ||
                P::can('doc/qayta_qabul/index') ||
                P::can('doc/topshirish/index') ||
                P::can('doc/boss_kirim/index') ||
                P::can('doc/hisobdan_chiqarish/index'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Kirim'),
                    'url' => ['/doc/kirim/index'],
                    'visible' => P::can('doc/kirim/index'),
                ],
                [
                    'label' => Yii::t('app', 'Topshirish'),
                    'url' => ['/doc/topshirish/index'],
                    'visible' => P::can('doc/topshirish/index'),
                ],
                [
                    'label' => Yii::t('app', 'Qayta Qabul'),
                    'url' => ['/doc/qayta_qabul/index'],
                    'visible' => P::can('doc/qayta_qabul/index'),
                ],
                [
                    'label' => Yii::t('app', 'Rahbar kirim'),
                    'url' => ['/doc/boss_kirim/index'],
                    'visible' => P::can('doc/boss_kirim/index'),
                ],
                [
                    'label' => Yii::t('app', 'Rahbar qaytarish'),
                    'url' => ['/doc/boss_return/index'],
                    'visible' => P::can('doc/boss_return/index'),
                ],
                [
                    'label' => Yii::t('app', 'Hisobdan chiqarish'),
                    'url' => ['/doc/hisobdan_chiqarish/index'],
                    'visible' => P::can('doc/hisobdan_chiqarish/index'),
                ],
            ],
        ];
        $report = [
            'label' => Yii::t('app', 'Report'),
            'visible' =>
                P::can('report/mijoz-agent') ||
                P::can('report/report-agent-all-sell') ||
                P::can('report/kirim-report-boss') ||
                P::can('report/kirim-report') ||
                P::can('report/have-product') ||
                P::can('report/finished-product') ||
                P::can('report/report-hisobdan-chiqarish'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Mijozning agentdan qarzi'),
                    'url' => ['/report/mijoz-agent'],
                    'visible' => P::can('report/mijoz-agent'),
                ],
                [
                    'label' => Yii::t('app', 'Agentnig ishlashi'),
                    'url' => ['/report/report-agent-all-sell'],
                    'visible' => P::can('report/report-agent-all-sell'),
                ],
                [
                    'label' => Yii::t('app', 'Kirim xisobot rahbar uchun'),
                    'url' => ['/report/kirm-report-boss'],
                    'visible' => P::can('report/kirm-report-boss'),
                ],
                [
                    'label' => Yii::t('app', 'Qaytarilgan mahsulotlar xisoboti'),
                    'url' => ['/report/return-boss-report'],
                    'visible' => P::can('report/return-boss-report'),
                ],
                [
                    'label' => Yii::t('app', 'Kirim xisobot'),
                    'url' => ['/report/kirim-report'],
                    'visible' => P::can('report/kirim-report'),
                ],
                [
                    'label' => Yii::t('app', 'Ombordagi maxsulotlar'),
                    'url' => ['/report/have-product'],
                    'visible' => P::can('report/have-product'),
                ],
                [
                    'label' => Yii::t('app', 'Tugagan maxsulotlar'),
                    'url' => ['/report/finished-product'],
                    'visible' => P::can('report/finished-product'),
                ],
                [
                    'label' => Yii::t('app', 'Hisobdan chiqarish xisoboti'),
                    'url' => ['/report/report-hisobdan-chiqarish'],
                    'visible' => P::can('report/report-hisobdan-chiqarish'),
                ],
            ]
        ];
        $money = [
            'label' => Yii::t('app', 'Kassa'),
            'visible' =>  P::can('report/index') ||
                        P::can('report/contragent-report-boss'),
                        P::can('transaction-boss/index'),
            'items' => [
                [
                    'label' => Yii::t('app', 'Ishchilar xisoboti'),
                    'url' => ['/report'],
                    'visible' => P::can('report/index'),
                ],
                [
                    'label' => Yii::t('app', 'Contragentlarga to`lov'),
                    'url' => ['/transaction-boss/index'],
                    'visible' => P::can('transaction-boss/index'),
                ],
                [
                    'label' => Yii::t('app', 'Contragentlar xisoboti'),
                    'url' => ['/report/contragent-report-boss'],
                    'visible' => P::can('report/contragent-report-boss'),
                ],
            ],
        ];
    }
    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
        'brandLabel' => '<i class="glyphicon glyphicon-apple"></i>' . Yii::t('app', 'Vodiy sitrus'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        $menuItems = [
            $report,
            $money,
            $directory,
            $warehouse,
            $settings,
        ];
    }
    if (Yii::$app->user->isGuest) {
        $menuItems[] = [
            'label' => Yii::t('app', 'Kirish'),
            'url' => ['/site/login']
        ];
    } else {
        $menuItems[] =
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                Yii::t('app', 'Chiqish') . '(' . Yii::$app->user->identity->fullname . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?php

        if (!Yii::$app->user->isGuest) {
            echo Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]);
        }

        //        foreach (Yii::$app->params['language'] as $key => $val){
        //            echo "<a href='".\yii\helpers\Url::to(['site/chang-lang','lang' => $key])."'
        //            class='btn btn-info'>".$val."</a>";
        //        }
        ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left"><?= '<i class="glyphicon glyphicon-apple"></i>Vodiy Sitrus' ?> <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>


<?php $this->endBody() ?>
</body>
<?php
//$response = message();
//$message = $response['message'];
//$status = $response['status'];
$js = <<<JS
    
    $('body').delegate('.delete-button-ajax','click',function (event){
      event.preventDefault();
      let url = $(this).attr('href');
      swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
        }).then(function(result) {
          if (result.value) {    
              $.ajax({
                    url: url,
                    type: "GET",
                    success: function (res) {
                        if (res['status'] === 'true') {
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            setTimeout(function(event) {
                              location.reload(true);
                            }, 1500);
                        } else {
                            swal(
                              res['error_one'],
                              res['error'],
                              'warning'
                            )
                            setTimeout(function(event) {
                              location.reload(true);
                            }, 1500);
                        }
                    }
              });
          }
        })
    });
    
     $('body').delegate('.change-password-button', 'click', function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        let title = $(this).attr('title');
        $('.modal-body').load(href);
        setTimeout(function () {
            $('.modal-title').html(title);
        }, 500);
    });
     
    $('.modal-title').html('<i class="spinner-border text-danger align-self-center loader-sm"></i>');

JS;
$this->registerJs($js);
?>
</html>
<?php $this->endPage() ?>
