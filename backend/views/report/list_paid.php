<?php

use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use backend\models\Transaction;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ro`yhat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        .table > tbody > tr > td {
            vertical-align: center !important;
        }

    </style>
    <div class="pricing-index">
        <?php
        Pjax::begin()
        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'showPageSummary' => true,
            'resizableColumns' => false,
            'filterModel' => $searchModel,
            'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Ishchilar bergan pullari')],
            'columns' => [
                ['class' => SerialColumn::class],

                [
                    'attribute' => 'user_id',
                    'label' => Yii::t('app', 'Mijoz'),
                    'filter' => \backend\models\Report::getAllCustomer(),
                    'value' => function ($model) {
                        return $model->user->fullname;
                    },
                    'options' => [
                        'style' => 'vertical-align:center!important'
                    ],
                    'group' => true,
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_CENTER,
                    'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
                        return [
                            'mergeColumns' => [[1, 1]], // columns to merge in summary
                            'content' => [             // content to show in each summary cell
                                1 => 'Jami (' . $model->user->fullname . ')',
                                2 => GridView::F_SUM
                            ],
                            'contentFormats' => [      // content reformatting for each summary cell
                                2 => ['format' => 'number', 'decimals' => 1],
                            ],
                            'contentOptions' => [      // content html attributes for each summary cell
                                1 => ['style' => 'font-variant:small-caps'],
                                2 => ['style' => 'text-align:right']
                            ],
                            // html attributes for group summary row
                            'options' => ['class' => 'info table-success', 'style' => 'font-weight:bold;']
                        ];
                    }
                ],
                [
                    'label' => Yii::t('app', 'Summa(UZS)'),
                    'attribute' => 'amount',
                    'filterInputOptions' => [
                        'class' => 'number form-control',
                        'type' => 'number'
                    ],
                    'value' => function ($model) {
                        return $model->amount;
                    },
//                    'hAlign' => 'right',
                    'format' => ['decimal', 0],
                    'pageSummary' => true,
                    'pageSummaryFunc' => GridView::F_SUM
                ],
                [
                    'attribute' => 'reg_date',
                    'filter' => false,
                    'label' => Yii::t('app', 'Pull to`langan sana'),
                    'value' => function ($model) {
                        return date('d.m.Y', $model->reg_date);
                    }
                ],
                [
//                    'label' => Yii::t('app', 'Kiritilgan Vaqti'),
                    'attribute' => 'updated_at',
                    'filter' => false,
                    'value' => function ($model) {
                        return (time() - $model->updated_at < (60 * 60 * 24)) ? Yii::$app->formatter->format(date($model->updated_at), 'relativeTime') : date('d.m.Y H:i', $model->updated_at);
                    }
                ],
                [
                        'attribute' => 'add_info'
                ],
                [
                    'class' => ActionColumn::class,
                    'template' => '{update} {delete}',
                    'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                    'visibleButtons' => [
                        'update' => function ($model) {
                            return Yii::$app->user->can('report/update'); // && $model->status < $model::STATUS_SAVED;
                        },
                        'delete' => function ($model) {
                            return Yii::$app->user->can('report/delete'); // && $model->status < $model::STATUS_SAVED;
                        }
                    ],
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Update'),
                                'class' => 'update-dialog btn btn-xs btn-success mr1 modalButton',
                                'data-form-id' => $model->id,
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Delete'),
                                'class' => 'btn btn-xs btn-danger',
                                'data-form-id' => $model->id,
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]);
                        },

                    ],
                ],
            ],
        ]) ?>

        <?php Pjax::end(); ?>

    </div>

<?php Modal::begin([
    'header' => 'O`zgartirish',
    'size' => 'modal-lg',
    'id' => 'modalContent'
]) ?>
    <div id="modal"></div>

<?php Modal::end() ?>

<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important; 
        text-align: center;
        font-size: 1.2em;
    }
}
CSS;
$this->registerCss($css);
?>
<?php
$js = <<<JS
      $('.modalButton').click(function(e) {
              e.preventDefault();
              $('#modalContent').modal('show')
              .find('#modal')
              .load($(this).attr('href'));
      });
JS;
$this->registerJS($js);
?>
<?php
