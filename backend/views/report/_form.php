<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'data-pjax' => true,
            'id' => $model->formName(),
            'class' => 'customAjaxForm'
        ]
    ]); ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <?php echo $form->field($model, 'reg_date')->widget(\kartik\date\DatePicker::class, [
        'options' => [
            'placeholder' => 'Enter birth date ...',
            'readonly' => true,
            'required' => true
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighLight' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]) ?>
    <?= $form->field($model, 'amount')->widget(\kartik\money\MaskMoney::class, [
        'options' => [
            'required' => true
        ],
        'pluginOptions' => [
            'prefix' => 'UZS ',
            'suffix' => '',
            'precision' => 0
        ]
    ]) ?>

    <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'add_info')->textInput()->label(Yii::t('app', 'Izoh')) ?>


    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>' . ' ' . Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
