<?php

use backend\models\Report;
use backend\models\Transaction;
use backend\modules\employees\models\Sales;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Modal::begin([
    'header' => Yii::t('app', 'Paid'),
    'size' => 'modal-lg',
    'id' => 'modalContent',
]) ?>
<div id="modal"></div>

<?php Modal::end() ?>


<?php $js = <<<JS
    $(document).ready(function(){
      $('.modalButton').click(function(e) {
      e.preventDefault();
      let url = $(this).attr('url');
      let customer_id = $('#customers_id').val(); 
      if (customer_id == '' || customer_id ==null){
          $('.info_warning').removeClass('hide').addClass('show');
      }else{
          $('.info_warning').addClass('hide').removeClass('show')
          url = url+'?user_id='+customer_id
          $('#modalContent').modal('show').find('#modal').load(url);
      }      
      });
    });
JS;
$this->registerJS($js); ?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 text-right">
            <a href="<?php echo Url::to(['report/list-paid']) ?>" target="_blank" class="btn btn-success">
                <?php echo Yii::t('app', 'To`lov tarixi') ?>
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6" style="margin: 0 auto;">
            <?php
            echo Select2::widget([
                'name' => 'customers',
                'data' => Report::getAllCustomer(),
                'size' => Select2::SIZE_MEDIUM,
                'hideSearch' => true,
                'options' => [
                    'placeholder' => Yii::t('app','Agent tanlang ... '),
                    'id' => 'customers_id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'append' => [
                        'content' => Html::button(Yii::t('app', 'Pull to`lash'), [
                            'url' => Url::to(['report/create']),
                            'class' => 'btn btn-sm btn-primary modalButton',
                            'title' => 'To`lamoq',
                            'data-toggle' => 'tooltip'
                        ]),
                        'asButton' => true
                    ]
                ]
            ]);
            ?>
            <p class="info_warning hide text-center" style="font-size: 14px;font-weight: bold; color: red"><?php echo Yii::t('app', 'Mijozni tanlang') ?></p>
            <div class="col-lg-3 col-md-3 col-sm-3"></div>

        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered table-responsive">
                <thead style="background-color: #0a73bb; color: whitesmoke">
                <tr>
                    <th><?php echo Yii::t('app', 'Qarz Summa') ?></th>
                    <th><?php echo Yii::t('app', 'To`langan summa') ?></th>
                    <th><?php echo Yii::t('app', 'Qoldiq') . '($)' ?></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><span style="color: black;font-weight: bold;font-size: 14px" class="customer_dept"></span></td>
                    <td><span style="color: black;font-weight: bold;font-size: 14px" class="customer_paid"></span></td>
                    <td><span style="color: black;font-weight: bold;font-size: 14px" class="customer_balance"></span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<br>
<div class="container">
    <div class="row">
        <div class="col-lg-12 text-center">
            <button class="btn btn-lg btn-success report_button"
                    id="days"><?php echo Yii::t('app', 'Kunlik') ?></button>
            <button class="btn btn-lg btn-success report_button"
                    id="month"><?php echo Yii::t('app', 'Oylik') ?></button>
<!--            <button class="btn btn-lg btn-success report_button"-->
<!--                    id="year">--><?php //echo Yii::t('app', 'Yillik') ?><!--</button>-->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 days hide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h5><?php echo Yii::t('app', 'Qarz miqdori') ?></h5>
                        <table class="table-bordered table">
                            <thead>
                            <tr>
                                <th><?php echo Yii::t('app', 'Kun') ?></th>
                                <th><?php echo Yii::t('app', 'Summa') ?></th>
                            </tr>
                            </thead>
                            <tbody class="dept_summ">

                            </tbody>
                            <tfoot class="dept_footer">

                            </tfoot>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <h5><?php echo Yii::t('app', 'To`langan miqdori') ?></h5>
                        <table class="table-bordered table">
                            <thead>
                            <tr>
                                <th><?php echo Yii::t('app', 'Kun') ?></th>
                                <th><?php echo Yii::t('app', 'Summa') ?></th>
                            </tr>
                            </thead>
                            <tbody class="paid_days"></tbody>
                            <tfoot class="paid_footer">

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 month hide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h5><?php echo Yii::t('app', 'Qarz miqdori') ?></h5>
                        <table class="table-bordered table">
                            <thead>
                            <tr>
                                <th><?php echo Yii::t('app', 'Kun') ?></th>
                                <th><?php echo Yii::t('app', 'Summa') ?></th>
                            </tr>
                            </thead>
                            <tbody class="dept_month">

                            </tbody>
                            <tfoot class="dept_month_footer">

                            </tfoot>
                        </table>
                    </div>
                    <div class="col-lg-6">
                        <h5><?php echo Yii::t('app', 'To`langan miqdori') ?></h5>
                        <table class="table-bordered table">
                            <thead>
                            <tr>
                                <th><?php echo Yii::t('app', 'Kun') ?></th>
                                <th><?php echo Yii::t('app', 'Summa') ?></th>
                            </tr>
                            </thead>
                            <tbody class="paid_month"></tbody>
                            <tfoot class="paid_month_footer">

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 year hide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-6"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .activeBtn{
        background-color: #c475e7;
        color: #551cc4;
    }
    .report_button:hover{
        background-color: #87f811;
        border: 1px solid #384804;
        color: #5b5b58;
    }
</style>
<?php
$url = Url::to(['report/report']);
$js = <<<JS
    $('body').delegate('.report_button','click', function (e){
        e.preventDefault();
        let id = $(this).attr('id');
       
        if (id == 'days'){
            $('#days').addClass('activeBtn');
            $('#month').removeClass('activeBtn');
            $('#year').removeClass('activeBtn');
            $('.days').addClass('show');
            $('.month').removeClass('show').addClass('hide');
            $('.year').removeClass('show').addClass('hide');
        }
        if (id == 'month'){
            $('#month').addClass('activeBtn');
            $('#days').removeClass('activeBtn');
            $('#year').removeClass('activeBtn');
            $('.month').addClass('show');
            $('.days').removeClass('show').addClass('hide');
            $('.year').removeClass('show').addClass('hide');
        }
        if (id == 'year'){
            $('#year').addClass('activeBtn');
            $('#days').removeClass('activeBtn');
            $('#month').removeClass('activeBtn');
            $('.year').addClass('show');
            $('.month').removeClass('show').addClass('hide');
            $('.days').removeClass('show').addClass('hide');
        }
    })
JS;
$this->registerJS($js);


$js = <<<JS
         $('body').delegate('#customers_id','change' , function() {
               let id= $(this).val();
               $.ajax({
                      url:'$url',
                      data:{q:id},
                      type:'GET',
                          success: function(response){   
                              if(response.status){ 
                                  let qarz = 0;
                                  let tolandi = 0;
                                  if (response.dept.length > 0){
                                        $('.customer_dept').html(CurrencyFormat(response.dept[0]['inventory']));
                                        qarz = response.dept[0]['inventory'];
                                  }else{
                                        $('.customer_dept').html(0);
                                        qarz = 0;
                                  }
                                  if (response.paid.length > 0 && response.paid[0]['amount'] != null){
                                        $('.customer_paid').html(CurrencyFormat(response.paid[0]['amount']));
                                        tolandi = response.paid[0]['amount'];
                                  }else{
                                        $('.customer_paid').html(0);
                                        tolandi = 0;
                                  }
                                  let balance = qarz*1 + 1*tolandi;
                                  $('.customer_balance').html(CurrencyFormat(balance))
                                  
                              }else{
                                  $('.customer_paid').html('')   
                                  $('.customer_dept').html('')
                                  $('.customer_balance').html('')
                                  $('.paid_days').html('');
                                  $('.dept_summ').html('');
                                  $('.paid_month').html('');
                                  $('.dept_month').html('');
                              }  
                              if (response.status){
                                  let dept_summ =''
                                  if (response.days_dept.length >0){
                                      response.days_dept.map(function (item){
                                      dept_summ +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                    
                                               // '<td>'+CurrencyFormat(item.qoldiq)+'</td>'+                                                    
                                       '</tr>';
                                      })
                                  }else {
                                      dept_summ = '';
                                  }
                                  let paid = ''
                                  if (response.days_paid.length > 0){
                                      response.days_paid.map(function (item){
                                      paid +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                                                                     
                                       '</tr>';
                                      })
                                  }
                                   let dept_m = ''
                                  if (response.month_dept.length > 0){
                                      response.month_dept.map(function (item){
                                      dept_m +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                                                                        
                                       '</tr>';
                                      })
                                  }
                                  
                                  let paid_m = ''
                                  if (response.month_paid.length > 0){
                                      response.month_paid.map(function (item){
                                      paid_m +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                                                                        
                                       '</tr>';
                                      })
                                  }
                                 
                                  $('.paid_days').html(paid);
                                  $('.dept_summ').html(dept_summ);
                                  $('.paid_month').html(paid_m);
                                  $('.dept_month').html(dept_m);
                              }
                          }
               });
         });
        function CurrencyFormat(currency) {
            if (isNaN(currency)) {
                return 0;
            }
            return parseFloat(currency, 10).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString();
        }
JS;
$this->registerJS($js);
?>
