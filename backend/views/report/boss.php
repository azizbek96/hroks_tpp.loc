<div class="row">
    <div class="col-sm-3">
        <a class="btn-success btn" href="<?php echo \yii\helpers\Url::to('contragent-report-boss') ?>"><?php echo Yii::t('app', 'Contragent') ?></a>
    </div>
    <div class="col-sm-3">
        <button class="btn-info btn"><?php echo Yii::t('app', 'Agentlar') ?></button>
    </div>
    <div class="col-sm-3">
        <button class="btn-primary btn"><?php echo Yii::t('app', 'Bazadig maxsulotlar') ?></button>
    </div>
    <div class="col-sm-3">
        <button class="btn-warning btn"><?php echo Yii::t('app', 'Tugagan maxsulotlar') ?></button>
    </div>
</div>
