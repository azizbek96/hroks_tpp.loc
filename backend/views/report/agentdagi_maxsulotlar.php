<?php

use kartik\grid\SerialColumn;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Agentlardagi mahsulotlar');
$this->params['breadcrumbs'][] = $this->title;
/* @var $searchModel backend\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */
?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'resizableColumns' => false,
        'showPageSummary' => true,
        'panel' => ['type' => 'default', 'heading' => ''],
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'columns' => [
            [
                'class' => SerialColumn::class,
//                'pageSummary' => '',
            ],
            [
                'attribute' => 'agent',
                'value' => function ($model) {
                    return $model['agent'];
                },
                'group' => true,
            ],
            [
                'attribute' => 'product',
                'label' => Yii::t('app', 'Mahsulot'),
                'value' => function ($model) {
                    return $model['praduct'];
                }
            ],
            [
                'attribute' => 'ibe_inventory',
                'label' => Yii::t('app', 'Qoldiq'),
                'value' => function ($model) {
                    return \backend\models\Report::getNumberFormat($model['ibe_inventory']);
                }
            ],
            [
                'attribute' => 'm_id',
                'label' => Yii::t('app', 'Birlik'),
                'value' => function ($model) {
                    return \backend\models\Report::getMeasurementLabel($model['m_id']);
                }
            ],

        ],
    ]) ?>
</div>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important;
        text-align: center;
        font-size: 1.2em;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>

