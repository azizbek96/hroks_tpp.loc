<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Transaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transaction-view">
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="pull-right" style="margin-bottom: 15px;">
            <?php //if (Yii::$app->user->can('transaction/update')): ?>
            <?php if ($model->status < $model::STATUS_SAVED): ?>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
            <?php //endif; ?>
            <?php //if (Yii::$app->user->can('transaction/delete')): ?>
            <?php if ($model->status < $model::STATUS_SAVED): ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
            <?php //endif; ?>
            <?= Html::a(Yii::t('app', 'Back'), ["index"], ['class' => 'btn btn-info']) ?>
        </div>
    <?php } ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'user.username',
            ],

            [
                'attribute' => 'amount',
            ],

            [
                'attribute' => 'status',
                'value' => function ($model) {
                    $status = $model::getStatusList($model->status);
                    return isset($status) ? $status : $model->status;
                }
            ],
            [
                'attribute' => 'reg_date',
                'filter' => false,
                'value' => function ($model) {
                    return date('d.m.Y', $model['reg_date']);
                }
            ],
            [
                'attribute' => 'add_info',
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    $username = \common\models\User::findOne($model->created_by)['fullname'];
                    return isset($username) ? $username : $model->created_by;
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    $username = \common\models\User::findOne($model->updated_by)['fullname'];
                    return isset($username) ? $username : $model->updated_by;
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return (time() - $model->created_at < (60 * 60 * 24)) ? Yii::$app->formatter->format(date($model->created_at), 'relativeTime') : date('d.m.Y H:i', $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return (time() - $model->updated_at < (60 * 60 * 24)) ? Yii::$app->formatter->format(date($model->updated_at), 'relativeTime') : date('d.m.Y H:i', $model->updated_at);
                }
            ],

        ],
    ]) ?>

</div>
