<?php

use common\models\Doc;
use common\models\DocItems;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;

?>
<div class="row">
    <div class="col-sm-6">
        <?php
        echo '<label class="control-label-warning">' . Yii::t('app', 'Oraliqni tanlang') . '</label>';
        echo '<div class="drp-container">';
        echo DateRangePicker::widget([
            'name' => 'date-range',
            'startAttribute' => 'datetime_min',
            'endAttribute' => 'datetime_max',
            'presetDropdown' => true,
            'pickerIcon' => '<i class=""></i>',
            'convertFormat' => true,
            'includeMonthsFilter' => false,
            'pluginOptions' => [
                'language' => 'uz-latn',
                'locale' => [
                    'format' => 'd.m.Y',
                    "applyLabel" => "Tanlash",
                    "cancelLabel" => "Bekor",
                    "fromLabel" => "Dan",
                    "toLabel" => "Gacha",
                    "customRangeLabel" => "Tanlangan",
                    "daysOfWeek" => [
                        "Ya",
                        "Du",
                        "Se",
                        "Ch",
                        "Pa",
                        "Ju",
                        "Sh"
                    ],
                    "monthNames" => [
                        "Yanvar",
                        "Fevral",
                        "Mart",
                        "Aprel",
                        "May",
                        "Iyun",
                        "Iyul",
                        "Avgust",
                        "Sentabr",
                        "Oktabr",
                        "Noyabr",
                        "Dekabr"
                    ],
                    "firstDay" => 1
                ],
                'ranges' => [
                    Yii::t('app', "Bugun") => ["moment().startOf('day')", "moment()"],
                    Yii::t('app', "Kecha") => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                    Yii::t('app', "Ohirgi {n} kun", ['n' => 7]) => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                    Yii::t('app', "Ohirgi {n} kun", ['n' => 30]) => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
                    Yii::t('app', "Shu oy") => ["moment().startOf('month')", "moment().endOf('month')"],
                    Yii::t('app', "O'tgan oy") => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                ],
            ],
            'options' => [
                'placeholder' => 'Oralqni tanlang...',
                'class' => 'date-range',
                'id' => 'date_between',
            ],


        ]);
        echo '</div>';
        ?><br>
    </div>
    <div class="col-sm-6">
        <label class="contragent"> <?php echo Yii::t('app', 'Contragent') ?></label>
        <?php
        echo Select2::widget([
            'name' => 'contragent',
            'data' => Doc::getCustomerId(),
            'theme' => Select2::THEME_CLASSIC,
            'options' => [
                'placeholder' => Yii::t('app', 'Contragent'),
                'id' => 'contragent_id'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ]

        ])
        ?>
    </div>
</div><br>
<div class="row">
    <div class="col-lg-8" style="margin: 0 auto;">
        <label class="product_class"> <?php echo Yii::t('app', 'Products') ?> </label>
        <?php echo Select2::widget([
            'name' => 'product',
            'data' => DocItems::getProductList(),
            'theme' => Select2::THEME_CLASSIC,
            'options' => [
                'placeholder' => Yii::t('app', 'Products'),
                'id' => 'product_id',
                'autocomplete' => 'off'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ]
        ]) ?>
    </div>
    <div class="col-lg-3" style="padding-top: 3px;"><br>
        <button class="btn btn-success" id="search"><?php echo Yii::t('app', 'Qidirish') ?></button>
    </div>
</div>
<hr>
<div class="container">
    <div class="col-lg-12">
        <h3></h3>
        <h4 class="text-center"><?php echo Yii::t('app', 'Olingan maxsulotlar ro\'yhati') ?></h4>
        <table class="table table-bordered">
            <thead style="background-color: #0a73bb!important; color: black!important;" class="result_none hide">
            <tr>
                <th><?php echo Yii::t('app', 'Sana') ?></th>
                <th><?php echo Yii::t('app', 'Product') ?></th>
                <th><?php echo Yii::t('app', 'Amount') ?></th>
                <th colspan=""><?php echo Yii::t('app', 'Measurements') ?></th>
            </tr>
            </thead>
            <thead style="background-color: #0a73bb!important; color: black!important;" class="result_yes hide">
            <tr>
                <th><?php echo Yii::t('app', 'Sana') ?></th>
                <th><?php echo Yii::t('app', 'Contragent') ?></th>
                <th><?php echo Yii::t('app', 'Product') ?></th>
                <th><?php echo Yii::t('app', 'Amount') ?></th>
                <th colspan=""><?php echo Yii::t('app', 'Measurements') ?></th>
            </tr>
            </thead>
            <tbody class="result">

            </tbody>
            <tfoot style="background-color: #fffcfc!important; color: black!important;" class="result_none hide">
            <tr>
                <th colspan="2"><?php echo Yii::t('app', 'Jami:') ?></th>
                <th class="jami"></th>
                <th></th>
            </tr>
            </tfoot>
            <tfoot style="background-color: #FFFCFC!important; color: black!important;" class="result_yes hide">
                <tr>
                    <th colspan="3"><?php echo Yii::t('app', 'Jami:') ?></th>
                    <th class = "jami_c"></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<style>
    .colors {
        color: red;
    }

    .kv-drp-dropdown .kv-clear {
        padding: 0 0.9rem;
        font-size: 2.5rem;
        cursor: pointer;
        right: 1.6rem;
        line-height: 18px;
    }
</style>
<?php
$url = \yii\helpers\Url::to(['report/kirim-ajax']);
$js = <<<JS
    function isEmpty(object){
        for (let key in object){
            return false;
        }
        return true;
    }
    function CurrencyFormat(currency) {
    if (isNaN(currency)) {
        return 0;
    }
    return parseFloat(currency, 10).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString();
    }
     $('body').delegate('#search','click' , function(){
        let p = $('#product_id').val(); 
        let c = $('#contragent_id').val();
        let start = $('#date_between-start').val();
        let end = $('#date_between-end').val();
        if((!isEmpty(start) && !isEmpty(end))){
              if (isEmpty(c)){
                  $('.result_none').removeClass('hide');
                  $('.result_yes').addClass('hide');
                  $.ajax({
                     url:'$url',
                     data:{product:p,contragent:c,start:start,end:end},
                     type:'GET',
                         success: function(response){
                             let result = ''
                             let sum = 0;
                             if(response.status){   
                                 response.data.map(function (item){
                                     sum +=item.amount*1;
                                     result +="" +
                                      "<tr>" +
                                             "<td>"+dataFormat(item.sana)+"</td>" +
                                             "<td>"+item.product+"</td>" +
                                             "<td>"+CurrencyFormat(item.amount)+"</td>" +
                                             "<td>"+item.b_name+"("+item.birlik+")"+"</td>" +
                                             // "<td>"+CurrencyFormat(item.amount*item.birlik*1)+"("+item.birlik+")"+"</td>" +
                                      "</tr>"
                                 })
                                 $('.result').html(result);
                                 $('.jami').html(CurrencyFormat(sum));
                             }else{
                                 $('.result').html('<tr align="center" style="color:red;font-weight:bold"><td colspan=5>Ma`lumot mavjud emas</td></tr>');
                                 $('.jami').html('')
                             }              
                         }
              });
              }
              if (!isEmpty(c)){
                  $('.result_none').addClass('hide');
                  $('.result_yes').removeClass('hide');
                  $.ajax({
                     url:'$url',
                     data:{product:p,contragent:c,start:start,end:end},
                     type:'GET',
                         success: function(response){
                             let result = '';
                             let sum = 0;
                             if(response.status){   
                                 response.data.map(function (item){
                                     sum +=item.amount*1;
                                     result +="" +
                                      "<tr>" +
                                             "<td>"+dataFormat(item.sana)+"</td>" +
                                             "<td>"+item.contraget+"</td>" +
                                             "<td>"+item.product+"</td>" +
                                             "<td>"+CurrencyFormat(item.amount)+"</td>" +
                                             "<td>"+item.b_name+"("+item.birlik+")"+"</td>" +
                                             // "<td>"+CurrencyFormat(item.amount*item.birlik*1)+"("+item.birlik+")"+"</td>" +
                                      "</tr>"
                                 })
                                 $('.result').html(result);
                                 $('.jami_c').html(sum);
                             }else{
                                 $('.result').html('<tr align="center" style="color:red;font-weight:bold"><td colspan=5>Ma`lumot mavjud emas</td></tr>');
                                 $('.jami_c').html('');
                             }              
                         }
                  });
              }
             $('.control-label-warning').removeClass('colors');
        }else{ 
            $('.control-label-warning').addClass('colors'); 
        }
          
     });
    function dataFormat(date){
            let formattedDate = new Date(date);
            let d = formattedDate.getDate();
            let m =  formattedDate.getMonth();
            m += 1;  // JavaScript months are 0-11
            if (d < 10) {
                d = "0" + d;
            }
            if (m < 10) {
                m = "0" + m;
            }
            let y = formattedDate.getFullYear();
    
            return  d+"."+m+"."+y;
    }
JS;
$this->registerJS($js);

?>
