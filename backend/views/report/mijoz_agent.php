<?php

use kartik\grid\SerialColumn;
use kartik\grid\GridView;

$this->title = Yii::t('app', 'Mijozlarning Agentlardan qarzi');
$this->params['breadcrumbs'][] = $this->title;
/* @var $searchModel backend\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */
?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'resizableColumns' => false,
        'showPageSummary' => true,
        'panel' => ['type' => 'default', 'heading' => ''],
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'rowOptions' => function ($model) {
            if ($model['saldo'] < 0) {
                return [
                    'class' => 'warning'
                ];
            }
            if ($model['saldo'] > 0) {
                return [
                    'class' => 'success'
                ];
            }
            return [
                'class' => ''
            ];
        },
        'columns' => [
            [
                'class' => SerialColumn::class,
//                'pageSummary' => '',
            ],
            [
                'attribute' => 'employee',
                'label' => Yii::t('app', 'Agentlar'),
                'value' => function($model){
                    return $model['employee'];
                },
                'filter' => \backend\models\Report::getAgent(),
                'group' => true,
                'pageSummary' => Yii::t('app', 'Jami:'),
                'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns' => [[1, 1]], // columns to merge in summary
                        'content' => [             // content to show in each summary cell
                            1 => 'Jami :',
                            2 => '',
                            3 => GridView::F_SUM,
                            4 => GridView::F_SUM,
                            5 => GridView::F_SUM,
                        ],
                        'contentFormats' => [      // content reformatting for each summary cell
                            1 => [],
                            2 => [],
                            3 => ['format' => 'number', 'decimals' => 0],
                            4 => ['format' => 'number', 'decimals' => 0],
                            5 => ['format' => 'number', 'decimals' => 0],
                        ],
                        'contentOptions' => [      // content html attributes for each summary cell
                            1 => ['style' => 'font-variant:small-caps'],
                            2 => ['style' => 'font-variant:small-caps'],
                            3 => ['style' => 'text-align:left'],
                            4 => ['style' => 'text-align:left'],
                            5 => ['style' => 'text-align:left'],
                        ],
                        // html attributes for group summary row
                        'options' => ['class' => 'info table-success', 'style' => 'font-weight:bold;']
                    ];
                }
            ],
            [
                'attribute' => 'contragent',
                'options' => [
                    'style' => 'font-weight:bold;font-size:15px!important'
                ],
                'label' => Yii::t('app', 'Contragent'),
                'value' => function ($model) {
                    return $model['contragent']."(".$model['address'].") ".$model['tel1']??'';
                },
                'hAlign' => GridView::ALIGN_CENTER,
                'vAlign' => GridView::ALIGN_CENTER,

            ],
            [
                'attribute' => 'dept',
                'label' => Yii::t('app', 'Qarz miqdori(UZS)'),
                'value' => function ($model) {
                    if ($model['dept'] == null) {
                        return 0;
                    }
                    return $model['dept'];
                },
                'hAlign' => 'right',
                'format' => ['decimal', 0],
                'pageSummary' => false,
                'pageSummaryFunc' => GridView::F_SUM,
            ],
            [
                'attribute' => 'paid',
                'label' => Yii::t('app', 'To`langan summa'),
                'value' => function ($model) {
                    if ($model['paid'] == null) {
                        return 0;
                    }
                    return $model['paid'];
                },
                'hAlign' => 'right',
                'format' => ['decimal', 0],
                'pageSummary' => false,
                'pageSummaryFunc' => GridView::F_SUM
            ],
            [
                'attribute' => 'saldo',
                'format' => ['decimal', 0],
                'label' => Yii::t('app', 'Qoldiq(UZS)'),
                'value' => function ($model) {
                    if ($model['saldo'] == null) {
                        if ($model['dept'] == null) {
                            return 0;
                        }
                        return $model['dept'];
                    }
                    return $model['saldo'] * 1;
                },
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
            ],

        ],
    ]) ?>
</div>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important;
        text-align: center;
        font-size: 1.2em;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>

