<?php

use yii\grid\SerialColumn;
use kartik\grid\GridView;


/* @var $searchModel backend\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */


?>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Ombordagi maxsulotlar ro\'yhati')],
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'headerRowOptions' => [
            'class' => 'thead-dark'
        ],
        'columns' => [
            ['class' => SerialColumn::class],


            [
                'attribute' => 'department',
                'label' => Yii::t('app', 'Departments'),
                'value' => function ($model) {
                    return $model['department'];
                },
                'group' => true
            ],
            [
                'attribute' => 'product',
                'label' => Yii::t('app', 'Products'),
                'value' => function ($model) {
                    return $model['product'];
                }
            ],
            [
                'attribute' => 'inventory_item',
                'label' => Yii::t('app', 'Qoldiq'),
                'value' => function ($model) {
                    return \backend\models\Transaction::getNumberFormat($model['inventory_item']);
                }
            ],
            [
                'attribute' => 'measurement_id',
                'value' => function ($model) {
                    return \common\models\Doc::getMeasurementLabel($model['measurement_id']);
                }
            ],


        ],
    ]) ?>
</div>
<?php
$css =<<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important; 
        text-align: center;
        font-size: 1.2em;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>

