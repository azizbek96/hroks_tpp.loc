<?

use backend\models\Report;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\data\ArrayDataProvider;
$this->title = Yii::t('app', 'Agentlarning sotgan mahsulotlar ro\'yhati');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-responsive">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'resizableColumns' => false,
        'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Agentlarning sotgan mahsulotlar ro\'yhati')],
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'headerRowOptions' => [
            'class' => 'thead-dark'
        ],
        'rowOptions' => function ($model) {
            if ($model['status'] == 0) {
                return [
                    'class' => 'danger'
                ];
            }
            if ($model['status'] = 1) {
                return [
                    'class' => 'success'
                ];
            }
            return [
                'class' => ''
            ];
        },
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'attribute' => 'name',
                'label' => Yii::t('app', 'Agentlar'),
                'value' => function ($model) {
                    return $model['name'];
                },
                'group' => true,
            ],
            [
                'attribute' => 'product',
                'label' => Yii::t('app', 'Products'),
                'value' => function ($model) {
                    return $model['product'];
                }
            ],
            [
                'attribute' => 'summa',
                'label' => Yii::t('app', 'Miqdori'),
                'value' => function ($model) {
                    return Report::getNumberFormat($model['summa']);
                }
            ],
            [
                'attribute' => 'm_id',
                'label' => Yii::t('app', 'Birligi'),
                'value' => function ($model) {
                    return Report::getMeasurementLabel($model['m_id']);
                }
            ],
            [
                'attribute' => 'price',
                'label' => Yii::t('app', 'Sotilish narxi'),
                'value' => function ($model) {
                    return Report::getNumberFormat($model['price']);
                }
            ],
            [
                'attribute' => 'paid',
                'label' => Yii::t('app', 'To`langan summa'),
                'value' => function ($model) {
                    return Report::getNumberFormat( $model['paid']);
                },
                'group' => true
            ],
            [
                'attribute' => 'dept',
                'label' => Yii::t('app', 'Qarz miqdori(UZS)'),
                'value' => function ($model) {
                    return Report::getNumberFormat( $model['dept']+$model['paid']);
                },
                'group' => true

            ],
        ],
    ]) ?>
</div>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important;
        text-align: center;
        font-size: 1.2em;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>
