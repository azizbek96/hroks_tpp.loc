<?php

use yii\grid\SerialColumn;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserRelEmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Rel Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-rel-employee-index">
    <?php if (Yii::$app->user->can('user-rel-employee/create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
            ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>

    </p>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'user-rel-employee_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],


            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->username;
                }
            ],
            [
                'attribute' => 'Employee',
                'format' => 'html',
                'label' => Yii::t('app', 'Employee'),
                'value' => function($model){
                    return $model->user->fullname;
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {view} {delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('user-rel-employee/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('user-rel-employee/update');
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('user-rel-employee/delete');
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'user-rel-employee',
    'crud_name' => 'user-rel-employee',
    'modal_id' => 'user-rel-employee-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'User Rel Employee') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'user-rel-employee_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]); ?>
