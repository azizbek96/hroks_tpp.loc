<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRelEmployee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-rel-employee-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'customAjaxForm']]); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => $model->getUserList(),
        'options' => ['placeholder' => Yii::t('app', 'Tanlang')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'employee')->widget(Select2::classname(), [
        'data' => $model->getEmployeeList(),
        'options' => ['placeholder' => Yii::t('app', 'Tanlang')],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label(Yii::t('app', 'Employee')) ?>
   <?= $form->field($model, 'status')->dropDownList([
                       $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
                       $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
   ]) ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
