<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRelEmployee */

$this->title = Yii::t('app', 'Create User Rel Employee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Rel Employees'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="yee-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
