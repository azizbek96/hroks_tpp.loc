<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductCategories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-categories-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class'=> 'customAjaxForm']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([
                        $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
                        $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
