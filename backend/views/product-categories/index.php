<?php

use backend\models\ProductCategories;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductCategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Product Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-categories-index">
    <?php if (Yii::$app->user->can('product-categories/create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
            ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>

    </p>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'product-categories_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'name',

            [
                'attribute' => 'created_by',
                'filter' => false,
                'value' => function ($model) {
                    $username = \common\models\User::findOne($model->created_by)['fullname'];
                    return isset($username) ? $username : $model->created_by;
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'center',
                    'style' => 'text-align:center'
                ],
                'filter' => [
                    ProductCategories::STATUS_ACTIVE => Yii::t('app', 'Active'),
                    ProductCategories::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
                ],
                'value' => function ($model) {
                    if ($model->status =  ProductCategories::STATUS_ACTIVE) {
                        return '<b class="badge" style="background-color: #17a2b8">'.Yii::t('app', 'Active').'</b>';
                    }
                    return '<b class="badge" style="background-color: #e03939">' .Yii::t('app', 'Inactive').'</b>';

                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view} {delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('product-categories/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('product-categories/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('product-categories/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'product-categories',
    'crud_name' => 'product-categories',
    'modal_id' => 'product-categories-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'Product Categories') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'product-categories_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]); ?>
