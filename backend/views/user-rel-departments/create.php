<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRelDepartments */

$this->title = Yii::t('app', 'Create User Rel Departments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Rel Departments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-rel-departments-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
