<?php

use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use backend\models\UserRelEmployee;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserRelDepartmentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'User Rel Departments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-rel-departments-index">
    <?php if (Yii::$app->user->can('user-rel-departments/create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
            ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>

    </p>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'user-rel-departments_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],

            [
                'attribute' => 'user_id',
                'filter' => (new backend\models\UserRelDepartments)->getUserList(),
                'value' => function ($model) {
                    return $model->user->fullname;
                }
            ],
            [
                'attribute' => 'out',
                'format' =>'html',
                'label' => Yii::t('app', 'Chiqarish'),
                'value' => function ($model) {
                    return $model::getUserDepartmentOutgoing($model->user_id);
                }
            ],
            [
                'attribute' => 'in',
                'label' => Yii::t('app', 'Qabul qilish'),
                'format' =>'html',
                'value' => function ($model) {
                    return $model::getUserDepartmentReciving($model->user_id);
                }
            ],

            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:60px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('user-rel-departments/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('user-rel-departments/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('user-rel-departments/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
//                    'view' => function ($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
//                            'title' => Yii::t('app', 'View'),
//                            'class' => 'btn btn-xs btn-default view-dialog mr1',
//                            'data-form-id' => $model->id,
//                        ]);
//                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'user-rel-departments',
    'crud_name' => 'user-rel-departments',
    'modal_id' => 'user-rel-departments-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'User Rel Departments') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'user-rel-departments_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]); ?>
