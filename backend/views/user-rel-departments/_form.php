<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserRelDepartments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-rel-departments-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => $model->formName(),'data-pjax' => true, 'class' => 'customAjaxForm']]); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\select2\Select2::class, [
        'data' => $model->getUserList(),
        'options' => [
                'placeholder' => Yii::t('app', 'Tanlang')
        ],
        'pluginOptions' => [
                'allowClear' => true,
        ]
    ]) ?>

    <?= $form->field($model, 'out')->widget(\kartik\select2\Select2::class, [
        'data' => $model->getDepartmentList(),
        'options' => [
            'placeholder' => Yii::t('app', 'Tanlang')
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple'  =>true
        ]
    ])->label(Yii::t('app', 'Chiqarish')) ?>
    <?= $form->field($model, 'in')->widget(\kartik\select2\Select2::class, [
        'data' => $model->getDepartmentList(),
        'options' => [
            'placeholder' => Yii::t('app', 'Tanlang')
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple'  =>true
        ]
    ])->label(Yii::t('app', 'Qabul qilish')) ?>

    <?= $form->field($model, 'status')->dropDownList([
                        $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
                        $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
