<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Kirish';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login" style="padding-top: 60px!important;font-size: 14px">

    <div class="row text-center ">
        <div class="col-lg-3 col-sm-3 col-md-3"></div>
        <div class="col-lg-6 card-1 card">
            <h1><?= Html::encode($this->title) ?></h1>

            <p><?php echo Yii::t('app', 'Login va parolingizni kiriting') ?></p>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => Yii::t('app', 'Login...')])->label(false) ?>
            <br>
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Parol...')])->label(false) ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>


            <?= Html::submitButton(Yii::t('app', 'OK'), ['class' => 'btn btn-primary button', 'name' => 'login-button']) ?>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-3 col-sm-3 col-md-3"></div>
    </div>
</div>
<style>
    input{
        height: 40px!important;
    }

    .card {
        background: #fff;
        border-radius: 2px;
        display: inline-block;
        margin: 1rem;
        position: relative;
        height: 350px;
        box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
    }

    .button {
        border-radius: 5px;
        width: 100px !important;
    }
</style>
