<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="employee-form">

        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'customAjaxForm']]); ?>

        <div class="form-group row">
            <div class="col-sm-6">
                <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'birth_date')->widget(DatePicker::classname(), [
                    'options' => [
                            'placeholder' => 'Enter birth date ...',
                            'autocomplete' => 'off'
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'autocomplete' => 'off',
                    ]
                ]) ?>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'position_id')->widget(\kartik\select2\Select2::class, [
                    'data' => \backend\models\Position::getPositionList(),
                    'options' => [
                            'placeholder' => Yii::t('app', 'Tanlang ...')
                    ],
                    'pluginOptions' => [
                            'allowClear' => true
                    ]
                ])->label(Yii::t('app', 'Lavozim')) ?>

            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-6">
                <?= $form->field($model, 'phone')->widget(MaskedInput::className(),
                    ['mask' => '\+\9\9\8\(99\) 999 99 99']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'tel2')->widget(MaskedInput::className(),
                    ['mask' => '\+\9\9\8\(99\) 999 99 99']) ?>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-6">
                <?= $form->field($model, 'passport')->widget(MaskedInput::className(),
                    ['mask' => 'AA 9999999']) ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'status')->dropDownList([
                    $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
                    $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
                ]) ?>
            </div>
        </div>


        <?= $form->field($model, 'add_info')->textInput() ?>
        <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$css = <<<CSS
.form-group {
    margin-bottom: 0!important;
}
CSS;
$this->registerCss($css);
?>
