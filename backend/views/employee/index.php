<?php

use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use backend\models\Employee;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employees');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if (Yii::$app->user->can('employee/create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
            ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>
    </p><br><br>
<?php endif ?>

<div class="table-responsive">
    <?php Pjax::begin(['id' => 'employee_pjax']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'hover' => true,
        'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Ishchilar ro`yhati')],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],
            'fullname',
            [
                'attribute' => 'birth_date',
                'filter' => false,
                'contentOptions' => [
                    'style' => 'width:60px!important'
                ],
                'value' => function ($model) {
                    return date('d.m.Y', strtotime($model['birth_date']));
                }
            ],
            'address',
            [
                'attribute' => 'phone',
                'options' => [
                    'style' => 'width:100px'
                ],
                'value' => function ($model) {
                    return $model->phone;
                }
            ],
            [
                'attribute' => 'passport',
                'contentOptions' => [
                    'style' => 'width:50px!important'
                ]
            ],
            [
                'attribute' => 'position_id',
                'label' => Yii::t('app', 'Lavozim'),
                'filter' => \backend\models\Position::getPositionList(),
                'value' => function ($model) {
                    return $model->position->name;
                },
                'group' => true
            ],
            [
                'attribute' => 'status',

                'format' => 'html',
                'contentOptions' => [
                    'class' => 'center',
                    'style' => 'text-align:center;width:50px'
                ],
                'filter' => [
                    Employee::STATUS_ACTIVE => Yii::t('app', 'Active'),
                    Employee::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
                ],
                'value' => function ($model) {
                    if ($model->status = Employee::STATUS_ACTIVE) {
                        return '<b class="badge" style="background-color: #17a2b8">' . Yii::t('app', 'Active') . '</b>';
                    }
                    return '<b class="badge" style="background-color: #e03939">' . Yii::t('app', 'Inactive') . '</b>';

                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {view} {delete}',
                'header' => false,
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('employee/view'),
                    'update' => function ($model) {
                        return Yii::$app->user->can('employee/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function ($model) {
                        return Yii::$app->user->can('employee/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]) ?>
    <?php Pjax::end(); ?>
</div>


<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'employee',
    'crud_name' => 'employee',
    'modal_id' => 'employee-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'Employee') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'client_options' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ],
    'modal_size' => 'modal-md',
    'grid_ajax' => 'employee_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]) ?>

<?php

$css = <<<CSS
     .kv-panel-before{
            padding: 2px!important;
     }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important; 
        text-align: center;
        font-size: 1.2em;
    }
}
CSS;
$this->registerCss($css);
?>
