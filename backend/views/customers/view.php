<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="customers-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'region_id',
                'value' => function ($model) {
                    return \backend\models\Regions::findOne($model->region_id)->name_uz;
                }
            ],
            [
                'attribute' => 'address',
            ],
            [
                'attribute' => 'fullname',
            ],
            [
                'attribute' => 'tel1',
            ],
            [
                'attribute' => 'tel2',
            ],
            [
                'attribute' => 'add_info',
            ],
            [
                'attribute' => 'type',
                'format' => 'html',
                'value' => function ($model) {
                    if ($model->type === \common\models\Customers::CUSTOMERS_TYPE_CLIENT) {
                        return '<b class="label label-success">' . Yii::t('app', 'Mijoz') . '</b>';
                    }
                    if ($model->type === \common\models\Customers::CUSTOMERS_TYPE_SUPPLIER) {
                        return '<b class="label label-danger">' . Yii::t('app', 'Ta`minolovchi') . '</b>';

                    }
                    return '';
                }
            ],

            [
                'attribute' => 'created_by',
                'value' => function($model){
                    $username = \common\models\User::findOne($model->created_by)['fullname'];
                    return isset($username)?$username:$model->created_by;
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function($model){
                    $username = \common\models\User::findOne($model->updated_by)['fullname'];
                    return isset($username)?$username:$model->updated_by;
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model){
                    return (time()-$model->created_at<(60*60*24))?Yii::$app->formatter->format(date($model->created_at), 'relativeTime'):date('d.m.Y H:i',$model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model){
                    return (time()-$model->updated_at<(60*60*24))?Yii::$app->formatter->format(date($model->updated_at), 'relativeTime'):date('d.m.Y H:i',$model->updated_at);
                }
            ],
        ],
    ]) ?>

</div>
