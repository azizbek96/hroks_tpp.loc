<?php

use yii\grid\SerialColumn;
use yii\grid\ActionColumn;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customers-index">
    <?php if (Yii::$app->user->can('customers/create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
            ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>

    </p>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'customers_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => SerialColumn::class],


            'fullname',
            'name',
            [
                'attribute' => 'region_id',
                'format' => 'html',
//                'filter' => \common\models\Customers::getRegionsList(),
//                'contentOptions' => [
//                    'style' =>'width:160px'
//                ],
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'region_id',
                    'data' => \common\models\Customers::getRegionsList(),
                    'size' => Select2::SIZE_SMALL,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select region..........    '),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    ],
                ]),
                'value' => function ($model) {
                    return \backend\models\Regions::findOne($model->region_id)->name_uz??'';
                }
            ],
            'tel1',

            [
                'attribute' => 'type',
                'format' => 'html',
                'filter' => [
                    \common\models\Customers::CUSTOMERS_TYPE_CLIENT => Yii::t('app', 'Mijoz'),
                    \common\models\Customers::CUSTOMERS_TYPE_SUPPLIER => Yii::t('app', 'Ta`minolvchi'),

                ],
                'value' => function ($model) {
                    if ($model->type === \common\models\Customers::CUSTOMERS_TYPE_CLIENT) {
                        return '<b class="label label-success">' . Yii::t('app', 'Mijoz') . '</b>';
                    }
                    if ($model->type === \common\models\Customers::CUSTOMERS_TYPE_SUPPLIER) {
                        return '<b class="label label-info">' . Yii::t('app', 'Ta`minolovchi') . '</b>';

                    }
                    return '';
                }
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {view} {delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('customers/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('customers/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('customers/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'customers',
    'crud_name' => 'customers',
    'modal_id' => 'customers-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'Customers') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'customers_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]) ?>
