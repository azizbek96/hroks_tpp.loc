<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'customAjaxForm']]); ?>

    <div class="container"></div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true,'required' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'fullname')->textInput(['maxlength' => true, 'required' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'region_id')->widget(\kartik\select2\Select2::className(), [
                'data' => $model::getRegionsList(),
                'options' => [
                        'placeholder' => Yii::t('app','Hududni tanlang')
                ],
                'pluginOptions' => [
                    'class' => 'form-control form-control-solid',
                    'allowClear' => true,
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                ],
            ]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'required' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'tel1')->widget(MaskedInput::className(),
                ['mask' => '+\9\9\8\(99\) 999 99 99']
            )?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'tel2')->widget(MaskedInput::className(),
                ['mask' => '+\9\9\8\(99\) 999 99 99']
            )?>
        </div>
    </div>

    <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

    <?= $form->field($model, 'type')->dropDownList([
        $model::CUSTOMERS_TYPE_SUPPLIER => Yii::t('app', 'Ta\'minlovchi'),
        $model::CUSTOMERS_TYPE_CLIENT => Yii::t('app', 'Mijoz'),
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
