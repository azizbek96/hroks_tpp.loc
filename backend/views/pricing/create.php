<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Pricing */

$this->title = Yii::t('app', 'Create Pricing');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pricings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pricing-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
