<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\DocItems;
use kartik\select2\Select2;
use kartik\money\MaskMoney;
use unclead\multipleinput\TabularInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Pricing */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pricing-form">
    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'customAjaxForm']]); ?>
    <?= TabularInput::widget([
        'models' => $model,
        'form' => $form,
        'id' => 'doc_price',
        'addButtonOptions' => [
            'class' => 'btn btn-sm btn-success'
        ],
        'removeButtonOptions' => [
            'class' => 'btn btn-sm btn-danger'
        ],
        'columns' => [
            [
                'title' => Yii::t('app', 'Product'),
                'name' => 'product_id',
                'headerOptions' => [
                    'style' => 'width: 250px;',
                ],
                'type' => Select2::class,
                'options' => [
                    'options' => ['placeholder' => 'Mahsulotni tanlang  ...'],
                    'data' => DocItems::getProductList(),
                ],
            ],
            [
                'name' => 'price',
                'type' => MaskMoney::class,
                'options' => [
                    'pluginOptions' => [
                        'prefix' => 'UZS ',
                        'suffix' => '',
                        'precision' => 0
                    ]
                ],

            ]
        ],
    ]) ?>
    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
