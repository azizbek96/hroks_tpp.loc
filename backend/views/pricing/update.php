<?php
/* @var $this yii\web\View */
/* @var $model backend\models\Pricing */
/* @var $update backend\models\Pricing */

$this->title = Yii::t('app', 'Update Pricing: {name}', [
    'name' => $update->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pricings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $update->id, 'url' => ['view', 'id' => $update->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pricing-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
