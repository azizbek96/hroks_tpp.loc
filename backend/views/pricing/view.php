<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Pricing */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pricings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="pricing-view">
    <?php if(!Yii::$app->request->isAjax){?>
    <div class="pull-right" style="margin-bottom: 15px;">
        <?php //if (Yii::$app->user->can('pricing/update')): ?>
            <?php  if ($model->status < $model::STATUS_SAVED): ?>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        <?php //endif; ?>
        <?php //if (Yii::$app->user->can('pricing/delete')): ?>
            <?php  if ($model->status < $model::STATUS_SAVED): ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
        <?php //endif; ?>
        <?=  Html::a(Yii::t('app', 'Back'), ["index"], ['class' => 'btn btn-info']) ?>
    </div>
    <?php }?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            [
                'label' => Yii::t('app', 'Mahsulot'),
                'attribute' => 'product_id',
                'value' => function($model){
                    return $model->product->name;
                }
            ],
            [
                'label' => Yii::t('app', 'Narx'),
                'attribute' => 'price',
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    $status = $model::getStatusList($model->status);
                    return isset($status)?$status:$model->status;
                }
            ],
            // 'type',
            [
                'label' => Yii::t('app', 'Kiritgan Shaxs'),
                'attribute' => 'created_by',
                'value' => function($model){
                    $username = \common\models\User::findOne($model->created_by)['fullname'];
                    return isset($username)?$username:$model->created_by;
                }
            ],
            [
                'label' => Yii::t('app', 'O\'zgartirgan Shaxs'),
                'attribute' => 'updated_by',
                'value' => function($model){
                    $username = \common\models\User::findOne($model->updated_by)['fullname'];
                    return isset($username)?$username:$model->updated_by;
                }
            ],
            [
                'label' => Yii::t('app', 'Kiritilgan Vaqt'),
                'attribute' => 'created_at',
                'value' => function($model){
                    return (time()-$model->created_at<(60*60*24))?Yii::$app->formatter->format(date($model->created_at), 'relativeTime'):date('d.m.Y H:i',$model->created_at);
                }
            ],
            [
                'label' => Yii::t('app', 'O\'zgartirilgan Vaqt'),
                'attribute' => 'updated_at',
                'value' => function($model){
                    return (time()-$model->updated_at<(60*60*24))?Yii::$app->formatter->format(date($model->updated_at), 'relativeTime'):date('d.m.Y H:i',$model->updated_at);
                }
            ],
        ],
    ]) ?>

</div>
