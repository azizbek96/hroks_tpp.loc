<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DepArea */

$this->title = Yii::t('app', 'Create Dep Area');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dep Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dep-area-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
