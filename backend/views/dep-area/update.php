<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DepArea */

$this->title = Yii::t('app', 'Update Dep Area: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dep Areas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dep-area-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
