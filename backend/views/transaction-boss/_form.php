<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TransactionBoss */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-boss-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'data-pjax' => true,
            'id' => $model->formName(),
            'class' => 'customAjaxForm'
        ]
    ]); ?>


    <?=
    $form->field($model, 'customer_id')->widget(Select2::class, [
        'data' => \common\models\Doc::getCustomerId(),
        'theme' => Select2::THEME_DEFAULT,
        'options' => ['placeholder' => '',],// 'multiple' => true
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'amount')->widget(\kartik\money\MaskMoney::class, [
        'pluginOptions' => [
            'prefix' => 'UZS ',
            'suffix' => '',
            'precision' => 0
        ]
    ]) ?>

    <?= $form->field($model, 'type')->hiddenInput(['maxlength' => true, 'value' => \backend\models\TransactionBoss::T_SUPPLIER_BOSS_PAID])->label(false) ?>
    <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>
    <?= $form->field($model, 'add_info')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon-save glyphicon">&nbsp;</i>' . Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
