<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TransactionBoss */

$this->title = Yii::t('app', 'Qabul qilish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transaction Bosses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-boss-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
