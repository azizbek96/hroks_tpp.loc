<?php

use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\grid\SerialColumn;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionBossSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'To\'lov');
$this->params['breadcrumbs'][] = $this->title;
?>
    <?php if (Yii::$app->user->can('transaction-boss/create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
            ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>
    </p>
    <?php endif; ?>
    <br><br>
    <?php Pjax::begin(['id' => 'transaction-boss_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Kontragentlarga to\'langan pullar')],
        'columns' => [
            ['class' => SerialColumn::class],


            [
                'attribute' => 'customer_id',
                'filter' => \common\models\Doc::getCustomerId(),
                'value' => function ($model) {
                    return $model->customer->name;
                },
                'group' => true
            ],
            [
                'attribute' => 'amount',
                'value' => function ($model) {
                    return \backend\models\TransactionBoss::getNumberFormat($model['amount']);
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => false,
                'value' => function($model){
                    return (time()-$model->created_at<(60*60*24))?Yii::$app->formatter->format(date($model->created_at), 'relativeTime'):date('d.m.Y H:i',$model->created_at);
                }
            ],
            [
                'attribute' => 'add_info',
                'label' => Yii::t('app', 'Izoh')
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {view} {delete}',
                'header' =>false,
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('transaction-boss/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('transaction-boss/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('transaction-boss/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]) ?>

    <?php Pjax::end(); ?>

<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'transaction-boss',
    'crud_name' => 'transaction-boss',
    'modal_id' => 'transaction-boss-modal',
    'modal_header' => '<h3>' . $this->title . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'transaction-boss_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]) ?>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important; 
        text-align: center;
        font-size: 1.2em;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>
