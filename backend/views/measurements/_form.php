<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Measurements */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="measurements-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class' => 'customAjaxForm']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'koefficient')->textInput(['class' => 'form-control number', 'maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([
                        $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
                        $model::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
    ]) ?>
    <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>


    <div class="form-group">
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
