<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Measurements */

$this->title = Yii::t('app', 'Create Measurements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Measurements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="measurements-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
