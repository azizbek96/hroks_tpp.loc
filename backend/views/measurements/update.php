<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Measurements */

$this->title = Yii::t('app', 'Update Measurements: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Measurements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="measurements-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
