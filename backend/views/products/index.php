<?php

use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use backend\models\Products;
use common\widgets\ModalWindow\ModalWindow;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">
    <?php if (Yii::$app->user->can('products/create')): ?>
        <p class="pull-right no-print">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
                ['class' => 'create-dialog btn btn-sm btn-success', 'id' => 'buttonAjax']) ?>

        </p><br>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'products_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'tableOptions' => [
                'class' => 'table-hover table table-bordered'
        ],
        'columns' => [
            ['class' => SerialColumn::class],

            [
                'attribute' => 'cat_id',
                'label' => Yii::t('app', 'Cat'),
                'value' => function ($model) {
                    return $model->cat->name;
                },
                'group' => true,
                'hAlign' => true,
                'vAlign' => true,
                'filter' => (new backend\models\Products)->getProductCategoriesList(),
            ],
            'name',
            'add_info:ntext',

            [
                'attribute' => 'status',
                'format' => 'html',
                'contentOptions' => [
                    'class' => 'center',
                    'style' => 'text-align:center'
                ],
                'filter' => [
                    Products::STATUS_ACTIVE => Yii::t('app', 'Active'),
                    Products::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
                ],
                'value' => function ($model) {
                    if ($model->status = Products::STATUS_ACTIVE) {
                        return '<b class="badge" style="background-color: #17a2b8">' . Yii::t('app', 'Active') . '</b>';
                    }
                    return '<b class="badge" style="background-color: #e03939">' . Yii::t('app', 'Inactive') . '</b>';
                },

            ],
            [
                'class' => ActionColumn::class,
                'header' => false,
                'template' => '{update} {view} {delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('products/view'),
                    'update' => function ($model) {
                        return Yii::$app->user->can('products/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function ($model) {
                        return Yii::$app->user->can('products/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]) ?>

    <?php Pjax::end(); ?>

</div>
<?= ModalWindow::widget([
    'model' => 'products',
    'crud_name' => 'products',
    'modal_id' => 'products-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'Products') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'products_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]) ?>
