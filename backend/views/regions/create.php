<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Create Directory Regions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Directory Regions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

<?php $js=<<<JS
    $('.modal-title').html('{$this->title}');
JS;
$this->registerJs($js);
