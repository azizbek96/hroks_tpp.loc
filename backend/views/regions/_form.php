<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing card">
    <div class="widget-chart-one">
        <div class="widget-content">

            <?php $form = ActiveForm::begin([
                'id' => $model->formName(),
                'method' => 'post',
//                'enableAjaxValidation' => true,
//                'validationUrl' => Url::toRoute('directory-regions/validate'),
            ]); ?>

            <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>



            <?= $form->field($model, 'parent_id')->label(false)->hiddenInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'status')->dropDownList([
                $model::STATUS_ACTIVE => Yii::t('app', 'Active'),
                $model::STATUS_INACTIVE => Yii::t('app', 'Inactive')
            ]) ?>

            <?= Html::submitButton('<i class="la la-save"></i>' . Yii::t('app', 'Save'),
                ['class' => 'btn btn-primary btn-sm saveButton']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
