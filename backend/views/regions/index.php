<?php

use yii\helpers\Url;
use app\components\PermissionHelper as P;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $regions_tree  */

$this->title = Yii::t('app', 'Directory Regions');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('@web/asset-cork/assets/css/elements/custom-tree_view.css', ['depends' => [\yii\web\JqueryAsset::className()]]);

?>
<style>
    .file-tree li.file-tree-folder::before {
        display: none;
    }
    .file-tree-folder {
        margin-left: 15px;
    }
</style>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <i class="spinner-border text-danger align-self-center loader-sm"></i>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">x</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>


<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-2 layout-spacing">
    <div class="widget widget-chart-one">
<!--        <div class="widget-content p-3" style="border-radius: 8px; border: 1px solid lightgrey; text-align: left">-->
<!--            --><?php //if(P::can('directory-regions/create')):?>
                <button class="btn btn-xlx btn-primary save-parent"
                        href="<?=Url::to(['regions/create'])?>"
                        data-toggle="modal" data-target="#exampleModal">
                    <i class="p-1 font-weight-bolder">+</i>
                </button>
<!--            --><?php //endif;?>
<!--            --><?php //if(P::can('regions/create')):?>
                <button disabled="disabled" class="btn btn-xlx btn-success child-parent"
                        date-create="true" href="<?=Url::to(['regions/create'])?>"
                        data-toggle="modal" data-target="#exampleModal"
                        id="" data-parent="">
                    <i class="far fa-arrow-alt-circle-down"></i>
                </button>
<!--            --><?php //endif;?>
<!--            --><?php //if(P::can('regions/update')):?>
                <button disabled="disabled" class="btn btn-xlx btn-primary update-parent"
                        href="<?=Url::to(['regions/update']) ?>"
                        data-toggle="modal" data-target="#exampleModal"
                        id="">
                    <i class="far fa-edit"></i>
                </button>
<!--            --><?php //endif;?>
<!--            --><?php //if(P::can('regions/delete')):?>
                <button disabled="disabled" class="btn btn-xlx btn-danger delete-parent"
                        href="<?=Url::to(['regions/delete'])?>" id="">
                    <i class="far fa-trash-alt"></i>
                </button>
<!--            --><?php //endif;?>
            <div id="kt_tree_1" class="tree-demo mt-3">
                <?= $regions_tree ?>
            </div>
        </div>
    </div>
</div>
<?php
$title = Yii::t('app','Region');
$js = <<< JS

// create
$('.save-parent').click(function (){
    let url = $(this).attr('href');
    $('.modal-body').html('');
    $('.modal-body').load(url);
});
   
  
// update
$('.update-parent').click(function() {
    let url=$(this).attr('href');
    var id = $(this).attr('id');
    url = url+'?id='+id;
    $('.modal-body').html('');
    $('.modal-body').load(url);
    setTimeout(function (){
        $('.modal-title').html('$title');
    }, 500);
});

// child
$('.child-parent').click(function() {
    let url = $(this).attr('href');
    let id = $(this).attr('id');
    url = url+'?parent='+id;
    $('.modal-body').html('');
    $('.modal-body').load(url);
    setTimeout(function (){
        $('.modal-title').html('$title');
    }, 500);
});


// delete 
$('.delete-parent').click(function(e) {
    e.preventDefault();
    let url=$(this).attr('href');
    var id = $(this).attr('id');
    url = url+'?id='+id;
    swal({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Delete',
          padding: '2em'
    }).then(function(result) {
          if (result.value) {    
              $.ajax({
                    url: url,
                    type: "GET",
                    success: function (res) {
                        if (res['status'] === 'true') {
                            swal(
                              res['saved_one'],
                              res['saved'],
                              'success'
                            )
                            setTimeout(location.reload.bind(location), 500);
                        } else {
                            swal(
                              res['error_one'],
                              res['error'],
                              'warning'
                            )
                        }
                    }
              });
          }
        });
});



// li
$('li').click(function (){
    $('li').css('color', '#0e1726');
    $(this).css('color', 'blue');
    $('.update-parent').attr('id', $(this).attr('value'));
    $('.delete-parent').attr('id', $(this).attr('value'));
    $('.child-parent').attr('id', $(this).attr('value'));
    $('.child-parent').attr('parent-id', $(this).attr('data-parent-id'));
    
    $('.update-parent').removeAttr('disabled');
    $('.delete-parent').removeAttr('disabled');
    $('.child-parent').removeAttr('disabled');
});

JS;
$this->registerJs($js);

$this->registerJsFile('@web/asset-cork/plugins/treeview/custom-jstree.js', ['depends' => 'yii\web\JqueryAsset']);
?>

