<?php

use common\models\Doc;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $modelItem common\models\DocItems */
/* @var $form yii\widgets\ActiveForm */
/* @var $employeeId*/

/* @var $slug */
$slug = Yii::$app->request->get('slug');
$this->title = Yii::t('app', Doc::getDocTypeBySlug($slug));
?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="text-right" style="display: inline-flex;float: right;">
                <label for="invoice_number"><?php echo Yii::t('app', 'Yuk xati raqami') ?>&nbsp;</label>
                <input type="text" name="invoice_number" id="invoice_number" class="number form-control" style="width: 100px!important; display: inline">
                <span class="check-invoice btn btn-sm btn-success" style="display: block"><?php echo Yii::t('app', 'Qidirish') ?></span>
                <?= Html::a(Yii::t('app', 'Qidirish'), ['doc/create', 'slug' => $slug],
                    ['class' => 'btn btn-sm btn-success off-display','id' => 'create-button']) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?=
            $form->field($model, 'employee_id')->widget(Select2::classname(), [
                'data' => $model::getEmployeeId(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => [
                    'disabled' => true,
                    ],// 'multiple' => true
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ]) ?>

            <?= $form->field($model, 'department_id')->widget(Select2::classname(), [
                'data' => $model::getDeparmentsId($model::OUTGOING),
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ]) ?>
            <p class="hide department_hide" style="color: red"><?php echo Yii::t('app', 'Bo`limni tanlang') ?></p>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'reg_date')->widget(DatePicker::classname(), [
                'options' => ['value' => (empty($model->reg_date) ? date('d-m-Y') : null), 'placeholder' => 'sana ...', 'autocomplete' => 'off',],
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy'
                ]
            ]) ?>

            <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>

            <?= $form->field($model, 'type')->label(false)->hiddenInput(['value' => $model::DOC_TYPE_SELL]) ?>

            <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

        </div>
        <div class="col-lg-12">

        </div>
    </div>
    <div class="row">
        <div class="table-responsive col-lg-12 col-sm-12 col-md-12">
            <table class="table table-bordered">
                <thead>
                <th>#</th>
                <th align="cente"><?= Yii::t('app', 'Product name') ?></th>
                <th><?= Yii::t('app', 'quantity') ?></th>
                <th><?= Yii::t('app', 'Sold price') ?></th>
                <th><?= Yii::t('app', 'measurement') ?></th>
                <th><?= Yii::t('app', 'Fakt') ?></th>
                <th><?= Yii::t('app', 'Dona') ?></th>
                </thead>
                <tbody>
                <?php if (!empty($model->cp['modelItems'])):?>
                    <?php $i = 1;
                    foreach ($model->cp['modelItems'] as $item) : ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $item['product_name'] ?></td>
                            <td class="quantity">
                                <?= Doc::getNumberFormat($item['quantity']) ?>
                                <input type="hidden" name="DocItems[<?= $i ?>][product_id]"
                                       value="<?= $item['product_id'] ?>">
                                <input type="hidden" name="DocItems[<?= $i ?>][measurement_id]"
                                       value="<?= $item['measurement_id'] ?>">
                            </td>
                            <td>
                                <?= Doc::getNumberFormat($item['sold_price']) ?>
                                <input type="hidden" name="DocItems[<?= $i ?>][sold_price]"
                                       value="<?= $item['sold_price'] ?>">
                            </td>
                            <td>
                                <?= Doc::getMeasurementLabel($item['measurement_id']) ?>
                                <input type="hidden" name="DocItems[<?= $i ?>][measurement_id]"
                                       value="<?= $item['measurement_id'] ?>">
                            </td>
                            <td width="200">
                                <div class="input-group input_group">
                                    <span class="input-group-addon minus">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </span>
                                    <input type="text" name="DocItems[<?= $i ?>][amount]" value="<?=empty($item['quantity1'])?'0':$item['quantity1']?>"
                                           class="form-control amount number">
                                    <span class="input-group-addon plus">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </span>
                                </div>
                            </td>
                            <td width="200">
                                <div class="input-group input_group">
                                    <span class="input-group-addon minus_1">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </span>
                                    <input type="text" name="DocItems[<?= $i ?>][karopka_quantity]" value="<?=empty($item['karopka_quantity'])?'0':$item['karopka_quantity']?>"
                                           class="form-control karopka_quantity number">
                                    <span class="input-group-addon plus_1">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    <?php $i++; endforeach; ?>
                <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div>
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>
</div>

<?php
$css = <<<CSS

      input{
            height: 28px!important;
      }
      .input-group-addon{
      /*border: 1px solid red!important;*/
      background: #a6f1b1;
      }
      .input_group .input-group-addon:hover{
            background: #0BB7AF;
      }
     #sendButton{
        font-size: 24px!important;
        padding-left: 100px!important;
        padding-right: 100px!important;
        display: block;
        float:right;
    }
    .item-table th {
        border: 1px solid gray;
    }
    .document-table th {
        border: 1px solid lightgrey;
        padding-left: 10px;
    }
    .item-tr th {
        padding-left: 5px;
        background-color: #75ED8B;
        border: 1px solid black;
    }
    .item-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid black;
        padding-right: 5px;
        color: #0a73bb;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    tr {
      height: 3px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
CSS;
$this->registerCss($css); $views = Url::to(['ajax-invoice']);

$views = Url::to(['doc/qayta_qabul/ajax-invoice']);
$create = Url::to(['doc/create', 'slug' => $slug]);
$js = <<<JS
    var date = new Date();
    $('#doc-reg_date').val(((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '.' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '.' + date.getFullYear());
    $('#doc-employee_id').val('{$model->cp["employeeId"]}').trigger('change');
     $(document).ready(function (){
        $('.check-invoice').click(function (event){
            let hr = $('#create-button').attr('href');
            let inputValue = $('#invoice_number').val();
            if (inputValue !== ''){
                let hrf = '$create' + '?invoice_number=' + inputValue;
                $.ajax({
                    type: 'POST',
                    url: '$views',
                    data: {id:inputValue},
                    success: function(response){
                        if (response.status === 'true') {
                            window.location.href=hrf;
                        } else {
                            $('#invoice_number').focus();
                            $('#invoice_number').css({"box-shadow": "0px 0px 3px red", "border-color": "red"});
                        }
                    }
                });

            }else {
                $('#invoice_number').focus();
                $('#invoice_number').css({"box-shadow": "0px 0px 3px red", "border-color": "red"});
            }
        })
    });
    
    $('.minus').on('click',function (event){
        let td = $(this).parents('td').find('input');
        let val = td.val()*1;
        if (val > 0)
        {
            td.val(val-1);
        }else td.val('0');
    });
    $('.plus').on('click',function (event){
        let tr = $(this).parents('tr');
        let tdInput= tr.find('.amount');
        let maxNum = tr.find('.quantity').text()*1;

        let val = tdInput.val()*1;
        if (val < maxNum){
            tdInput.val(val+1);
        }
    });
    $('.minus_1').on('click',function (event){
        let td = $(this).parents('td').find('input');
        let val = td.val()*1;
        if (val > 0)
        {
            td.val(val-1);
        }else td.val('0');
    });
    $('.plus_1').on('click',function (event){
        let tr = $(this).parents('tr');
        let tdInput= tr.find('.karopka_quantity'); 

        let val = tdInput.val()*1;
        tdInput.val(val+1);
    });
    $('.amount').on('keyup',function(event) {
      let val = $(this).val()*1;
      let tr = $(this).parents('tr');
      let maxNum = tr.find('.quantity').text()*1;
      if (val > maxNum){
          $(this).val(maxNum);
      }
    })
JS;
$this->registerJs($js);
?>

