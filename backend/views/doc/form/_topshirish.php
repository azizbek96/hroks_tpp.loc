<?php

use common\models\DocItems;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use unclead\multipleinput\TabularInput;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $modelItem common\models\DocItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <?=
            $form->field($model, 'employee_id')->widget(Select2::classname(), [
                'data' => $model::getEmployeeId(),
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ]) ?>

            <?= $form->field($model, 'department_id')->widget(Select2::class, [
                'data' => $model::getDeparmentsId($model::OUTGOING),
                'theme' => Select2::THEME_DEFAULT,
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ]) ?>
            <p class="hide department_hide" style="color: red"><?php echo Yii::t('app', 'Bo`limni tanlang') ?></p>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'reg_date')->widget(DatePicker::class, [
                'options' => [
                    'placeholder' => Yii::t('app', 'Sana'),
                    'autocomplete' => "off",
                ],
                'language' => 'ru',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true,
                    'todayBtn' => true
                ]
            ]) ?>

            <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>

            <?= $form->field($model, 'type')->label(false)->hiddenInput(['value' => $model::DOC_TYPE_SELL]) ?>

            <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

        </div>
        <div class="col-lg-12">

        </div>
    </div>
    <div class="row">
        <div class="table-responsive col-lg-12 col-sm-12 col-md-12">
            <?= TabularInput::widget([
                'models' => $modelItem,
                'form' => $form,
                'addButtonOptions' => [
                    'class' => 'btn btn-sm btn-success'
                ],
                'removeButtonOptions' => [
                    'class' => 'btn btn-sm btn-danger'
                ],
                'columns' => [
                    [
                        'name' => 'product_id',
                        'title' => Yii::t('app', 'Product'),
                        'type' => Select2::class,
                        'options' => [
                            'data' => DocItems::getProductList(),
                            'theme' => Select2::THEME_DEFAULT,
                            'options' => [
                                'placeholder' => 'Tanlang',
                                'class' => 'product',
                            ],
                            'pluginOptions' => [
                                'allowclear' => true,
                            ]

                        ],
                        'headerOptions' => [
                            'style' => 'width: 250px;',
                        ]
                    ],
                    [
                        'name' => 'karopka_quantity',
                        'title' => Yii::t('app', 'Dona'),
                        'options' => ['class' => 'number quantityKaropka'],
                    ],
                    [
                        'name' => 'quantity',
                        'title' => Yii::t('app', 'Quantity'),
                        'options' => ['class' => 'number quantity',],
                    ],
                    [
                        'name' => 'count',
                        'value' => function ($data) {
//                            return $data->getRemainCount();
                        },
                        'title' => Yii::t('app', 'Mavjud'),
                        'options' => ['class' => 'number remain', 'readonly' => true],
                    ],
                    [
                        'name' => 'measurement_id',
                        'title' => Yii::t('app', 'Measurement'),
                        'type' => Select2::class,
                        'options' => [
                            'class' => 'price',
                            'data' => DocItems::getMeasurementList(),
                            'hideSearch' => true,
                            'theme' => Select2::THEME_DEFAULT,
                            'pluginOptions' => [
                                'allowclear' => true,
                            ],
                        ]
                    ],
                    [
                        'name' => 'sold_price',
                        'title' => Yii::t('app', 'Sold Price') . '(SUM)',
                        'options' => [
                            'class' => 'number',
                            'readonly' => true,
                        ],
                    ],
                ],
            ]) ?>
        </div>
    </div>
    <br>
    <div>
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>' . ' ' . Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>
</div>

<?php
$url = Url::to(['pricing/price-item-balance-inventory']);
$js = <<<JS
    $('body').delegate('.product','change', function (){ 
        let id = $(this).attr('id'); 
        let department = $('#doc-department_id').val();
        let price_input = id.split("-");
        let s1 = '#'+price_input[0]+'-'+price_input[1]+'-'+'sold_price';
        let quantity = '#'+price_input[0]+'-'+price_input[1]+'-'+'quantity';
        let count = '#'+price_input[0]+'-'+price_input[1]+'-'+'count';
        let value = $(this).val(); 
        
        if(!isEmpty(department)){
            $('.department_hide').addClass('hide');
            $.ajax({
             url:'$url',
             data:{id:value,department:department},
             type:'GET',
                 success: function(response){
                     if(response.status){
                         let sum = response.data;
                         let soni = response.inventory;
                         if ((sum != 0 && sum != null) && (soni != 0 && soni != null)){
                                $(s1).val(sum);
                                $(count).val(soni)
                                $(quantity).attr('readonly', false)
                         }else{
                             $(quantity).attr('readonly', true);
                             $(quantity).val('');
                         }
                        
                     }else{
                      $(s1).val('');
                      $(count).val('');
                      $(quantity).val('');
                      $(quantity).attr('readonly', true)
                     }              
                 }
            });
        }else{
            $('.department_hide').removeClass('hide');
        }
    });
    function isEmpty(object){
        for (let key in object){
            return false;
        }
        return true;
    }

    $('body').delegate('.quantity','blur',function(e){
        let id = $(this).attr('id'); 
        let parent = $(this).parents('tr.multiple-input-list__item');
        let value = parseFloat($(this).val());
        let remain = parseFloat(parent.find('.remain').val());
        if (remain){
            if (remain < value){
                $(this).val(remain);
            }
            if (remain == 0){
                $(this).val(remain)
            }
        }
    });
     
JS;
$this->registerJS($js);
?>
<?php
$js = <<<JS
    $('body').delegate('.quantity','blur',function(e){
        let department = $('#doc-department_id').val();
        let value = parseFloat($(this).val());
        let parent = $(this).parents('tr.multiple-input-list__item');
        let product =  parent.find('.product').val();
                    let id = $(this).attr('id');  
                    let price_input = id.split("-");
                    let s1 = '#'+price_input[0]+'-'+price_input[1]+'-'+'sold_price';
                    let quantity = '#'+price_input[0]+'-'+price_input[1]+'-'+'quantity';
                    let count = '#'+price_input[0]+'-'+price_input[1]+'-'+'count';
         $.ajax({
             url:'$url',
             data:{id:product,department:department},
             type:'GET',
                 success: function(response){
                     if(response.status){
                         let sum = response.data;
                         let soni = response.inventory;
                         if ((sum != 0 && sum != null) && (soni != 0 && soni != null)){
                                $(s1).val(sum);
                                $(count).val(soni);
                                $(quantity).attr('readonly', false);
                                 if (value >= parseFloat(soni)){
                                     $(quantity).attr('value', soni);
                                     $(quantity).val(soni);
                                 }
                         }else{
                             $(quantity).attr('readonly', true);
                             $(quantity).val('');
                         }
                     }else{
                      $(s1).val('');
                      $(count).val('');
                      $(quantity).val('');
                      $(quantity).attr('readonly', true)
                     }              
                 }
            });
    });
JS;
$this->registerJS($js);
$css = <<<CSS

      input{
            height: 28px!important;
      }
CSS;
$this->registerCss($css);
if (!$model->isNewRecord) {

}
?>

