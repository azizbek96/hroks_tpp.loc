<?php

use common\models\DocItems;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                use kartik\date\DatePicker;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use common\components\TabularInput\CustomTabularInput;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DocSearch */
/* @var $modelItem common\models\DocItems */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?=
            $form->field($model, 'customer_id')->widget(Select2::class, [
                'data' => $model::getCustomerId(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => [
                    'placeholder' => '',
                    'readonly' => true,
                ],
                    'class' => 'readonly_select',
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ]) ?>

            <?= $form->field($model, 'department_id')->widget(Select2::class, [
                'data' => $model::getDeparmentsBoss(),
                'theme' => Select2::THEME_DEFAULT,
                'options' => [
                    'placeholder' => '',
                    'readonly' => true,
                    'class' => 'readonly_select'
                ],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                    'readonly' => true
                ],
            ]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?= $form->field($model, 'reg_date')->widget(DatePicker::classname(), [
                'options' => [
                    'placeholder' => Yii::t('app', 'Sana'),
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true,
                ]
            ])->label(Yii::t('app', 'Sana')) ?>

            <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

            <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>

            <?= $form->field($model, 'type')->label(false)->hiddenInput(['value' => $model::DOC_TYPE_INCOMING_BOSS]) ?>

        </div>
    </div>
    <div class="row">
        <div class="table-responsive col-lg-12 col-sm-12 col-md-12">
            <?= CustomTabularInput::widget([
                'id' => 'incoming_tabular_id',
                'models' => $modelItem,
                'form' => $form,
                'addButtonOptions' => [
                    'class' => 'btn btn-sm btn-success remove-td hide'
                ],
                'removeButtonOptions' => [
                    'class' => 'btn btn-sm btn-danger remove-td hide'
                ],
                'showFooter' => true,
                'attributes' => [
                    [
                        'id' => 'footer_product_id',
                        'value' => Yii::t('app', 'Jami'),
                    ],
                    [
                        'id' => 'footer_quantity',
                        'value' => 0,
                        'class' => 'text-bold font-size-16',
                    ],
                    [
                        'id' => 'footer_measurement_id',
                        'value' => null,
                    ],
                    [
                        'id' => 'footer_incoming_price',
                        'value' => null,
                    ],
                    [
                        'id' => 'footer_return_quantity',
                        'value' => null,
                    ],
//                    [
//                        'id' => 'footer_return_price',
//                        'value' => null,
//                    ],
                    [
                        'id' => 'footer_summa',
                        'value' => 0,
                        'class' => 'text-bold font-size-16'
                    ],
                ],
                'rowOptions' => [
                    'id' => 'row{multiple_index_incoming_tabular_id}',
                    'data-row-index' => '{multiple_index_incoming_tabular_id}'
                ],
                'columns' => [
                    [
                        'name' => 'product_id',
                        'title' => Yii::t('app', 'Maxsulot'),
                        'type' => Select2::class,
                        'options' => [
                            'data' => DocItems::getProductList(),
                            'theme' => Select2::THEME_DEFAULT,
                            'options' => [
                            ],
                            'readonly' => true,
                            'pluginOptions' => [
                                'allowclear' => true,
                            ],
                        ],
                        'columnOptions' => [
                            'class' => 'readonly_select'
                        ]
                    ],
                    [
                        'name' => 'ret_quantity',
                        'title' => Yii::t('app', 'Miqdori'),
                        'options' => [
                                'readonly' => true,
                                'class' => 'number quantityMoving'
                        ],
                    ],
                    [
                        'name' => 'measurement_id',
                        'title' => Yii::t('app', 'O`lchov'),
                        'type' => Select2::class,
                        'options' => [
                            'readonly' => true,
                            'data' => DocItems::getMeasurementListBoss(),
                            'theme' => Select2::THEME_DEFAULT,
                            'pluginOptions' => [
                            ],
                        ],
                        'columnOptions' => [
                            'class' => 'readonly_select',
                            'tabindex' => '-1'
                        ]
                    ],
                    [
                        'title' => Yii::t('app', 'Kelish narxi'),
                        'name' => 'incoming_price',
                        'type' => MaskMoney::class,
                        'options' => [
                            'options' => [
                                'class' => 'incomingPrice',
                                'readonly' => true,
                            ],
                            'pluginOptions' => [
                                'prefix' => 'UZS ',
                                'suffix' => '',
                                'precision' => 0
                            ]
                        ],

                    ],
                    [
                        'name' => 'quantity',
                        'title' => Yii::t('app', 'Qaytarish Miqdori '),
                        'options' => [
                            'class' => 'number',
                            'autocomplete' => 'off'
                        ],
//                        'value' => function($mod){
//                            return $mod->doc_item->quantity ?? null;
//                        }
                    ],
                ],
            ]) ?>
        </div>
    </div>
    <br>
    <div>
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>'.' '.Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>
</div>


<?php
$js = <<<JS
    $(document).ready(function (){
        $('.list-cell__button').remove();
    });
    $('#incoming_tabular_id').on('afterInit', function (e, row, currentIndex) {
          calculateSum('#footer_quantity', '.quantityMoving');
          calculateSum('#footer_summa', '.tabularSum');
     });
     $('body').delegate('.quantityMoving','change',function(e){
         calculateSum('#footer_quantity', '.quantityMoving');
     });
     
      function calculateSumma(className, secondClassName){
          $('body').delegate(className,'change',function(e){
          let quantityObj = $(this).parents('tr').find(secondClassName);
          let tabularSum = $(this).parents('tr').find('.tabularSum');
          if(quantityObj.length > 0){
             let qty = $(quantityObj[0]).val();
             if(qty && parseFloat(qty) > 0){
                 $(tabularSum[0]).val(parseFloat(qty)*$(this).val());
             }
          }
          calculateSum('#footer_summa', '.tabularSum');
        });    
      }      
            calculateSumma('input:hidden[class="incomingPrice"]', '.quantityMoving');
            calculateSumma('.quantityMoving','input:hidden[class="incomingPrice"]');
            $('body').delegate('.tabularSum','change',function(e){
                calculateSum('#footer_summa', '.tabularSum');
            });
            function calculateSum(id, className) {
                let rmParty = $('#incoming_tabular_id table tbody tr').find(className);
                let totalRMParty = 0;
                rmParty.each(function (key, item) {
                     if ($(this).val()) {
                        totalRMParty += parseFloat($(this).val());    
                     }
                });
                $(id).html(totalRMParty.toFixed(0));
            }
            $('body').delegate('.select2-container','keydown',function (e){
               let select = $(this).parent().find('s' +
                'elect');
               if(select.attr('readonly')){
                   // $('.select2-container--open').remove();
                   $(select).select2('close');
               }
            });
JS;
$this->registerJs($js);
$css = <<<CSS
      .js-input-plus {
            color: #090909!important; 
            border-color: #17dda6!important;
            background-color: #28d928!important;
      }       
      input{
            height: 28px!important;
      }
/*.readonly_select */.select2-container {
  background: #eeeeee!important;
  border-color: #a8a8a8;
  pointer-events: none;
}
CSS;
$this->registerCss($css)
?>
