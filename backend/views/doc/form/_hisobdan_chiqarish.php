<?php

use common\models\DocItems;
use common\models\Products;
use kartik\date\DatePicker;
use kartik\money\MaskMoney;
use kartik\select2\Select2;
use unclead\multipleinput\TabularInput;
use common\components\TabularInput\CustomTabularInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DocSearch */
/* @var $modelItem common\models\DocItems */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?= $form->field($model, 'reg_date')->widget(DatePicker::classname(), [
                'options' => [
                    'placeholder' => Yii::t('app', 'Sana'),
                    'autocomplete' => "off",
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true,
                    'todayBtn' => true
                ]
            ])->label(Yii::t('app', 'Sana')) ?>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?= $form->field($model, 'department_id')->widget(Select2::classname(), [
                'data' => $model::getDeparmentsId($model::OUTGOING),
                'theme' => Select2::THEME_DEFAULT,
                'hideSearch' =>true,
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true,
                    'tokenSeparators' => [',', ' '],
                    'maximumInputLength' => 10,
                ],
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

            <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>

            <?= $form->field($model, 'type')->label(false)->hiddenInput(['value' => $model::DOC_TYPE_OUTGOING]) ?>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive col-lg-12 col-sm-12 col-md-12">
            <?= CustomTabularInput::widget([
                'id' => 'incoming_tabular_id',
                'models' => $modelItem,
                'form' => $form,
                'addButtonOptions' => [
                    'class' => 'btn btn-sm btn-success'
                ],
                'removeButtonOptions' => [
                    'class' => 'btn btn-sm btn-danger'
                ],
                'showFooter' => true,
                'attributes' => [
                    [
                        'id' => 'footer_product_id',
                        'value' => Yii::t('app', 'Jami'),
                    ],
                    [
                        'id' => 'footer_quantity',
                        'value' => 0,
                        'class' => 'text-bold font-size-16',
                    ],
                    [
                        'id' => 'footer_measurement_id',
                        'value' => null,
                    ],
                    [
                        'id' => 'footer_incoming_price',
                        'value' => null,
                    ],
                    [
                        'id' => 'footer_summa',
                        'value' => 0,
                        'class' => 'text-bold font-size-16'
                    ],
                ],
                'rowOptions' => [
                    'id' => 'row{multiple_index_incoming_tabular_id}',
                    'data-row-index' => '{multiple_index_incoming_tabular_id}'
                ],
                'columns' => [
                    [
                        'name' => 'product_id',
                        'title' => Yii::t('app', 'Maxsulot'),
                        'type' => Select2::className(),
                        'options' => [
                            'class' => 'price',
                            'data' => DocItems::getProductList(),
                            'theme' => Select2::THEME_DEFAULT,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Maxsulotni tanlang..'),
                            ],
                            'pluginOptions' => [
                                'allowclear' => true,
                            ]
                        ],
                        'headerOptions' => [
                            'style' => 'width: 350px;',
                        ]

                    ],
                    [
                        'name' => 'quantity',
                        'title' => Yii::t('app', 'Miqdori'),
                        'options' => ['class' => 'number quantityMoving'],
                    ],
                    [
                        'name' => 'measurement_id',
                        'title' => Yii::t('app', 'O`lchov'),
                        'type' => Select2::className(),
                        'options' => [
                            'class' => 'price',
                            'data' => DocItems::getMeasurementList(),
                            'theme' => Select2::THEME_DEFAULT,
                            'hideSearch' => true,
                            'options' => [
//                                    'placeholder' => 'Tanlang',
                            ],
                            'pluginOptions' => [
                                'allowclear' => true,
                            ],
                        ]
                    ],
                ],
            ]) ?>
        </div>
    </div>
    <br>
    <div>
        <?= Html::submitButton('<i class="glyphicon glyphicon-save"> </i>' . ' ' . Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton']) ?>
    </div>
</div>


<?php
$js = <<<JS
    // var date = new Date();
    // $('#doc-reg_date').val(((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '.' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '.' + date.getFullYear());
JS;
//$this->registerJs($js);
$js = <<<JS
    function CurrencyFormat(currency) {
        if (isNaN(currency)) {
            return 0;
        }
        return parseFloat(currency, 10).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString();
    }
    $('#incoming_tabular_id').on('afterInit', function (e, row, currentIndex) {
          calculateSum('#footer_quantity', '.quantityMoving');
          calculateSum('#footer_summa', '.tabularSum');
     });
     $('body').delegate('.quantityMoving','change',function(e){
         calculateSum('#footer_quantity', '.quantityMoving');
     });
     
      // function calculateSumma(className, secondClassName){
      //     $('body').delegate(className,'change',function(e){
      //     let quantityObj = $(this).parents('tr').find(secondClassName);
      //     let tabularSum = $(this).parents('tr').find('.tabularSum');
      //     if(quantityObj.length > 0){
      //        let qty = $(quantityObj[0]).val();
      //        if(qty && parseFloat(qty) > 0){
      //            $(tabularSum[0]).val(parseFloat(qty)*$(this).val());
      //        }
      //     }
      //     calculateSum('#footer_summa', '.tabularSum');
      //   });    
      // }      
      //       calculateSumma('input:hidden[class="incomingPrice"]', '.quantityMoving');
      //       calculateSumma('.quantityMoving','input:hidden[class="incomingPrice"]');
            $('body').delegate('.tabularSum','change',function(e){
                calculateSum('#footer_summa', '.tabularSum');
            });
            function calculateSum(id, className) {
                let rmParty = $('#incoming_tabular_id table tbody tr').find(className);
                let totalRMParty = 0;
                rmParty.each(function (key, item) {
                     if ($(this).val()) {
                        totalRMParty += parseFloat($(this).val());    
                     }
                });
                $(id).html(CurrencyFormat(totalRMParty.toFixed(0)));
            }
JS;
$this->registerJs($js);
$css = <<<CSS
      .js-input-plus {
            color: #090909!important; 
            border-color: #17dda6!important;
            background-color: #28d928!important;
      }       
      input{
            height: 28px!important;
      }
CSS;
$this->registerCss($css)
?>
