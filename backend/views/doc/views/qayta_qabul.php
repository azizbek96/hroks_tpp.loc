<?php

use common\models\Doc;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\PermissionHelper as P;
/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $modelItems common\models\DocItems */

/* @var $incoming */
/* @var $sold_quantity */
$sold_quantity = 0;
$incoming = 0;
$slug = $this->context->slug;
$this->title = $model->reg_date;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Xujjat №') . "(" . Doc::getDocTypeBySlug($this->context->slug) . ")",
    'url' => ['index', 'slug' => $slug]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
if ($slug == $model::DOC_TYPE_RETURN_LABEL) {
    $invoice_number_href = ['invoice_number' => $model->invoice_number];
}
?>

    <div class="doc-view">
        <p>
            <?php if ($model::STATUS_SAVED != $model->status): ?>
<!--                --><?php //if(P::can('doc/qayta_qabul/save-and-finish')):?>
                    <?= Html::a(Yii::t('app', 'Save And Finish'), ['save-and-finish', 'id' => $model->id, 'slug' => $slug],
                        [
                            'class' => 'btn btn-success',
                            'data' => [
                                'confirm' => Yii::t('app', 'Ishonchingiz komilmi?'),
                                'method' => 'post',
                            ],
                        ])
                    ?>
<!--                --><?php //endif;?>
                <?= Html::a(Yii::t('app', 'Update'), ['update', 'slug' => $slug, 'id' => $model->id, $invoice_number_href], ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id, 'slug' => $slug,], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            <?php endif; ?>
            <?= Html::a(Yii::t('app', 'Back'), ['index', 'slug' => $slug], ['class' => 'btn btn-sm btn-warning']) ?>
            <?php
            if ($model::STATUS_SAVED == $model->status) {
                echo Html::submitButton(Yii::t('app', 'Print'), ['class' => 'btn btn-sm btn-info', 'onclick' => 'window.print()']);
            }
            ?>
        </p>
        <div class="row">
            <div class="col-lg-12">
                <table class="document-table">
                    <thead>
                    <tr>
                        <th><b><?= Yii::t('app', 'Xujjat') ?> No:</b> <span><?php echo $model->doc_number ?></span>
                        </th>
                        <th><b><?= Yii::t('app', 'Qabul qilish sanasi:') ?></b> <span><?= $model->reg_date; ?></span>
                        </th>
                    </tr>
                    <tr>
                        <th><b><?= Yii::t('app', 'Agent:') ?></b> <span><?= $model->employee['fullname'] ?></span>
                        </th>
                        <th><b><?= Yii::t('app', 'Department:') ?></b> <span><?= $model->department['name'] ?></span>
                        </th>
                    </tr>
                    <tr>
                        <th><b><?= Yii::t('app', 'Qo\'shimcha ma\'lumot:') ?></b>
                            <span><?= $model->add_info ?></span></th>
                        <th><b><?= Yii::t('app', 'Topshirdi:') ?></b>
                            <span><?php $username = \common\models\User::findOne($model->created_by)['fullname'];
                                echo $username ?? $model->created_by; ?></span>
                        </th>
                    </tr>
                    </thead>
                </table>
                <h4 align="center"><?php echo Yii::t('app', 'Mahsulotlar ro`yhati') ?></h4>
                <table class="item-table">
                    <thead>
                    <tr class="item-tr">
                        <th>#</th>
                        <th><?= Yii::t('app', 'Product') ?></th>
                        <th><?= Yii::t('app', 'Dona') ?></th>
                        <th><?= Yii::t('app', 'Quantity') ?></th>
                        <th><?= Yii::t('app', 'Measurement') ?></th>
                        <th><?= Yii::t('app', 'Sold Price (UZS)') ?></th>
                    </tr>
                    </thead>
                    <tbody class="item-tbody">
                    <?php $i = 1; $quantity = 0; ?>
                    <?php foreach ($modelItems as $item): ?>

                        <tr class="item-tbody-tr">
                            <td><?= $i++ ?></td>
                            <td><?= $item->product->name ?></td>
                            <td><?= $item['karopka_quantity'] ?></td>
                            <td class="income-price"><span>
                                        <?php
                                        echo Doc::getNumberFormat($item->quantity);
                                        $quantity += $item['quantity'] * 1;
                                        ?>
                                    </span></td>
                            <td><?= !empty($item->measurement_id) ? Doc::getMeasurementLabel($item->measurement_id) : NULL ?></td>

                            <td class="price">
                                    <span>
                                        <?php
                                        echo Doc::getNumberFormat($item->sold_price);
                                        $sold_quantity += $item->sold_price * $item->quantity * 1;
                                        ?>
                                    </span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="footer-out">
                        <td colspan="3" class="margin-text"><b><?= Yii::t('app', 'Jami:') ?></b></td>
                        <td align="right"><b><?php echo $quantity ?></b></td>
                        <td colspan="1"></td>
                        <td class="text-right price-out"><b><?= Doc::getNumberFormat($sold_quantity) ?>&nbsp;UZS</b>
                        </td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
<?php
$css = <<< CSS
    .item-table {
        margin-top: 20px;
        font-family: Arial;
    }
    .item-table th {
        border: 1px solid gray;
    }
    .document-table th {
        border: 1px solid lightgrey;
        padding-left: 10px;
    }
    .item-tr th {
        padding-left: 5px;
        background-color: #75ED8B;
        border: 1px solid black;
    }
    .item-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid black;
        padding-right: 5px;
        color: #0a73bb;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    tr {
      height: 30px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
CSS;
$this->registerCss($css);
