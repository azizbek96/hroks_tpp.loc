<?php

use common\models\Doc;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $modelItem common\models\DocItems */

$slug = $this->context->slug;
$this->title = Yii::t('app', 'Create Doc');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Docs') . "(".Doc::getDocTypeBySlug($this->context->slug) . ")",
    'url' => ['index','slug' => $this->context->slug]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-create">
    <?php $form = ActiveForm::begin(); ?>

            <?= $this->render("form/_{$this->context->slug}", [
                'model' => $model,
                'modelItem' => $modelItem,
                'form' => $form,
            ]) ?>
            <?php ActiveForm::end(); ?>

</div>
