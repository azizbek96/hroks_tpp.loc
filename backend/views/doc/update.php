<?php

use common\models\Doc;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $modelItem common\models\DocItems */
/* @var $employeeId */
/* @var $modelItems */
$slug = $this->context->slug;

$this->title = Yii::t('app', 'Update Doc: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Docs'),
    'url' => ['index','slug' => $this->context->slug]];
$this->params['breadcrumbs'][] = [
    'label' => $model->id,
    'url' => ['view','slug' => $this->context->slug, 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="doc-update">
    <h1 ><?= Html::encode($this->title) ?></h1>
    <?php  $form = ActiveForm::begin();  ?>
    <?= $this->render("form/_{$this->context->slug}", [
        'model' => $model,
        'modelItem' => $modelItem,
        'form' => $form,
    ]) ?>
    <?php ActiveForm::end(); ?>
</div>
