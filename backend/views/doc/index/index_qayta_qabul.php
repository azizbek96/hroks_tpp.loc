<?php

use common\models\Customers;
use common\models\Doc;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DocSearch */
/* @var $searchModel common\models\Doc */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $slug */
$slug = Yii::$app->request->get('slug');
$this->title = Yii::t('app', Doc::getDocTypeBySlug($slug));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-index">

    <div class="text-right" style="display: inline-flex;float: right;">

        <label for="invoice_number"><?php echo Yii::t('app', 'Yuk xati raqami') ?>&nbsp;</label><input type="text" name="invoice_number" id="invoice_number" class="number form-control" style="width: 100px!important; display: inline">
        <span class="check-invoice btn btn-sm btn-success" style="display: block"><?php echo Yii::t('app', 'Qidirish') ?></span>
        <?= Html::a(Yii::t('app', 'Qidirish'), ['doc/create', 'slug' => $slug],
            ['class' => 'btn btn-sm btn-success off-display','id' => 'create-button']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>
            function ($model) {
                if ($model['status'] == Doc::STATUS_SAVED) {
                    return [
                        'class' => 'success'
                    ];
                }
                if ($model['status'] != Doc::STATUS_SAVED) {
                    return [
                        'class' => 'warning'
                    ];
                }
                return [
                    'class' =>''
                ];
            },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'doc_number'
            ],
            [
                'attribute' => 'invoice_id',
                'label' => Yii::t('app','Yuk xati raqami'),
            ],
//            [
//                'attribute' => 'customer_id',
//                'label' => Yii::t('app', 'Xamkor'),
//                'filter' =>Doc::getCustomerId(),
//                'value' => function ($mod) {
//                    return $mod->customer->name;
//                }
//            ],
            [
                'attribute' => 'department_id',
                'filter' =>  Doc::getDeparmentsId(Doc::RECIVING),
                'value' => function ($mod) {
                    return $mod->department->name;
                }
            ],
            'reg_date',
            'add_info:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:30px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('doc/qayta_qabul/view'),
                ],
                'buttons' => [
                    'view' => function ($url, $model) use ($slug) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [$url, 'slug' => $slug], [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-info view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>

<?php
$views = Url::to(['doc/qayta_qabul/ajax-invoice']);
$create = Url::to(['doc/create', 'slug' => $slug]);
$js = <<<JS
    $(document).ready(function (){
       $('.check-invoice').click(function (event){
           let hr = $('#create-button').attr('href');
           let inputValue = $('#invoice_number').val();
           if (inputValue !== ''){
                let hrf = '$create' + '?invoice_number=' + inputValue;
                $.ajax({
                    type: 'POST',
                    url: '$views',
                    data: {id:inputValue},
                    success: function(response){
                        if (response.status === 'true') {
                            window.location.href=hrf;
                        } else {
                            $('#invoice_number').focus();
                            $('#invoice_number').css({"box-shadow": "0px 0px 3px red", "border-color": "red"});
                        }
                    }
                });
                 
           }else {
               $('#invoice_number').focus();
               $('#invoice_number').css({"box-shadow": "0px 0px 3px red", "border-color": "red"});
           }
       }) 
    });
JS;
$this->registerJs($js);
