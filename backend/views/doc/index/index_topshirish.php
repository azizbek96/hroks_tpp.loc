<?php

use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use common\models\Doc;
use backend\models\UserRelDeparments;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider */
/* @var $searchModel */
$slug = Yii::$app->request->get('slug');
$this->title = Yii::t('app', Doc::getDocTypeBySlug($slug));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-index">

    <div class="text-right" >
        <?= Html::a(Yii::t('app', ''), ['doc/create', 'slug' => $slug], ['class' => 'btn btn-success glyphicon glyphicon-plus']) ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'tableOptions' => [
            'class' => 'table table-condensed table-bordered table-hover'
        ],
        'rowOptions' => function ($model) {
            return ($model->status == 3) ? ['class' => 'success'] : ['class' => 'warning'];
        },
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'attribute' => 'reg_date',
                'filter' => false,
                'value' => function ($model) {
                    return $model['reg_date'];
                },
                'group' => true,
                'hAlign' => true,
                'vAlign' => true
            ],
            [
                'attribute' => 'employee_id',
                'filter' => Doc::getEmployeeId(),
                'value' => function ($mod) {
                    return $mod->employee->fullname;
                },
                'group' => true,
                'hAlign' => true,
                'vAlign' => true
            ],
            [
                'attribute' => 'invoice_number',
                'label' => Yii::t('app','Invoice Number'),
//                'group' => true,
                'hAlign' => true,
                'vAlign' => true
            ],
            'doc_number',
            [
                'attribute' => 'department_id',
                'filter' => Doc::getDeparmentsId(Doc::OUTGOING),
                'value' => function ($mod) {
                    return $mod->department->name;
                }
            ],

            'add_info:ntext',
            //'type',
            //'status',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            [
                'class' => ActionColumn::class,
                'template' => '{view}',
                'contentOptions' => ['class' => 'no-print text-center', 'style' => 'width:50px;'],
                /*'visibleButtons' => [
                    'view' => Yii::$app->user->can('groups/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('groups/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('groups/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],*/
                'buttons' => [

                    'view' => function ($url, $model) use ($slug) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [$url, 'slug' => $slug], [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-info view-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },


                ],
            ],
        ],
    ]); ?>


</div>
