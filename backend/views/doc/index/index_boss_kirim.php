<?php

use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use common\models\Doc;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DocSearch */
/* @var $searchModel common\models\Doc */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $slug */
$slug = Yii::$app->request->get('slug');
$this->title = Yii::t('app', Doc::getDocTypeBySlug($slug));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doc-index">

    <div class="text-right">
        <?= Html::a('<b style="color: #cef1c5" class="glyphicon glyphicon-plus"></b>', ['doc/create', 'slug' => $slug], ['class' => 'btn btn-sm btn-success']) ?>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
//        'floatHeader'=>true,
//        'floatHeaderOptions'=>[
//            'top' => '51',
//            'debug'=>true,
//        ],
        'rowOptions' =>
            function ($model) {
//                if ($model['status'] == Doc::STATUS_SAVED) {
//                    return [
//                        'style' => 'background-color:#CEF1C5'
//                    ];
//                }
                if ($model['status'] != Doc::STATUS_SAVED) {
                    return [
                        'class' => 'warning'
                    ];
                }
                return [
                    'class' => ''
                ];
            },
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'attribute' => 'reg_date',
                'filter' => false,
                'value' => function ($model) {
                    return $model['reg_date'];
                },
                'group' => true,
                'vAlign' => GridView::ALIGN_CENTER,
                'hAlign' => GridView::ALIGN_CENTER
            ],
            [
                'attribute' => 'customer_id',
                'label' => Yii::t('app', 'Xamkor'),
                'filter' => Doc::getCustomerId(),
                'value' => function ($mod) {
                    return $mod->customer->name;
                },
                'group' => true,
                'vAlign' => GridView::ALIGN_CENTER,
                'hAlign' => GridView::ALIGN_CENTER
            ],
            [
                'attribute' => 'doc_number'
            ],

            [
                'attribute' => 'department_id',
                'filter' => Doc::getDeparmentsId(Doc::RECIVING),
                'value' => function ($mod) {
                    return $mod->department->name;
                }
            ],

            'add_info:ntext',

            [
                'class' => ActionColumn::class,
                'template' => '{view}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:30px;'],
                /*'visibleButtons' => [
                    'view' => Yii::$app->user->can('groups/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('groups/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('groups/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],*/
                'buttons' => [
                    'view' => function ($url, $model) use ($slug) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', [$url, 'slug' => 'boss_kirim'], [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-info view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                ],
            ],
        ],
    ]) ?>


</div>
