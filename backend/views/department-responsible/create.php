<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DepartmentResponsible */

$this->title = Yii::t('app', 'Create Department Responsible');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Department Responsibles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-responsible-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
