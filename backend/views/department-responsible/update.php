<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DepartmentResponsible */

$this->title = Yii::t('app', 'Update Department Responsible: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Department Responsibles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="department-responsible-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
