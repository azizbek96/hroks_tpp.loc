<?php

namespace backend\modules\admin\controllers;

use backend\controllers\BaseController;
use backend\modules\admin\models\AuthItem;
use backend\modules\admin\models\AuthItemSearch;
use yii\bootstrap\ActiveForm;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\web\Response;

class PermissionController extends BaseController
{
    /**
     * @return string
     */
    public function actionIndex(){
        $searchModel = new AuthItemSearch();
        $variable = new AuthItem();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $variable::CATEGORY_PERMISSION);
        $model = AuthItem::find()->where(['type' => $variable::CATEGORY_PERMISSION])->all();

        return $this->render('permission_index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionCreate(){

        $model = new AuthItem();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $children = $model->elements;
            if (!empty($children)) {
                $model->setAttributes([
                    'elements' => true,
                    'type' => $model::CATEGORY_PERMISSION,
                    'created_at' => strtotime(date('Y-m-d H:i:s')),
                    'updated_at' => strtotime(date('Y-m-d H:i:s'))
                ]);
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    if ($model->save()) {
                        foreach ($children as $item) {
                            $permission = new AuthItem();
                            $permission->setAttributes([
                                'name' =>  $model->name . '/' . $item['name'],
                                'description' => NULL,
                                'type' => $model::PERMISSION,
                                'created_at' => strtotime(date('Y-m-d H:i:s')),
                                'updated_at' => strtotime(date('Y-m-d H:i:s')),
                                'elements' => true
                            ]);
                            if ($permission->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->set('success', Yii::t('app', 'Permission saved!'));
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->set('danger', Yii::t('app', 'Error saving permission!'));
                    }
                } catch (Exception $e){
                    Yii::info('Error Permission Saved '.$e->getMessage(),'save');
                }
            } else {
                Yii::$app->session->set('danger', Yii::t('app', 'Items not included!'));
            }
            return $this->redirect(['index']);
        }

        $model->elements = ['index', 'create', 'update', 'delete', 'view'];
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('permission_create', [
                'model' => $model,
            ]);
        }
        return $this->render('permission_create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|string|Response
     */
    public function actionUpdate($id){

        $model = AuthItem::findOne(['name' => $id]);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $children = $model->elements;
            if (!empty($children)) {
                $model->setAttributes([
                    'elements' => true,
                    'type' => $model::CATEGORY_PERMISSION,
                    'created_at' => strtotime(date('Y-m-d H:i:s')),
                    'updated_at' => strtotime(date('Y-m-d H:i:s'))
                ]);

                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    if ($model->save()) {
                        foreach ($children as $item) {
                            $permission_name = $model->name . '/' . $item['name'];
                            $one = AuthItem::findOne(['name' => $permission_name]);
                            if (empty($one)) {
                                $permission = new AuthItem();
                                $permission->setAttributes([
                                    'name' => $permission_name,
                                    'description' => NULL,
                                    'type' => $model::PERMISSION,
                                    'elements' => true,
                                    'created_at' => strtotime(date('Y-m-d H:i:s')),
                                    'updated_at' => strtotime(date('Y-m-d H:i:s'))
                                ]);
                                if ($permission->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            } else {
                                $saved = true;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->set('success', Yii::t('app', 'Permission edited!'));
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->set('danger', Yii::t('app', 'Error editing permission!'));
                    }
                } catch (Exception $e){
                    Yii::info('Error Permission Update '.$e->getMessage(),'update');
                }
            } else {
                Yii::$app->session->set('danger', Yii::t('app', 'Items not included!'));
            }
            return $this->redirect(['index']);
        }

        $response = AuthItem::find()
            ->where(['like', 'name', $model->name.'/%', false])
            ->andWhere(['type' => '2'])
            ->asArray()->all();


        $elements = [];
        if (!empty($response)) {
            foreach ($response as $item) {
                if (strlen($model->name) == strrpos($item['name'], '/')) {
                    $item['name'] = strrev($item['name']);
                    $name = strstr($item['name'], '/', true);
                    array_push($elements, strrev($name));
                }
            }
            $model->elements = $elements;
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('permission_update', [
                'model' => $model,
            ]);
        }

        return $this->render('permission_update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $name
     * @return bool
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionRemoveOneAjax($name){
        $permission = AuthItem::findOne(['name' => (string)$name]);
        if (!empty($permission)) {
           if ($permission->delete()) {
               return true;
           } else {
               return false;
           }
        }
        return true;
    }

    /**
     * @param $id
     * @return array|ActiveRecord[]
     * @throws \Throwable
     */
    public function actionRemove($id){
        $model = AuthItem::findOne($id);
        $name = $model->name.'/%';
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response['status'] = 'false';
        $response['error'] = Yii::t('app', 'Error deleting permission!');
        $response['saved'] = Yii::t('app', 'Permission deleted!');
        $response['error_one'] = Yii::t('app', 'Deleted Error!');
        $response['saved_one'] = Yii::t('app', 'Deleted!');
        if (!empty($model) && $model->type == $model::CATEGORY_PERMISSION) {
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                if ($model->delete()) {
                    $saved = true;
                    $res = AuthItem::find()->where(['like', 'name', $name, false])->andWhere(['type' => '2'])->asArray()->all();
                    if (!empty($res)) {
                        foreach ($res as $item) {
                            if (strlen($name)-2 == strrpos($item['name'], '/')) {
                                AuthItem::deleteAll(['name' => $item['name']]);
                            }
                        }
                    }
                }
                if ($saved) {
                    $transaction->commit();
                    $response['status'] = 'true';
                } else {
                    $transaction->rollBack();
                    $response['status'] = 'false';
                }
            } catch (Exception $e){
                Yii::info('Error Permission Deleted'.$e->getMessage(),'delete');
            }
        }
        return $response;
    }
}
