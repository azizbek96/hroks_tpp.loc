<?php

namespace backend\modules\admin\controllers;


use backend\models\ChangeInfoForm;
use backend\models\ChangePasswordForm;
use backend\modules\admin\models\AuthAssignment;
use backend\controllers\BaseController;
use backend\models\User;
use common\models\UsersSearch;
use Exception;
use Vtiful\Kernel\Excel;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends BaseController
{

    public function actionValidate()
    {
        $model = new User();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionCreate()
    {
        $model = new User();

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $AuthAssignment = $data['User']['rule'];
            if ($model->load($data)) {
                $saved = false;
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $password = $data['User']['password_hash'];
                    if (!empty($data)) {
                        $model->setAttributes([
                            'auth_key' => uniqid('', true),
                            'password_hash' => Yii::$app->security->generatePasswordHash($password),
                            'password_reset_token' => uniqid(time(), true),
                            'status' => User::STATUS_ACTIVE,
                            'fullname' => $data['User']['fullname'],
                            'username' => $data['User']['username'],
                            'email' => $data['User']['email'],
                            'type' => 1
                        ]);

                        if ($model->save()) {
                            $user_id = $model->id;
                            foreach ($AuthAssignment as $items) {
                                $addRule = new AuthAssignment();
                                $addRule->setAttributes([
                                    'item_name' => $items,
                                    'user_id' => $user_id,
                                ]);
                                if ($addRule->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            }
                        } else {
                            $saved = false;
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }

                } catch (Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = $model->errors;//Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['index']);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $AuthAssignment = $data['User']['rule'];
            if ($model->load($data)) {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                try {
                    if ($model->save()) {
                        $user_id = $model->id;
                        AuthAssignment::deleteAll(['user_id' => $user_id]);
                        foreach ($AuthAssignment as $items) {
                            $addRule = new AuthAssignment();
                            $addRule->setAttributes([
                                'item_name' => $items,
                                'user_id' => $user_id,
                            ]);
                            if ($addRule->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = false;
        $model = $this->findModel($id);
        if ($model->id == 1){
            throw  new ForbiddenHttpException('O`chirish mumkin emas');
        }
        try {
            if (User::updateAll(['status' => $model::STATUS_DELETED], ['=', 'id', $id])) {
                $isDeleted = true;
            }
            if ($isDeleted) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (Exception $e) {
            Yii::info('Not saved' . $e, 'save');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if ($isDeleted) {
                $response['status'] = 0;
                $response['message'] = Yii::t('app', 'Deleted Successfully');
            }
            return $response;
        }
        if ($isDeleted) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted Successfully'));
            return $this->redirect(['index']);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
        return $this->redirect(['index']);
    }

    public function actionReset()
    {
        $modelChangeInfo = new  ChangeInfoForm();
        $modelChangePassword = new ChangePasswordForm();
        $user = User::findByUsername(Yii::$app->user->identity->username);
        $messagePassword = '';
        $messageInfo = '';
        if ($modelChangePassword->load(Yii::$app->request->post())) {
            if ($modelChangePassword->resetPassword($user)) {
                $messagePassword = Yii::t('app', 'Yangi parol o\'rnatildi');
            }
        }

        if ($modelChangeInfo->load(Yii::$app->request->post())) {

            $data = Yii::$app->request->post();
            if (!empty($data) && !empty($data['ChangeInfoForm'])) {
                if ($modelChangeInfo->resetInfo($user)) {
                    $messageInfo = Yii::t('app', 'Ma\'lumotlar muvaffaqiyatli o\'zgartirildi');
                }
            }
        }
        return $this->render('reset',
            [
                'user' => $user,
                'isAdmin' => self::isAdmin(),
                'message1' => $messagePassword,
                'message2' => $messageInfo,
                'modelChangeInfo' => $modelChangeInfo,
                'modelChangePassword' => $modelChangePassword
            ]);
    }

    public function actionExportExcel()
    {
        header('Content-Type: application/vnd.ms-excel');
        $filename = "user_" . date("d-m-Y-His") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        Excel::export([
            'models' => User::find()->select([
                'id',
            ])->all(),
            'columns' => [
                'id',
            ],
            'headers' => [
                'id' => 'Id',
            ],
            'autoSize' => true,
        ]);
    }

    /**
     * @param $id
     * @return User|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
