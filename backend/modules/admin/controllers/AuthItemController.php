<?php

namespace backend\modules\admin\controllers;

use backend\controllers\BaseController;
use backend\modules\admin\models\AuthItemChild;
use Yii;
use backend\modules\admin\models\AuthItem;
use backend\modules\admin\models\AuthItemSearch;
use yii\bootstrap\ActiveForm;
use yii\db\Exception;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * AuthItemController implements the CRUD actions for AuthItem model.
 */
class AuthItemController extends BaseController
{

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $searchModel::RULE);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();
        $model->type = $model::RULE;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }

        $permission_category = AuthItem::find()->where(['type' => $model::CATEGORY_PERMISSION])->orderBy(['name' => SORT_ASC])->all();

        if ($model->load(Yii::$app->request->post())) {
            $permission = Yii::$app->request->post('Items');
            if (!empty($permission)) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $model->elements = true;
                    $saved = false;
                    if ($model->save()){
                        $saved = true;
                        foreach ($permission as $item) {
                            $auth_item_child = new AuthItemChild();
                            $auth_item_child->setAttributes([
                               'parent' => $model->name,
                               'child' => $item
                            ]);
                            if ($auth_item_child->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->set('success', Yii::t('app', 'The rule is saved!'));
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->set('danger', Yii::t('app', 'Error saving the rule!'));
                    }
                } catch (Exception $e){
                    Yii::info('Error Rules Saved '.$e->getMessage(),'save');
                }
            } else {
                Yii::$app->session->set('danger', Yii::t('app', 'Permission not selected!'));
                return $this->render('create', [
                    'model' => $model,
                    'permission_category' => $permission_category,
                ]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'permission_category' => $permission_category,
        ]);
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = 'json';
            return ActiveForm::validate($model);
        }

        $permission_category = AuthItem::find()->where(['type' => $model::CATEGORY_PERMISSION])->orderBy(['name' => SORT_ASC])->all();

        $child = AuthItemChild::find()->where(['parent' => $model->name])->asArray()->all();
        $all_children = [0 => ''];
        if (!empty($all_children)) {
            foreach ($child as $item) {
                array_push($all_children, $item['child']);
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $parent = Yii::$app->request->post('name_old');
            $permission = Yii::$app->request->post('Items');
            if (!empty($permission)) {
                AuthItemChild::deleteAll(['parent' => $parent]);
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    $model->elements = true;
                    if ($model->save()){
                        $saved = true;
                        foreach ($permission as $item) {
                            $auth_item_child = new AuthItemChild();
                            $auth_item_child->setAttributes([
                                'child' => $item,
                                'parent' => $model->name
                            ]);
                            if ($auth_item_child->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->set('success', Yii::t('app', 'Rule edited!'));
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->set('danger', Yii::t('app', 'Error editing the rule!'));
                    }

                } catch (Exception $e){
                    Yii::info('Error Rules Saved '.$e->getMessage(),'save');
                }
            } else {
                Yii::$app->session->set('danger', Yii::t('app', 'Permission not selected!'));
                return $this->render('update', [
                    'model' => $model,
                    'permission_category' => $permission_category,
                    'all_children' => $all_children,
                ]);
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'permission_category' => $permission_category,
            'all_children' => $all_children,
        ]);
    }


    /**
     * @param $id
     * @return mixed
     * @throws \Throwable
     * @throws yii\db\StaleObjectException
     */
    public function actionRemove($id)
    {
        $model = $this->findModel($id);
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        $response['status'] = 'false';
//        $response['error'] = Yii::t('app', 'Error deleting rule!');
//        $response['saved'] = Yii::t('app', 'Rule Deleted!');
//        $response['error_one'] = Yii::t('app', 'Deleted Error!');
//        $response['saved_one'] = Yii::t('app', 'Deleted!');
        if ($model !== null) {
            AuthItemChild::deleteAll(['parent' => $id]);
            if ($model->delete()) {
                $response['status'] = 'true';
            }
        }
        return $this->redirect(['index']);
    }


    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
