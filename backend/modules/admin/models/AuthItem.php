<?php

namespace backend\modules\admin\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property int $type
 * @property string|null $description
 * @property string|null $rule_name
 * @property resource|null $data
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 * @property AuthItem[] $children
 * @property AuthItem[] $parents
 */
class AuthItem extends ActiveRecord
{

    public static function tableName()
    {
        return 'auth_item';
    }

    const RULE = 1;
    const PERMISSION = 2;
    const CATEGORY_PERMISSION = 3;

    public $elements;
    /**
     * {@inheritdoc}
     */
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type', 'elements'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthRule::className(), 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'rule_name' => Yii::t('app', 'Rule Name'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[AuthAssignments]].
     *
     * @return ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * Gets query for [[RuleName]].
     *
     * @return ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * Gets query for [[AuthItemChildren]].
     *
     * @return ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * Gets query for [[AuthItemChildren0]].
     *
     * @return ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * Gets query for [[Children]].
     *
     * @return ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getChildren()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
    }

    /**
     * Gets query for [[Parents]].
     *
     * @return ActiveQuery
     * @throws yii\base\InvalidConfigException
     */
    public function getParents()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
    }


    /**
     * @param $name
     * @return array|ActiveRecord[]
     */
    public static function getPermissionAll($name)
    {
        $id = $name . '/%';
        return AuthItem::find()->where(['like', 'name', $id, false])->andWhere(['type' => self::PERMISSION])->orderBy(['name' => SORT_DESC])->asArray()->all();
    }

    /**
     * @param $name
     * @return string
     */
    public static function getChildrenAll($name)
    {
        $response = AuthItemChild::find()->where(['parent' => $name])->orderBy(['parent' => SORT_DESC])->asArray()->all();
        $string = '';
        $i = 0;
        foreach ($response as $item) {
            if ($i % 4 == 0) {
                $string .= '<br>';
            }
            $i++;
            $string .= "<span class='badge-info badge' style='color: whitesmoke;  background-color:#37a1e9 ; margin: 1px 1px'>" . $item['child'] . "</span>";

        }
        return $string;
    }

    /**
     * @param $name
     * @return string
     */
    public static function getChildrenPermissionCategory($name)
    {
        $id = $name . '/%';
        $response = AuthItem::find()->where(['like', 'name', $id, false])->andWhere(['type' => self::PERMISSION])->orderBy(['name' => SORT_DESC])->asArray()->all();
        $string = '';
        foreach ($response as $item) {
            if (strlen($id) - 2 == strrpos($item['name'], '/')) {
                $string .= "<span class='badge outline-badge-secondary mb-1 mr-2'>" . $item['name'] . "</span>";
            }
        }
        return $string;
    }
    public static function getRules(){
        $data = AuthItem::find()->where(['type' => self::RULE])->andWhere(['!=','name' , 'super-admin'])->asArray()->all();

        return ArrayHelper::map($data,'name', 'name');
    }
}
