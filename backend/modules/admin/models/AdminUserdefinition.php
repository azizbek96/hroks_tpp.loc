<?php

namespace backend\modules\admin\models;

use backend\modules\cases\models\Bonus;
use backend\modules\cases\models\Tourists;
use backend\modules\directory\models\References;
use backend\modules\hotels\models\HotelContract;
use backend\modules\hotels\models\Hotels;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "admin_userdefinition".
 *
 * @property int $id
 * @property string|null $fio
 * @property string|null $created_date
 * @property string|null $end_date
 * @property int|null $status
 * @property string|null $description
 * @property string|null $phone
 * @property string|null $email
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $updated_at
 * @property int|null $bonus_percent_id
 *
 * @property References $bonusPercent
 * @property Bonus[] $bonuses
 * @property HotelContract[] $hotelContracts
 * @property HotelContract[] $hotelContracts0
 * @property HotelContract[] $hotelContracts1
 * @property Hotels[] $hotels
 * @property Tourists[] $tourists
 * @property Users[] $users
 */
class AdminUserdefinition extends \app\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_userdefinition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_date', 'end_date'], 'safe'],
            [['status', 'created_at', 'created_by', 'updated_by', 'updated_at', 'bonus_percent_id'], 'default', 'value' => null],
            [['status', 'created_at', 'created_by', 'updated_by', 'updated_at', 'bonus_percent_id'], 'integer'],
            [['fio'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 512],
            [['phone', 'email'], 'string', 'max' => 30],
            [['email'], 'unique'],
            [['fio'], 'unique'],
            [['bonus_percent_id'], 'exist', 'skipOnError' => true, 'targetClass' => References::className(), 'targetAttribute' => ['bonus_percent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fio' => Yii::t('app', 'Fio'),
            'created_date' => Yii::t('app', 'Created Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
            'description' => Yii::t('app', 'Description'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'bonus_percent_id' => Yii::t('app', 'Bonus Percent ID'),
        ];
    }

    /**
     * Gets query for [[BonusPercent]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getBonusPercent()
    {
        return $this->hasOne(References::className(), ['id' => 'bonus_percent_id']);
    }

    /**
     * Gets query for [[Bonuses]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonus::className(), ['user_definition_id' => 'id']);
    }

    /**
     * Gets query for [[HotelContracts]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getHotelContracts()
    {
        return $this->hasMany(HotelContract::className(), ['who_created' => 'id']);
    }

    /**
     * Gets query for [[HotelContracts0]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getHotelContracts0()
    {
        return $this->hasMany(HotelContract::className(), ['changed_date' => 'id']);
    }

    /**
     * Gets query for [[HotelContracts1]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHotelContracts1()
    {
        return $this->hasMany(HotelContract::className(), ['who_changed' => 'id']);
    }

    /**
     * Gets query for [[Hotels]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getHotels()
    {
        return $this->hasMany(Hotels::className(), ['who_create' => 'id']);
    }

    /**
     * Gets query for [[Tourists]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getTourists()
    {
        return $this->hasMany(Tourists::className(), ['who_create' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['user_info_id' => 'id']);
    }

    /**
     * @param $id
     * @return mixed
     * @throws yii\db\Exception
     */
    public static function getUserDefinition($id){
        $sql = "SELECT ud.id
                FROM admin_userdefinition ud
                LEFT JOIN users u ON ud.id = u.user_info_id
                WHERE u.id = %d";
        $sql = sprintf($sql, $id);
        $user_id = Yii::$app->db->createCommand($sql)->queryOne();
        return $user_id['id'];
    }

    /**
     * @return array
     */
    public static function getUserInfo(){
        return ArrayHelper::map(AdminUserdefinition::find()
            ->where(['status' => self::STATUS_ACTIVE])->all(), 'id', 'fio');
    }

    /**
     * @return array
     */
    public static function getUserBonus(){
        return ArrayHelper::map(AdminUserdefinition::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->where('bonus_percent_id is not null')->all(), 'id', 'fio');
    }

}
