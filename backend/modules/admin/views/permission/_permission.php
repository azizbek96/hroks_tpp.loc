<?php

use common\components\TabularInput\CustomMultipleInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="col-xxl-8 order-2 order-xxl-1">
    <div class="card card-custom card-stretch gutter-b">
        <div class="card-body pt-4 pb-1 pl-4 pr-4">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
            ]); ?>
            <div class="row">

                <div class="col-sm-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-6">
                    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-sm-12">
                    <?= $form->field($model, 'elements')->widget(CustomMultipleInput::className(), [
                        'removeButtonOptions' => ['class' => 'btn btn-danger btn-input remove-button-click'],
                        'addButtonOptions' => ['class' => 'btn btn-primary btn-input'],
                        'theme' => CustomMultipleInput::THEME_BS,
                        'id' => 'form',
                        'columns' => [
                            [
                                'name' => 'name',
                                'type' => 'textinput',
                                'title' => Yii::t('app', 'Name Items'),
                            ],
                            [
                                'name' => 'description',
                                'type' => 'textinput',
                                'title' => Yii::t('app', 'Description Items'),
                            ],
                        ]
                    ])->label(false); ?>
                </div>

                <div class="col-sm-12 mb-4">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-sm saveButton btn-save']) ?>
                </div>

            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
$url = \yii\helpers\Url::to(['permission/remove-one-ajax']);
$js = <<<JS
    var name = $('#authitem-name').val();
    jQuery('#w1').on('beforeDeleteRow',function(e, row,currentIndex) {
       let inputName = name + '/' + row.find(':input').val();
       $.ajax({
           url: '$url',
           data: {name: inputName},
           type: 'GET',
           success: function (res) {
               console.log(res);
           }
       });
    });
JS;

$this->registerJs($js, \yii\web\View::POS_READY);
