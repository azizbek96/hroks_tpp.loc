<?php

use backend\modules\admin\models\AuthItem;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Permission');
$this->params['breadcrumbs'][] = $this->title;
?>


<?php Modal::begin([
    'header' => '',
    'size' => 'modal-lg',
    'id' => 'exampleModalSizeLg'
]) ?>
    <div id="modal-body"></div>

<?php Modal::end() ?>

    <div class="col-12 layout-spacing mt-2">
        <div class="widget widget-chart-one">
            <div class="widget-heading">
                <?= Html::a(Yii::t('app', 'Create Permission'), ['create'], ['class' => 'btn btn-primary btn-sm button-modal-show', 'data-toggle' => "modal", 'data-target' => "#exampleModalSizeLg"]); ?>
            </div>
            <div class="widget-content">
                <div class="table-responsive">
                    <?php
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'tableOptions' => ['class' => 'table table-bordered'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'name',
                                'label' => Yii::t('app', 'Permission Category'),
                            ],
                            [
                                'attribute' => 'children',
                                'headerOptions' => ['style' => 'color: #1b55e2;'],
                                'label' => Yii::t('app', 'Permissions'),
                                'options' => ['style' => 'width: 50%'],
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return AuthItem::getChildrenPermissionCategory($model->name);
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {remove}',
                                'header' => Yii::t('app', 'Actions'),
                                'headerOptions' => ['class' => 'text-center', 'style' => 'color: #1b55e2;'],
                                'contentOptions' => ['style' => 'width: 70px!important'],
                                'buttons' => [
                                    'update' => function ($url) {
                                        return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, [
                                            'title' => Yii::t('app', 'Update'),
                                            'class' => "btn btn-sm btn-info button-modal-show",
                                            'data-toggle' => "modal", 'data-target' => "#exampleModalSizeLg"
                                        ]);
                                    },
                                    'remove' => function ($url) {
                                        return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                            'title' => Yii::t('app', 'Delete'),
                                            'class' => "btn btn-sm btn-danger delete-button-ajax",
                                        ]);
                                    },

                                ],
//                                    'visibleButtons' => [
//                                        'update' => function () {
//                                            return Yii::$app->user->can('permission/update');
//                                        },
//                                        'remove' => function () {
//                                            return Yii::$app->user->can('permission/delete');
//                                        },
//                                    ],
                            ],
                        ]
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
    $('.button-modal-show').click(function(e){
        e.preventDefault();
        let url = $(this).attr('href');
        $('.modal-body').load(url);
         setTimeout(function (){
             $('.modal-title').html('$this->title');
         }, 1000);
    });
JS;
$this->registerJs($js);
