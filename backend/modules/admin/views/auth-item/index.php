<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="text-right">
            <?= Html::a('<b class="glyphicon-plus glyphicon"></b>', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        </div>
        <br>
        <div class="widget-content table-responsive">
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'summary' => '',
                'tableOptions' => ['class' => 'table table-bordered'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'name',
                    [
                        'attribute' => 'children',
                        'label' => Yii::t('app', 'Children'),
                        'headerOptions' => ['style' => 'color: #1b55e2;'],
                        'format' => 'raw',
                        'contentOptions' => [
                                'style' => 'width:600px!important'
                        ],
                        'value' => function ($model) {
                            return $model::getChildrenAll($model->name);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update} {remove}',
                        'header' => Yii::t('app', 'Actions'),
                        'headerOptions' => ['class' => 'text-center', 'style' => 'color: #1b55e2;'],
                        'contentOptions' => ['style' => 'width: 70px!important'],
                        'buttons' => [
                            'update' => function ($url) {
                                return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, [
                                    'title' => Yii::t('app', 'Update'),
                                    'class' => "btn btn-sm btn-info",
                                ]);
                            },
                            'remove' => function ($url) {
                                return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'class' => "btn btn-sm btn-danger",
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Ishonchingiz komilmi?'),
                                        'method' => 'post',
                                    ],
                                ]);
                            },

                        ],
                        'visibleButtons' => [
                            'update' => function () {
                                return Yii::$app->user->can('auth-item/update');
                            },
                            'remove' => function ($model) {
                                if ($model->name =='super-admin' || $model->name == 'Tarqatuvchi'){
                                    return '';
                                }
                                return Yii::$app->user->can('auth-item/delete');
                            },
                        ],

                    ],
                ]
            ]);
            ?>

        </div>
    </div>
</div>
