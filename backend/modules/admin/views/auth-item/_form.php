<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\modules\admin\models\AuthItem */
/* @var $permission_category backend\modules\admin\models\AuthItem */
/* @var $all_children backend\modules\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
$i = 0;
$status = explode('/', Yii::$app->controller->route);
$update = $status[count($status) - 1];
$select = '';
$data = '0';
?>

<div class="col-12 layout-spacing">
    <div class="widget widget-chart-one">
        <div class="widget-content">
            <?php $form = ActiveForm::begin([
                'enableAjaxValidation' => true,
            ]); ?>
            <div class="row input-all">
                <div class="col-sm-6">
                    <input type="hidden" name="name_old" value="<?= $model->name; ?>">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-12"
                     style="border: 1px solid black; border-radius: 3px; box-shadow: 1px 1px 1px 1px #303730">
                    <div class="row"><br>
                        <div class="col-sm-12 mb-2">
                            <span class="select-all-category btn btn-sm btn-primary btn-rule select-all-true">
                                <?= Yii::t('app', 'Select all') ?>
                            </span>
                        </div>
                        <br> <br>
                        <?php foreach ($permission_category as $item):

                            $permission = $model::getPermissionAll($item->name);

                            if (!empty($permission)):
                                ?>
                                <div class="col-sm-6 col-md-3 mb-1">
                                    <div class="card card-outline category-all">
                                        <div class="card-header p-0 text-center">
                                            <span class="mb-0 btn btn-sm btn-info btn-block btn-rule select-all-category"><?= $item->name; ?></span>
                                        </div>
                                        <div class="card-body" style="border:1px solid grey;border-radius: 2px;padding-left: 3px;padding-top: 3px;">
                                            <?php foreach ($permission as $value): ?>
                                                <?php
                                                if (strlen($item->name) == strrpos($value['name'], '/')):
                                                    if ($update == 'update') {
                                                        if (array_search($value['name'], (array)$all_children) > 0) {
                                                            $data = '1';
                                                            $select = 'checked';
                                                        } else {
                                                            $data = '0';
                                                            $select = '';
                                                        }
                                                    }
                                                    ?>
                                                    <div class="custom-control custom-checkbox parent-item">
                                                        <input class="custom-control-input" <?= $select ?>
                                                               type="checkbox" data-var="<?= $data ?>"
                                                               name="<?= 'Items[]' . $i++ . '' ?>"
                                                               value="<?= $value['name'] ?>"
                                                               id="input">
                                                        <label class="custom-control-label select-item"><?= $value['name'] ?></label>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <br>
                    </div>
                    <br>
                </div>
                <div class="col-sm-12" style="padding-top: 20px!important;">
                    <label for="input"></label>
                    <?= Html::submitButton('<b class="glyphicon glyphicon-save"> </b>' . ' ' . Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-sm btn-save saveButton']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?php
$js = <<<JS
    // select one
    $('.select-item').click(function (){
        let input = $(this).parents('.parent-item').find('input');
        if (input.attr('data-var') === '1' && input.attr('checked') === 'checked') {
            input.removeAttr('checked');
            input.attr('data-var', '0');
        } else {
            input.attr('checked', true);
            input.attr('data-var', '1');
        }
    });
    // category select all
    $('.select-all-category').click(function (){
        var j = false;
        let input = $(this).parents('.category-all').find('input');
        input.map(function (index, item){
            if ($(item).attr('data-var') === '1' && $(item).attr('checked') === 'checked') {
                j = true;
                return false;
            }
        });
        input.map(function (index, item){
            if (j === true) {
               $(item).attr('checked', false);
               $(item).attr('data-var', '0');
            } else {
               $(item).attr('checked', true);
               $(item).attr('data-var', '1');
            }
        });
    });
    // select all
    $('body').delegate('.select-all-true', 'click', function (){
        let input = $('.input-all').find('input');
        input.map(function (index, item){
           $(item).attr('checked', true);
           $(item).attr('data-var', '1');
        });
        $(this).addClass('select-all-false');
        $(this).removeClass('select-all-true');
    });
    // delete all
    $('body').delegate('.select-all-false', 'click', function (){
        let input = $('.input-all').find('input');
        input.map(function (index, item){
           $(item).attr('checked', false);
           $(item).attr('data-var', '0');
        });
        $(this).addClass('select-all-true');
        $(this).removeClass('select-all-false');
    });
JS;
$this->registerJs($js);
?>
