<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItem */
/* @var $permission_category backend\modules\admin\models\AuthItem */

$this->title = Yii::t('app', 'Create Rule');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
    'permission_category' => $permission_category,
]) ?>

