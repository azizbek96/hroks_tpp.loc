<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItem */
/* @var $permission_category backend\modules\admin\models\AuthItem */
/* @var $all_children backend\modules\admin\models\AuthItem */

$this->title = Yii::t('app', 'Update Rule: {name}', ['name' => $model->name]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<?= $this->render('_form', [
    'model' => $model,
    'permission_category' => $permission_category,
    'all_children' => $all_children,
]); ?>
