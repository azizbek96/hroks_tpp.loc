<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php if (Yii::$app->user->can('user/create')): ?>
        <p class="pull-right no-print">
            <?= Html::a('<span class="glyphicon glyphicon-plus"></span>', ['create'],
                ['class' => 'btn btn-sm btn-success create-dialog', 'id' => 'buttonAjax']) ?>


        </p>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'user_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {

            if (User::actionSuperAdmin() && $model->id == 1) {
                return [];
            }
            if ($model->id == 1 && !User::actionSuperAdmin()){
                return ['class' => 'hide'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'username',
            'fullname',
            'email:email',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view} {delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width: 100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('user/view'),
                    'update' => function ($model) {
                        return Yii::$app->user->can('user/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function ($model) {
                        if ($model->id == Yii::$app->user->id) {
                            return '';
                        }

                        return Yii::$app->user->can('user/delete');
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-xs btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-xs btn-info view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger delete-dialog',
                            'data-form-id' => $model->id,
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<?= \common\widgets\ModalWindow\ModalWindow::widget([
    'model' => 'user',
    'crud_name' => 'user',
    'modal_id' => 'user-modal',
    'modal_header' => '<h3>' . Yii::t('app', 'User') . '</h3>',
    'active_from_class' => 'customAjaxForm',
    'update_button' => 'update-dialog',
    'create_button' => 'create-dialog',
    'view_button' => 'view-dialog',
    'delete_button' => 'delete-dialog',
    'modal_size' => 'modal-md',
    'grid_ajax' => 'user_pjax',
    'confirm_message' => Yii::t('app', 'Haqiqatan ham ushbu mahsulotni yo\'q qilmoqchimisiz?')
]); ?>
