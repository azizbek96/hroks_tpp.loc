<?php

use backend\modules\admin\models\AuthItem;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $assigment */
?>

<?php $form = ActiveForm::begin([
        'id' => $model->formName()
]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'fullname')->textInput(['maxlength' => true])->label(Yii::t('app', 'FIO')) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'email')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label(Yii::t('app', 'Login')) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'rule')->widget(\kartik\select2\Select2::class, [
            'data' => AuthItem::getRules(),
            'options' => [
                'placeholder' => Yii::t('app', 'Rollarni briktiring')
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?php
        if (empty($model->password_hash)) { ?>
            <?= $form->field($model, 'password_hash')->passwordInput()->label(Yii::t('app', 'Parol')) ?>
        <?php } ?>

    </div>
</div>


<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php
$css = <<<Css
    .select2-container--krajee .select2-selection--multiple .select2-selection__choice{
    color:black;
    background: #4cf39e;
    box-shadow: inset 0 0 5px #666;
    }
Css;
$this->registerCss($css);
?>
