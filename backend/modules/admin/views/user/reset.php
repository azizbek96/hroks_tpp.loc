<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
/* @var $message1 */
/* @var $user */
/* @var $message2  */
/* @var $modelChangeInfo  */
/* @var $modelChangePassword  */

use common\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = Yii::t('app','Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="row">
        <div class="col-lg-5">
            <h3><?=Yii::t('app','Change full name and login:')?></h3>
            <?php if($message2):?>
                <div class="alert alert-info"><?= $message2; ?></div>
            <?php endif;?>
            <?php $form = ActiveForm::begin(['id' => 'login-form-2']); ?>
            <?php if(User::isAdmin()):?>
            <?= $form->field($modelChangeInfo, 'fullname')->textInput(['value' => $user->fullname]) ?>
            <?php endif;?>
            <?= $form->field($modelChangeInfo, 'username')->textInput(['value' => $user->username]) ?>

            <?= $form->field($modelChangeInfo, 'email')->textInput(['value' => $user->email])->label(true) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app','Ozgartirish'), ['class' => 'btn btn-primary', 'name' => 'login-button-2']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <div class="col-lg-5  col-lg-offset-2">
            <h3><?=Yii::t('app','Change Password:')?></h3>
            <?php if($message1):?>
                <div class="alert alert-info"><?= $message1; ?></div>
            <?php endif;?>
            <?php $form = ActiveForm::begin(['id' => 'login-form-3']); ?>

            <?= $form->field($modelChangePassword, 'oldPassword')->passwordInput()->label(Yii::t('app', 'Eski parolni kiritng')) ?>

            <?= $form->field($modelChangePassword, 'newPassword')->passwordInput()->label(Yii::t('app', 'Yangi parolni kiriting')) ?>

            <?= $form->field($modelChangePassword, 'confirmPassword')->passwordInput()->label(Yii::t('app', 'Yangi parolni qayta kiriting')) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app','Ozgartirish'), ['class' => 'btn btn-primary', 'name' => 'login-button-3']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
