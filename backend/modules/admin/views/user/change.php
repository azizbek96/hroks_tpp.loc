<?php

use backend\models\AuthItem;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $assigment */
?>

<?php $form = ActiveForm::begin([
    // 'options' => ['data-pjax' => true, 'class'=> 'customAjaxForm'],
    // 'enableAjaxValidation' => true
]); ?>

<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label(Yii::t('app', 'Yangi login')) ?>

<!--        --><?//= $form->field($model, 'password_hash')->textInput(['maxlength' => true])->label(Yii::t('app', 'Parol')) ?>
        <?= $form->field($model, 'new_password')->textInput(['maxlength' => true])->label(Yii::t('app', 'Yangi Parol')) ?>
    </div>
</div>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>



