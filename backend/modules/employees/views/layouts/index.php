<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="icon" type="image/ico" href="<?= Url::base() . "/fruits.png" ?>">
        <?php $this->head() ?>
        <style>
            div.scrollmenu form {
                position: absolute;
                top: 5px;
                right: 5px;
            }

            div.scrollmenu {
                background-color: #333;
                white-space: nowrap;
            }

            div.scrollmenu a {
                display: inline-block;
                text-align: center;
                margin-top: 5px !important;
                margin-bottom: 5px !important;
                margin-left: 5px !important;
                width: 19% !important;
            }

            .scrollmenu {
                color: white;
            }

            @media screen and (max-width: 600px) {
                .a_width{
                    width: 60px!important;
                    /*border: 1px solid red!important;*/
                    margin-left: 10px;
                }
                .scrollmenu{
                    display: flex;
                    justify-content: flex-start;
                }
                .sozlar {
                    display: none;
                    font-size: 11px !important;
                }
                .chiqish{
                    display: none;
                }
                .pull{
                    margin-bottom: 10px!important;
                }
            }
            @media screen and (max-width: 700px) {
                .sozlar {
                    display: none;
                    font-size: 6px!important;
                }
                .chiqish{
                    display: none;
                    font-size: 6px!important;
                }
                .pull{
                    margin-bottom: 10px!important;
                }
            }
            @media screen and (max-width: 800px) {
                .sozlar {
                    font-size: 10px!important;
                }
                .chiqish{
                    display: none;
                    font-size: 8px!important;
                }
                .pull{
                    margin-bottom: 10px!important;
                }
            }
            @media screen and (max-width: 1024px) {
                .sozlar {
                    font-size: 20px !important;
                }
                .chiqish{
                    display: none;
                    font-size: 20px!important;
                }

            }



        </style>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrap">
        <header>
            <div class="scrollmenu text-center">
                <?php if ($_SERVER['REQUEST_URI'] === '/employees/default/index' || $_SERVER['REQUEST_URI'] === '/employees'): ?>
                    <?= Html::a('<span class="glyphicon glyphicon-home">&nbsp;</span><span class="sozlar">' . Yii::t('app', '') . '</span>', ['#'],
                        ['class' => 'btn btn-lg btn-primary a_width no-print',]) ?>
                <?php else: ?>
                    <?= Html::a('<span class="glyphicon glyphicon-home">&nbsp;</span><span class="sozlar">' . Yii::t('app', 'Orqaga') . '</span>', ['/employees'],
                        ['class' => 'btn btn-lg btn-primary a_width no-print',]) ?>
                <?php endif; ?>
                <?= Html::a('<span class="glyphicon glyphicon-briefcase">&nbsp;</span><span class="sozlar">' . Yii::t('app', 'Hisobot') . '</span>', ['/employees/shop/report'],
                    ['class' => 'btn btn-lg btn-primary a_width no-print',]) ?>

                <?= Html::a('<span class="glyphicon glyphicon-user">&nbsp;</span><span class="sozlar">' . Yii::t('app', 'Mijozlar') . '</span>', ['/employees/default/shop-index'],
                    ['class' => 'btn btn-lg btn-primary a_width no-print',]) ?> &nbsp; &nbsp; &nbsp;
                <?php
                if (Yii::$app->user->isGuest) {
                    echo Html::a('<span class="glyphicon glyphicon-user">&nbsp;</span><span class="sozlar">' . Yii::t('app', 'Logout') . '</span>', ['/site/login'],
                        ['class' => 'btn btn-lg btn-primary a_width no-print',]);
                } else {

                    echo Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            '<span class="glyphicon glyphicon-log-out">&nbsp;</span><span class="chiqish"> ' . Yii::t('app', 'Chiqish') .'('.Yii::$app->user->identity->username.')'. '</span>',
                            ['class' => 'btn btn-lg btn-primary logout']
                        )
                        . Html::endForm();
                }
                ?>
            </div>
        </header>


        <div class="container" style="padding-top: 10px!important;">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>



    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
    <style>
        div.container{
            margin: 0 auto;
        }
        .breadcrumb li a {
            background-color: #46a8e7;
            border: 0.1rem solid #1b91d0;
            border-radius: 2px;
            font-size: 14px!important;
            color: whitesmoke;
            text-decoration: none;
            padding: 2px 50px!important;
            font-family: Verdana,'Merriweather', 'Helvetica Neue', Arial, sans-serif;
        }
        .breadcrumb li a:hover{
            background-color: #f34b4b;
        }
        @media screen and (max-width: 420px) {
            .breadcrumb li a {
                background-color: #46a8e7;
                border: 0.1rem solid #1b91d0;
                border-radius: 2px;
                font-size: 12px!important;
                color: whitesmoke;
                text-decoration: none;
                padding: 1px 25px!important;
            }
            .breadcrumb li a:hover{
                background-color: #f34b4b;
            }
        }
        @media screen and (max-width: 530px) {
            .breadcrumb li a {
                background-color: #46a8e7;
                border: 0.1rem solid #1b91d0;
                border-radius: 2px;
                font-size: 12px!important;
                color: whitesmoke;
                text-decoration: none;
                padding: 1px 25px!important;
            }
            .breadcrumb li a:hover{
                background-color: #f34b4b;
            }
        }
    </style>


