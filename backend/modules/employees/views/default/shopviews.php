<?php

use common\models\Customers;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mijoz'), 'url' => ['default/shop-index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="customers-view">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['default/shop-update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary from-control']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id,], [
            'class' => 'btn btn-danger btn-sm from-control',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            [
                'attribute' => 'name',
            ],
            [
                'attribute' => 'region_id',
                'value' => function($mod){
                    return Customers::getRegionOne($mod->region_id)['name_'.Yii::$app->language];
                }
            ],
            [
                'attribute' => 'address',
            ],
            [
                'attribute' => 'fullname',
            ],
            [
                'attribute' => 'tel1',
            ],
            [
                'attribute' => 'tel2',
            ],
            [
                'attribute' => 'add_info',
            ],
            [
                'attribute' => 'type',
                'value' => function($mod){
                    if (Customers::CUSTOMERS_TYPE_CLIENT == $mod->type){
                        return  Yii::t('app', 'Mijoz');
                    }
                    return false;
                }
            ],

        ],
    ]) ?>

</div>
