<?php

use backend\models\Regions;
use common\models\Customers;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\JsExpression;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mijoz');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-responsive">
    <?php if (Yii::$app->user->can('default/shop-create')): ?>
    <p class="pull-right no-print">
        <?= Html::a('<span class="glyphicon glyphicon-plus">&nbsp;</span>'.Yii::t('app','Create'), ['shop-create'],
            ['class' => 'create-dialog btn crateMijoz btn-lg btn-success',]) ?>

    </p>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'customers_pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            'fullname',
            'name',
            [
                'attribute' => 'region_id',
                'format' => 'html',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'region_id',
                    'data' => Customers::getRegionsList(),
                    'size' => Select2::SIZE_SMALL,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Select region..........    '),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    ],
                ]),
                'value' => function ($model) {
                    return Regions::findOne($model->region_id)->name_uz;
                }
            ],
            'tel1',
//            [
//                'attribute' => 'type',
//                'format' => 'html',
//                'filter' => [
//                    Customers::CUSTOMERS_TYPE_CLIENT => Yii::t('app', 'Mijoz'),
//                ],
//                'value' => function ($model) {
//                    if ($model->type === Customers::CUSTOMERS_TYPE_CLIENT) {
//                        return '<b class="label label-success glyphicon-text-color btn-sm">' . Yii::t('app', 'Mijoz') . '</b>';
//                    }
//                    if ($model->type === Customers::CUSTOMERS_TYPE_SUPPLIER) {
//                        return '<b class="label-primary ">' . Yii::t('app', 'Ta`minolovchi') . '</b>';
//
//                    }
//                    return '';
//                }
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{shop-update} {shop-view} {shop-delete}',
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('customers/view'),
                    'update' => function($model) {
                        return Yii::$app->user->can('customers/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function($model) {
                        return Yii::$app->user->can('customers/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'shop-update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'update-dialog btn btn-lg btn-success mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'shop-view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'View'),
                            'class' => 'btn btn-lg btn-default view-dialog mr1',
                            'data-form-id' => $model->id,
                        ]);
                    },
                    'shop-delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-lg btn-danger delete-dialog',
                            'data' => [
                                'confirm' => Yii::t('app', 'Ishonchingiz komilmi?'),
                                'method' => 'post',
                            ],
                        ]);
                    },

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

<?php
$css = <<<CSS
/*    .crateMijoz  {*/
/*padding-left: 80px!important;*/
/*padding-right: 80px!important;*/
/*font-size: 28px;*/
/*    }*/
CSS;
$this->registerCss($css);
