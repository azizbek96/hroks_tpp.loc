<?php

use dosamigos\chartjs\ChartJs;
use yii\helpers\Html;

/** @var $params */
?>
    <div class="text-center">
       <table class="table-bordered table table-responsive">
           <thead>
                <tr>
                    <th><?php echo Yii::t('app', 'Mahsulot') ?></th>
                    <th><?php echo Yii::t('app', 'Narxi')."(UZS)" ?></th>
                </tr>
           </thead>
           <tbody>
           <?php foreach ($params as $item):?>
                <tr>
                    <td><?php echo $item['name'] ?></td>
                    <td><?php echo \backend\models\Report::getNumberFormat($item['price']) ?></td>
                </tr>
            <?php endforeach; ?>
           </tbody>
       </table>
    </div>
    <div class="row  justify-content-center">
        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-center">
            <?= Html::a('<span class="glyphicon glyphicon-usd">&nbsp;</span>' . Yii::t('app', 'Pull olish'), ['transaction/'],
                ['class' => 'btn btn-lg btn-info no-print pull']) ?>
            <!--                <button class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-home">&nbsp;</span>Qo'shish</button>-->
        </div>
        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-center">
            <?= Html::a('<span class="glyphicon glyphicon-minus-sign">&nbsp;</span>' . Yii::t('app', 'Mahsulot sotish'), ['shop/index'],
                ['class' => 'btn btn-lg btn-danger sotish no-print']) ?>
            <!--                <button class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-home">&nbsp;</span>Qo'shish</button>-->
        </div>
    </div> <br>
    <div class="row">
        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-center">
            <?= Html::a('<span class="glyphicon glyphicon-list-alt">&nbsp;</span>' . Yii::t('app', 'Qaytarilganlar'), ['shop/index-return'],
                ['class' => 'btn btn-xl btn-primary  no-print form-control']) ?>
        </div>
        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-6 text-center">
            <?= Html::a('<span class="glyphicon glyphicon glyphicon-retweet">&nbsp;</span>' . Yii::t('app', 'Qaytarib olish'), ['shop/return'],
                ['class' => 'btn btn-xl btn-primary  no-print form-control']) ?>
        </div>
    </div>


<?php

$css = <<<CSS
   @media (min-width: 600px) {
  .pull{
        /*padding-left: 80px!important;*/
        /*padding-right: 80px!important;*/
        width: 300px;
        font-size: 28px;
    }
.sotish{
        width: 300px;
        font-size: 28px;
    }  
   }
@media (max-width: 590px) {

 .pull{
        width: 150px;
        text-align: center;
        font-size: 13px;
    }
.sotish{
        width: 150px;
        text-align: center;
        font-size: 13px;
    }   
}
CSS;
$this->registerCss($css);
