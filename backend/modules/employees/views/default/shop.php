<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */

$this->title = Yii::t('app', 'Create Mijoz');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mijoz'), 'url' => ['shop-index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="clients-create">

        <?php $form = ActiveForm::begin([
            'options' => [
                'id' => $model->formName(),
                'data-pjax' => true,
                'class' => 'customAjaxForm']
        ]); ?>

        <div class="container"></div>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-sm-6">
                <?= $form->field($model, 'name')->label(Yii::t('app', 'Do\'kon nomi'))->textInput(['maxlength' => true,'required' => true]) ?>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6">
                <?= $form->field($model, 'fullname')->label(Yii::t('app', 'Do\'kon egasi'))->textInput(['maxlength' => true,'required' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6">
                <?= $form->field($model, 'region_id')->widget(\kartik\select2\Select2::className(), [
                    'data' => $model::getRegionsList(),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Tanlang ...'),
                        'required' => true
                    ],
                    'hideSearch' => false,
                    'pluginOptions' => [
                        'class' => 'form-control form-control-solid',
                        'allowClear' => true,
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    ],
                ])->label(Yii::t('app', 'Tumanni tanlang')) ?>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true,'required' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6">
                <?= $form->field($model, 'tel1')->widget(MaskedInput::className(),
                    ['mask' => '+\9\9\8\(99\) 999 99 99']
                ) ?>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6">
                <?= $form->field($model, 'tel2')->widget(MaskedInput::className(),
                    ['mask' => '+\9\9\8\(99\) 999 99 99']
                ) ?>
            </div>
        </div>

        <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

        <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_ACTIVE])->label(false) ?>

        <?= $form->field($model, 'type')->hiddenInput(['value' => $model::CUSTOMERS_TYPE_CLIENT])->label(false) ?>

        <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'save-button btn saveButton  btn-success form-control']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
<?php
$css = <<<CSS
     
CSS;
$this->registerCss($css);

