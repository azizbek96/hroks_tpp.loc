<?php


use common\models\Doc;
use kartik\select2\Select2;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Qaytarish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mijozlar ro`yhat'), 'url' => ['shop/return']];
$this->params['breadcrumbs'][] = $this->title;
/**
 * @var $id
 */
?>
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <label class="contragent"> <?php echo Yii::t('app', 'Topshirilgan sana') ?></label>
        <?php
        \yii\widgets\ActiveForm::begin([
//                'action' => \yii\helpers\Url::to(['shop/return-agent'])
        ])?>
        <input type="hidden" value="<?php echo $id ?>" name="contragent">
        <?php
        echo Select2::widget([
            'name' => 'date',
            'data' => Doc::getShopSellDate($id),
            'theme' => Select2::THEME_CLASSIC,
            'options' => [
                'placeholder' => Yii::t('app', 'Sanani tanlang'),
                'id' => 'contragent_id'
            ],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true,
            ]

        ]);
        ?>
        <br><br>
        <div class="form-group">
            <?= Html::submitButton('<i class="glyphicon-search glyphicon">&nbsp;</i>' . Yii::t('app', 'Qidirish'), ['class' => 'btn btn-primary saveButton button-tekshirish form-control']) ?>
        </div>
        <?php
        \yii\widgets\ActiveForm::end();
        ?>
    </div>
</div>

<?php

$js = <<<JS
    $('.button-tekshirish').on('click', function (e){
        let selectValue = $('#contragent_id').val();
        if(selectValue == ''){
             $('#contragent_id').focus();
            e.preventDefault();
        }        
    })
JS;
$this->registerJs($js);