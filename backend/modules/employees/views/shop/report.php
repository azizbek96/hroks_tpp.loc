<?php


/* @var $query */
/* @var $params */

use yii\helpers\Html;

$this->title = Yii::t('app', 'Maxsulotlar ro`yhati');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <!--                    <th>--><?php //echo Yii::t('app', 'Qarz miqdori') ?><!--</th>-->
                <th><?php echo Yii::t('app', 'Shu oyda to`langan summa') ?></th>
                <th><?php echo Yii::t('app', 'Balance') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <!--                    <td><b style="font-size: 16px; font-weight: bold">-->
                <?php //echo number_format($params['dept_month'],'0','.',' ') ?><!-- </b>SOM</td>-->
                <td>
                    <b style="font-size: 16px; font-weight: bold"><?php echo number_format($params['paid_month'], '0', '.', ' ') ?> </b>UZS
                </td>
                <td>
                    <b style="font-size: 16px; font-weight: bold"><?php echo number_format($params['balance'], '0', '.', ' ') ?> </b>UZS
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?= Html::a(Yii::t('app', 'Batafsil'), ['transaction/list-paid'], ['class' => 'btn btn-primary form-control']) ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row text-center">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <table class="table table-bordered table-hover">
            <thead>
            <th>#</th>
            <th><?= Yii::t('app', 'Product name') ?></th>
            <th><?= Yii::t('app', 'Quantity') ?></th>
            <th><?= Yii::t('app', 'Sold Price') ?></th>
            </thead>
            <tbody>
            <?php
            if (!empty($query)) { ?>
                <?php $i = 1;
            foreach ($query as $item) : ?>
            <tr>
                <td><?= $i++ ?></td>
                <td><?= $item['name'] ?></td>
                <td class="ibe_inventory">
                    <?= \common\models\Doc::getNumberFormat($item['ibe_inventory']) ?>
                    <input type="hidden" name="DocItems[<?= $i ?>][product_id]"
                           value="<?= $item['product_id'] ?>">
                    <input type="hidden" name="DocItems[<?= $i ?>][measurement_id]"
                           value="<?= $item['measurement_id'] ?>">
                    <input type="hidden" name="DocItems[<?= $i ?>][currency]" value="<?= $item['currency'] ?>">
                </td>
                <td>
                    <?= \common\models\Doc::getNumberFormat($item['sold_price']) ?>
                    <input type="hidden" name="DocItems[<?= $i ?>][sold_price]"
                           value="<?= $item['sold_price'] ?>">
                </td>
            </tr>
            <?php endforeach; ?>
            <?php } else { ?>
                <tr>
                    <td colspan="4" style="color: red; font-weight: bold"><?php echo Yii::t('app', 'Sizda maxsulot mavjud emas') ?></td>
                </tr>
          <?php } ?>
            </tbody>
        </table>
    </div>
</div>


