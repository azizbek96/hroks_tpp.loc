<?php

use kartik\date\DatePicker;
use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use backend\modules\employees\models\Sales;
use common\models\Customers;
use common\models\Doc;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DocSearch */
/* @var $searchModel common\models\Doc */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $slug */
$slug = $slug;
$this->title = Yii::t('app', 'Maxsulot Qaytarish');
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="doc-index table-responsive">

        <div class="text-right">
            <?= Html::a('<b class="glyphicon glyphicon-plus">&nbsp;</b>' . Yii::t('app', 'Mahsulot qaytarish'),
                ['return'],
                ['class' => 'btn btn-md btn-success form-control']) ?>
        </div>


        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'hover' => true,
            'rowOptions' =>
                function ($model) {
                    if ($model['status'] == Doc::STATUS_SAVED) {
                        return [
                            'class' => 'success'
                        ];
                    }
                    if ($model['status'] != Doc::STATUS_SAVED) {
                        return [
                            'class' => 'warning'
                        ];
                    }
                    return [
                        'class' => ''
                    ];
                },
            'columns' => [
                ['class' => SerialColumn::class],
                [
                    'attribute' => 'customer_id',
                    'filter' => Sales::getCustomer(),
                    'label' => Yii::t('app', 'Xamkor'),
                    'value' => function ($model) {
                        $m = Customers::find()->where(['id' => $model['customer_id']])->asArray()->one();
                        return !empty($m['name']) ? $m['name'] : '';
                    },
                    'group' => true,
                ],
                [
                    'attribute' => 'reg_date',
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'reg_date',
                        'options' => [
                            'autocomplete' => 'off',
                            'readonly' => true
                        ],
                        'language' => 'ru',
                        'pluginOptions' => [
                            'removeButton' => true,
                            'autoclose' => true,
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]),
                    'value' => function ($model) {
                        return $model['reg_date'];
                    }
                ],
                [
                    'class' => ActionColumn::class,
                    'header' => false,
                    'template' => '{update-return} {view-return} {delete}',
                    'contentOptions' => ['class' => 'no-print', 'style' => 'width:30px;'],
                    'visibleButtons' => [
                        'update-return' => function ($model) {
                            if ($model->status == Doc::STATUS_SAVED) {
                                return '';
                            }
                            return Yii::$app->user->can('shop/update');
                        },
                        'delete' => function ($model) {
                            if ($model->status == Doc::STATUS_SAVED) {
                                return '';
                            }
                            return Yii::$app->user->can('shop/delete');
                        }
                    ],
                    'buttons' => [
                        'view-return' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'View'),
                                'class' => 'btn  btn-info view-dialog',
                            ]);
                        },
                        'update-return' => function ($url, $model) use ($slug) {
                            return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url . '&slug=' . $slug, [
                                'title' => Yii::t('app', 'update'),
                                'class' => 'btn  btn-success',
                            ]);
                        },
                        'delete' => function ($url, $model) use ($slug) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url . '&slug=' . $slug, [
                                'title' => Yii::t('app', 'delete'),
                                'class' => 'btn  btn-danger',
                                'data-method' => 'POST',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]);
                        },
                    ],
                ],
            ],
        ]) ?>


    </div>
<?php
$css = <<<CSS
.Product-Sales{
padding-left: 30px!important;
padding-right: 30px!important;
font-size: 15px;
}
@media screen and (max-width: 767px){
    .table-responsive{
        border: 1px solid white;
    }
}
@media screen and (max-width: 600px){
    th:nth-child(4){
        display: revert;
    }
}
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important;
        text-align: center;
        font-size: 1em;
    }
    .panel-title .empty{
        font-size: 10px!important;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
