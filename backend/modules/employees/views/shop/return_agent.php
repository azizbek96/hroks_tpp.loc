<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data */
/* @var $model */
/* @var $query */
$this->title = Yii::t('app', 'Sotish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ro`yhat'), 'url' => ['shop/index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<?php Modal::begin([
    'header' => Yii::t('app', 'Mijoz qo\'shish'),
    'size' => 'modal-lg',
    'id' => 'modalContent',
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]) ?>
    <div id="modal"></div>

<?php Modal::end() ?>


<?php $js = <<<JS
    $('body').delegate('.modalButton','click',function(e){
      e.preventDefault();
      let url = $(this).attr('url');
      $('#modalContent').modal('show').find('#modal').load(url);
    }) 
JS;
$this->registerJS($js); ?>

    <div class="clients-create">

        <?php $form = ActiveForm::begin(['options' => ['class' => 'form']]); ?>
        <div class="row text-center">

            <div class="col-lg-6 col-md-6 col-sm-6">
                <?= $form->field($model, 'reg_date')->widget(DatePicker::classname(), [
                    'size' => DatePicker::SIZE_MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('app', 'Sana'),
                        'readonly' => true,
                        'required' => true,
                        'autocomplete' => 'off'
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'todayBtn' => true,
                        'format' => 'dd.mm.yyyy'
                    ]
                ])->label(Yii::t('app', 'Sana')) ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <?php
                if(!empty($query)){?>
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th align="cente"><?= Yii::t('app', 'Product name') ?></th>
                        <th><?= Yii::t('app', 'quantity') ?></th>
                        <th><?= Yii::t('app', 'sold_price') . "(UZS)" ?></th>
                        <th><?= Yii::t('app', 'Qaytarish miqdori') ?></th>
                        </thead>
                        <tbody>
                        <?php $i = 1;
                        foreach ($query as $item) : ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $item['name'] ?></td>
                                <td class="ibe_inventory">
                                    <?= number_format($item['quantity'],0, '.', '') ?>

                                    <input type="hidden" name="DocItems[<?= $i ?>][product_id]"
                                           value="<?= $item['product_id'] ?>">
                                    <input type="hidden" name="DocItems[<?= $i ?>][measurement_id]"
                                           value="<?= $item['measurement_id'] ?>">
                                </td>
                                <td>
                                    <?= number_format($item['sold_price'],0,'.', ' ') ?>
                                    <input type="hidden" name="DocItems[<?= $i ?>][sold_price]"
                                           value="<?= $item['sold_price'] ?>">
                                </td>
                                <td width="200">
                                    <div class="input-group">
                                    <span class="input-group-addon minus">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </span>
                                        <input type="number" name="DocItems[<?= $i ?>][amount]" value="0"
                                               class="form-control amount number">
                                        <span class="input-group-addon plus">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </span>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php } else {
                    echo "<b style='color: red; font-weight: bold; font-size:16px'>".Yii::t('app', 'Sizda mahsulot qolmadi')."</b>";
                } ?>
            </div>
        </div>
        <br>
        <div class="form-group">
            <?php
            if (!empty($query)) {
                echo Html::submitButton('<i class="glyphicon-save glyphicon">&nbsp;</i>' . Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton form-control',
                    'id' => 'sendButton'
                ]);
            }else{
                echo  Html::a('<span class="	glyphicon glyphicon-arrow-left">&nbsp;</span>' . Yii::t('app', 'Orqaga'),
                    ['index'], ['class' => 'btn btn-md btn-warning form-control']);
            }
            ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
<?php
$css = <<< CSS
      input{
            height: 28px!important;
      } 
      @media screen and (max-width: 430px) {
            .minus{
                display: none;
            }
            .plus{
                display: none;
            }
            table{
                font-size: 15px!important;
            }
            table > thead{
                font-size: 15px!important;  
            }
      }
      .input-group-addon{
      /*border: 1px solid red!important;*/
      background: #a6f1b1;
      }
      .input_group .input-group-addon:hover{
            background: #0BB7AF;
      }
      .s2-input-group .input-group-btn > .btn{
        position: inherit!important;
      }
    #sendButton{
        font-size: 18px!important;
        padding-left: 40px!important;
        padding-right: 40px!important;
        display: block;
        float:right;
    }
    .item-table th {
        border: 1px solid gray;
    }
    .document-table th {
        border: 1px solid lightgrey;
        padding-left: 10px;
    }
    .item-tr th {
        padding-left: 5px;
        background-color: #75ED8B!important;
        border: 1px solid black;
    }
    .item-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid black;
        padding-right: 5px;
        color: #0a73bb!important;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
    .background {
        background-color: orange!important;
    }
CSS;
$this->registerCss($css);
$js = <<<JS
    $('body').delegate('tbody tr', 'click', function (){
    let bool = $(this).hasClass('background');
        if (!bool){
            $('tr').removeClass('background');
            $(this).addClass('background');
        }
    });
    // var date = new Date();
    // $('#doc-reg_date').val(((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '.' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '.' + date.getFullYear());
    $('.minus').on('click',function (event){
    let td = $(this).parents('td').find('input');
    let val = td.val()*1;
    if (val > 0)
        {
            td.val(val-1);
        }else td.val('0');
    });
    $('.plus').on('click',function (event){
    let tr = $(this).parents('tr');
    let tdInput= tr.find('.amount');
    let maxNum = tr.find('.ibe_inventory').text();
    let val = tdInput.val()*1;
    if (val < maxNum){
        tdInput.val(val+1);
    }
    });
    $('.amount').on('keyup',function(event) {
      let val = $(this).val()*1;
      let tr = $(this).parents('tr');
      let maxNum = tr.find('.ibe_inventory').text()*1;
      if (val > maxNum){
          $(this).val(maxNum);
      }
    })
JS;
$this->registerJs($js);


