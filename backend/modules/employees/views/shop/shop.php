<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clients-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'data-pjax' => true,
            'id' => $model->formName(),
            'class' => 'customAjaxForm'
        ]
    ]); ?>

    <div class="container"></div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'required' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'fullname')->textInput(['maxlength' => true, 'required' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'region_id')->widget(\kartik\select2\Select2::className(), [
                'data' => $model::getRegionsList(),
                'options' => [
                    'placeholder' => Yii::t('app', 'Tanlang ...')
                ],
                'hideSearch' => true,
                'pluginOptions' => [
                    'class' => 'form-control form-control-solid',
                    'allowClear' => true,
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                ],
            ])->label(Yii::t('app', 'Tuman yoki shaxarni tanlang')) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'required' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'tel1')->widget(MaskedInput::className(),
                ['mask' => '+\9\9\8\(99\) 999 99 99']
            ) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'tel2')->widget(MaskedInput::className(),
                ['mask' => '+\9\9\8\(99\) 999 99 99']
            ) ?>
        </div>
    </div>

    <?= $form->field($model, 'add_info')->textarea(['rows' => 1]) ?>

    <?= $form->field($model, 'type')->hiddenInput([
        'value' => $model::CUSTOMERS_TYPE_CLIENT,
    ])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success form-control saveButton']) ?>
    </div>
    <span class="btn btn-warning form-control yopish"><?php echo Yii::t('app', 'Chiqish') ?></span>
    <br><br>
    <?php ActiveForm::end(); ?>

</div>

<?php

$js = <<<JS
    $('body').delegate('.yopish','click',function(e){
            $('.close').click();
    })
JS;
$this->registerJS($js);

?>
