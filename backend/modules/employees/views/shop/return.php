<?php
/** @var $data */

$this->title = Yii::t('app', 'Mijoz nomi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 col-xl-12 text-center">
        <h4 align="center"><?php echo Yii::t('app', 'Sizning mijozlaringiz ro\'yhati')?></h4>
        <?php
                foreach ($data as $item => $child) {
                    echo "<a href= ".\yii\helpers\Url::to(['shop/return-form', 'id' => $child['id']])." class='form-control btn-md btn-xl btn-primary'>".$child['name']."(".$child['fullname'].")"."</a><br>";
                }
        ?>
    </div>
</div>
<style>

</style>
