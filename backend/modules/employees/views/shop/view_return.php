<?php

use common\models\Doc;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Doc */
/* @var $modelItems common\models\DocItems */
/* @var $incoming */
/* @var $sold_quantity */
$sold_quantity = 0;
$incoming = 0;
$slug = Doc::DOC_TYPE_RETURN_AGENT_LABEL;
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('app', 'Xujjat №') . "(" . Doc::getDocTypeBySlug($slug) . ")",
    'url' => ['index-return', 'slug' => $slug]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
    <div class="doc-view">

        <p>
            <?= Html::a('<span class="	glyphicon glyphicon-arrow-left">&nbsp;</span>' . Yii::t('app', 'Orqaga'),
                ['index-return', 'slug' => $slug, 'id' => $model->id], ['class' => 'btn btn-md btn-warning']) ?>
            <?php if ($model::STATUS_SAVED != $model->status): ?>
                <?= Html::a(Yii::t('app', 'Save And Finish'), ['save-and-finish', 'id' => $model->id, 'slug' => $slug], ['class' => 'btn btn-md btn-success']) ?>
                <?= Html::a(Yii::t('app', 'Update'), ['update-return','slug' => $slug, 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        </p>
        <div class="row">
            <div class="col-lg-12">
                <table class="document-table">
                    <thead>
                    <tr>
                        <th><b><?= Yii::t('app', 'Xujjat') ?> №:</b> <span><?php echo $model->doc_number ?></span></th>
                        <th><b><?= Yii::t('app', 'Qabul qilish sanasi:') ?></b> <span><?= $model->reg_date ?></span>
                        </th>
                    </tr>
                    <tr>
                        <th><b><?= Yii::t('app', 'Mijoz:') ?></b> <span><?= $model->customer['name'] ?? null ?></span>
                        </th>
                        <th><?= Yii::t('app', 'Agent:') ?><span><?php $username = \common\models\User::findOne($model->created_by)['fullname'];
                                echo $username ?? $model->created_by; ?></span></th>
                    </tr>
                    <tr>
                        <th colspan="2"><b><?= Yii::t('app', 'Qo`shimcha ma`lumot:') ?></b>
                            <span><?= $model->add_info ?></span></th>
                    </tr>
                    </thead>
                </table>

                <table class="item-table">
                    <thead>
                    <tr class="item-tr">
                        <th>#</th>
                        <th><?= Yii::t('app', 'Product') ?></th>
                        <th><?= Yii::t('app', 'Qaydarilgan miqdor') ?></th>
                        <th><?= Yii::t('app', 'Measurement') ?></th>
                        <th><?= Yii::t('app', 'Sold Price') ?></th>
                        <th><?= Yii::t('app', 'Summa') ?></th>
                    </tr>
                    </thead>
                    <tbody class="item-tbody">
                    <?php $i = 1;
                    $quantity = 0;
                    $sold_quantity = 0 ?>
                    <?php foreach ($modelItems as $item): ?>

                        <tr class="item-tbody-tr">
                            <td><?= $i++ ?></td>
                            <td><?= $item->product->name ?></td>
                            <td class="income-price"><span>
                                        <?php
                                        echo number_format($item->quantity, 0, '.', ' ');
                                        $quantity += $item->quantity * 1;
                                        ?>
                                    </span></td>
                            <td><?= !empty($item->measurement['name']) ? $item->measurement['name'] : NULL ?></td>
                            <td><?= number_format($item->sold_price, 0, '.', ' '); ?></td>
                            <td class="price">
                                    <span>
                                        <?php
                                        echo number_format($item->sold_price * $item->quantity, 0, '.', ' ');
                                        $sold_quantity += $item->sold_price * 1 * $item->quantity;
                                        ?>
                                    </span>
                            </td>
                            <!--                           <td>-->

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <tr class="footer-out">
                        <td colspan="2" class="margin-text"><b><?= Yii::t('app', 'Jami:') ?></b></td>
                        <td align="right"><?php echo '<b>' . $quantity . '</b>' ?></td>
                        <td></td>
                        <td></td>
                        <td class="text-right income-out"><?= number_format($sold_quantity, '0', '.', ' ') ?> UZS</td>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
<?php
$css = <<< CSS
    .item-table {
        margin-top: 20px;
        font-family: Arial;
    }
    .income-out{
        font-weight: bold;
        font-size: 15px!important;
    }
    .item-table th {
        border: 1px solid gray;
    }
    .document-table th {
        border: 1px solid lightgrey;
        padding-left: 10px;
    }
    .item-tr th {
        padding-left: 5px;
        background-color: #75ED8B;
        border: 1px solid black;
    }
    .item-tbody-tr td {
        border: 1px solid black;
        padding-left: 5px;
    }
    .footer-out td {
        border: 1px solid black;
        padding-right: 5px;
        color: #0a73bb;
    }
    th span {
        color: gray;
        margin-left: 8px;
    }
    tr {
      height: 30px;
    }
    table {
      width: 100%;
    }
    hr {
        background-color: #0c5460;
        height: 2px;
    }
CSS;
$this->registerCss($css);
