<?php

use kartik\money\MaskMoney;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
/* @var $customer */
/* @var $saldo */
?>

<div class="transaction-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->formName()
    ]); ?>

    <?=
    $form->field($model, 'customer_id')->hiddenInput(['value' => $model->customer_id])->label(false) ?>
    <?php
    $label = Yii::t('app', 'Amount')."(".number_format($saldo,0,'.', ' ').") UZS";
    ?>
    <?php echo $form->field($model, 'reg_date')->widget(\kartik\date\DatePicker::class, [
        'options' => [
            'placeholder' => Yii::t('app', 'Sanani tanlang'),
            'readonly' => true
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]) ?>

    <?= $form->field($model, 'amount')->textInput(['type' => 'number', 'maxlength' => true, 'class' => 'number form-control', 'required' => true])
        ->label($label) ?>

    <?php echo $form->field($model, 'add_info')->textInput()->label(Yii::t('app', 'Qo\'shimcha ma\'lumot')) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton form-control']) ?>
    </div>

    <span class="btn btn-warning form-control yopish"><?php echo Yii::t('app', 'Chiqish') ?></span>
    <br><br>
    <?php ActiveForm::end(); ?>
</div>
<?php

$js =<<<JS
    $('body').delegate('.yopish','click',function(e){
            $('.close').click();
    })
JS;
  $this->registerJS($js);

?>
