<?php

use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use backend\models\Transaction;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ro`yhat'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        .table > tbody > tr > td {
            vertical-align: center !important;
        }

    </style>
    <div class="pricing-index">
        <?php
        Pjax::begin()
        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => true,
            'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Topshirilgan pullar ro`yhati')],
            'columns' => [
                ['class' => SerialColumn::class],


                [
                    'label' => Yii::t('app', 'Summa(UZS)'),
                    'attribute' => 'amount',
                    'filterInputOptions' => [
                        'class' => 'number form-control',
                        'type' => 'number'
                    ],
                    'pageSummary' =>true,
                    'value' => function ($model) {
                        return Transaction::getNumberFormat($model->amount*1);
                    }
                ],
                [
                    'attribute' => 'reg_date',
                    'filter' => false,
                    'label' => Yii::t('app', 'Pull to`langan sana'),
                    'value' => function ($model) {
                        return date('d.m.Y', $model->reg_date);
                    }
                ],
                [
//                    'label' => Yii::t('app', 'Kiritilgan Vaqti'),
                    'attribute' => 'updated_at',
                    'filter' => false,
                    'value' => function ($model) {
                        return (time() - $model->updated_at < (60 * 60 * 24)) ? Yii::$app->formatter->format(date($model->updated_at), 'relativeTime') : date('d.m.Y H:i', $model->updated_at);
                    }
                ],
                [
                    'attribute' => 'add_info'
                ],
            ],
        ]) ?>

        <?php Pjax::end(); ?>

    </div>

<?php Modal::begin([
    'header' => 'O`zgartirish',
    'size' => 'modal-lg',
    'id' => 'modalContent'
]) ?>
    <div id="modal"></div>

<?php Modal::end() ?>

<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
        display: table-cell!important; 
        text-align: center;
        font-size: 1em;
    }
}
CSS;
$this->registerCss($css);
?>
<?php
$js = <<<JS
      $('.modalButton').click(function(e) {
              e.preventDefault();
              $('#modalContent').modal('show')
              .find('#modal')
              .load($(this).attr('href'));
      });
JS;
$this->registerJS($js);
?>
<?php
