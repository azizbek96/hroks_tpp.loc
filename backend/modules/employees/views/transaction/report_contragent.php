<?php

use kartik\grid\ActionColumn;
use yii\bootstrap\Modal;
use yii\grid\SerialColumn;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Mijozlar bilan aloqa');
$this->params['breadcrumbs'][] = $this->title;
/* @var $searchModel backend\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $this yii\web\View */
?>
<p>
    <a href="<?php echo \yii\helpers\Url::to(['transaction/batafsil']) ?>" class="btn btn-md btn-info form-control">
        <?php echo Yii::t('app', 'Batafsil') ?>
    </a>
</p>
<div class="table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'panel' => ['type' => 'default', 'heading' => Yii::t('app', '')],
        'tableOptions' => [
            'class' => 'table table-hover'
        ],
        'rowOptions' => function ($model) {
            if ($model['saldo'] < 0) {
                return [
                    'class' => 'warning'
                ];
            }
            if ($model['saldo'] > 0) {
                return [
                    'class' => 'success'
                ];
            }
            return [
                'class' => ''
            ];
        },
        'columns' => [
            ['class' => SerialColumn::class],

//            [
//                'class' => ActionColumn::class,
//                'template' => '{paid}',
//                'header' => false,
//                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
//                'visibleButtons' => [
//                    'paid' => function ($model) {
//                        return Yii::$app->user->can('transaction/create'); // && $model->status < $model::STATUS_SAVED;
//                    },
//                ],
//                'buttons' => [
//                    'paid' => function ($url, $model) {
//                        return Html::a('<span class="glyphicon glyphicon-minus"></span>', ['transaction/create','customer_id' => $model['contragent_id']], [
//                            'title' => Yii::t('app', 'Update'),
//                            'class' => 'modalButton btn btn-xs btn-success mr1',
//                        ]);
//                    },
//                ],
//            ],
            [
                'attribute' => 'contragent_id',
                'options' => [
                    'style' => 'font-weight:bold;font-size:15px!important'
                ],
                'format' => 'html',
                'filter' => \backend\models\Customers::getCurrentAgent(),
                'label' => Yii::t('app', 'Mijoz'),
                'value' => function ($model) {
                    if ($model['saldo'] == null) {
                        $model['saldo'] = $model['dept'];
                    }
                    return Html::a('<span>' . $model['contragent'] . '</span>', [
                        'transaction/create',
                        'customer_id' => $model['contragent_id'],
                        'saldo' => $model['saldo']
                    ], [
                        'title' => Yii::t('app', 'To\'lash'),
                        'class' => 'modalButton btn  btn-default form-control',
                    ]);
                },
                'group' => true,
            ],
            [
                'attribute' => 'saldo',
                'format' => 'html',
                'label' => Yii::t('app', 'Qoldiq(UZS)'),
                'value' => function ($model) {
                    if ($model['saldo'] == null) {
                        return '<b style="font-weight: bold; font-family: Verdana">' . \backend\models\Transaction::getNumberFormat($model['dept']) . '</b>';
                    }
                    return '<b style="font-weight: bold; font-family: Verdana">' . \backend\models\Transaction::getNumberFormat($model['saldo']) . '</b>';
                }
            ],
            [
                'attribute' => 'dept',
                'format' => 'html',
                'label' => Yii::t('app', 'Qarz miqdori(UZS)'),
                'value' => function ($model) {
                    if ($model['dept'] == null) {
                        return 0;
                    }
                    return '<b style="font-weight: bold; font-family: Verdana">' . \backend\models\Transaction::getNumberFormat($model['dept']) . '</b>';
                },
            ],
            [
                'attribute' => 'paid',
                'format' => 'html',
                'label' => Yii::t('app', 'To`langan summa'),
                'value' => function ($model) {
                    if ($model['paid'] == null) {
                        return 0;
                    }
                    return '<b style="font-weight: bold; font-family: Verdana">' . \backend\models\Transaction::getNumberFormat($model['paid']) . '</b>';
                },
            ],
        ],
    ]) ?>
</div>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important;
        text-align: center;
        font-size: 1.2em;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>
<?php Modal::begin([
    'header' => Yii::t('app', 'Mijozlardan pul qabul qilish'),
    'size' => 'modal-lg',
    'id' => 'modalContent',
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]) ?>
<div id="modal"></div>

<?php Modal::end() ?>


<?php
$js = <<<JS
      $('.modalButton').click(function(e) {
              e.preventDefault();
              $('#modalContent').modal('show')
              .find('#modal')
              .load($(this).attr('href'));
      });
JS;
$this->registerJS($js);
?>
