<?php

use backend\models\Transaction;
use backend\modules\employees\models\Sales;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Batafsil'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Modal::begin([
    'header' => '',
    'size' => 'modal-lg',
    'id' => 'modalContent',
    'clientOptions' => [
        'keyboard' => false,
        'backdrop' => 'static',
    ]
]) ?>
<div id="modal"></div>

<?php Modal::end() ?>


<?php $js = <<<JS
    $(document).ready(function(){
      $('.modalButton').click(function(e) {
      e.preventDefault();
      let url = $(this).attr('url');
      let customer_id = $('#customers_id').val(); 
      if (customer_id == '' || customer_id ==null){
          $('.info_warning').removeClass('hide').addClass('show');
      }else{
          $('.info_warning').addClass('hide').removeClass('show');
          let saldo_str = $('.customer_balance').html();
          let saldo = saldo_str.split(' ').join('');
          console.log(saldo)
          url = url+'?customer_id='+customer_id+"&saldo="+saldo;
          $('#modalContent').modal('show').find('#modal').load(url);
      }      
      });
    });
JS;
$this->registerJS($js); ?>
<div class="row">
    <div class="col-sm-12 text-center">
        <a href="<?php echo \yii\helpers\Url::to(['transaction/index']) ?>"
           class="btn btn-warning kichik"><?php echo '<i class="glyphicon glyphicon-arrow-left">&nbsp;</i>' . Yii::t('app', 'Orqaga') ?></a>
        <button class="btn btn-info" id='customers_id'><i class="glyphicon glyphicon-search">&nbsp;</i><?php echo Yii::t('app', 'Izlash') ?></button>
        <a href="<?php echo \yii\helpers\Url::to(['transaction/all']) ?>"
           class="btn btn-success kichik"><i class="glyphicon glyphicon-globe">&nbsp;</i><?php echo Yii::t('app', 'Umumiy') ?></a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2">
    </div>
    <div class="col-lg-8 col-md-8 col-sm-8">
        <?php
        echo Select2::widget([
            'name' => 'customers',
//            'theme' => Select2::THEME_MATERIAL,
            'data' => Sales::getCustomer(),
            'size' => Select2::SIZE_MEDIUM,
            'hideSearch' => true,
            'options' => [
                'placeholder' => Yii::t('app', 'Tanlang ...'),
                'class' => 'customers_id'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
            'addon' => [
                'append' => [
                    'content' => Html::button("<i class='glyphicon-usd glyphicon'></i>", [
                        'url' => \yii\helpers\Url::to(['transaction/create']),
                        'class' => 'btn-success btn btn-sm modalButton addon_btn',
                        'title' => 'To`lamoq',
                        'data-toggle' => 'tooltip'
                    ]),
                    'asButton' => true
                ]
            ]
        ]);
        ?>
        <p class="info_warning hide text-center"
           style="font-size: 14px;font-weight: bold; color: red"><?php echo Yii::t('app', 'Mijozni tanlang') ?></p>
        <div class="col-lg-2 col-md-2 col-sm-2"></div>

    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12">
        <table class="table table-bordered table-responsive">
            <thead style="background-color: #0a73bb; color: whitesmoke">
            <tr>
                <th><?php echo Yii::t('app', 'Qarz Summa') ?></th>
                <th><?php echo Yii::t('app', 'To`langan summa') ?></th>
                <th><?php echo Yii::t('app', 'Qoldiq') . '($)' ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><span style="color: black;font-weight: bold;font-size: 14px" class="customer_dept"></span></td>
                <td><span style="color: black;font-weight: bold;font-size: 14px" class="customer_paid"></span></td>
                <td><span style="color: black;font-weight: bold;font-size: 14px" class="customer_balance"></span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 text-center">
        <button class="btn btn-lg btn-success report_button"
                id="days"><?php echo Yii::t('app', 'Kunlik') ?></button>
        <button class="btn btn-lg btn-success report_button"
                id="month"><?php echo Yii::t('app', 'Oylik') ?></button>
        <!--            <button class="btn btn-lg btn-success report_button"-->
        <!--                    id="year">--><?php //echo Yii::t('app', 'Yillik') ?><!--</button>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-sm-12 col-md-12 days hide">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <h5><?php echo Yii::t('app', 'Qarz miqdori') ?></h5>
                    <table class="table-bordered table">
                        <thead>
                        <tr>
                            <th><?php echo Yii::t('app', 'Kun') ?></th>
                            <th><?php echo Yii::t('app', 'Summa') ?></th>
                        </tr>
                        </thead>
                        <tbody class="dept_summ">

                        </tbody>
                        <tfoot class="dept_footer">

                        </tfoot>
                    </table>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <h5><?php echo Yii::t('app', 'To`langan miqdori') ?></h5>
                    <table class="table-bordered table">
                        <thead>
                        <tr>
                            <th><?php echo Yii::t('app', 'Kun') ?></th>
                            <th><?php echo Yii::t('app', 'Summa') ?></th>
                        </tr>
                        </thead>
                        <tbody class="paid_days"></tbody>
                        <tfoot class="paid_footer">

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 month hide">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <h5><?php echo Yii::t('app', 'Qarz miqdori') ?></h5>
                    <table class="table-bordered table">
                        <thead>
                        <tr>
                            <th><?php echo Yii::t('app', 'Kun') ?></th>
                            <th><?php echo Yii::t('app', 'Summa') ?></th>
                        </tr>
                        </thead>
                        <tbody class="dept_month">

                        </tbody>
                        <tfoot class="dept_month_footer">

                        </tfoot>
                    </table>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <h5><?php echo Yii::t('app', 'To`langan miqdori') ?></h5>
                    <table class="table-bordered table">
                        <thead>
                        <tr>
                            <th><?php echo Yii::t('app', 'Kun') ?></th>
                            <th><?php echo Yii::t('app', 'Summa') ?></th>
                        </tr>
                        </thead>
                        <tbody class="paid_month"></tbody>
                        <tfoot class="paid_month_footer">

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 year hide">
        <div class="container">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-6"></div>
            </div>
        </div>
    </div>
</div>
<style>
    .activeBtn {
        background-color: #c475e7;
        color: #551cc4;
    }
</style>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: center!important;
    }
@media screen and (max-width: 480px){
     
    .kichik{
        width: 80px!important;
        height: 30px;  
        font-size: 11px!important;
        vertical-align: center;
        text-align: center;
        margin-bottom: 3px;
    }
   .addon_btn{
         /*width: 70px!important;*/
        /*height: 35px;  */
        font-size: 12px!important;
        vertical-align: center;
   }
   .report_button{
        width: 60px!important;
        height: 30px;
        padding: 3px;
        vertical-align: center;
        /*line-height: 30px!important;*/
        font-size: 13px!important;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>
<?php
$url = \yii\helpers\Url::to(['transaction/report']);
$js = <<<JS
    $('body').delegate('.report_button','click', function (e){
        e.preventDefault();
        let id = $(this).attr('id');
       
        if (id == 'days'){
            $('#days').addClass('activeBtn');
            $('#month').removeClass('activeBtn');
            $('#year').removeClass('activeBtn');
            $('.days').addClass('show');
            $('.month').removeClass('show').addClass('hide');
            $('.year').removeClass('show').addClass('hide');
        }
        if (id == 'month'){
            $('#month').addClass('activeBtn');
            $('#days').removeClass('activeBtn');
            $('#year').removeClass('activeBtn');
            $('.month').addClass('show');
            $('.days').removeClass('show').addClass('hide');
            $('.year').removeClass('show').addClass('hide');
        }
        if (id == 'year'){
            $('#year').addClass('activeBtn');
            $('#days').removeClass('activeBtn');
            $('#month').removeClass('activeBtn');
            $('.year').addClass('show');
            $('.month').removeClass('show').addClass('hide');
            $('.days').removeClass('show').addClass('hide');
        }
    })
JS;
$this->registerJS($js);


$js = <<<JS
         $('body').delegate('#customers_id','click' , function() {
               let id= $('.customers_id').val();
               
               if (!isEmpty(id)){
                    $('.info_warning').addClass('hide').removeClass('show');
                       $.ajax({
                      url:'$url',
                      data:{q:id},
                      type:'GET',
                          success: function(response){   
                              if(response.status){ 
                                  let qarz = 0;
                                  let tolandi = 0;
                                  if (response.dept.length > 0){
                                        $('.customer_dept').html(CurrencyFormat(response.dept[0]['inventory']));
                                        qarz = response.dept[0]['inventory'];
                                  }else{
                                        $('.customer_dept').html(0);
                                        qarz = 0;
                                  }
                                  if (response.paid.length > 0 && response.paid[0]['amount'] != null){
                                        $('.customer_paid').html(CurrencyFormat(response.paid[0]['amount']));
                                        tolandi = response.paid[0]['amount'];
                                  }else{
                                        $('.customer_paid').html(0);
                                        tolandi = 0;
                                  }
                                  let balance = qarz*1 + 1*tolandi; 
                                  $('.customer_balance').html(CurrencyFormat(balance))
                                  
                              }else{
                                  $('.customer_paid').html('')   
                                  $('.customer_dept').html('')
                                  $('.customer_balance').html('')
                                  $('.paid_days').html('');
                                  $('.dept_summ').html('');
                                  $('.paid_month').html('');
                                  $('.dept_month').html('');
                              }  
                              if (response.status){
                                  let dept_summ =''
                                  if (response.days_dept.length >0){
                                      response.days_dept.map(function (item){
                                      dept_summ +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                        
                                       '</tr>';
                                      })
                                  }else {
                                      dept_summ = '';
                                  }
                                  let paid = ''
                                  if (response.days_paid.length > 0){
                                      response.days_paid.map(function (item){
                                      paid +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                                                                     
                                       '</tr>';
                                      })
                                  }
                                   let dept_m = ''
                                  if (response.month_dept.length > 0){
                                      response.month_dept.map(function (item){
                                      dept_m +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                                                                        
                                       '</tr>';
                                      })
                                  }
                                  
                                  let paid_m = ''
                                  if (response.month_paid.length > 0){
                                      response.month_paid.map(function (item){
                                      paid_m +='' +
                                       '<tr>' +
                                               '<td>'+item.date+'</td>'+                                                    
                                               '<td>'+CurrencyFormat(item.sum)+'</td>'+                                                                                                        
                                       '</tr>';
                                      })
                                  }
                                 
                                  $('.paid_days').html(paid);
                                  $('.dept_summ').html(dept_summ);
                                  $('.paid_month').html(paid_m);
                                  $('.dept_month').html(dept_m);
                              }
                          }
                   });    
               } else {
                    $('.info_warning').removeClass('hide').addClass('show');
                      $('.customer_paid').html('')   
                      $('.customer_dept').html('')
                      $('.customer_balance').html('')
                      $('.paid_days').html('');
                      $('.dept_summ').html('');
                      $('.paid_month').html('');
                      $('.dept_month').html(''); 
               }
         })
         $('body').delegate('.select2-selection__clear','click',function(e){
             alert('ASS')
              $('.customer_paid').html('')   
              $('.customer_dept').html('')
              $('.customer_balance').html('')
              $('.paid_days').html('');
              $('.dept_summ').html('');
              $('.paid_month').html('');
              $('.dept_month').html('');         
         })
        function CurrencyFormat(currency) {
        if (isNaN(currency)) {
            return 0;
        }
        return parseFloat(currency, 10).toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, "$1 ").toString();
        }
        function isEmpty(object){
            for (let key in object){
                return false;
            }
        return true;
}

JS;
$this->registerJS($js);
?>
