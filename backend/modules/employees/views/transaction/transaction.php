<?php

use kartik\date\DatePicker;
use kartik\grid\ActionColumn;
use kartik\grid\SerialColumn;
use kartik\grid\GridView;
use backend\models\Transaction;
use backend\modules\employees\models\Sales;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Qabul qilish'), 'url' => ['batafsil']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pricing-index">

    <?php Pjax::begin(['id' => 'pricing_pjax']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterRowOptions' => ['class' => 'filters no-print'],
        'filterModel' => $searchModel,
        'hover' => true,
        'panel' => ['type' => 'default', 'heading' => Yii::t('app', 'Mijozlarning bergan pullari')],
        'showPageSummary' => true,
        'columns' => [
            ['class' => SerialColumn::class],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}',
                'header' => false,
                'contentOptions' => ['class' => 'no-print', 'style' => 'width:100px;'],
                'visibleButtons' => [
                    'update' => function ($model) {
                        return Yii::$app->user->can('transaction/update'); // && $model->status < $model::STATUS_SAVED;
                    },
                    'delete' => function ($model) {
                        return Yii::$app->user->can('transaction/delete'); // && $model->status < $model::STATUS_SAVED;
                    }
                ],
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Update'),
                            'class' => 'modalButton btn btn-xs btn-success mr1',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Delete'),
                            'class' => 'btn btn-xs btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ]);
                    },

                ],
            ],
            [
                'attribute' => 'customer_id',
                'label' => Yii::t('app', 'Mijoz'),
                'filter' => Sales::getCustomer(),
                'value' => function ($model) {
                    return $model->customer->name;
                },
                'group' => true,
                'hAlign' => GridView::ALIGN_CENTER,
                'vAlign' => GridView::ALIGN_CENTER,
                'groupFooter' => function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns' => [[1, 1]], // columns to merge in summary
                        'content' => [             // content to show in each summary cell
                            2 => 'Jami (' . $model->customer->name . ') :',
                            3 => GridView::F_SUM
                        ],
                        'contentFormats' => [      // content reformatting for each summary cell
                            3 => ['format' => 'number', 'decimals' => 1],
                        ],
                        'contentOptions' => [      // content html attributes for each summary cell
                            2 => ['style' => 'font-variant:small-caps'],
                            3 => ['style' => 'text-align:right']
                        ],
                        // html attributes for group summary row
                        'options' => ['class' => 'info table-success', 'style' => 'font-weight:bold;']
                    ];
                }
            ],
            [
                'label' => Yii::t('app', 'Summa'),
                'attribute' => 'amount',
                'filterInputOptions' => [
                    'class' => 'number form-control',
                    'type' => 'number'
                ],
                'value' => function ($model) {
                    return $model->amount;
                },
                'hAlign' => 'right',
                'format' => ['decimal', 0],
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM
            ],
            [
                'attribute' => 'reg_date',
                'filter' => false,
                'options' => [
                        'style' => 'width:100px!important'
                ],
                'label' => Yii::t('app', 'Sana'),
                'value' => function ($model) {
                    return date('d.m.Y', $model->reg_date);
                }
            ],
            [
                'attribute' => 'add_info',
                'filter' => false,
                'label' => Yii::t('app', 'Izoh'),
                'value' => function ($model) {
                    return $model['add_info'];
                }
            ],
        ],
    ]) ?>

    <?php Pjax::end(); ?>

</div>
<?php Modal::begin([
    'header' => Yii::t('app', 'O\'zgartirish'),
    'size' => 'modal-lg',
    'id' => 'modalContent',
    'clientOptions' => [
    'keyboard' => false,
    'backdrop' => 'static',
]
]) ?>
<div id="modal"></div>

<?php Modal::end() ?>


<?php
$js = <<<JS
      $('.modalButton').click(function(e) {
              e.preventDefault();
              $('#modalContent').modal('show')
              .find('#modal')
              .load($(this).attr('href'));
      });
JS;
$this->registerJS($js);
?>
<?php
$css = <<<CSS
    .table > tbody > tr> td{
        vertical-align: inherit!important;
    }
@media screen and (max-width: 480px){
    .kv-table-wrap th, .kv-table-wrap td {
         display: table-cell!important;
        text-align: center;
        font-size: 1.2em;
    }
    .panel-title{
        font-size: 10px!important;
    }
    table{
        font-size: 10px!important;
    }
}
CSS;
$this->registerCss($css);
?>
