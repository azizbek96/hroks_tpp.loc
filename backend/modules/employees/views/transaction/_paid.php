<?php


use backend\modules\employees\models\Sales;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
/* @var $customer */
?>

<div class="transaction-form">

    <?php $form = ActiveForm::begin([
            'id' => $model->formName()
    ]); ?>

    <?=
    $form->field($model, 'customer_id')->widget(Select2::class, [
        'data' => Sales::getCustomer(),
        'size' => Select2::SIZE_MEDIUM,
        'hideSearch' => true,
        'options' => [
            'placeholder' => Yii::t('app', 'Mijozni tanlang'),
            'id' => 'customers_id'
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?php echo $form->field($model, 'reg_date')->widget(\kartik\date\DatePicker::class, [
        'options' => [
            'placeholder' => 'Enter birth date ...',
            'readonly' => true
        ],
        'pluginOptions' => [
            'autoclose' => true,
            'todayHighlight' => true,
            'todayBtn' => true,
            'format' => 'dd.mm.yyyy'
        ]
    ]) ?>

    <?= $form->field($model, 'amount')->textInput(['type' => 'number','maxlength' => true, 'class' => 'number form-control']) ?>

    <?php echo $form->field($model, 'add_info')->textInput()->label(Yii::t('app', 'Qo\'shimcha ma\'lumot')) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success saveButton form-control']) ?>
    </div>
    <span class="btn btn-warning form-control yopish"><?php echo Yii::t('app', 'Chiqish') ?></span>
    <br><br>
    <?php ActiveForm::end(); ?>

</div>
<?php

$js =<<<JS
    $('body').delegate('.yopish','click',function(e){
            $('.close').click();
    })
JS;
$this->registerJS($js);

?>
