<?php


namespace backend\modules\employees\models;

use backend\models\UserRelEmployee;
use common\models\Doc;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DocSearch represents the model behind the search form of `common\models\Doc`.
 */
class DocReturnSearch extends Doc
{
    public $contragent;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['customer_id', 'doc_number', 'employee_id', 'department_id', 'reg_date', 'add_info', 'contragent'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param $slug
     * @return ActiveDataProvider
     */
    public function search($slug)
    {
        $params = Yii::$app->request->get();
        $employe_id = UserRelEmployee::findOne(['user_id' => Yii::$app->user->id])['employee_id'];

        $query = Doc::find()->where(['!=', 'status', self::STATUS_DELETED]);
        $query->andWhere(['type' => self::DOC_TYPE_RETURN_AGENT]);
        $query->andWhere(['created_by' => Yii::$app->user->id]);
        $query->andWhere(['employee_id' => $employe_id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                    'reg_date' => SORT_ASC,
                    'customer_id' => SORT_ASC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'doc_number' => $this->doc_number,
            'customer_id' => $this->customer_id,
            'employee_id' => $this->employee_id,
            'department_id' => $this->department_id,
            'reg_date' => $this->reg_date,
            'type' => $this->type,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'add_info', $this->add_info]);

        return $dataProvider;
    }
}
