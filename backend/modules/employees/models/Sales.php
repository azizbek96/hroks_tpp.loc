<?php

namespace backend\modules\employees\models;

use backend\controllers\BaseController;
use common\models\BaseModel;
use common\models\Customers;
use common\models\Doc;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Sales extends BaseModel
{
    public static function getIndex($employeeId)
    {
        $sql = "select d.id as doc_id,
                       di.id as doc_item_id,
                       p.name,
                       di.quantity as di_quantity,
                       p.id as product_id,
                       di.sold_price,
                       ibe.inventory as ibe_inventory,        
                       d.employee_id,
                       ibe.measurement_id,
                       di.currency 
                from item_balance_employee ibe
                left join products p on ibe.product_id = p.id    
                left join doc d on ibe.doc_id = d.id
                left join doc_items di on d.id = di.doc_id
                where ibe.id IN (select MAX(ibe2.id) from item_balance_employee ibe2 where ibe2.employee_id = %d GROUP BY ibe2.product_id, ibe2.measurement_id)
                AND di.product_id = ibe.product_id AND ibe.inventory > 0 GROUP BY ibe.product_id, ibe.measurement_id 
                order by p.name asc 
                ;";
        $sql = sprintf($sql, $employeeId);
        $query = Yii::$app->db->createCommand($sql)->queryAll();

        return $query;
    }

    public static function getCustomer()
    {
        $data = Customers::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andwhere(['status' => self::STATUS_ACTIVE])
            ->andwhere(['type' => self::CUSTOMERS_TYPE_CLIENT])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
        return ArrayHelper::map($data, 'id', 'name');
    }
    public static function getCustomerShop()
    {
        $data = Customers::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andwhere(['status' => self::STATUS_ACTIVE])
            ->andwhere(['type' => self::CUSTOMERS_TYPE_CLIENT])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
        return $data;
    }
    public static function getReturnSellCustomer($param)
    {
        $sql = "SELECT
                p.name,
                sum(di.quantity) as quantity,
                p.id as product_id,
                di.sold_price,
                di.measurement_id
            FROM doc d
                     left join doc_items di on d.id = di.doc_id
                     left join products p on di.product_id = p.id
                     left join measurements m on di.measurement_id = m.id
            WHERE d.customer_id = %d and d.reg_date in ('%s') AND d.status = %d AND d.type = %d GROUP BY p.id, di.sold_price, m.id

        ";
        $sql = sprintf($sql, $param['customer_id'], $param['reg_date'],self::STATUS_SAVED,Doc::DOC_TYPE_EMPLOYEE_SELL);
        $query = Yii::$app->db->createCommand($sql)->queryAll();
       return $query;
    }
}
