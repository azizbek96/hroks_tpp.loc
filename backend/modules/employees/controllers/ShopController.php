<?php


namespace backend\modules\employees\controllers;


use backend\controllers\BaseController;
use backend\models\ItemBalanceEmployee;
use backend\models\Transaction;
use backend\modules\employees\models\DocReturnSearch;
use backend\modules\employees\models\Sales;
use backend\modules\employees\models\SearchDoc;
use common\models\Customers;
use common\models\Doc;
use common\models\DocItems;
use Yii;
use yii\db\Query;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ShopController extends BaseController
{
    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $slug = Doc::DOC_TYPE_EMPLOYEE_SELL_LABEL;
        $searchModel = new SearchDoc();
        $dataProvider = $searchModel->search($slug);
        $models = Doc::find()->where(['type' => Doc::DOC_TYPE_EMPLOYEE_SELL])
            ->andWhere(['!=', 'status', Doc::STATUS_DELETED])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();
        return $this->render("index", [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'models' => $models,
            'slug' => $slug
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $model = new Doc();
        $model->scenario = $model::SCENARIO_SHOP;
        $model->reg_date = date('d.m.Y');
        $model->getCurrentEmployee();
        $query = Sales::getIndex($model->currentEmployeeId);
        $slug = Doc::DOC_TYPE_EMPLOYEE_SELL;

        $modelItem = [new DocItems()];
        $getId = Doc::find()->select('id')->orderBy(['id' => SORT_DESC])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($model->load($post)) {

                $model->doc_number = 'D' . substr(Doc::getDocTypeBySlug(Doc::DOC_TYPE_EMPLOYEE_SELL_LABEL), 0, 1) . '_' . (!empty($getId->id) ? $getId->id + 1 : 1) . '-' . date('dm/Y');
                $date = $post['Doc']['reg_date'];

                $model->reg_date = $date;
                $model->type = Doc::DOC_TYPE_EMPLOYEE_SELL;
                $model->status = Doc::STATUS_ACTIVE;
                $model->amount =  $post['Doc']['amount'];
                if ($model->save()) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $saved = false;
                        $docId = $model->id;
                        if (!empty($post['DocItems'])) {
                            foreach ($post['DocItems'] as $docItem => $item) {

                                if ($item['amount'] > 0) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'quantity' => $item['amount'],
                                        'measurement_id' => $item['measurement_id'],
                                        'sold_price' => $item['sold_price'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                } else {
                                    continue;
                                }

                                if ($modelDocItem->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                            return $this->redirect(['view','id' => $model->id]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                        }


                    } catch (\Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }

            }
        }
        return $this->render('create',
            [
                'model' => $model,
                'query' => $query,
            ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = $model::SCENARIO_SHOP;
        $model->getCurrentEmployee();
        $query = Sales::getIndex($model->currentEmployeeId);
        $slug = Doc::DOC_TYPE_EMPLOYEE_SELL;
        if (!empty($model->docItems)) {
            $modelItem = $model->docItems;
        } else {
            $modelItem = [new DocItems()];
        }
        $getId = Doc::find()->select('id')->orderBy(['id' => SORT_DESC])->one();
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            if ($model->load($post)) {
                $model->doc_number = 'D' . substr(Doc::getDocTypeBySlug(Doc::DOC_TYPE_EMPLOYEE_SELL_LABEL), 0, 1) . '_' . (!empty($getId->id) ? $getId->id + 1 : 1) . '-' . date('dm/Y');
                $date = $post['Doc']['reg_date'];
                $model->reg_date = $date;
                $model->type = Doc::DOC_TYPE_EMPLOYEE_SELL;
                $model->status = Doc::STATUS_ACTIVE;
                $model->amount =  $post['Doc']['amount'];
                if ($model->save()) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $saved = false;
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($post['DocItems'])) {
                            foreach ($post['DocItems'] as $docItem => $item) {

                                if ($item['amount'] > 0) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'quantity' => $item['amount'],
                                        'measurement_id' => $item['measurement_id'],
                                        'sold_price' => $item['sold_price'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                } else {
                                    continue;
                                }

                                if ($modelDocItem->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                            return $this->redirect(['view','id' => $model->id]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', Yii::t('app', 'Not Saved'));
                        }


                    } catch (\Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }

            }
        }
        return $this->render('update',
            [
                'model' => $model,
                'modelItem' => $modelItem,
                'query' => $query,
            ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        $this->layout='index.php';
        $model = Doc::findOne($id);
        $modelItems = DocItems::find()->where(['doc_id' => $id])->all();
        return $this->render("view", [
            'model' => $model,
            'modelItems' => $modelItems,
//            'slug' => $this->slug
        ]);
    }

    /**
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionSaveAndFinish()
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $data = Yii::$app->request->get();
        $slug = $data['slug'];
        $id = $data['id'];
        $user_id = Yii::$app->user->id;

        if (empty($id)) {
            throw new \yii\web\ForbiddenHttpException('Parametr mavjud emas !');
        }
        $model = $this->findModel($id);
        $model->getCurrentEmployee();
        $model->status = $model::STATUS_SAVED;
        $department_id = $model->department_id;
        $customer_id = $model->customer_id;
        $employee_id = $model->currentEmployeeId;
        $current_paid = $model->amount;
        if ($current_paid < 0) {
            $current_paid *= (-1);
        }
        if (!empty($model->docItems)) {
            $models = $model->docItems;
        } else {
            $models = [new DocItems()];
        }
        if (!empty($models)) {
            if ($slug == Doc::DOC_TYPE_EMPLOYEE_SELL_LABEL) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $amount = 0;
                    foreach ($models as $row) {
                        $employee_item_balance = new ItemBalanceEmployee();
                        $employee_item_balance_result = ItemBalanceEmployee::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['employee_id' => $employee_id])
                            ->andWhere(['measurement_id' => $row['measurement_id']])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        if (!empty($employee_item_balance_result)) {
                            $inventory = 1 * $employee_item_balance_result['inventory'] - 1 * $row['quantity'];
                            if ($inventory < 0) {
                                Yii::$app->session->setFlash('error', Yii::t('app', '{product} dan {count} birlikda kop kiritildi', ['product' => $row['product_id'], 'count' => (-1) * $inventory]));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
                            }
                            $employee_item_balance->setAttributes([
                                'product_id' => $row['product_id'],
                                'amount' => -1 * $row['quantity'],
                                'inventory' => $inventory,
                                'doc_id' => $row['doc_id'],
                                'user_id' => $user_id,
                                'employee_id' => $employee_id,
                                'department_id' => $department_id,
                                'measurement_id' => $row['measurement_id'],
                                'customer_id' => $customer_id,
                            ]);
                            $employee_item_balance->status = $model::STATUS_ACTIVE;

                        } else {
                            $employee_item_balance->setAttributes([
                                'product_id' => $row['product_id'],
                                'amount' => -1 * $row['quantity'],
                                'inventory' => 1 * $row['quantity'],
                                'doc_id' => $id,
                                'user_id' => $user_id,
                                'employee_id' => $employee_id,
                                'department_id' => $department_id,
                                'measurement_id' => $row['measurement_id'],
                                'customer_id' => $customer_id,
                            ]);
                            $employee_item_balance->status = $model::STATUS_ACTIVE;
                        }
                        if ($employee_item_balance->save()) {
                            $amount += $row['quantity'] * $row['sold_price'];
                            $saved = true;
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $tran = Transaction::find()
                        ->where(['user_id' => $user_id, 'customer_id' => $customer_id, 'type' => $model::T_DEPT])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransaction = new Transaction();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] - $amount;
                        $employeeTransaction->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => $user_id,
                            'reg_date' => $model->created_at,
                            'amount' => (-1) * $amount,
                            'inventory' => $tranInventory,
                            'type' => $employeeTransaction::T_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    } else {
                        $employeeTransaction->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => $user_id,
                            'amount' => (-1) * $amount,
                            'inventory' => (-1) * $amount,
                            'reg_date' => $model->created_at,
                            'type' => $employeeTransaction::T_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    }
                    // documntdagi pulni sahranit qilish
                    if (empty($current_paid)){
                        $current_paid = 0;
                    }
                    $new_tran = new Transaction();
                    $trans = Transaction::find()
                        ->where(['user_id' => $user_id, 'status' => $model::STATUS_ACTIVE, 'customer_id' => $customer_id])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    if (!empty($trans)) {
                        $tranInventorys = $trans['inventory'] * 1 + $current_paid;
                        $new_tran->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => $user_id,
                            'amount' => $current_paid,
                            'add_info' => $model->add_info,
                            'inventory' => $tranInventorys,
                            'reg_date' => strtotime(date("d.m.Y")),
                            'type' => $model::T_PAID,
                            'status' => $model::STATUS_ACTIVE,
                        ]);
                    }else{
                        $new_tran->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => $user_id,
                            'amount' => $current_paid,
                            'add_info' => $model->add_info,
                            'inventory' => $current_paid,
                            'reg_date' => strtotime(date("d.m.Y")),
                            'type' => $model::T_PAID,
                            'status' => $model::STATUS_ACTIVE,
                        ]);
                    }

                    if ($saved && $model->save() && $employeeTransaction->save() && $new_tran->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app', 'Not saved'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
                    }
                } catch (\Exception $e) {
                    Yii::info('Error massage ' . $e->getMassage());
                }
            }
            if ($slug == Doc::DOC_TYPE_RETURN_AGENT_LABEL) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $amount = 0;
                    foreach ($models as $row) {
                        $employee_item_balance = new ItemBalanceEmployee();
                        $employee_item_balance_result = ItemBalanceEmployee::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['employee_id' => $employee_id])
                            ->andWhere(['measurement_id' => $row['measurement_id']])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        if (!empty($employee_item_balance_result)) {
                            $inventory = 1 * $employee_item_balance_result['inventory'] + 1 * $row['quantity'];
                            if ($inventory < 0) {
                                Yii::$app->session->setFlash('error',
                                    Yii::t('app', '{product} dan {count} birlikda kop kiritildi',
                                    ['product' => $row['product_id'], 'count' => (-1) * $inventory]));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
                            }
                            $employee_item_balance->setAttributes([
                                'product_id' => $row['product_id'],
                                'amount' => $row['quantity'],
                                'inventory' => $inventory,
                                'doc_id' => $row['doc_id'],
                                'user_id' => $user_id,
                                'employee_id' => $employee_id,
                                'department_id' => $department_id,
                                'measurement_id' => $row['measurement_id'],
                                'customer_id' => $customer_id,
                            ]);
                            $employee_item_balance->status = $model::STATUS_ACTIVE;

                        } else {
                            $employee_item_balance->setAttributes([
                                'product_id' => $row['product_id'],
                                'amount' => $row['quantity'],
                                'inventory' => 1 * $row['quantity'],
                                'doc_id' => $id,
                                'user_id' => $user_id,
                                'employee_id' => $employee_id,
                                'department_id' => $department_id,
                                'measurement_id' => $row['measurement_id'],
                                'customer_id' => $customer_id,
                            ]);
                            $employee_item_balance->status = $model::STATUS_ACTIVE;
                        }
                        if ($employee_item_balance->save()) {
                            $amount += $row['quantity'] * $row['sold_price'];
                            $saved = true;
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $tran = Transaction::find()
                        ->where(['user_id' => $user_id, 'customer_id' => $customer_id, 'type' => $model::T_DEPT])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransaction = new Transaction();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] + $amount;
                        $employeeTransaction->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => $user_id,
                            'reg_date' => $model->created_at,
                            'amount' => $amount,
                            'inventory' => $tranInventory,
                            'type' => $employeeTransaction::T_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    } else {
                        $employeeTransaction->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => $user_id,
                            'amount' => -1*$amount,
                            'inventory' => -1*$amount,
                            'reg_date' => $model->created_at,
                            'type' => $employeeTransaction::T_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    }
                    if ($saved && $model->save() && $employeeTransaction->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app', 'Not saved'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
                    }
                } catch (\Exception $e) {
                    Yii::info('Error massage ' . $e->getMassage());
                }
            }

        } else {
            Yii::$app->session->setFlash('error', 'Qiymat kelmadi!...');
        }
        return $this->redirect(['view', 'id' => $model->id, 'slug' => $slug]);
    }

    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = false;
        $model = $this->findModel($id);
        try {
            if(Doc::updateAll(['status' => $model::STATUS_DELETED], ['=', 'id', $id])){
                $isDeleted = true;
            }
            if($isDeleted){
                $transaction->commit();
            }else{
                $transaction->rollBack();
            }
        }catch (\Exception $e){
            Yii::info('Not saved' . $e, 'save');
        }
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if($isDeleted){
                $response['status'] = 0;
                $response['message'] = Yii::t('app','Deleted Successfully');
            }
            return $response;
        }
        if($isDeleted){
            Yii::$app->session->setFlash('success',Yii::t('app','Deleted Successfully'));
            return $this->redirect(['index']);
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
            return $this->redirect(['index']);
        }
    }

    protected function findModel($id)
    {
        if (($model = Doc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionReport(){
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $model = new Doc();
        $model->getCurrentEmployee();
        $query = Sales::getIndex($model->currentEmployeeId);
        $params = Transaction::getCurrentSaldo();
        return $this->render('report',[
                'query' => $query,
                'params' => $params
            ]);
    }
    public function actionShop()
    {
        $model = new Customers();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                try {
                    $model->user_id = Yii::$app->user->identity->getId();
                    if($model->save()){
                        $saved = true;
                    }else{
                        $saved = false;
                    }
                    if($saved) {
                        $transaction->commit();
                    }else{
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['create']);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('shop', [
                'model' => $model,
            ]);
        }
        return $this->render('shop', [
            'model' => $model,
        ]);
    }
    public function actionReturn(){
            $data = Sales::getCustomerShop();
        return $this->render('return',[
            'data' => $data
        ]);
    }
    public function actionReturnForm(){
        $id = Yii::$app->request->get('id');
        if(Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            $response['reg_date'] = implode("','",$data['date']);
            $response['customer_id'] = $id;
            return $this->redirect(['shop/return-agent', 'response' => $response]);
        }
        return $this->render('form/return_form',[
            'id' => $id
        ]);
    }

    public function actionReturnAgent()
    {
        $model = new Doc();
        $model->scenario = Doc::SCENARIO_RETURN_AGENT;
        $response = Yii::$app->request->get()['response'];
        $model->getCurrentEmployee();
        $model->reg_date = date('d.m.Y');
        $query = Sales::getReturnSellCustomer($response);
        if (Yii::$app->request->isPost){
            $data = Yii::$app->request->post();
            $model->doc_number = 'D' . substr(Doc::getDocTypeBySlug(Doc::DOC_TYPE_EMPLOYEE_SELL_LABEL), 0, 1) . '_' . (!empty($getId->id) ? $getId->id + 1 : 1) . '-' . date('dm/Y');
            $date = $data['Doc']['reg_date'];
            $model->reg_date = $date;
            $model->employee_id = $model->currentEmployeeId;
            $model->type = Doc::DOC_TYPE_RETURN_AGENT;
            $model->customer_id = $response['customer_id'];
            $model->status = Doc::STATUS_ACTIVE;
            $model->reg_date_save = $response['reg_date'];
            if ($model->load($data) && $model->save())
            {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $saved = false;
                    $docId = $model->id;
                    if (!empty($data['DocItems'])) {
                        foreach ($data['DocItems'] as $docItem => $item) {
                            if ($item['amount'] > 0) {
                                $modelDocItem = new DocItems();
                                $modelDocItem->setAttributes([
                                    'product_id' => $item['product_id'],
                                    'quantity' => $item['amount'],
                                    'measurement_id' => $item['measurement_id'],
                                    'sold_price' => $item['sold_price'],
                                    'currency' => $model::CURRENSY_SUM,
                                    'doc_id' => $docId,
                                ]);
                            } else {
                                continue;
                            }

                            if ($modelDocItem->save()) {
                                $saved = true;
                            } else {
                                $saved = false;
                                break;
                            }
                        }
                    }
                    if ($saved) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                        return $this->redirect(['view-return','id' => $model->id]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                    }


                } catch (\Exception $e) {
                    Yii::info('Error Document ' . $e->getMessage(), 'save');
                }
            }
        }
        return $this->render('return_agent',[
            'model' => $model,
            'query' => $query
        ]);
    }

    public function actionViewReturn($id)
    {
        $this->layout='index.php';
        $model = Doc::findOne($id);
        $modelItems = DocItems::find()->where(['doc_id' => $id])->all();
        return $this->render("view_return", [
            'model' => $model,
            'modelItems' => $modelItems,
            'slug' => Doc::DOC_TYPE_RETURN_AGENT_LABEL
        ]);
    }
    public function actionUpdateReturn($id)
    {
        $model = $this->findModel($id);
        $model->scenario = $model::SCENARIO_RETURN_AGENT;
        $response = [
            'reg_date' => $model->reg_date_save,
            'customer_id'=>$model->customer_id
        ];
        $model->getCurrentEmployee();
        $query=Sales::getReturnSellCustomer($response);
        $slug = Doc::DOC_TYPE_RETURN_AGENT_LABEL;
        if (!empty($model->docItems)) {
            $modelItem = $model->docItems;
        } else {
            $modelItem = [new DocItems()];
        }
        $getId = Doc::find()->select('id')->orderBy(['id' => SORT_DESC])->one();
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            if ($model->load($data)) {
                $model->doc_number = 'D' . substr(Doc::getDocTypeBySlug($slug), 0, 1) . '_' . (!empty($getId->id) ? $getId->id + 1 : 1) . '-' . date('dm/Y');
                $date = $data['Doc']['reg_date'];
                $model->reg_date = $date;
                $model->employee_id = $model->currentEmployeeId;
                $model->type = Doc::DOC_TYPE_RETURN_AGENT;
                $model->status = Doc::STATUS_ACTIVE;
                $model->reg_date_save = $response['reg_date'];

                if ($model->save()) {
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $saved = false;
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {

                                if ($item['amount'] > 0) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'quantity' => $item['amount'],
                                        'measurement_id' => $item['measurement_id'],
                                        'sold_price' => $item['sold_price'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                } else {
                                    continue;
                                }

                                if ($modelDocItem->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                            return $this->redirect(['view-return','id' => $model->id]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', Yii::t('app', 'Not Saved'));
                        }
                    } catch (\Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }
            }
        }
        return $this->render('update_return',
            [
                'model' => $model,
                'modelItem' => $modelItem,
                'query' => $query
            ]);
    }
    public function actionIndexReturn()
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $slug = Doc::DOC_TYPE_RETURN_AGENT_LABEL;
        $searchModel = new DocReturnSearch();
        $dataProvider = $searchModel->search($slug);
        $models = Doc::find()->where(['type' => Doc::DOC_TYPE_RETURN_AGENT])
            ->andWhere(['!=', 'status', Doc::STATUS_DELETED])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();
        return $this->render("index_return", [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'slug' => $slug
        ]);
    }

}
