<?php

namespace backend\modules\employees\controllers;

use backend\modules\employees\models\Sales;
use Yii;
use backend\models\Transaction;
use backend\models\TransactionSearch;
use backend\controllers\BaseController;
use yii\db\Exception;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class TransactionController extends BaseController
{


    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if ( $this->actionTarqatuvchi()){
            $searchModel = new TransactionSearch();
            $dataProvider = $searchModel->searchAgentContragent();
            return $this->render('report_contragent',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
    }
    public function actionBatafsil()
    {
        if ( $this->actionTarqatuvchi()){
            $searchModel = new TransactionSearch();
            $dataProvider = $searchModel->searchAgentContragent();
            return $this->render('index',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
        throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
    }
    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {

        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $user_id = Yii::$app->user->id;
        $customers_id = Yii::$app->request->get('customer_id');
        $saldo = Yii::$app->request->get('saldo');
        $customer = Sales::getCustomer();
        $model = new Transaction();
        $model->customer_id = $customers_id;
        $model->reg_date = date("d.m.Y");

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $customer_id = $customers_id;
            $amount = $data['Transaction']['amount'];
            $date = strtotime($data['Transaction']['reg_date']);
            if ($amount < 0) {
                $amount *= (-1);
            }
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $tran = Transaction::find()
                    ->where(['user_id' => $user_id, 'status' => $model::STATUS_ACTIVE, 'customer_id' => $customer_id])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()
                    ->one();
                if (!empty($tran)) {
                    $tranInventory = $tran['inventory'] * 1 + $amount;
                    $model->setAttributes([
                        'customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $tranInventory,
                        'reg_date' => $date,
                        'type' => $model::T_PAID,
                        'status' => $model::STATUS_ACTIVE,
                    ]);
                } else {
                    $model->setAttributes([
                        'customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $amount,
                        'reg_date' => $date,
                        'type' => $model::T_PAID,
                        'status' => $model::STATUS_ACTIVE,
                    ]);
                }

                if ($model->save()) {
                    $saved = true;
                }
                if ($saved) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                    return $this->redirect(['index']);
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                }
            } catch (Exception $exception) {
                Yii::info('Not saved' . $exception->getMessage(), 'save');
            }


        }
        return $this->renderAjax('create', [
            'model' => $model,
            'customer' => $customer,
            'saldo' => $saldo
        ]);
    }

    /**
     * Updates an existing Transaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException|ForbiddenHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $model = $this->findModel($id);
        $user_id = Yii::$app->user->id;
        $customer = Sales::getCustomer();
        $model->reg_date = date('d.m.Y', $model->reg_date);
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $customer_id = $data['Transaction']['customer_id'];
            $amount = $data['Transaction']['amount'];
            $date = strtotime($data['Transaction']['reg_date']);
            if ($amount < 0) {
                $amount *= (-1);
            }
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $tran = Transaction::find()
                    ->where(['user_id' => $user_id, 'customer_id' => $customer_id])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()
                    ->one();
                if (!empty($tran)) {
                    $tranInventory = ($tran['inventory'] - $tran['amount']) + $amount;
                    $model->setAttributes([
                        'customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $tranInventory,
                        'type' => $model::T_PAID,
                        'reg_date' => $date,
                        'status' => $model::STATUS_ACTIVE,
                    ]);
                } else {
                    $model->setAttributes([
                        'customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $amount,
                        'type' => $model::T_PAID,
                        'reg_date' => $date,
                        'status' => $model::STATUS_ACTIVE,
                    ]);
                }

                if ($model->save()) {
                    $saved = true;
                }
                if ($saved) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                    return $this->redirect(['all']);
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('danger',  Yii::t('app', 'Ma`lumot saqlanmadi'));
                }
            } catch (Exception $exception) {
                Yii::info('Not saved' . $exception->getMessage(), 'save');
            }


        }
        return $this->renderAjax('update', [
            'model' => $model,
            'customer' => $customer,
        ]);
    }

    /**
     * Deletes an existing Transaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException|ForbiddenHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        if (($model = $this->findModel($id)) && is_numeric($id)) {
            $model->status = $model::STATUS_DELETED;
            if ($model->save()) {
                return $this->redirect(['all']);
            }
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));

    }

    /**
     * @param $id
     * @return Transaction|null
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $d = Yii::$app->request->get('q');
        if (!empty($d)) {
            $data = Transaction::find()
                ->where([
                    'status' => Transaction::STATUS_ACTIVE,
                    'type' => Transaction::T_DEPT,
                    'customer_id' => $d
                ])->orderBy(['id' => SORT_DESC])->asArray()->all();
            $paid = Transaction::find()
                ->select('sum(amount) as amount')
                ->where([
                    'status' => Transaction::STATUS_ACTIVE,
                    'type' => Transaction::T_PAID,
                    'customer_id' => $d,
                    'user_id' => Yii::$app->user->id
                ])->orderBy(['id' => SORT_DESC])->asArray()->all();
            $customer = $d;
            $report = Transaction::getReport($customer);
            if (!empty($data) || !empty($paid)) {
                $response['status'] = true;
                $response['dept'] = !empty($data) ? $data : 0;
                $response['paid'] = !empty($paid) ? $paid : 0;
                $response['days_dept'] = $report['dept_days'] != null ? $report['dept_days'] : 0;
                $response['days_paid'] = $report['paid_days'] != null ? $report['paid_days'] : 0;
                $response['month_paid'] = $report['month_paid'] != null ? $report['month_paid'] : 0;
                $response['month_dept'] = $report['month_dept'] != null ? $report['month_dept'] : 0;
                $response['message'] = 'OK';
            } else {
                $response['message'] = 'Empty';
                $response['dept'] = 0;
                $response['paid'] = 0;
            }
        }
        return $response;
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionAll(){
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search(Transaction::T_PAID);

        return $this->render('transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * @return string|Response
     * @throws ForbiddenHttpException
     */
    public function actionCustomerPaid()
    {
        if (!$this->actionTarqatuvchi()){
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $user_id = Yii::$app->user->id;
        $customer = Sales::getCustomer();
        $model = new Transaction();
        $model->reg_date = date("d.m.Y");

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $amount = $data['Transaction']['amount'];
            $date = strtotime($data['Transaction']['reg_date']);
            if ($amount < 0) {
                $amount *= (-1);
            }
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $tran = Transaction::find()
                    ->where(['user_id' => $user_id, 'status' => $model::STATUS_ACTIVE, 'customer_id' => $customer_id])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()
                    ->one();
                if (!empty($tran)) {
                    $tranInventory = $tran['inventory'] * 1 + $amount;
                    $model->setAttributes([
                        'customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $tranInventory,
                        'reg_date' => $date,
                        'type' => $model::T_PAID,
                        'status' => $model::STATUS_ACTIVE,
                    ]);
                } else {
                    $model->setAttributes([
                        'customer_id' => $customer_id,
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $amount,
                        'reg_date' => $date,
                        'type' => $model::T_PAID,
                        'status' => $model::STATUS_ACTIVE,
                    ]);
                }

                if ($model->save()) {
                    $saved = true;
                }
                if ($saved) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                    return $this->redirect(['all']);
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                }
            } catch (Exception $exception) {
                Yii::info('Not saved' . $exception->getMessage(), 'save');
            }


        }
        return $this->renderAjax('_paid', [
            'model' => $model,
        ]);
    }
    public function actionListPaid()
    {
        $searchModel = new TransactionSearch();
        $status = Transaction::T_EMPLOYEE_PAID;
        $dataProvider = $searchModel->searchOwen($status);

        return $this->render('list_paid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
