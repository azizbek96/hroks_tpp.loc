<?php

namespace backend\modules\employees\controllers;

use backend\controllers\BaseController;
use backend\models\Transaction;
use backend\modules\employees\models\SearchCustomer;
use common\models\Customers;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `employees` module
 */
class DefaultController extends BaseController
{

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $params = Transaction::getPrice();
        if ($this->actionTarqatuvchi()) {
            return $this->render('index', [
                'params' => $params
            ]);
        }
        throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionShopIndex()
    {
        if (!$this->actionTarqatuvchi()) {
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $searchModel = new SearchCustomer();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('shopindex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return array|string|Response
     * @throws ForbiddenHttpException
     */
    public function actionShopCreate()
    {
        if (!$this->actionTarqatuvchi()) {
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $model = new Customers();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                try {
                    if ($model->save()) {
                        $saved = true;
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e->getMessage(), 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['shop-index']);
                }
            }
        }
        return $this->render('shop', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionShopView($id)
    {

        if (!$this->actionTarqatuvchi()) {
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        return $this->render('shopviews', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionShopUpdate($id)
    {
        if (!$this->actionTarqatuvchi()) {
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                try {
                    if ($model->save()) {
                        $saved = true;
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e->getMessage(), 'save');
                    $transaction->rollBack();
                }

                if ($saved) {
                    return $this->redirect(['shop-view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('shopupdate', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionShopDelete($id)
    {
        if (!$this->actionTarqatuvchi()) {
            throw new ForbiddenHttpException('Sizga ushbu sahifaga kirishga ruxsat berilmagan');
        }
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = false;
        $model = $this->findModel($id);
        try {
            if (Customers::updateAll(['status' => $model::STATUS_DELETED], ['id' => $id])) {
                $isDeleted = true;
            }
            if ($isDeleted) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            Yii::info('Not saved' . $e, 'save');
        }
        if ($isDeleted) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted Successfully'));
            return $this->redirect(['shop-index']);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
        return $this->redirect(['shop-index']);
    }


    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
