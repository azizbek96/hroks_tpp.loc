<?php
function message() {

    $response['status'] = 'false';
    $response['message'] = 'Empty';
    if (Yii::$app->session->has('danger')) {
        $response['status'] = 'error';
        $response['message'] = Yii::$app->session->get('danger');
        Yii::$app->session->remove('danger');

    } elseif (Yii::$app->session->has('success')) {
        $response['status'] = 'success';
        $response['message'] = Yii::$app->session->get('success');
        Yii::$app->session->remove('success');
    }
    return $response;
}
