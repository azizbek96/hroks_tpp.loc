$( document ).ready(function() {
    $('.print-button').on('click', function (e) {
        e.preventDefault();
        window.print();
    });
    $('.change-status-payment').on('change', function (e) {
        var sub = $(this).attr('data-sub');
        var sum = $(this).attr('data-sum');
        var confirm = window.confirm('Сиз ростдан хам '+sub+' мижоз учун '+sum+' сум тўлов амалга оширмоқчимисиз?');
        e.preventDefault();
        if(confirm){
            var id = $(this).attr('data-payment-id');
            $.ajax({
                url: '/sub/change-status-payment?id='+id,
                success: function (response) {
                    if(response.status == 'ok'){
                        window.location.reload();
                    }
                }
            });
        }else {
            $(this).attr('checked', false);
        }
    });
    $('.change-status-sub').on('change', function (e) {
        var name = $(this).attr('data-name');
        var confirm = window.confirm('Сиз ростдан хам '+name+' мижозни кушмокчимисиз?');
        e.preventDefault();
        if(confirm){
            var id = $(this).attr('data-sub-id');
            $.ajax({
                url: '/sub/change-status-sub?id='+id,
                success: function (response) {
                    if(response.status == 'ok'){
                        window.location.reload();
                    }
                }
            });
        }else {
            $(this).attr('checked', false);
        }
    });
});
