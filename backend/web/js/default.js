$('body').delegate( '.saveButton', 'click', function(event) {
    const submitButton = $(this);
    if ( !submitButton.hasClass('disabled') ) {
        let a = submitButton.html();
        submitButton.addClass('disabled');
        submitButton.html("<i class='spinner-border'></i> " + a);

        setTimeout(() => {
            submitButton.removeClass('disabled');
            submitButton.html(a);
        }, 5000);
    } else {
        event.preventDefault();
    }
});

function ajaxInvoice()
{
    $(document).ready(function (){
        $('.check-invoice').click(function (event){
            let hr = $('#create-button').attr('href');
            let inputValue = $('#invoice_number').val();
            if (inputValue !== ''){
                let hrf = '$create' + '?invoice_number=' + inputValue;
                $.ajax({
                    type: 'POST',
                    url: '$views',
                    data: {id:inputValue},
                    success: function(response){
                        if (response.status === 'true') {
                            window.location.href=hrf;
                        } else {
                            $('#invoice_number').focus();
                            $('#invoice_number').css({"box-shadow": "0px 0px 3px red", "border-color": "red"});
                        }
                    }
                });

            }else {
                $('#invoice_number').focus();
                $('#invoice_number').css({"box-shadow": "0px 0px 3px red", "border-color": "red"});
            }
        })
    });
}
