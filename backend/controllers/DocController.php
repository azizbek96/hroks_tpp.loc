<?php

namespace backend\controllers;

use backend\models\DepArea;
use backend\models\ItemBalanceBoss;
use backend\models\ItemBalanceEmployee;
use backend\models\Pricing;
use backend\models\Transaction;
use backend\models\TransactionBoss;
use common\models\DocItems;
use common\models\ItemBalance;
use Exception;
use Vtiful\Kernel\Excel;
use Yii;
use common\models\Doc;
use common\models\DocSearch;
use yii\helpers\VarDumper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DocController
 * @package backend\controllers
 */
class DocController extends BaseController
{
    /**
     * @var string
     */
    public $slug;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $slug = Yii::$app->request->get('slug');
            $flag = false;
            if (!empty($slug) && array_key_exists($slug, Doc::getDocTypeBySlug())) {
                $flag = true;
                $this->slug = $slug;
            }
            if (!$flag) {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            }

            return true;
        }

        return false;
    }

    /**
     * Lists all Doc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $docType = 1;
        switch ($this->slug) {
            case Doc::DOC_TYPE_INCOMING_LABEL:
                $docType = Doc::DOC_TYPE_INCOMING;
                break;
            case Doc::DOC_TYPE_SELL_LABEL:
                $docType = Doc::DOC_TYPE_SELL;
                break;
            case Doc::DOC_TYPE_RETURN_LABEL:
                $docType = Doc::DOC_TYPE_RETURN;
                break;
            case Doc::DOC_TYPE_OUTGOING_LABEL:
                $docType = Doc::DOC_TYPE_OUTGOING;
                break;
            case Doc::DOC_TYPE_INCOMING_BOSS_LABEL:
                $docType = Doc::DOC_TYPE_INCOMING_BOSS;
                break;
            case Doc::DOC_TYPE_RETURN_BOSS_LABEL:
                $docType = Doc::DOC_TYPE_RETURN_BOSS;
                break;
        }
        $searchModel = new DocSearch();
        $dataProvider = $searchModel->search($this->slug);
        $models = Doc::find()->where(['type' => $docType])
//            ->orWhere(['type' => $docType])
            ->andWhere(['!=', 'status', Doc::STATUS_DELETED])
            ->orderBy(['id' => SORT_ASC])->asArray()->all();

        return $this->render("index/index_{$this->slug}", [
            'model' => $models,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionSaveAndFinish()
    {
        $data = Yii::$app->request->get();
        $id = $data['id'];
        $error = Yii::t('app', 'Ma`lumot saqlanmadi');
        if (empty($id)) {
            throw new ForbiddenHttpException('Parametr mavjud emas !');
        }
        $model = $this->findModel($id);
        if ($model->status != $model::STATUS_ACTIVE) {
            throw new ForbiddenHttpException(Yii::t('app', 'Notg\'ri amal bajarildi'));
        }
        $model->status = $model::STATUS_SAVED;
        $department_id = $model->department_id;
        $customer_id = $model->customer_id;
        $employee_id = $model->employee_id;
        if (!empty($model->docItems)) {
            $models = $model->docItems;
        } else {
            $models = [new DocItems()];
        }
        $saved = false;
        if (!empty($models)) {
            if ($this->slug == Doc::DOC_TYPE_INCOMING_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $amount = 0;
                    foreach ($models as $row) {
                        $result = ItemBalance::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['currency' => $row['currency']])
                            ->andWhere(['department_id' => $department_id])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        $itemBalance = new ItemBalance();
                        if (!empty($result)) {
                            $itemBalance->setAttributes([
                                'product_id' => $result['product_id'],
                                'count' => $row['quantity'],
                                'inventory' => $result['inventory'] + $row['quantity'],
                                'karopka_count' => $row['karopka_quantity'],
                                'karopka_inventory' => $result['karopka_inventory'] + $row['karopka_quantity'],
                                'doc_id' => $result['doc_id'],
                                'doc_type' => $model::DOC_TYPE_INCOMING,
                                'customer_id' => $customer_id,
                                'doc_item_id' => $row['id'],
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $result['department_id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                            ]);
                        } else {
                            $itemBalance->setAttributes([
                                'product_id' => $row['product_id'],
                                'count' => $row['quantity'],
                                'inventory' => $row['quantity'],
                                'karopka_count' => $row['karopka_quantity'],
                                'karopka_inventory' => $row['karopka_quantity'],
                                'doc_id' => $id,
                                'doc_type' => $model::DOC_TYPE_INCOMING,
                                'customer_id' => $customer_id,
                                'doc_item_id' => $row['id'],
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $department_id,
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                            ]);
                        }
                        if ($itemBalance->save()) {
                            $saved = true;
                            $amount += $row['quantity'] * $row['incoming_price'];
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $tran = Transaction::find()
                        ->where([
                            'customer_id' => $customer_id,
                            'type' => $model::T_CONTRAGENT_DEPT_IT,
                            'status' => $model::STATUS_ACTIVE
                        ])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransaction = new Transaction();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] - $amount;
                        $employeeTransaction->setAttributes([
                            'customer_id' => $customer_id,
                            'reg_date' => $model->created_at,
                            'user_id' => Yii::$app->user->id,
                            'amount' => (-1) * $amount,
                            'inventory' => $tranInventory,
                            'type' => $employeeTransaction::T_CONTRAGENT_DEPT_IT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    } else {
                        $employeeTransaction->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => Yii::$app->user->id,
                            'reg_date' => $model->created_at,
                            'amount' => (-1) * $amount,
                            'inventory' => (-1) * $amount,
                            'type' => $employeeTransaction::T_CONTRAGENT_DEPT_IT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    }

                    if ($saved && $model->save() && $employeeTransaction->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app', 'Not saved'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    }
                } catch (Exception $e) {
                    Yii::info('Error massage ' . $e);
                }
            }
            if ($this->slug == Doc::DOC_TYPE_SELL_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $amount = 0;
                    foreach ($models as $row) {

                        $itemBalance = new ItemBalance();
                        $employee_item_balance = new ItemBalanceEmployee();
                        $employee_item_balance_result = ItemBalanceEmployee::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['employee_id' => $employee_id])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();

                        $result = ItemBalance::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['currency' => $row['currency']])
                            ->andWhere(['department_id' => $department_id])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        if (!empty($result)) {
                            $amount_quantity = 1 * $row['quantity']*Doc::getMeasurementValue($row['measurement_id']);
                            $inventory = 1 * $result['inventory'] - $amount_quantity;
                            $karopka_count = 1*$row['karopka_quantity'];
                            $karopka_inventory = 1*$result['karopka_inventory'] - $karopka_count;

                            if($karopka_inventory < 0 && $inventory == 0){
                                $karopka_inventory = 0;
                            }
                            if($karopka_inventory < 0 && $inventory > 0){
                                $karopka_inventory = 1;
                            }
                            if (($inventory < 0 && $karopka_inventory < 0)) {
                                $saved = false;
                                $error = Yii::t('app', 'Yetarli maxsulot yo`q. Sizda ' . $result['inventory'] . ' ' . Doc::getMeasurementLabelToken($row['measurement_id']) . ' miqdorda maxsulot mavjud');
                                break;
                            }
                            $itemBalance->setAttributes([
                                'count' => (-1) * $amount_quantity,
                                'inventory' => $inventory,
                                'karopka_count' => $karopka_count,
                                'karopka_inventory' => $karopka_inventory,
                                'product_id' => $row['product_id'],
                                'doc_id' => $id,
                                'doc_type' => $model::DOC_TYPE_SELL,
                                'employee_id' => $employee_id,
                                'doc_item_id' => $row['id'],
                                'department_id' => $result['department_id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'measurement_id' => $row['measurement_id'],
                                'currency' => $model::CURRENSY_SUM,
                            ]);
                            if (!empty($employee_item_balance_result)) {
                                $inventory = 1 * $employee_item_balance_result['inventory'] + 1 * $row['quantity'];
                                if ($inventory < 0) {
                                    $saved = false;
                                    break;
                                }
                                $employee_item_balance->setAttributes([
                                    'product_id' => $row['product_id'],
                                    'amount' => $amount_quantity,
                                    'inventory' => $inventory,
                                    'doc_id' => $id,
                                    'user_id' => Yii::$app->user->id,
                                    'status' => Doc::STATUS_ACTIVE,
                                    'employee_id' => $employee_id,
                                    'measurement_id' => $row['measurement_id'],
                                    'department_id' => $result['department_id'],
                                    'type' => Doc::DOC_TYPE_SELL,
                                ]);
                            } else {
                                $employee_item_balance->setAttributes([
                                    'product_id' => $row['product_id'],
                                    'amount' => $amount_quantity,
                                    'inventory' => $amount_quantity,
                                    'doc_id' => $id,
                                    'user_id' => Yii::$app->user->id,
                                    'employee_id' => $employee_id,
                                    'status' => Doc::STATUS_ACTIVE,
                                    'department_id' => $department_id,
                                    'measurement_id' => $row['measurement_id'],
                                    'type' => Doc::DOC_TYPE_SELL,
                                ]);
                            }
                        } else {
                            $error = Yii::t('app', 'Bazadagi mahsulot  miqdoriga etibor bering');
                            break;
                        }
                        if ($employee_item_balance->save() && $itemBalance->save()) {
                            $amount += $row['quantity']*Doc::getMeasurementValue($row['measurement_id']) * $row['sold_price'];
                            $saved = true;
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $empId = Doc::getEmployeeUserId($employee_id);
                    $tran = Transaction::find()
                        ->where([
                            'user_id' => $empId,
                            'type' => $model::T_EMPLOYEE_DEPT,
                            'status' => $model::STATUS_ACTIVE
                        ])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransaction = new Transaction();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] - $amount;
                        $employeeTransaction->setAttributes([
                            'user_id' => $empId,
                            'amount' => (-1) * $amount,
                            'inventory' => $tranInventory,
                            'reg_date' => strtotime(date('Y-m-d H:i:s')),
                            'type' => $employeeTransaction::T_EMPLOYEE_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    } else {
                        $employeeTransaction->setAttributes([
                            'user_id' => $empId,
                            'amount' => (-1) * $amount,
                            'inventory' => (-1) * $amount,
                            'reg_date' => strtotime(date('Y-m-d H:i:s')),
                            'type' => $employeeTransaction::T_EMPLOYEE_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                        ]);
                    }

                    if ($saved && $model->save() && $employeeTransaction->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', $error);
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    }
                } catch (Exception $e) {
                    Yii::info('Error massage ' . $e->getMessage());
                }
            }
            if ($this->slug == Doc::DOC_TYPE_RETURN_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $amount = 0;
                    foreach ($models as $row) {
                        $itemBalance = new ItemBalance();
                        $employee_item_balance = new ItemBalanceEmployee();
                        $employee_item_balance_result = ItemBalanceEmployee::find()
                            ->asArray()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['employee_id' => $employee_id])
                            ->orderBy(['id' => SORT_DESC])
                            ->one();

                        $result = ItemBalance::find()
                            ->asArray()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['currency' => $row['currency']])
                            ->andWhere(['department_id' => $department_id])
                            ->orderBy(['id' => SORT_DESC])
                            ->one();
                        $karopka_quantity = $result['karopka_inventory'];
                        if (!empty($employee_item_balance_result)) {
                            $inventory = 1 * $employee_item_balance_result['inventory'] - 1 * $row['quantity'];
                            $karopka_inventory_new =  $karopka_quantity + $row['karopka_quantity'];
                            if ($inventory < 0) {
                                $saved = false;
                                $error = Yii::t('app', 'Yetarli maxsulot yo`q. Sizda ' . $employee_item_balance_result['inventory'] . ' miqdorda maxsulot mavjud');
                                break;
                            }
                            $employee_item_balance->setAttributes([
                                'product_id' => $result['product_id'],
                                'amount' => (-1) * $row['quantity'],
                                'inventory' => $inventory,
                                'doc_id' => $result['doc_id'],
                                'user_id' => Yii::$app->user->id,
                                'type' => Doc::DOC_TYPE_RETURN,
                                'status' => Doc::STATUS_ACTIVE,
                                'employee_id' => $employee_id,
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $result['department_id'],
                            ]);

                            if (!empty($result)) {
                                $inventory = 1 * $result['inventory'] + 1 * $row['quantity'];

                                if ($inventory < 0) {
                                    $saved = false;
                                    break;
                                }
                                $itemBalance->setAttributes([
                                    'product_id' => $result['product_id'],
                                    'count' => 1 * $row['quantity'],
                                    'inventory' => $inventory,
                                    'doc_id' => $result['doc_id'],
                                    'doc_type' => $model::DOC_TYPE_RETURN,
                                    'employee_id' => $employee_id,
                                    'doc_item_id' => $row['id'],
                                    'department_id' => $result['department_id'],
                                    'dep_area_id' => DepArea::getDepAre(),
                                    'measurement_id' => $row['measurement_id'],
                                    'currency' => $model::CURRENSY_SUM,
                                    'karopka_count' => $row['karopka_quantity'],
                                    'karopka_inventory' => $karopka_inventory_new,
                                ]);

                            } else {
                                $itemBalance->setAttributes([
                                    'product_id' => $result['product_id'],
                                    'count' => $row['quantity'],
                                    'inventory' => $row['quantity'],
                                    'doc_id' => $result['doc_id'],
                                    'doc_type' => $model::DOC_TYPE_RETURN,
                                    'employee_id' => $employee_id,
                                    'doc_item_id' => $row['id'],
                                    'department_id' => $result['department_id'],
                                    'dep_area_id' => DepArea::getDepAre(),
                                    'measurement_id' => $row['measurement_id'],
                                    'currency' => $model::CURRENSY_SUM,
                                    'karopka_count' => $row['quantity'],
                                    'karopka_inventory' => $row['quantity'],
                                ]);
                            }
                        } else {
                            $error = Yii::t('app', 'Maxsulot miqdoriga etibor bering');
                            break;
                        }
                        if ($employee_item_balance->save() && $itemBalance->save()) {
                            $amount += $row['quantity'] * $row['sold_price'] * 1 * $model::getMeasurementValue($row['measurement_id']);
                            $saved = true;
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $empId = Doc::getEmployeeUserId($employee_id);
                    $tran = Transaction::find()
                        ->where([
                            'user_id' => $empId,
                            'type' => $model::T_EMPLOYEE_DEPT,
                            'status' => $model::STATUS_ACTIVE
                        ])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransaction = new Transaction();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] + $amount;
                        $employeeTransaction->setAttributes([
                            'user_id' => $empId,
                            'amount' => $amount,
                            'inventory' => $tranInventory,
                            'type' => $employeeTransaction::T_EMPLOYEE_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                            'reg_date' => time(),
                        ]);
                    } else {
                        $employeeTransaction->setAttributes([
                            'user_id' => $empId,
                            'amount' => $amount,
                            'inventory' => $amount,
                            'type' => $employeeTransaction::T_EMPLOYEE_DEPT,
                            'status' => $employeeTransaction::STATUS_ACTIVE,
                            'reg_date' => time(),
                        ]);
                    }

                    if ($saved && $model->save() && $employeeTransaction->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', $error);
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    }
                } catch (Exception $e) {
                    Yii::info('Error massage ' . $e->getMessage());
                }
            }
            if ($this->slug == Doc::DOC_TYPE_INCOMING_BOSS_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $amount = 0;
                    foreach ($models as $row) {
                        $result = ItemBalanceBoss::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['currency' => $row['currency']])
                            ->andWhere(['department_id' => $department_id])
                            ->andWhere(['measurement_id' => $row['measurement_id']])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        $itemBalanceBoss = new ItemBalanceBoss();
                        if (!empty($result)) {
                            $itemBalanceBoss->setAttributes([
                                'product_id' => $result['product_id'],
                                'amount' => $row['quantity'],
                                'inventory' => $result['inventory'] + $row['quantity'],
                                'doc_id' => $id,
                                'type' => $model::DOC_TYPE_INCOMING_BOSS,
                                'customer_id' => $customer_id,
                                'doc_item_id' => $row['id'],
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $result['department_id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                                'user_id' => Yii::$app->user->id
                            ]);
                        } else {
                            $itemBalanceBoss->setAttributes([
                                'product_id' => $row['product_id'],
                                'amount' => $row['quantity'],
                                'inventory' => $row['quantity'],
                                'doc_id' => $id,
                                'type' => $model::DOC_TYPE_INCOMING_BOSS,
                                'customer_id' => $customer_id,
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $department_id,
                                'doc_item_id' => $row['id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                                'user_id' => Yii::$app->user->id
                            ]);
                        }
                        if ($itemBalanceBoss->save()) {
                            $saved = true;
                            $amount += $row['quantity'] * $row['incoming_price'] * 1; //* $model::getMeasurementValue($row['measurement_id']);
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $tran = TransactionBoss::find()
                        ->where([
                            'customer_id' => $customer_id,
                            'type' => $model::T_SUPPLIER_BOSS_DEPT,
                            'status' => $model::STATUS_ACTIVE
                        ])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransactionBoss = new TransactionBoss();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] - $amount;
                        $employeeTransactionBoss->setAttributes([
                            'customer_id' => $customer_id,
                            'reg_date' => $model->created_at,
                            'user_id' => Yii::$app->user->id,
                            'amount' => (-1) * $amount,
                            'inventory' => $tranInventory,
                            'type' => $employeeTransactionBoss::T_SUPPLIER_BOSS_DEPT,
                            'status' => $employeeTransactionBoss::STATUS_ACTIVE,
                        ]);
                    } else {
                        $employeeTransactionBoss->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => Yii::$app->user->id,
                            'reg_date' => $model->created_at,
                            'amount' => (-1) * $amount,
                            'inventory' => (-1) * $amount,
                            'type' => $employeeTransactionBoss::T_SUPPLIER_BOSS_DEPT,
                            'status' => $employeeTransactionBoss::STATUS_ACTIVE,
                        ]);
                    }

                    if ($saved && $model->save() && $employeeTransactionBoss->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    }
                } catch (Exception $e) {
                    Yii::info('Error massage ' . $e);
                }
            }
            if ($this->slug == Doc::DOC_TYPE_RETURN_BOSS_LABEL){
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $amount = 0;
                    foreach ($models as $row) {
                        $result = ItemBalanceBoss::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['currency' => $row['currency']])
                            ->andWhere(['department_id' => $department_id])
                            ->andWhere(['measurement_id' => $row['measurement_id']])
                            ->andWhere(['customer_id' => $customer_id])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        $itemBalanceBoss = new ItemBalanceBoss();
                        $row['quantity'] = ($row['quantity'] > 0) ? $row['quantity']: (-1)*$row['quantity'];
                        if (!empty($result)) {
                            $itemBalanceBoss->setAttributes([
                                'product_id' => $result['product_id'],
                                'amount' => (-1)*$row['quantity'],
                                'inventory' => $result['inventory'] - $row['quantity'],
                                'doc_id' => $id,
                                'type' => $model::DOC_TYPE_RETURN_BOSS,
                                'customer_id' => $customer_id,
                                'doc_item_id' => $row['id'],
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $result['department_id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                                'user_id' => Yii::$app->user->id
                            ]);
                        } else {
                            $itemBalanceBoss->setAttributes([
                                'product_id' => $row['product_id'],
                                'amount' => (-1)*$row['quantity'],
                                'inventory' => (-1)*$row['quantity'],
                                'doc_id' => $id,
                                'type' => $model::DOC_TYPE_RETURN_BOSS,
                                'customer_id' => $customer_id,
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $department_id,
                                'doc_item_id' => $row['id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                                'user_id' => Yii::$app->user->id
                            ]);
                        }
                        if ($itemBalanceBoss->save()) {
                            $saved = true;
                            $amount += $row['quantity'] * $row['incoming_price'] * 1; //* $model::getMeasurementValue($row['measurement_id']);
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    $tran = TransactionBoss::find()
                        ->where([
                            'customer_id' => $customer_id,
                            'type' => $model::T_SUPPLIER_BOSS_PAID,
                            'status' => $model::STATUS_ACTIVE
                        ])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    $employeeTransactionBoss = new TransactionBoss();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] + $amount;
                        $employeeTransactionBoss->setAttributes([
                            'customer_id' => $customer_id,
                            'reg_date' => $model->created_at,
                            'user_id' => Yii::$app->user->id,
                            'amount' => $amount,
                            'inventory' => $tranInventory,
                            'type' => $employeeTransactionBoss::T_SUPPLIER_BOSS_PAID,
                            'status' => $employeeTransactionBoss::STATUS_ACTIVE,
                            'add_info' => 'Mahsulot qaytarildi',
                        ]);
                    } else {
                        $employeeTransactionBoss->setAttributes([
                            'customer_id' => $customer_id,
                            'user_id' => Yii::$app->user->id,
                            'reg_date' => $model->created_at,
                            'amount' => $amount,
                            'inventory' => $amount,
                            'type' => $employeeTransactionBoss::T_SUPPLIER_BOSS_PAID,
                            'status' => $employeeTransactionBoss::STATUS_ACTIVE,
                            'add_info' => 'Mahsulot qaytarildi',
                        ]);
                    }

                    if ($saved && $model->save() && $employeeTransactionBoss->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    }
                } catch (Exception $e) {
                    Yii::info('Error massage ' . $e);
                }
            }
            if ($this->slug == Doc::DOC_TYPE_OUTGOING_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $inv =0;
                    foreach ($models as $row) {
                        $result = ItemBalance::find()
                            ->where(['product_id' => $row['product_id']])
                            ->andWhere(['currency' => $row['currency']])
                            ->andWhere(['department_id' => $department_id])
                            ->andWhere(['measurement_id' => $row['measurement_id']])
                            ->orderBy(['id' => SORT_DESC])
                            ->asArray()
                            ->one();
                        $itemBalance = new ItemBalance();
                        if (!empty($result)) {
                            $inv =  $result['inventory'] - $row['quantity'];
                            if ($inv < 0){
                                $error = Yii::t('app', 'Siz bazadagi mahsulotdan oshiqcha kiritdingiz iltimos qaytadan tekshirib ko\'ring');
                            }
                            $itemBalance->setAttributes([
                                'product_id' => $result['product_id'],
                                'count' => -$row['quantity'] * Doc::getMeasurementValue($row['measurement_id']),
                                'inventory' => $inv,
                                'doc_id' => $result['doc_id'],
                                'doc_type' => $model::DOC_TYPE_OUTGOING,
                                'doc_item_id' => $row['id'],
                                'measurement_id' => $row['measurement_id'],
                                'department_id' => $result['department_id'],
                                'dep_area_id' => DepArea::getDepAre(),
                                'currency' => $model::CURRENSY_SUM,
                            ]);
                        }
                        if ($itemBalance->save()) {
                            $saved = true;
                        } else {
                            $saved = false;
                            break;
                        }
                    }
                    if ($saved && $model->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', 'SuccessFully Add'));
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    } else {
                        $transaction->rollBack();
                        Yii::$app->session->setFlash('danger', $error);
                        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                    }
                } catch (Exception $e) {
                    Yii::info('Error massage ' . $e);
                }
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Qiymat kelmadi!...'));
        }
        return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionView($id)
    {
        if(is_numeric($id)){
            $model = Doc::findOne($id);
            if($model !== null){
                $modelItems = DocItems::find()->where(['doc_id' => $id])->all();
                return $this->render("views/{$this->slug}", [
                    'model' => $model,
                    'modelItems' => $modelItems,
                ]);
            }
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));

    }

    /**
     * @return string|Response
     */
    public function actionCreate()
    {
        $slug = Yii::$app->request->get('slug');
        switch ($slug) {
            case Doc::DOC_TYPE_INCOMING_LABEL:
                $model = new Doc();
                $model->scenario = Doc::SCENARIO_INCOMING;
                break;
            case Doc::DOC_TYPE_SELL_LABEL:
                $model = new Doc();
                $model->scenario = Doc::SCENARIO_SELLING;
                break;
            case Doc::DOC_TYPE_INCOMING_BOSS_LABEL:
                $model = new Doc();
                $model->scenario = Doc::SCENARIO_BOSS;
                break;
            case Doc::DOC_TYPE_OUTGOING_LABEL:
                $model = new Doc();
                $model->scenario = Doc::SCENARIO_OUTGOING;
                break;
            case Doc::DOC_TYPE_RETURN_LABEL:
                $invoice_number = Yii::$app->request->get('invoice_number') ?? null;
                $modelData = Doc::find()
                    ->where(['invoice_number' => $invoice_number])
                    ->asArray()->all();
                $model = new Doc();
                $model->scenario = Doc::SCENARIO_RETURN;
                break;
        }
        $modelItem = [new DocItems()];
        $getId = Doc::find()->select('id')->orderBy(['id' => SORT_DESC])->one();
        $error = Yii::t('app', 'Not Saved');
        if ($slug == Doc::DOC_TYPE_RETURN_LABEL && !empty($modelData)) {
            $employeeId = $modelData[0]['employee_id'];
            $modelItems = $model::getDocItemsSql($employeeId);
            $model->cp = [
                'modelItems' => $modelItems,
                'employeeId' => $employeeId,
            ];
        } else {
            $modelItems = null;
            $employeeId = null;
        }
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            if ($model->load($data)) {
                $model->doc_number = 'D' . substr(Doc::getDocTypeBySlug($slug), 0, 1) . '_' . (!empty($getId->id) ? $getId->id + 1 : 1) . '-' . date('dm/Y');
                if ($slug == Doc::DOC_TYPE_INCOMING_LABEL) {
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        $saved = false;
                        if ($model->save()) {
                            $docId = $model->id;
                            if (!empty($data['DocItems'])) {
                                foreach ($data['DocItems'] as $docItem => $item) {
                                    $income_price = Pricing::getPriceProduct($item['product_id']);
                                    if(!empty($income_price)){
                                        if (!empty($item['quantity']) && !empty($docId)) {
                                            $modelDocItem = new DocItems();
                                            $modelDocItem->scenario = DocItems::SCENARIO_INCOMING;
                                            $amount = $item['quantity']; //$model::getMeasurementValue($item['measurement_id']) *;
                                            $modelDocItem->setAttributes([
                                                'product_id' => $item['product_id'],
                                                'quantity' => $amount,
                                                'karopka_quantity' => $item['karopka_quantity'],
                                                'dep_area_id' => DepArea::getDepAre(),
                                                'measurement_id' => $item['measurement_id'],
                                                'incoming_price' => $income_price,
                                                'currency' => $model::CURRENSY_SUM,
                                                'doc_id' => $docId,
                                            ]);
                                            if ($modelDocItem->save()) {
                                                $saved = true;
                                            } else {
                                                $saved = false;
                                                break;
                                            }
                                        } else {
                                           $error = Yii::t('app', 'Mahsulot miqdori kiritilmagan e`tibor bering!');
                                            break;
                                        }
                                    }else{
                                        $error = Yii::t('app', 'Mahsulot narxi kiritilmagan!');
                                        break;
                                    }
                                }
                            }
                            if ($saved) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                            } else {
                                $transaction->rollBack();
                                Yii::$app->session->setFlash('danger', Yii::t('app', $error));
                            }
                        }

                    } catch (Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }
                if ($slug == Doc::DOC_TYPE_SELL_LABEL) {
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        $saved = false;
                        if ($model->save()) {
                            $docId = $model->id;
                            if (!empty($data['DocItems'])) {
                                foreach ($data['DocItems'] as $docItem => $item) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->scenario = DocItems::SCENARIO_SELLING;
                                    if (!empty($item['sold_price']) && !empty($item['product_id'] && !empty($item['karopka_quantity']))) {
                                        $amount = $item['quantity'];/// *$model::getMeasurementValue($item['measurement_id']) * ;
                                        $modelDocItem->setAttributes([
                                            'product_id' => $item['product_id'],
                                            'quantity' => $amount,
                                            'dep_area_id' => DepArea::getDepAre(),
                                            'measurement_id' => $item['measurement_id'],
                                            'sold_price' => $item['sold_price'],
                                            'karopka_quantity' => $item['karopka_quantity'],
                                            'currency' => $model::CURRENSY_SUM,
                                            'doc_id' => $docId,
                                        ]);
                                        if ($modelDocItem->save()) {
                                            $saved = true;
                                        } else {
                                            $saved = false;
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                            if ($saved) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                            } else {
                                $transaction->rollBack();
                                Yii::$app->session->setFlash('danger', $error);
                            }
                        }

                    } catch (Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }
                if ($slug == Doc::DOC_TYPE_RETURN_LABEL) {
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        $saved = false;
                        $model->employee_id = $employeeId;
                        $model->type = Doc::DOC_TYPE_RETURN;
                        $model->invoice_id = $invoice_number;
                        if ($model->save()) {
                            $docId = $model->id;
                            if (!empty($data['DocItems'])) {
                                foreach ($data['DocItems'] as $docItem => $item) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->scenario = DocItems::SCENARIO_RETURN;
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'measurement_id' => $item['measurement_id'],
                                        'quantity' => $item['amount'],
                                        'karopka_quantity' => $item['karopka_quantity'],
                                        'dep_area_id' => DepArea::getDepAre(),
                                        'sold_price' => $item['sold_price'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                    if ($item['amount'] <= 0 || $item['karopka_quantity'] <= 0) {
                                        continue;
                                    }
                                    if ($modelDocItem->save()) {
                                        $saved = true;
                                    } else {
                                        $saved = false;
                                        break;
                                    }
                                }
                            }
                            if ($saved) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                            } else {
                                $transaction->rollBack();
                                Yii::$app->session->setFlash('danger', $error);
                            }
                        }
                    } catch (Exception $exception) {
                        Yii::info('Error Document Return' . $exception->getMessage(), 'save');
                    }
                }
                if ($slug == Doc::DOC_TYPE_INCOMING_BOSS_LABEL) {
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        $saved = false;
                        if ($model->save()) {
                            $docId = $model->id;
                            if (!empty($data['DocItems'])) {
                                foreach ($data['DocItems'] as $docItem => $item) {
                                    if (!empty($item['incoming_price']) && !empty($item['quantity']) && !empty($docId)) {
                                        $modelDocItem = new DocItems();
                                        $amount = $item['quantity'];
                                        $modelDocItem->setAttributes([
                                            'product_id' => $item['product_id'],
                                            'quantity' => $amount ,
                                            'dep_area_id' => DepArea::getDepAre(),
                                            'measurement_id' => $item['measurement_id'],
                                            'incoming_price' => $item['incoming_price'],
                                            'currency' => $model::CURRENSY_SUM,
                                            'doc_id' => $docId,
                                        ]);
                                        if ($modelDocItem->save()) {
                                            $saved = true;
                                        } else {
                                            $saved = false;
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                            if ($saved) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                            } else {
                                $transaction->rollBack();

                                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi!'));
                            }
                        }
                    } catch (Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }
                if ($slug == Doc::DOC_TYPE_OUTGOING_LABEL) {
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        $saved = false;
                        if ($model->save()) {
                            $docId = $model->id;
                            if (!empty($data['DocItems'])) {
                                foreach ($data['DocItems'] as $docItem => $item) {
                                    if (!empty($docId)) {
                                        $modelDocItem = new DocItems();
                                        $amount = $item['quantity']; //$model::getMeasurementValue($item['measurement_id']) *;
                                        $modelDocItem->setAttributes([
                                            'product_id' => $item['product_id'],
                                            'quantity' => $amount,
                                            'dep_area_id' => DepArea::getDepAre(),
                                            'measurement_id' => $item['measurement_id'],
                                            'currency' => $model::CURRENSY_SUM,
                                            'doc_id' => $docId,
                                        ]);
                                        if ($modelDocItem->save()) {
                                            $saved = true;
                                        } else {
                                            $saved = false;
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                            if ($saved) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                                return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                            } else {
                                $transaction->rollBack();
                                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                            }
                        }

                    } catch (Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }
            }
        }
        if ($model !== null) {
            $model->reg_date = date("d.m.Y");
        }
        return $this->render('create', [
            'model' => $model,
            'modelItem' => $modelItem,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $slug = Yii::$app->request->get('slug');
        $model = $this->findModel($id);
        switch ($slug) {
            case Doc::DOC_TYPE_INCOMING_LABEL:
                $model->scenario = Doc::SCENARIO_INCOMING;
                break;
            case Doc::DOC_TYPE_SELL_LABEL:
                $model->scenario = Doc::SCENARIO_SELLING;
                break;
            case Doc::DOC_TYPE_INCOMING_BOSS_LABEL:
                $model->scenario = Doc::SCENARIO_BOSS;
                break;
            case Doc::DOC_TYPE_OUTGOING_LABEL:
                $model->scenario = Doc::SCENARIO_OUTGOING;
                break;
            case Doc::DOC_TYPE_RETURN_BOSS_LABEL:
                $modelKirim = $this->findModel($model->parent_id);
                $modelKirimItems = $modelKirim->getDocItems()->asArray()->all();
                $modelItem = $model->docItems;
                $oldItems = [];
                foreach ($modelKirimItems as $item) {
                    $oldItems[$item['product_id']] [$item['dep_area_id']] [$item['measurement_id']] [$item['incoming_price']] [$item['currency']] = $item;
                }
                foreach ($modelItem as $key => $item) {
                    $item->ret_quantity = $oldItems[$item['product_id']][$item['dep_area_id']][$item['measurement_id']][$item['incoming_price']][$item['currency']]['quantity'];
                    $modelItem[$key] = $item;
                }
                break;
            case Doc::DOC_TYPE_RETURN_LABEL:
                $invoice_number = $model->invoice_id;
                $modelData = Doc::find()
                    ->where(['invoice_number' => $invoice_number])
                    ->asArray()->all();
                break;
        }
        $error = Yii::t('app', 'Not saved');
        if (!empty($model->docItems)) {
            $modelItem = $model->docItems;
        } else {
            $modelItem = [new DocItems()];
        }
        if (!empty($model) && $slug == Doc::DOC_TYPE_RETURN_LABEL) {

            $employeeId = $modelData[0]['employee_id'];
            $modelItems = $model::getDocItemsSql($employeeId, $model->invoice_id,$id);
            $model->cp = [
                'modelItems' => $modelItems,
                'employeeId' => $employeeId,
            ];
        }
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            if ($slug == Doc::DOC_TYPE_INCOMING_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    $model->status = $model::STATUS_ACTIVE;

                    if ($model->load($data) && $model->save()) {
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {
                                $income_price = Pricing::getPriceProduct($item['product_id']);
                                if(!empty($income_price)){
                                    if (!empty($item['quantity']) && !empty($docId)) {
                                        $modelDocItem = new DocItems();
                                        $modelDocItem->setAttributes([
                                            'product_id' => $item['product_id'],
                                            'quantity' => $item['quantity'],
                                            'karopka_quantity' => $item['karopka_quantity'],
                                            'dep_area_id' => DepArea::getDepAre(),
                                            'measurement_id' => $item['measurement_id'],
                                            'incoming_price' => $income_price,
                                            'currency' => $model::CURRENSY_SUM,
                                            'doc_id' => $docId,
                                        ]);
                                        if ($modelDocItem->save()) {
                                            $saved = true;
                                        } else {
                                            $saved = false;
                                            break;
                                        }
                                    } else {
                                        $error = Yii::t('app', 'Narxlashda yoki maxsulot miqdorida xatolik mavjud');
                                        break;
                                    }
                                }else{
                                    $error = Yii::t('app', 'Mahsulot narxi kiritilmagan');
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                            return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                } catch (Exception $e) {
                    Yii::info('Error Document ' . $e->getMessage(), 'save');
                }
            }
            if ($slug == Doc::DOC_TYPE_SELL_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    $model->status = $model::STATUS_ACTIVE;
                    if ($model->load($data) && $model->save()) {
                        $docId = $model->id;
                        $department = $model->department_id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {
                                if (!DocItems::getHasQuantityAmount($item['product_id'], $department, $item['sold_price'], $item['quantity'])) {
                                    $error = Yii::t('app', 'Noto`g`ri ma`lumotlar kiritildi yoki maxsulot miqdoriga e`tibor bering');
                                    break;
                                }
                                $modelDocItem = new DocItems();
                                $modelDocItem->setAttributes([
                                    'product_id' => $item['product_id'],
                                    'quantity' => $item['quantity'],
                                    'dep_area_id' => DepArea::getDepAre(),
                                    'measurement_id' => $item['measurement_id'],
                                    'sold_price' => $item['sold_price'],
                                    'karopka_quantity' => $item['karopka_quantity'],
                                    'currency' => $model::CURRENSY_SUM,
                                    'doc_id' => $docId,
                                ]);
                                if ($modelDocItem->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                            return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                } catch (Exception $e) {
                    Yii::info('Error Document ' . $e->getMessage(), 'save');
                }
            }
            if ($slug == Doc::DOC_TYPE_RETURN_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    $model->employee_id = $employeeId;
                    $model->type = Doc::DOC_TYPE_RETURN;
                    $model->invoice_id = $invoice_number;
                    if ($model->save()) {
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {
                                $modelDocItem = new DocItems();
                                $modelDocItem->setAttributes([
                                    'product_id' => $item['product_id'],
                                    'measurement_id' => $item['measurement_id'],
                                    'quantity' => $item['amount'],
                                    'karopka_quantity' => $item['karopka_quantity'],
                                    'dep_area_id' => DepArea::getDepAre(),
                                    'sold_price' => $item['sold_price'],
                                    'doc_id' => $docId,
                                    'currency' => $model::CURRENSY_SUM,
                                ]);
                                if ($item['amount'] <= 0 || $item['karopka_quantity'] <= 0) {
                                    continue;
                                }
                                if ($modelDocItem->save()) {
                                    $saved = true;
                                } else {
                                    $saved = false;
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                            return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                } catch (Exception $exception) {
                    Yii::info('Error Document Return' . $exception->getMessage(), 'save');
                }
            }
            if ($slug == Doc::DOC_TYPE_INCOMING_BOSS_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    if ($model->load($data) && $model->save()) {
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {
                                if (!empty($item['incoming_price']) && !empty($item['quantity']) && !empty($docId)) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'quantity' => $item['quantity'],
                                        'dep_area_id' => DepArea::getDepAre(),
                                        'measurement_id' => $item['measurement_id'],
                                        'incoming_price' => $item['incoming_price'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                    if ($modelDocItem->save()) {
                                        $saved = true;
                                    } else {
                                        $saved = false;
                                        break;
                                    }
                                } else {
                                    $error = Yii::t('app', 'Narxlashda yoki maxsulot miqdorida xatolik mavjud');
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                            return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                } catch (Exception $e) {
                    Yii::info('Error Document ' . $e->getMessage(), 'save');
                }
            }
            if ($slug == Doc::DOC_TYPE_RETURN_BOSS_LABEL){
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    if ($model->load($data) && $model->save()) {
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {
                                if (!empty($item['incoming_price']) && !empty($item['quantity']) && !empty($docId)) {
                                    $modelDocItem = new DocItems();
                                    $amount = $item['quantity'];
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'quantity' => $amount ,
                                        'dep_area_id' => DepArea::getDepAre(),
                                        'measurement_id' => $item['measurement_id'],
                                        'incoming_price' => $item['incoming_price'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                    if ($modelDocItem->save()) {
                                        $saved = true;
                                    } else {
                                        $saved = false;
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                            return $this->redirect(['view', 'id' =>  $model->id, 'slug' => $this->slug]);
                        } else {
                            $transaction->rollBack();

                            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi!'));
                        }
                    }
                } catch (Exception $e) {
                    Yii::info('Error Document ' . $e->getMessage(), 'save');
                }
            }
            if ($slug == Doc::DOC_TYPE_OUTGOING_LABEL) {
                try {
                    $transaction = Yii::$app->db->beginTransaction();
                    $saved = false;
                    $model->status = $model::STATUS_ACTIVE;

                    if ($model->load($data) && $model->save()) {
                        $docId = $model->id;
                        DocItems::deleteAll(['doc_id' => $docId]);
                        if (!empty($data['DocItems'])) {
                            foreach ($data['DocItems'] as $docItem => $item) {
                                if (!empty($item['quantity']) && !empty($docId)) {
                                    $modelDocItem = new DocItems();
                                    $modelDocItem->setAttributes([
                                        'product_id' => $item['product_id'],
                                        'quantity' => $item['quantity'],
                                        'dep_area_id' => DepArea::getDepAre(),
                                        'measurement_id' => $item['measurement_id'],
                                        'currency' => $model::CURRENSY_SUM,
                                        'doc_id' => $docId,
                                    ]);
                                    if ($modelDocItem->save()) {
                                        $saved = true;
                                    } else {
                                        $saved = false;
                                        break;
                                    }
                                } else {
                                    $error = Yii::t('app', 'Xatolik mavjud');
                                    break;
                                }
                            }
                        }
                        if ($saved) {
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('app', 'Saved successfully'));
                            return $this->redirect(['view', 'id' => $model->id, 'slug' => $this->slug]);
                        } else {
                            $transaction->rollBack();
                            Yii::$app->session->setFlash('danger', $error);
                        }
                    }
                } catch (Exception $e) {
                    Yii::info('Error Document ' . $e->getMessage(), 'save');
                }
            }

        }
        return $this->render('create', [
            'model' => $model,
            'modelItem' => $modelItem,
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionReturn($id)
    {
        $slug = Yii::$app->request->get('slug');
        $model = $this->findModel($id);
        switch ($slug) {
            case Doc::DOC_TYPE_RETURN_BOSS_LABEL:
                $newModel = new Doc();
                $model->scenario = Doc::SCENARIO_INCOMING;
                $modelKirimItems = $model->getDocItems()->asArray()->all();
                $modelItem = $model->docItems;
                $oldItems = [];
                foreach ($modelKirimItems as $item) {
                    $oldItems[$item['product_id']] [$item['dep_area_id']] [$item['measurement_id']] [$item['incoming_price']] [$item['currency']] = $item;
                }
                foreach ($modelItem as $key => $item) {
                    $item->ret_quantity = $oldItems[$item['product_id']][$item['dep_area_id']][$item['measurement_id']][$item['incoming_price']][$item['currency']]['quantity'];
                    $modelItem[$key] = $item;
                    $item->quantity = null;
                }
                break;
        }
        $error = Yii::t('app', 'Not saved');
        if (!empty($model->docItems)) {
            $modelItem = $model->docItems;
        } else {
            $modelItem = [new DocItems()];
        }
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $newModel = new Doc();
            if ($newModel->load($data)) {
                $newModel->doc_number = 'D' . substr(Doc::getDocTypeBySlug($slug), 0, 1) . '_' . (!empty($getId->id) ? $getId->id + 1 : 1) . '-' . date('dm/Y');
                $newModel->parent_id = $id;
                $newModel->type = Doc::DOC_TYPE_RETURN_BOSS;
                if ($slug == Doc::DOC_TYPE_RETURN_BOSS_LABEL) {
                    try {
                        $transaction = Yii::$app->db->beginTransaction();
                        $saved = false;
                        if ($newModel->save()) {
                            $docId = $newModel->id;
                            if (!empty($data['DocItems'])) {
                                foreach ($data['DocItems'] as $docItem => $item) {
                                    if (!empty($item['incoming_price']) && !empty($item['quantity']) && !empty($docId)) {
                                        $modelDocItem = new DocItems();
                                        $amount = $item['quantity'];
                                        $modelDocItem->setAttributes([
                                            'product_id' => $item['product_id'],
                                            'quantity' => $amount ,
                                            'dep_area_id' => DepArea::getDepAre(),
                                            'measurement_id' => $item['measurement_id'],
                                            'incoming_price' => $item['incoming_price'],
                                            'currency' => $model::CURRENSY_SUM,
                                            'doc_id' => $docId,
                                        ]);
                                        if ($modelDocItem->save()) {
                                            $saved = true;
                                        } else {
                                            $saved = false;
                                            break;
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                            if ($saved) {
                                $transaction->commit();
                                Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                                return $this->redirect(['view', 'id' =>  $newModel->id, 'slug' => $this->slug]);
                            } else {
                                $transaction->rollBack();

                                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi!'));
                            }
                        }
                    } catch (Exception $e) {
                        Yii::info('Error Document ' . $e->getMessage(), 'save');
                    }
                }
            }

        }
        if ($model !== null) {
            $model->reg_date = date("d.m.Y");
        }
        return $this->render('create', [
            'model' => $model,
            'modelItem' => $modelItem,
        ]);
    }
    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $slug = Yii::$app->request->get('slug');
        $isDeleted = false;
        $model = $this->findModel($id);
        try {
            if (Doc::updateAll(['status' => $model::STATUS_DELETED], ['=', 'id', $id])) {
                $isDeleted = true;
            }
            if ($isDeleted) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (Exception $e) {
            Yii::info('Not saved' . $e, 'save');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if ($isDeleted) {
                $response['status'] = 0;
                $response['message'] = Yii::t('app', 'Deleted Successfully');
            }
            return $response;
        }
        if ($isDeleted) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted Successfully'));
            return $this->redirect(['index', 'slug' => $slug]);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
        return $this->refresh();
    }

    public function actionExportExcel()
    {
        header('Content-Type: application/vnd.ms-excel');
        $filename = "doc_" . date("d-m-Y-His") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        Excel::export([
            'models' => Doc::find()->select([
                'id',
            ])->all(),
            'columns' => [
                'id',
            ],
            'headers' => [
                'id' => 'Id',
            ],
            'autoSize' => true,
        ]);
    }

    /**
     * Finds the Doc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Doc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionDocTypeReturnAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');
        $response = [];
        $response['status'] = false;
        $response['message'] = "Bunday malumot yoq";
        if (isset($id)) {
//            $employe = Doc::getEmployes($id, $type);
            $response['status'] = true;
            $response['message'] = "Assalomu alaykum akaxonim";

//            $response['items'] = $employe;
        }
        return $response;
    }

    public function actionAjaxInvoice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response['status'] = 'false';
        $response['message'] = "Bunday malumot yoq";
        $id = Yii::$app->request->post('id');
        if (is_numeric($id)) {
            $result = Doc::find()
                ->where(['invoice_number' => $id,'type' => Doc::DOC_TYPE_SELL])
                ->asArray()->all();
            if (!empty($result)) {
                $response['status'] = 'true';
                $response['message'] = "Malumot qaytdi";
            }
        }
        return $response;
    }
}
