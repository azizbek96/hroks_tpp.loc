<?php

namespace backend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ConnectionTypesController implements the CRUD actions for ConnectionTypes model.
 */
class BaseCustomController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['login','error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => (!empty(Yii::$app->user->identity) && Yii::$app->user->identity->type == 1) ? true : false,
                        'roles' => ['@'],
                    ],
                    'denyCallback' => function ($rule, $action) {
                        $this->redirect(['/sub/index?q=deny']);
                    }
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public static function isAdmin(){
        return Yii::$app->user->identity->type == User::ROLE_ADMIN;
    }
    public static function isModerator(){
        return Yii::$app->user->identity->type == User::ROLE_MODER;
    }
    public static function isCommonUser(){
        return Yii::$app->user->identity->type == User::ROLE_USER;
    }

    public static function getCurrentUserId(){
        return Yii::$app->user->id;
    }
}
