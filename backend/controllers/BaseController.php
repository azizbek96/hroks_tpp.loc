<?php

namespace backend\controllers;

use backend\modules\admin\models\AuthAssignment;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/**
 * ConnectionTypesController implements the CRUD actions for ConnectionTypes model.
 */
class BaseController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws Yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {

        if (Yii::$app->authManager->getPermission(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
            if (!Yii::$app->user->can(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
                throw new ForbiddenHttpException(Yii::t('app', 'Sizga ushbu saxifaga kirishga ruxsat berilmagan'));
            }
        }

        if (!empty(Yii::$app->user->id)) {
            $permission = AuthAssignment::findOne(['user_id' => Yii::$app->user->id]);
            if (($permission !== null) && $permission['item_name'] === 'Tarqatuvchi') {
                $this->layout = 'index.php';
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['login','error'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
//    public function beforeAction($action)
//    {
//        if (Yii::$app->authManager->getPermission(Yii::$app->controller->id."/".Yii::$app->controller->action->id)&&Yii::$app->controller->action->id!='preview') {
//            if (!P::can(Yii::$app->controller->id . "/" . Yii::$app->controller->action->id)) {
//                throw new ForbiddenHttpException(Yii::t('app', 'Access denied'));
//            }
//        }
//        return parent::beforeAction($action);
//    }
    /**
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function actionTarqatuvchi(){
        $permission =  AuthAssignment::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['item_name' => 'Tarqatuvchi'])
            ->asArray()->all();

        if (!empty($permission)) {
            return true;
        }

        return false;
    }

    public function actionDeleteAll($model)
    {
        $response['status'] = 'false';
        $response['error'] = Yii::t('app', 'Error Deleting Data.');
        $response['error_one'] = Yii::t('app', 'Deleted Error!');

        $response['saved'] = Yii::t('app', 'Data Deleted Successfully.');
        $response['saved_one'] = Yii::t('app', 'Deleted!');
        if (!empty($model)) {
            $model->status = \backend\models\User::STATUS_DELETED;
        }
        if ($model->save()) {
            $response['status'] = 'true';
        }
        return $response;
    }

    public static function isAdmin(){
        return Yii::$app->user->identity->type == User::ROLE_ADMIN;
    }
    public static function isModerator(){
        return Yii::$app->user->identity->type == User::ROLE_MODER;
    }
    public static function isCommonUser(){
        return Yii::$app->user->identity->type == User::ROLE_USER;
    }

}
