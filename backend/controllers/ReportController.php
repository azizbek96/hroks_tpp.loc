<?php

namespace backend\controllers;

use backend\models\Report;
use backend\models\ReportSearch;
use Yii;
use backend\models\Transaction;
use backend\models\TransactionSearch;
use backend\controllers\BaseController;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TransactionController implements the CRUD actions for Transaction model.
 */
class ReportController extends BaseController
{

    /**
     * Lists all Transaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Transaction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Transaction();
        $users_id = Yii::$app->request->get('user_id');
        $model->user_id = $users_id;
        $model->reg_date = date("d.m.Y");
        $model->type = Transaction::T_EMPLOYEE_PAID;

        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $user_id = $users_id;
            $amount = $data['Transaction']['amount'];
            $date = strtotime($data['Transaction']['reg_date']);
            $add_info = $data['Transaction']['add_info'];
            if ($amount < 0) {
                $amount *= (-1);
            }
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $tran = Transaction::find()
                    ->where(['status' => $model::STATUS_ACTIVE, 'user_id' => $user_id, 'type' => $model::T_EMPLOYEE_PAID])
                    ->andWhere(['is','customer_id', null])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()
                    ->one();
                if (!empty($tran)) {
                    $tranInventory = $tran['inventory'] * 1 + $amount;
                    $model->setAttributes([
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $tranInventory,
                        'reg_date' => $date,
                        'type' => $model::T_EMPLOYEE_PAID,
                        'status' => $model::STATUS_ACTIVE,
                        'add_info' => $add_info
                    ]);
                } else {
                    $model->setAttributes([
                        'user_id' => $user_id,
                        'amount' => $amount,
                        'inventory' => $amount,
                        'reg_date' => $date,
                        'type' => $model::T_EMPLOYEE_PAID,
                        'status' => $model::STATUS_ACTIVE,
                        'add_info' => $add_info
                    ]);
                }

                if ($model->save()) {
                    $saved = true;
                }
                if ($saved) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                    return $this->redirect(['index']);
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                }
            } catch (Exception $exception) {
                Yii::info('Not saved' . $exception->getMessage(), 'save');
            }


        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     * @throws yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $users_id = $model->user_id;
        $model->reg_date = date('d.m.Y', $model->reg_date);
        $model->type = Transaction::T_EMPLOYEE_PAID;
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $amount = $data['Transaction']['amount'];
            $date = strtotime($data['Transaction']['reg_date']);
            $add_info = $data['Transaction']['add_info'];
            if ($amount < 0) {
                $amount *= (-1);
            }
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $tran = Transaction::find()
                    ->where(['user_id' => $users_id])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()
                    ->one();
                if (!empty($tran)) {
                    $tranInventory = ($tran['inventory'] - $tran['amount']) + $amount;
                    $model->setAttributes([
                        'user_id' => $users_id,
                        'amount' => $amount,
                        'inventory' => $tranInventory,
                        'type' => $model::T_EMPLOYEE_PAID,
                        'reg_date' => $date,
                        'status' => $model::STATUS_ACTIVE,
                        'add_info' => $add_info
                    ]);
                } else {
                    $model->setAttributes([
                        'user_id' => $users_id,
                        'amount' => $amount,
                        'inventory' => $amount,
                        'type' => $model::T_EMPLOYEE_PAID,
                        'reg_date' => $date,
                        'status' => $model::STATUS_ACTIVE,
                        'add_info' => $add_info
                    ]);
                }

                if ($model->save()) {
                    $saved = true;
                }
                if ($saved) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Saved Successfully'));
                    return $this->redirect(['report/list-paid']);
                } else {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('danger', Yii::t('app', 'Ma`lumot saqlanmadi'));
                }
            } catch (Exception $exception) {
                Yii::info('Not saved' . $exception->getMessage(), 'save');
            }


        }
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = false;
        $model = $this->findModel($id);
        try {
            if (Transaction::updateAll(['status' => $model::STATUS_DELETED], ['=', 'id', $id])) {
                $isDeleted = true;
            }
            if ($isDeleted) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            Yii::info('Not saved' . $e, 'save');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if ($isDeleted) {
                $response['status'] = 0;
                $response['message'] = Yii::t('app', 'Deleted Successfully');
            }
            return $response;
        }
        if ($isDeleted) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted Successfully'));
            return $this->redirect(['report/list-paid']);
        }

        Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
        return $this->redirect(['report/list-paid']);
    }

    public function actionExportExcel()
    {
        header('Content-Type: application/vnd.ms-excel');
        $filename = "transaction_" . date("d-m-Y-His") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        \moonland\phpexcel\Excel::export([
            'models' => Transaction::find()->select([
                'id',
            ])->all(),
            'columns' => [
                'id',
            ],
            'headers' => [
                'id' => 'Id',
            ],
            'autoSize' => true,
        ]);
    }

    /**
     * Finds the Transaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transaction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $d = Yii::$app->request->get('q');
        if (!empty($d)) {
            $data = Transaction::find()
                ->where([
                    'status' => Transaction::STATUS_ACTIVE,
                    'type' => Transaction::T_EMPLOYEE_DEPT,
                    'user_id' => $d
                ])->orderBy(['id' => SORT_DESC])->asArray()->all();
            $paid = Transaction::find()
                ->select('sum(amount) as amount')
                ->where([
                    'status' => Transaction::STATUS_ACTIVE,
                    'type' => Transaction::T_EMPLOYEE_PAID,
                    'user_id' => $d,
                ])->orderBy(['id' => SORT_DESC])->asArray()->all();
            $user_id = $d;
            $report = Report::getReport($user_id);
            if (!empty($data) || !empty($paid)) {
                $response['status'] = true;
                $response['dept'] = !empty($data) ? $data : 0;
                $response['paid'] = !empty($paid) ? $paid : 0;
                $response['days_dept'] = $report['dept_days'] != null ? $report['dept_days'] : 0;
                $response['days_paid'] = $report['paid_days'] != null ? $report['paid_days'] : 0;
                $response['month_paid'] = $report['month_paid'] != null ? $report['month_paid'] : 0;
                $response['month_dept'] = $report['month_dept'] != null ? $report['month_dept'] : 0;
                $response['message'] = 'OK';
            } else {
                $response['message'] = 'Empty';
                $response['dept'] = 0;
                $response['paid'] = 0;
            }
        }
        return $response;
    }

    public function actionListPaid()
    {
        $searchModel = new TransactionSearch();
        $status = Transaction::T_EMPLOYEE_PAID;
        $dataProvider = $searchModel->search($status);

        return $this->render('list_paid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionHaveProduct()
    {
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->searchItemBalance(1);
        return $this->render('have_product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFinishedProduct()
    {
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->searchItemBalance(0);
        return $this->render('finished_product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKirimReport()
    {
        return $this->render('kirim_report');
    }

    public function actionKirimAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $p = Yii::$app->request->get('product');
        $start = Yii::$app->request->get('start');
        $end = Yii::$app->request->get('end');
        $c = Yii::$app->request->get('contragent');
        if (!empty($start) && !empty($end)) {
            $data = Report::getKirimAjax($p, $c,$start, $end);
            if (!empty($data)) {
                $response['status'] = true;
                $response['message'] = 'OK';
                $response['data'] = $data;
                $response['start'] = $start;
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }

    public function actionKirmReportBoss()
    {
        return $this->render('kirm_report_boss');
    }
    public function actionKirimAjaxBoss()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $p = Yii::$app->request->get('product');
        $start = Yii::$app->request->get('start');
        $end = Yii::$app->request->get('end');
        $c = Yii::$app->request->get('contragent');
        if (!empty($start) && !empty($end)) {
            $data = Report::getKirimAjaxBoss($p, $c,$start, $end);
            if (!empty($data)) {
                $response['status'] = true;
                $response['message'] = 'OK';
                $response['data'] = $data;
                $response['start'] = $start;
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }
    public function actionReturnBossReport()
    {
        return $this->render('return_boss_report');
    }
    public function actionReturnBossAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $p = Yii::$app->request->get('product');
        $start = Yii::$app->request->get('start');
        $end = Yii::$app->request->get('end');
        $c = Yii::$app->request->get('contragent');
        if (!empty($start) && !empty($end)) {
            $data = Report::getReturnAjaxBoss($p, $c,$start, $end);
            if (!empty($data)) {
                $response['status'] = true;
                $response['message'] = 'OK';
                $response['data'] = $data;
                $response['start'] = $start;
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }
    public function actionReportHisobdanChiqarish()
    {
        return $this->render('report_hisobdan_chiqarish');
    }
    public function actionReportHisobdanChiqarishAjax()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $p = Yii::$app->request->get('product');
        $start = Yii::$app->request->get('start');
        $end = Yii::$app->request->get('end');
        $c = Yii::$app->request->get('contragent');
        if (!empty($start) && !empty($end)) {
            $data = Report::getReportHisobdanChiqarish($p, $c,$start, $end);
            if (!empty($data)) {
                $response['status'] = true;
                $response['message'] = 'OK';
                $response['data'] = $data;
                $response['start'] = $start;
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }
//    public function actionBoss()
//    {
//        return $this->render('boss');
//    }
    public function actionContragentReportBoss(){
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->searchContragentBoss();
        return $this->render('contragent_report_boss', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionMijozAgent(){
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->searchMijozlardanAgentlarningHaqqi();
        return $this->render('mijoz_agent', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionAgentdagiMahsulotlar(){
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->searchAgentdagiMahsulotlar();
        return $this->render('agentdagi_maxsulotlar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionReportAgentAllSell(){
        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->searchReportAgentAllSell();
        return $this->render('report_agent_all_sell',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
