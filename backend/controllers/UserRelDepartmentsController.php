<?php

namespace backend\controllers;

use Yii;
use backend\models\UserRelDepartments;
use backend\models\UserRelDepartmentsSearch;
use backend\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * UserRelDepartmentsController implements the CRUD actions for UserRelDepartments model.
 */
class UserRelDepartmentsController extends BaseController
{


    /**
     * Lists all UserRelDepartments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserRelDepartmentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserRelDepartments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserRelDepartments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserRelDepartments();
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            if ($model->load($data)) {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $isAllSaved = false;
                $user_id = $data['UserRelDepartments']['user_id'];
                $status = $data['UserRelDepartments']['status'];
                try {

                    if (!empty($data['UserRelDepartments']['in'])) {
                        foreach ($data['UserRelDepartments']['in'] as $departmentId) {
                            $modelLoop = new UserRelDepartments();
                            $modelLoop->setAttributes([
                                'user_id' => $user_id,
                                'department_id' => $departmentId,
                                'type' => $model::RECIVING,
                                'status' => $status
                            ]);
                            if ($modelLoop->save()) {
                                $isAllSaved = true;
                            } else {
                                $isAllSaved = false;
                                break;
                            }

                        }

                    }
                    if (!empty($data['UserRelDepartments']['out'])) {
                        foreach ($data['UserRelDepartments']['out'] as $departmentId) {
                            $modelLoop = new UserRelDepartments();
                            $modelLoop->setAttributes([
                                'user_id' => $user_id,
                                'department_id' => $departmentId,
                                'type' => $model::OUTGOING,
                                'status' => $status
                            ]);
                            if ($modelLoop->save()) {
                                $isAllSaved = true;
                            } else {
                                $isAllSaved = false;
                                break;
                            }

                        }
                    }
                    if ($isAllSaved) {
                        $saved = true;
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['index']);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UserRelDepartments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            if ($model->load($data)) {
                $transaction = Yii::$app->db->beginTransaction();
                $saved = false;
                $isAllSaved = false;
                $user_id = $data['UserRelDepartments']['user_id'];
                $status = $data['UserRelDepartments']['status'];
                try {
                    UserRelDepartments::deleteAll(['user_id' => $user_id]);
                    if (!empty($data['UserRelDepartments']['in'])) {
                        foreach ($data['UserRelDepartments']['in'] as $departmentId) {
                            $modelLoop = new UserRelDepartments();
                            $modelLoop->setAttributes([
                                'user_id' => $user_id,
                                'department_id' => $departmentId,
                                'type' => $model::RECIVING,
                                'status' => $status
                            ]);
                            if ($modelLoop->save()) {
                                $isAllSaved = true;
                            } else {
                                $isAllSaved = false;
                                break;
                            }

                        }

                    }
                    if (!empty($data['UserRelDepartments']['out'])) {
                        foreach ($data['UserRelDepartments']['out'] as $departmentId) {
                            $modelLoop = new UserRelDepartments();
                            $modelLoop->setAttributes([
                                'user_id' => $user_id,
                                'department_id' => $departmentId,
                                'type' => $model::OUTGOING,
                                'status' => $status
                            ]);
                            if ($modelLoop->save()) {
                                $isAllSaved = true;
                            } else {
                                $isAllSaved = false;
                                break;
                            }

                        }
                    }
                    if ($isAllSaved) {
                        $saved = true;
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['index']);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = true;
        $model = $this->findModel($id);
        try {
            if (UserRelDepartments::updateAll(['status' => $model::STATUS_DELETED], ['=', 'user_id', $model->user_id])) {
                $isDeleted = true;
            }
            if ($isDeleted) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            Yii::info('Not saved' . $e, 'save');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if ($isDeleted) {
                $response['status'] = 0;
                $response['message'] = Yii::t('app', 'Deleted Successfully');
            }
            return $response;
        }
        if ($isDeleted) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted Successfully'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionExportExcel()
    {
        header('Content-Type: application/vnd.ms-excel');
        $filename = "user-rel-departments_" . date("d-m-Y-His") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        \moonland\phpexcel\Excel::export([
            'models' => UserRelDepartments::find()->select([
                'id',
            ])->all(),
            'columns' => [
                'id',
            ],
            'headers' => [
                'id' => 'Id',
            ],
            'autoSize' => true,
        ]);
    }

    /**
     * Finds the UserRelDepartments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserRelDepartments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserRelDepartments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
