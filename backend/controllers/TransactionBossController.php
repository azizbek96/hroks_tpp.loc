<?php

namespace backend\controllers;

use Yii;
use backend\models\TransactionBoss;
use backend\models\TransactionBossSearch;
use backend\controllers\BaseController;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TransactionBossController implements the CRUD actions for TransactionBoss model.
 */
class TransactionBossController extends BaseController
{


    /**
     * Lists all TransactionBoss models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransactionBossSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TransactionBoss model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TransactionBoss model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    public function actionCreate()
    {
        $model = new TransactionBoss();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();

                $amount = $data['TransactionBoss']['amount'];
                $customers = $data['TransactionBoss']['customer_id'];
                $add_info = $data['TransactionBoss']['add_info'];
                if ($amount < 0) {
                    $amount *= (-1);
                }
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $saved = false;
                    $tran = TransactionBoss::find()
                        ->where(['status' => $model::STATUS_ACTIVE, 'customer_id' => $customers])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] * 1 + $amount;
                        $model->setAttributes([
                            'amount' => $amount,
                            'inventory' => $tranInventory,
                            'type' => $model::T_SUPPLIER_BOSS_PAID,
                            'status' => $model::STATUS_ACTIVE,
                             'add_info' => $add_info,
                        ]);
                    } else {
                        $model->setAttributes([
                            'amount' => $amount,
                            'inventory' => $amount,
                            'type' => $model::T_SUPPLIER_BOSS_PAID,
                            'status' => $model::STATUS_ACTIVE,
                            'add_info' => $add_info,
                        ]);
                    }

                    if ($model->save()) {
                        $saved = true;
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TransactionBoss model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public
    function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                $data = Yii::$app->request->post();

                $amount = $data['TransactionBoss']['amount'];
                $customers = $data['TransactionBoss']['customer_id'];
                $add_info = $data['TransactionBoss']['add_info'];
                if ($amount < 0) {
                    $amount *= (-1);
                }
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $saved = false;
                    $tran = TransactionBoss::find()
                        ->where(['status' => $model::STATUS_ACTIVE, 'customer_id' => $customers])
                        ->orderBy(['id' => SORT_DESC])
                        ->asArray()
                        ->one();
                    if (!empty($tran)) {
                        $tranInventory = $tran['inventory'] * 1 - $amount;
                        $model->setAttributes([
                            'amount' => $amount,
                            'inventory' => $tranInventory,
                            'type' => $model::T_SUPPLIER_BOSS_PAID,
                            'status' => $model::STATUS_ACTIVE,
                            'add_info' => $add_info,
                        ]);
                    } else {
                        $model->setAttributes([
                            'amount' => $amount,
                            'inventory' => $amount,
                            'type' => $model::T_SUPPLIER_BOSS_PAID,
                            'status' => $model::STATUS_ACTIVE,
                            'add_info' => $add_info,
                        ]);
                    }

                    if ($model->save()) {
                        $saved = true;
                    } else {
                        $saved = false;
                    }
                    if ($saved) {
                        $transaction->commit();
                    } else {
                        $transaction->rollBack();
                    }
                } catch (\Exception $e) {
                    Yii::info('Not saved' . $e, 'save');
                    $transaction->rollBack();
                }
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    $response = [];
                    if ($saved) {
                        $response['status'] = 0;
                        $response['message'] = Yii::t('app', 'Saved Successfully');
                    } else {
                        $response['status'] = 1;
                        $response['errors'] = $model->getErrors();
                        $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                    }
                    return $response;
                }
                if ($saved) {
                    return $this->redirect(['index']);
                }
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TransactionBoss model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = false;
        $model = $this->findModel($id);
        try {
            if (TransactionBoss::updateAll(['status' => $model::STATUS_DELETED], ['=', 'id', $id])) {
                $isDeleted = true;
            }
            if ($isDeleted) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (\Exception $e) {
            Yii::info('Not saved' . $e, 'save');
        }
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if ($isDeleted) {
                $response['status'] = 0;
                $response['message'] = Yii::t('app', 'Deleted Successfully');
            }
            return $response;
        }
        if ($isDeleted) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted Successfully'));
            return $this->redirect(['index']);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public
    function actionExportExcel()
    {
        header('Content-Type: application/vnd.ms-excel');
        $filename = "transaction-boss_" . date("d-m-Y-His") . ".xls";
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        \moonland\phpexcel\Excel::export([
            'models' => TransactionBoss::find()->select([
                'id',
            ])->all(),
            'columns' => [
                'id',
            ],
            'headers' => [
                'id' => 'Id',
            ],
            'autoSize' => true,
        ]);
    }

    /**
     * Finds the TransactionBoss model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TransactionBoss the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = TransactionBoss::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
