<?php

namespace backend\controllers;

use Yii;
use backend\models\Pricing;
use backend\models\PricingSearch;
use backend\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PricingController implements the CRUD actions for Pricing model.
 */
class PricingController extends BaseController
{


    /**
     * Lists all Pricing models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PricingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pricing model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', [
                'model' => $this->findModel($id),
            ]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionCreate()
    {
        $model = [new Pricing()];
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $saved = false;
            try {
                $transaction = Yii::$app->db->beginTransaction();
                foreach ($data['Pricing'] as $priItem => $item) {
                        Pricing::updateAll(['status' => Pricing::STATUS_INACTIVE],
                            ['and',['=','product_id', $item['product_id']],['!=','status', 4]]);
                        $modelPricing = new Pricing();
                        $modelPricing->setAttributes([
                            'product_id' => $item['product_id'],
                            'price' => $item['price'],
                            'status' => Pricing::STATUS_ACTIVE,
                        ]);
                    if ($modelPricing->save()) {
                        $saved = true;
                    } else {
                        $saved = false;
                        break;
                    }
                }

                if($saved) {
                    $transaction->commit();
                }else{
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                Yii::info('Not saved' . $e, 'save');
                $transaction->rollBack();
            }
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $response = [];
                if ($saved) {
                    $response['status'] = 0;
                    $response['message'] = Yii::t('app', 'Saved Successfully');
                } else {
                    $response['status'] = 1;
                    $response['errors'] = $model->getErrors();
                    $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                }
                return $response;
            }
            if ($saved) {
                return $this->redirect(['index']);
            }
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pricing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = [$this->findModel($id)];
        $update = $this->findModel($id);
        if (Yii::$app->request->isPost) {
            $data = Yii::$app->request->post();
            $transaction = Yii::$app->db->beginTransaction();
            $saved = false;
            try {
                foreach ($data['Pricing'] as $priItem => $item) {
                        $update->setAttributes([
                            'product_id' => $item['product_id'],
                            'price' => $item['price'],
                        ]);
                    if ($update->save()) {
                        $saved = true;
                    } else {
                        $saved = false;
                        break;
                    }
                }
                if ($saved) {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
            } catch (\Exception $e) {
                Yii::info('Not saved' . $e, 'save');
                $transaction->rollBack();
            }
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $response = [];
                if ($saved) {
                    $response['status'] = 0;
                    $response['message'] = Yii::t('app', 'Saved Successfully');
                } else {
                    $response['status'] = 1;
                    $response['errors'] = $update->getErrors();
                    $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
                }
                return $response;
            }
            if ($saved) {
                return $this->redirect(['view', 'id' => $update->id]);
            }

        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
                'update' => $update,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'update' => $update,
        ]);
    }

    /**
     * @param $id
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        $isDeleted = false;
        $model = $this->findModel($id);
        try {
            if(Pricing::updateAll(['status' => $model::STATUS_DELETED], ['=', 'id', $id])){
                $isDeleted = true;
            }
            if($isDeleted){
                $transaction->commit();
            }else{
                $transaction->rollBack();
            }
        }catch (\Exception $e){
            Yii::info('Not saved' . $e, 'save');
        }
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $response = [];
            $response['status'] = 1;
            $response['message'] = Yii::t('app', 'Hatolik yuz berdi');
            if($isDeleted){
                $response['status'] = 0;
                $response['message'] = Yii::t('app','Deleted Successfully');
            }
            return $response;
        }
        if($isDeleted){
            Yii::$app->session->setFlash('success',Yii::t('app','Deleted Successfully'));
            return $this->redirect(['index']);
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Hatolik yuz berdi'));
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionExportExcel(){
        header('Content-Type: application/vnd.ms-excel');
        $filename = "pricing_".date("d-m-Y-His").".xls";
        header('Content-Disposition: attachment;filename='.$filename .' ');
        header('Cache-Control: max-age=0');
        \moonland\phpexcel\Excel::export([
            'models' => Pricing::find()->select([
                'id',
            ])->all(),
            'columns' => [
                'id',
            ],
            'headers' => [
                'id' => 'Id',
            ],
            'autoSize' => true,
        ]);
    }
    /**
     * Finds the Pricing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pricing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pricing::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionPrice()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $d = Yii::$app->request->get('id');
        $department = Yii::$app->request->get('department');
        if (!empty($d)) {
            $data = Pricing::find()
                ->where(['status' => Pricing::STATUS_ACTIVE])
                ->andWhere(['product_id' => $d])
                ->asArray()
                ->one();
            $sql = "select
                            inventory
                        from item_balance
                        where product_id = %d and department_id = %d order by id desc limit 1";
            $inventory = Yii::$app->db->createCommand(sprintf($sql, $d, $department))->queryOne();
            if (!empty($data) &&  !empty($inventory)) {
                $response['status'] = true;
                $response['data'] = $data['price'];
                $response['inventory'] = $inventory['inventory'];
                $response['message'] = 'OK';
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }
    public function actionPriceItemBalanceInventory()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response = [];
        $response['status'] = false;
        $response['message'] = 'Error';
        $d = Yii::$app->request->get('id');
        $department = Yii::$app->request->get('department');
        if (!empty($d)) {
            $data = Pricing::find()
                ->where(['status' => Pricing::STATUS_ACTIVE])
                ->andWhere(['product_id' => $d])
                ->asArray()
                ->one();
            $sql = "select
                            inventory,
                            karopka_count
                        from item_balance
                        where product_id = %d and department_id = %d order by id desc limit 1";
            $inventory = Yii::$app->db->createCommand(sprintf($sql, $d, $department))->queryOne();
            if (!empty($data) &&  !empty($inventory)) {
                $response['status'] = true;
                $response['data'] = $data['price'];
                $response['inventory'] = $inventory['inventory'];
                $response['karopka_count'] = $inventory['karopka_count'];
                $response['message'] = 'OK';
            } else {
                $response['message'] = 'Empty';
            }
        }
        return $response;
    }
}
