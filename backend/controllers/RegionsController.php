<?php

namespace backend\controllers;

use backend\models\Regions;
use backend\models\RegionsSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * RegionsController implements the CRUD actions for Regions model.
 */
class RegionsController extends BaseController
{
    public function actionValidate()
    {
        $model = new Regions();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Lists all Regions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $regions_tree = Regions::getRegionTreeViewHtmlForm();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'regions_tree' => $regions_tree
        ]);
    }

    /**
     * Displays a single Regions model.
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Regions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param null $parent
     * @return mixed
     */
    public function actionCreate($parent = NULL)
    {

        $model = new Regions();
        if ($model->load(Yii::$app->request->post())) {
            $model->parent_id = $parent;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data Saved!'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Error Saved!'));
            }
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Regions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Data Updated!'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Error Updated!'));
            }
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Regions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete(int $id)
    {
        $model = $this->findModel($id);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $response['status'] = 'false';
        $response['error'] = Yii::t('app', 'Error Deleting Data.');
        $response['error_one'] = Yii::t('app', 'Deleted Error!');

        $response['saved'] = Yii::t('app', 'Data Deleted Successfully.');
        $response['saved_one'] = Yii::t('app', 'Deleted!');
        if (empty(Regions::find()->where(['parent_id' => $model->id])->andWhere(['status' => Regions::STATUS_ACTIVE])->all())) {
            $model->status = Regions::STATUS_DELETED;
            if ($model->save()) {
                $response['status'] = 'true';
            }
        } else {
            $response['error'] = Yii::t('app', 'The element has a parent. Cannot be delete!');
        }
        return $response;
    }

    /**
     * Finds the Regions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Regions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id)
    {
        if (($model = Regions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
