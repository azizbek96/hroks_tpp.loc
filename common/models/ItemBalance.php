<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "item_balance".
 *
 * @property int $id
 * @property int $product_id
 * @property string $count
 * @property string $inventory
 * @property int $doc_id
 * @property int $doc_type
 * @property int $entity_type
 * @property int $customer_id
 * @property int $measurement_id
 * @property int $department_id
 * @property int $dep_area_id
 * @property int $currency
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Customers $customer
 * @property DepArea $depArea
 * @property Departments $department
 * @property Measurements $measurement
 * @property Products $product
 * @property int $doc_item_id [int]
 * @property string $karopka_count [decimal(20,3)]
 * @property string $karopka_inventory [decimal(20,3)]
 */
class ItemBalance extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'doc_id', 'doc_type', 'entity_type', 'customer_id', 'measurement_id', 'department_id', 'dep_area_id', 'currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['count', 'inventory','karopka_count','karopka_inventory'], 'number'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['dep_area_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepArea::className(), 'targetAttribute' => ['dep_area_id' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['measurement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Measurements::className(), 'targetAttribute' => ['measurement_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'count' => Yii::t('app', 'Count'),
            'inventory' => Yii::t('app', 'Inventory'),
            'doc_id' => Yii::t('app', 'Doc ID'),
            'doc_type' => Yii::t('app', 'Doc Type'),
            'entity_type' => Yii::t('app', 'Entity Type'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'measurement_id' => Yii::t('app', 'Measurement ID'),
            'department_id' => Yii::t('app', 'Department ID'),
            'dep_area_id' => Yii::t('app', 'Dep Area ID'),
            'currency' => Yii::t('app', 'Currency'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepArea()
    {
        return $this->hasOne(DepArea::className(), ['id' => 'dep_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasurement()
    {
        return $this->hasOne(Measurements::className(), ['id' => 'measurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
