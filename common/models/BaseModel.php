<?php

namespace common\models;

use backend\models\Transaction;
use backend\models\UserRelEmployee;
use backend\modules\admin\models\AuthAssignment;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use common\components\behaviors\OurCustomBehavior;
use yii\web\ForbiddenHttpException;

class
BaseModel extends ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_SAVED = 3;
    const STATUS_DELETED = 4;

    const CUSTOMERS_TYPE_SUPPLIER = 1;
    const CUSTOMERS_TYPE_CLIENT = 2;

    // Tarqatuvchi ning pullari
    const T_PAID = 1;
    const T_DEPT = 2;

//    Firmanig  pullari
    const T_CONTRAGENT_DEPT_IT = 3;
    const T_CONTRAGENT_PAID_WE = 4;

    // Ishchi bilan oldi berdi
    const T_EMPLOYEE_DEPT = 5;
    const T_EMPLOYEE_PAID = 6;

//    const T_TYPE_EMPLOYEE = 1;
//    const T_TYPE_EMPLOYEE_RETURN = 2;
//    const T_TYPE_STORE = 3;

//   mijozlarning oldi berdisi uchun bosga
    const T_SUPPLIER_BOSS_DEPT = 1;
    const T_SUPPLIER_BOSS_PAID = 2;


    const RECIVING = 1;
    const OUTGOING = 0;

    const CURRENSY_SUM = 1;
    const CURRENSY_USD = 2;
    const CURRENSY_RUB = 3;

    public $cp = [];
    public $currentEmployeeId = null;

    public function behaviors()
    {
        return [
            [
                'class' => OurCustomBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }

    public function afterValidate()
    {
        if ($this->hasErrors()) {
            $res = [
                'status' => 'error',
                'module' => 'backend',
                'table' => self::tableName() ?? '',
                'url' => \yii\helpers\Url::current([], true),
                'message' => $this->getErrors(),
                'data' => $this->toArray(),
            ];
            Yii::error($res, 'save');
        }
    }

    /**
     * @param null $key
     * @return array|mixed
     */
    public static function getStatusList($key = null)
    {
        $result = [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INACTIVE => Yii::t('app', 'Deleted'),
            self::STATUS_SAVED => Yii::t('app', 'Saved')
        ];
        if (!empty($key)) {
            return $result[$key];
        }
        return $result;
    }

    /**
     * @param null $index
     * @return string|string[]
     */
    public static function getRegion($index = null)
    {
        $list = [
            1 => 'Toshkent',
            2 => 'Andijon',
            3 => "Farg'ona",
            4 => "Namangan"
        ];
        if (!empty($index)) {
            return $list[$index];
        }
        return $list;
    }

    public static function getCustomerType($index = null)
    {
        $list = [
            1 => 'Yetkazib beruvchi',
            2 => 'Mijoz'
        ];
        if (!empty($index)) return $list[$index];
        return $list;
    }

    public function getCurrentEmployee()
    {
        $empId = UserRelEmployee::find()->where(['user_id' => Yii::$app->user->id])->asArray()->one();
        if (!empty($empId)) {
            $this->currentEmployeeId = $empId['employee_id'];
        }
    }

    /**
     * @param $number
     * @return int|string
     */
    public static function getNumberFormat($number)
    {

        if (!empty($number)) {
            return number_format($number, 0, '.', ' ');
        }
        return 0;
    }
    public static function getMeasurementValue($id){
        $model = Measurements::find()
            ->select('koefficient as miqdor')
            ->where(['!=','status',Transaction::STATUS_DELETED])
            ->andWhere(['id' => $id])->asArray()->one();
        if (!empty($model)){
        return $model['miqdor'] * 1;
        }
        throw new ForbiddenHttpException('O`lchov birligida hatolik mavjud');
    }
    public static function getMeasurementLabel($id){
        $label = '';
        $model = Measurements::find()
            ->where(['!=','status',Transaction::STATUS_DELETED])
            ->andWhere(['id' => $id])->asArray()->one();
        if (!empty($model)){
                $label = $model['koefficient']."(".$model['token'].")"." ".$model['name'];
            return $label;
        }
        return $label;
    }
    public static function getMeasurementLabelToken($id){
        $model = Measurements::find()
            ->select('token')
            ->where(['!=','status',Transaction::STATUS_DELETED])
            ->andWhere(['id' => $id])->asArray()->one();
        if (!empty($model)){
            return $model['token'] * 1;
        }
        throw new ForbiddenHttpException('O`lchov birligida hatolik mavjud');
    }
    public static function actionSuperAdmin(){
        $permission =  AuthAssignment::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['item_name' => 'super-admin'])
            ->asArray()->all();

        if (!empty($permission)) {
            return true;
        }

        return false;
    }
    public static function actionBoss(){
        $permission =  AuthAssignment::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['item_name' => 'Boss'])
            ->asArray()->all();

        if (!empty($permission)) {
            return true;
        }

        return false;
    }
}
