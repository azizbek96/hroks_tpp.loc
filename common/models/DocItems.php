<?php

namespace common\models;

use backend\models\Pricing;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doc_items".
 *
 * @property int $id
 * @property int $doc_id
 * @property int $product_id
 * @property int $dep_area_id
 * @property int $measurement_id
 * @property string $incoming_price
 * @property string $sold_price
 * @property int $currency
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 * @property float $quantity
 *
 * @property DepArea $depArea
 * @property Doc $doc
 * @property Measurements $measurement
 * @property Products $product
 */
class DocItems extends BaseModel
{
    public $summa;
    public $karopka_count;
    public $ret_quantity;
    public $ret_price;
    public $ret_measurement;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doc_items';
    }
    public $count;
    const SCENARIO_INCOMING = Doc::DOC_TYPE_INCOMING_LABEL;
    const SCENARIO_SELLING = Doc::DOC_TYPE_SELL_LABEL;
    const SCENARIO_RETURN = Doc::DOC_TYPE_RETURN_LABEL;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doc_id', 'product_id', 'dep_area_id', 'measurement_id', 'currency', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['incoming_price', 'sold_price','karopka_quantity', 'quantity','count'], 'number'],
            [['product_id', 'quantity', 'measurement_id','karopka_quantity'], 'required', 'on' => self::SCENARIO_SELLING],
            [['product_id', 'quantity', 'measurement_id','karopka_quantity'], 'required', 'on' => self::SCENARIO_INCOMING],
            [['product_id', 'quantity', 'measurement_id','karopka_quantity'], 'required', 'on' => self::SCENARIO_RETURN],
            [['dep_area_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepArea::className(), 'targetAttribute' => ['dep_area_id' => 'id']],
            [['doc_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doc::className(), 'targetAttribute' => ['doc_id' => 'id']],
            [['measurement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Measurements::className(), 'targetAttribute' => ['measurement_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }
//    public function scenarios()
//    {
//
//        return [
//            self::SCENARIO_INCOMING =>  ['reg_date', 'department_id','customer_id'],
//            self::SCENARIO_SELLING => ['reg_date', 'department_id'],
//        ];
//    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'doc_id' => Yii::t('app', 'Doc ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'dep_area_id' => Yii::t('app', 'Dep Area ID'),
            'measurement_id' => Yii::t('app', 'Measurement ID'),
            'incoming_price' => Yii::t('app', 'Incoming Price'),
            'sold_price' => Yii::t('app', 'Sold Price'),
            'currency' => Yii::t('app', 'Currency'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepArea()
    {
        return $this->hasOne(DepArea::className(), ['id' => 'dep_area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoc()
    {
        return $this->hasOne(Doc::className(), ['id' => 'doc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeasurement()
    {
        return $this->hasOne(Measurements::className(), ['id' => 'measurement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public static function getProductList()
    {
        $data = Products::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->orderBy(['name' => SORT_ASC])
            ->asArray()
            ->all();
        return ArrayHelper::map($data,'id','name');
    }

    /**
     * @return array
     */
    public static function getDepAreaList()
    {
        $data = DepArea::find()
            ->asArray()
            ->where(['status' => self::STATUS_ACTIVE])
            ->all();
        return ArrayHelper::map($data,'id','name');
    }
    public static function getMeasurementList()
    {
        $sql = Yii::$app->db->createCommand(sprintf("SELECT id,
       concat(name, '(', koefficient, ')') AS name FROM
           measurements WHERE status = %d and koefficient = 1", self::STATUS_ACTIVE))->queryAll();
        return ArrayHelper::map($sql, 'id', 'name');
    }
    public static function getMeasurementListBoss()
    {
        $sql = Yii::$app->db->createCommand(sprintf("SELECT id,
       concat(name, '(', koefficient, ')') AS name FROM
           measurements WHERE status = %d", self::STATUS_ACTIVE))->queryAll();
        return ArrayHelper::map($sql, 'id', 'name');
    }
    /**
     * @param $product_id
     * @param $department
     * @param $price_row
     * @param $quantity_row
     * @return bool
     * @throws \yii\db\Exception
     */
    public static function getHasQuantityAmount($product_id,$department, $price_row, $quantity_row){
        if (!empty($product_id) && !empty($department)){
            $data = Pricing::find()
                ->where(['status' => Pricing::STATUS_ACTIVE])
                ->andWhere(['product_id' => $product_id])
                ->asArray()
                ->one();
            $sql = "select
                        inventory
                    from item_balance
                    where product_id = %d and 
                          department_id = %d 
                    order by id desc limit 1";
            $in= Yii::$app->db->createCommand(sprintf($sql, $product_id, $department))->queryOne();
            $inventory = $in['inventory'];
            $price = $data['price'];
            if ($inventory >= $quantity_row && $price == $price_row){
                return true;
            }
        }
        return false;
    }

    public function getRemainCount(){
        $sql = "select inventory from item_balance
                        where product_id = %d and department_id = %d order by id desc limit 1";
        $inventory = Yii::$app->db->createCommand(sprintf($sql, $this->product_id, $this->doc->department_id))->queryScalar();
        if(!empty($inventory)) return $inventory;
        return  0;
    }

    public static function getItemOldQuantity($model_parent_id = null, $product_id = null){
        if (!empty($model_parent_id) && !empty($product_id))
        {
            $sql = "select di.quantity from doc d
                            left join doc_items di on di.doc_id = d.id
                    where d.id = %d and di.product_id =%d  and di.status = 1;";
            $result = Yii::$app->db->createCommand(sprintf($sql,$model_parent_id,$product_id))->queryOne();
            return $result['quantity'];
        }
    }
}
