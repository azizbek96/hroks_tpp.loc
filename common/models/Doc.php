<?php

namespace common\models;

use phpDocumentor\Reflection\Types\Self_;
use phpDocumentor\Reflection\Types\This;
use Yii;
use backend\models\Employee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "doc".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $department_id
 * @property string $reg_date
 * @property string $add_info
 * @property string $reg_date_save
 * @property int $type
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $created_at
 * @property int $updated_at
 * @property int $invoice_count
 * @property int $invoice_number
 * @property int $invoice_id
 *
 * @property Customers $customer
 * @property Departments $department
 * @property DocItems[] $docItems
 * @property int $parent_id [int]
 * @property int $employee_id [int]
 * @property string $doc_number [varchar(255)]
 * @property-read yii\db\ActiveQuery $employee
 * @property string $amount [decimal(20,3)]
 */
class Doc extends BaseModel
{
    public const DOC_TYPE_INCOMING = 1;//Qabul qilish
    public const DOC_TYPE_SELL = 2;//Sotish
    public const DOC_TYPE_RETURN = 3;//Qayta qabul qilib olish
    public const DOC_TYPE_OUTGOING = 4;//Spisaniya muddati o'tgan va ayniganlarni hisobdan chiqarish
    public const DOC_TYPE_GIFT = 5;//Ishchi xodimlarga, tanishlarga berish bekorga
    public const DOC_TYPE_ADJUSTMENT = 6; //QOldiqni togrilash
    public const DOC_TYPE_ACCEPTED = 7;// Boshqa bolimdan qabul qilib olish
    public const DOC_TYPE_EMPLOYEE_SELL = 8;// Sotish Employee
    public const DOC_TYPE_INCOMING_BOSS = 9;// Boss uchun qabu;
    public const DOC_TYPE_RETURN_BOSS = 10;// Boss uchun qabu;
    public const DOC_TYPE_RETURN_AGENT = 11;// Qatarish Employee;


    public const DOC_TYPE_INCOMING_LABEL = 'kirim';
    public const DOC_TYPE_SELL_LABEL = 'topshirish';
    public const DOC_TYPE_RETURN_LABEL = 'qayta_qabul';
    public const DOC_TYPE_OUTGOING_LABEL = 'hisobdan_chiqarish';
    public const DOC_TYPE_GIFT_LABEL = 'sovga';
    public const DOC_TYPE_ADJUSTMENT_LABEL = 'togrilash';
    public const DOC_TYPE_ACCEPTED_LABEL = 'qabul';
    public const DOC_TYPE_EMPLOYEE_SELL_LABEL = 'sotish_employee';
    public const DOC_TYPE_INCOMING_BOSS_LABEL = 'boss_kirim';
    public const DOC_TYPE_RETURN_BOSS_LABEL = 'boss_return';
    public const DOC_TYPE_RETURN_AGENT_LABEL = 'agent_return';

    const SCENARIO_INCOMING = self::DOC_TYPE_INCOMING_LABEL;
    const SCENARIO_SELLING = self::DOC_TYPE_SELL_LABEL;
    const SCENARIO_RETURN = self::DOC_TYPE_RETURN_LABEL;
    const SCENARIO_SHOP = self::DOC_TYPE_EMPLOYEE_SELL;
    const SCENARIO_BOSS = self::DOC_TYPE_INCOMING_BOSS_LABEL;
    const SCENARIO_RETURN_AGENT = self::DOC_TYPE_RETURN_AGENT_LABEL;
    const SCENARIO_OUTGOING = self::DOC_TYPE_OUTGOING_LABEL;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'invoice_number', 'invoice_id', 'invoice_count','employee_id', 'department_id', 'type', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['reg_date'], 'safe'],
            [['add_info', 'doc_number','reg_date_save'], 'string'],
            [['customer_id', 'department_id', 'reg_date'], 'required', 'on' => self::SCENARIO_INCOMING],
            [['department_id', 'reg_date', 'employee_id'], 'required', 'on' => self::SCENARIO_SELLING],
            [['department_id', 'reg_date', 'employee_id'], 'required', 'on' => self::SCENARIO_RETURN],
            [['department_id', 'reg_date', 'customer_id'], 'required', 'on' => self::SCENARIO_BOSS],
            [['reg_date', 'add_info','department_id'], 'required', 'on' => self::SCENARIO_OUTGOING],
            [['reg_date', 'customer_id','amount'], 'required', 'on' => self::SCENARIO_SHOP],
            [['reg_date', 'customer_id','add_info'], 'required', 'on' => self::SCENARIO_RETURN_AGENT],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departments::className(), 'targetAttribute' => ['department_id' => 'id']],
        ];
    }
//    public function scenarios()
//    {
//
//        return [
////            self::SCENARIO_SHOP =>  ['reg_date','customer_id'],
//
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'doc_number' => Yii::t('app', 'Hujjat Raqami'),
            'customer_id' => Yii::t('app', 'Customer'),
            'invoice_number' => Yii::t('app', 'Invoice Number'),
            'employee_id' => Yii::t('app', 'Ishchi'),
            'department_id' => Yii::t('app', 'Bo`lim'),
            'reg_date' => Yii::t('app', 'Sana'),
            'add_info' => Yii::t('app', 'Qo`shimcha ma`lumot'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $slug = Yii::$app->request->get('slug');
            if($this->status == self::STATUS_SAVED && $slug == self::DOC_TYPE_SELL_LABEL){
                $models = self::find()->where(['employee_id' => $this->employee_id])
                    ->andWhere(['type' => self::DOC_TYPE_SELL])
                    ->andWhere(['status' => self::STATUS_SAVED])
                    ->andWhere(['between', 'created_at', strtotime(date("Y-m-d 00:00:00")), strtotime(date("Y-m-d 23:59:59"))])
                    ->orderBy(['id' => SORT_DESC])
                    ->asArray()->one();

                if (!empty($models)){
                   $this->invoice_number = $models['invoice_number'];
                   $this->invoice_count = $models['invoice_count']*1 + 1;
                }else{
                    $result = self::find()
                        ->where(['is not','invoice_number' , null])
                        ->andWhere(['status' => self::STATUS_SAVED])
                        ->orderBy(['id' => SORT_DESC])->asArray()->one();
                    $this->invoice_number = !empty($result)? $result['invoice_number']*1+1 : 1;
                    $this->invoice_count = 1;
                }
            }
            $this->reg_date = date('Y-m-d', strtotime($this->reg_date));
            return true;
        }
        return false;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->reg_date = date('d.m.Y', strtotime($this->reg_date));
    }

    /**
     * @param null $key
     * @return array|mixed
     */
    public static function getDocTypeBySlug($key = null)
    {
        $result = [
            self::DOC_TYPE_INCOMING_LABEL => Yii::t('app', 'Kirim'),
            self::DOC_TYPE_SELL_LABEL => Yii::t('app', "Topshirish"),
            self::DOC_TYPE_RETURN_LABEL => Yii::t('app', "Qaytgan tovar qabul"),
            self::DOC_TYPE_OUTGOING_LABEL => Yii::t('app', "Hisobdan chiqarish"),
            self::DOC_TYPE_ADJUSTMENT_LABEL => Yii::t('app', "To'grilash"),
            self::DOC_TYPE_ACCEPTED_LABEL => Yii::t('app', "Qabul"),
            self::DOC_TYPE_EMPLOYEE_SELL_LABEL => Yii::t('app', "Sotish Employee"),
            self::DOC_TYPE_INCOMING_BOSS_LABEL => Yii::t('app', "Boss Kirim"),
            self::DOC_TYPE_RETURN_BOSS_LABEL => Yii::t('app', "Boss Return"),
            self::DOC_TYPE_RETURN_AGENT_LABEL => Yii::t('app', "Agent Return"),
        ];
        if ($key) {
            return $result[$key];
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Departments::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocItems()
    {
        return $this->hasMany(DocItems::className(), ['doc_id' => 'id']);
    }

    public static function getCustomerId()
    {
        $data = Customers::find()
            ->select(['id', 'name'])
            ->where(['status' => self::STATUS_ACTIVE])
            ->andWhere(['type' => self::CUSTOMERS_TYPE_SUPPLIER])
            ->asArray()
            ->all();
        return ArrayHelper::map($data, 'id', 'name');

    }

    public static function getEmployeeId()
    {
        $sql = "select
                    concat(e.fullname,'(',u.username,')') as name,
                    e.id
                from user_rel_employee
                    left join employee e on e.id = user_rel_employee.employee_id
                    left join user u on user_rel_employee.user_id = u.id
                    left join position p on e.position_id = p.id
                where user_rel_employee.status = %d and u.status = %d and  e.status = %d and p.token = 'AGENT'";
        $sql = sprintf($sql, self::STATUS_ACTIVE, self::STATUS_ACTIVE, self::STATUS_ACTIVE);
        $data = Yii::$app->db->createCommand($sql)->queryAll();
        return ArrayHelper::map($data, 'id', 'name');


    }

    public static function getDeparmentsId($type)
    {
        $sql = "select  d.name,
                        d.id
                        from user_rel_departments urd
                         left join departments d on d.id = urd.department_id
                         left join user u on urd.user_id = u.id
                        where u.id = %d
                        and urd.type = %d  and urd.status = %d
                        ";
        $data = Yii::$app->db->createCommand(sprintf($sql, Yii::$app->user->id, $type, self::STATUS_ACTIVE))->queryAll();
        return ArrayHelper::map($data, 'id', 'name');
    }

    /**
     * @param $empid
     * @return array|false|yii\db\DataReader
     * @throws yii\db\Exception
     */
    public static function getEmployeeUserId($empId)
    {
        $data = "select 
                       u.id
                from user_rel_employee
                         left join employee e on e.id = user_rel_employee.employee_id
                         left join user u on user_rel_employee.user_id = u.id
                where user_rel_employee.status = %d 
                      and  u.status = %d 
                      and  e.status = %d 
                      and user_rel_employee.employee_id = %d";
        $datas = Yii::$app->db->createCommand(sprintf($data, self::STATUS_ACTIVE, self::STATUS_ACTIVE, self::STATUS_ACTIVE, $empId))->queryOne();

        return $datas['id'];
    }
    public static function getDocItemsSql($employeeId,$invoice_id=null,$docId=null)
    {
        $active = self::STATUS_ACTIVE;
        $sql = "
        select
           p.id as product_id,
            p.name as product_name,
            di.measurement_id,
            m.name as measurement_name,
            ibe.inventory as quantity,
            di.currency,
            di.dep_area_id,
            di.sold_price ";
        if (!empty($invoice_id))
        {
            $sql.=" ,doc2.karopka_quantity ,doc2.quantity as quantity1 ";
        }

         $sql .="from item_balance_employee ibe
            left join doc d on ibe.doc_id = d.id
            left join doc_items di on d.id = di.doc_id
            left join products p on ibe.product_id = p.id
            left join measurements m on ibe.measurement_id = m.id ";
        if (!empty($invoice_id)&&!empty($docId)){
            $sql .="
            left join (select
                           di1.id,
                           di1.quantity,
                           di1.product_id,
                           di1.karopka_quantity
                       from doc d1
                                left join doc_items di1 on d1.id = di1.doc_id
                       where d1.invoice_id = {$invoice_id} and d1.id={$docId} and di1.status = {$active} and d1.invoice_number is null) as doc2 on doc2.product_id = p.id";
        }
        $sql .=" where ibe.status=%d and ibe.id IN (select MAX(ibe2.id) from item_balance_employee ibe2 where ibe2.employee_id=%d GROUP BY ibe2.product_id, ibe2.measurement_id)
        GROUP BY ibe.product_id, ibe.measurement_id;
        ";
        $sql = sprintf($sql,$active,$employeeId);
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        return $result;
    }
    public static function getDeparmentsBoss()
    {
        $sql = "SELECT * FROM departments d WHERE d.status = %d";
        $data = Yii::$app->db->createCommand(sprintf($sql, self::STATUS_ACTIVE))->queryAll();
        return ArrayHelper::map($data, 'id', 'name');

    }

    public static function getShopSellDate($costumer_id)
    {
        $sql = "
        SELECT 
               d.reg_date,
               d.reg_date as id
        FROM doc d
        WHERE d.customer_id = %d and d.created_by = %d GROUP BY reg_date 
        ";
        if (!empty($costumer_id)){
            $result = Yii::$app->db->createCommand(sprintf($sql,$costumer_id, Yii::$app->user->id))->queryAll();
            return ArrayHelper::map($result,'id', 'reg_date');
        }
        return false;

    }
}
