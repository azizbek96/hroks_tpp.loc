<?php

namespace common\models;

use backend\models\Regions;
use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property int $id
 * @property string $name
 * @property int|null $region_id
 * @property string|null $address
 * @property string|null $fullname
 * @property string|null $tel1
 * @property string|null $tel2
 * @property string|null $add_info
 * @property int|null $type
 * @property int $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $user_id
 *
 * @property Doc[] $docs
 * @property ItemBalance[] $itemBalances
 * @property User $user
 */
class Customers extends \common\models\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','address','fullname','tel1', 'region_id'], 'required'],
            [['region_id', 'type', 'status', 'created_by','region_id', 'updated_by', 'created_at', 'updated_at', 'user_id'], 'integer'],
            [['address', 'add_info'], 'string'],
            [['name', 'fullname', 'tel1', 'tel2'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'region_id' => Yii::t('app', 'Tuman yoki shaxar'),
            'address' => Yii::t('app', 'Address'),
            'fullname' => Yii::t('app', 'Fullname'),
            'tel1' => Yii::t('app', 'Tel1'),
            'tel2' => Yii::t('app', 'Tel2'),
            'add_info' => Yii::t('app', 'Add Info'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * Gets query for [[Docs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocs()
    {
        return $this->hasMany(Doc::className(), ['customer_id' => 'id']);
    }

    /**
     * Gets query for [[ItemBalances]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItemBalances()
    {
        return $this->hasMany(ItemBalance::className(), ['customer_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public static function getRegionOne($id)
    {
        return Regions::findOne($id);
    }

    public static function getRegionsList($parent_id=2, $lvl = 0) {
        $regions = Regions::find()->where(['parent_id' => $parent_id])->andWhere(['status'=> self::STATUS_ACTIVE])->all();//->andWhere(['status'=> self::STATUS_ACTIVE])
        $regions_tree = [];
        $probel = "";
        for ($i = 0; $i < $lvl; $i++){
            $probel .= "&nbsp;&nbsp;&nbsp;&nbsp;";
        }
        $lvl++;
        foreach ($regions as $region)
        {
            if ($parent_id == null){
                $regions_tree[$region['id']] = '<b>'.$probel.$region['name_uz'].'</b>';
            } else {
                $regions_tree[$region['id']] = $probel.$region['name_uz'];
            }

            $regions_tree = \yii\helpers\ArrayHelper::merge($regions_tree, self::getRegionsList($region['id'], $lvl));
        }
        return $regions_tree;
    }
}
