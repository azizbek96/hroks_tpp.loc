<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DocSearch represents the model behind the search form of `common\models\Doc`.
 */
class DocSearch extends Doc
{
    public $contragent;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type','employee_id', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'], 'integer'],
            [['customer_id', 'doc_number', 'employee_id', 'department_id', 'reg_date', 'add_info','contragent'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $slug
     * @return ActiveDataProvider
     */
    public function search($slug)
    {
        $params = \Yii::$app->request->get();
        $query = Doc::find()->where(['!=', 'status', self::STATUS_DELETED]);
        switch ($slug) {
            case Doc::DOC_TYPE_INCOMING_LABEL:
                $docType = self::DOC_TYPE_INCOMING;
                $query->andWhere(['type' => $docType]);
                if (!Doc::actionBoss() && !Doc::actionSuperAdmin()){
                      $query->andWhere((['created_by' => \Yii::$app->user->id]));
                }
                break;
            case Doc::DOC_TYPE_SELL_LABEL:
                $docType = Doc::DOC_TYPE_SELL;
                $query->andWhere(['type' => $docType]);
                if (!Doc::actionBoss() && !Doc::actionSuperAdmin()){
                    $query->andWhere((['created_by' => \Yii::$app->user->id]));
                }
                break;
            case Doc::DOC_TYPE_RETURN_LABEL:
                $docType = Doc::DOC_TYPE_RETURN;
                $query->andWhere(['type' => $docType]);
                break;
            case Doc::DOC_TYPE_OUTGOING_LABEL:
                $docType = Doc::DOC_TYPE_OUTGOING;
                $query->andWhere(['type' => $docType]);
                break;
//            case Doc::DOC_TYPE_GIFT_LABEL:
//                $docType = Doc::DOC_TYPE_GIFT->$this;
                break;
            case Doc::DOC_TYPE_ADJUSTMENT_LABEL:
                $docType = Doc::DOC_TYPE_ADJUSTMENT;
                $query->andWhere(['type' => $docType]);
                break;
            case Doc::DOC_TYPE_ACCEPTED_LABEL:
                $docType = Doc::DOC_TYPE_ACCEPTED;
                $query->andWhere(['type' => $docType]);
                break;
            case Doc::DOC_TYPE_INCOMING_BOSS_LABEL:
                $docType = Doc::DOC_TYPE_INCOMING_BOSS;
                $query->andWhere(['type' => $docType]);
                break;
            case Doc::DOC_TYPE_RETURN_BOSS_LABEL:
                $docType = Doc::DOC_TYPE_RETURN_BOSS;
                $query->andWhere(['type' => $docType]);
                break;
            case Doc::DOC_TYPE_EMPLOYEE_SELL_LABEL:
                $docType = Doc::DOC_TYPE_EMPLOYEE_SELL;
                $query->andWhere(['type' => $docType]);
                break;
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'doc_number' => $this->doc_number,
            'invoice_number' => $this->invoice_number,
            'customer_id' => $this->customer_id,
            'employee_id' => $this->employee_id,
            'department_id' => $this->department_id,
            'reg_date' => $this->reg_date,
            'type' => $this->type,
            'status' => $this->status,
//            'created_by' => $this->created_by,
//            'updated_by' => $this->updated_by,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'add_info', $this->add_info]);

        return $dataProvider;
    }
}
